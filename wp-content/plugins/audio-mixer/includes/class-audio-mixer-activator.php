<?php

/**
 * Fired during plugin activation
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/includes
 * @author     Ali Arshad <email@aliarshad.info>
 */
class Audio_Mixer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		register_uninstall_hook( __FILE__, 'audio_mixer_uninstall' );

		global $wpdb;

		$table_name = $wpdb->prefix . "lummiq_app_tokens";

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
	            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	            `user_id` int(11) unsigned NOT NULL,
	            `access_token` text NOT NULL,
	            `expiry_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	            `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
	            `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
	            PRIMARY KEY (`id`)
			) $charset_collate";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		dbDelta($sql);

	}

	public function audio_mixer_uninstall() {

		global $wpdb;

		$table_name = $wpdb->prefix . "lummiq_app_tokens";

		$wpdb->query( "DROP TABLE IF EXISTS {$table_name}" );

	}

}
