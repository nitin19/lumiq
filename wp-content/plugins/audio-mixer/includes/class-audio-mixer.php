<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/includes
 * @author     Ali Arshad <email@aliarshad.info>
 */
class Audio_Mixer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Audio_Mixer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'audio-mixer';
		$this->version = '2.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Audio_Mixer_Loader. Orchestrates the hooks of the plugin.
	 * - Audio_Mixer_i18n. Defines internationalization functionality.
	 * - Audio_Mixer_Admin. Defines all hooks for the admin area.
	 * - Audio_Mixer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-audio-mixer-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-audio-mixer-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-audio-mixer-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-audio-mixer-public.php';


		/**
		 * The class responsible for defining all actions that occur in the app api handling
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'app/controllers/index.php';

		$this->loader = new Audio_Mixer_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Audio_Mixer_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Audio_Mixer_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Audio_Mixer_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'init', $plugin_admin, 'register_files_post_type' );
		$this->loader->add_action( 'init', $plugin_admin, 'register_downloads_post_type' );
		$this->loader->add_action( 'init', $plugin_admin, 'clean_files_schedule' );
		$this->loader->add_action( 'init', $plugin_admin, 'register_known_people_post_type' );
		$this->loader->add_action( 'delete_old_files', $plugin_admin, 'delete_old_files' );
		$this->loader->add_action( 'init', $plugin_admin, 'register_user_groups_taxonomy' );
		$this->loader->add_action( 'audio_mixer_user_group_taxonomy_add_form_fields', $plugin_admin ,'audio_mixer_user_group_taxonomy_add_user_group_field', 10, 2 );
		$this->loader->add_action( 'audio_mixer_user_group_taxonomy_edit_form_fields', $plugin_admin ,'audio_mixer_user_group_taxonomy_edit_user_group_field', 10, 2 );

		$this->loader->add_action( 'edited_audio_mixer_user_group_taxonomy', $plugin_admin ,'save_users_group_custom_meta', 10, 2 );
		$this->loader->add_action( 'create_audio_mixer_user_group_taxonomy', $plugin_admin ,'save_users_group_custom_meta', 10, 2 );

		
		// $this->loader->add_action( 'init', $plugin_admin, 'register_files_texonomy' );

		// $this->loader->add_action( 'restrict_manage_posts', $plugin_admin, 'restrict_files_by_tracks' );
		// $this->loader->add_action( 'parse_query', $plugin_admin, 'convert_track_id_to_taxonomy_term_in_query' );
		// $this->loader->add_action( 'manage_audio_mixer_files_posts_columns', $plugin_admin, 'add_tracks_column_to_files_list' );
		// $this->loader->add_action( 'manage_posts_custom_column', $plugin_admin, 'show_tracks_column_for_files_list' );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'file_meta_box' );
		/*$this->loader->add_action( 'admin_init', $plugin_admin, 'loop_meta_box' );*/
		$this->loader->add_action( 'admin_init', $plugin_admin, 'shortcode_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'known_people_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'markers_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'subtitles_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'downloads_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'emails_meta_box' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'setting_meta_box' );
		$this->loader->add_action( 'wp_ajax_get_srt', $plugin_admin, 'get_srt_content' );
		$this->loader->add_action( 'wp_ajax_mixer_downloads_data', $plugin_admin, 'mixer_downloads_data' );
		
		$this->loader->add_action( 'save_post', $plugin_admin, 'save_audio_mixer_file_details' );

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menu_item' );

		
		
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Audio_Mixer_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action( 'wp_ajax_nopriv_download_request', $plugin_public, 'download_request' );
		$this->loader->add_action( 'wp_ajax_download_request', $plugin_public, 'download_request' );
		

		add_shortcode( 'audioMixer', array($plugin_public,"shortcode_audioMixer") );
		add_filter('the_content', array($plugin_public,"download_files"));

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Audio_Mixer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
