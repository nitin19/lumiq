<?php

require_once( 'responseController.php' );

require_once( dirname( __DIR__ ) . '/src/JWT.php' );

use \Firebase\JWT\JWT;


Class mainController {

	const SECRET_KEY = 'vq_audio_mixer_app_module';
	const APP_TABLE = 'wp_lummiq_app_tokens';

	private $rules = array(
		array( 'name' => 'Response', 'access' => 'private', 'strict' => true ),
		array( 'name' => 'getUserSongs', 'access' => 'private', 'strict' => true ),
		array( 'name' => 'getSong', 'access' => 'private', 'strict' => true ),
		array( 'name' => 'login', 'access' => 'public', 'strict' => true ),
		array( 'name' => 'logout', 'access' => 'private', 'strict' => true ),
		array( 'name' => 'checkStatus', 'access' => 'private', 'strict' => true ),
		array( 'name' => 'downloadSong', 'access' => 'private', 'strict' => true ),
	);

	public function __construct() {

		add_action( 'wp_ajax_nopriv_vq_audio_mixer_api', array( $this, 'vq_add_session_cb' ) );
		add_action( 'wp_ajax_vq_audio_mixer_api', array( $this, 'vq_add_session_cb' ) );
		add_action( 'init', array( $this, 'allowCORS' ) );

	}

	public function vq_add_session_cb() {

		$this->allowCORS();

		$data = json_decode( file_get_contents( 'php://input' ), true );


		$method = $data['method'];

		// -1 for private,
		// 0 for no method found i.e bad request,
		// 1 for public method
		if ( $this->checkAccess( $method ) === - 1 ) {

			$token = $this->getAuthorizationHeader();

			if ( $this->authorized( $token ) ) {


				switch ( $method ) {

					case 'logout':

						$this->logout( $token );

						$this->respond( [], 200, 'Successfully Logged out.' );

						break;

					case 'getUserSongs':

						$userId = $this->getUserIdByToken( $token );

						if ( $userId != null ) {

							$songs = $this->getUserSongs( $userId );

							$settings['heading'] = get_option('thank_box_heading');
							$settings['details'] = get_option('thank_box_details');
							$settings['button'] = get_option('thank_box_button');
							$settings['long-marker'] = get_option('marker');
							$settings['short-marker'] = get_option('marker-short');

							$this->respond( [ 'songs' => $songs, 'settings' => $settings ], 200, 'Songs Sent.' );

							break;

						}


						$this->respond( [], 401, 'User not found.' );

						break;

					case 'getSong':

						$id = 44;

						$song = $this->getSongById( $id );

						$this->respond( [ 'song' => $song ], 200, 'Songs Sent.' );

						break;

					case 'checkStatus':

						$this->respond( [ 'status' => 'active' ], 200, 'User is still active' );

						break;


					default:

						$this->respond( [], 500, 'Request not handled on back-end.' );
				}

			} else {

				// not authorized
				$this->respond( [], 401, "Could not validate user. {$method}, Please Login." );

			}

		} else if ( $this->checkAccess( $method ) === 1 ) {

			switch ( $method ) {

				case  'login' :

					$username = $data['email'];
					$password = $data['password'];

					if ( $this->validateLogin( $username, $password ) ) {

						$data = $this->login( $username );

						$this->respond( [
							'_token' => $data['jwt'],
							'user'   => $data['user']
						], 200, 'Successfully Logged In.' );

					} else {

						$this->respond( [], 401, 'Invalid Credentials.' );

					}

					break;

				default:

					$this->respond( [], 404, "Request Ignored, Method Not Handled {$method}" );

			}


		} else {

			$this->respond( [], 404, 'Bad Request, Method Not Found.' );

		}


		wp_die();
	}

	public function allowCORS() {
		$URL = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'];
		$URL = $_SERVER['HTTP_ORIGIN'];

		if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ) {

			@header( 'Access-Control-Allow-Origin: ' . $URL );
			@header( "Access-Control-Allow-Credentials: true" );
			@header( 'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS' );
			@header( 'Access-Control-Max-Age: 1000' );
			@header( 'Access-Control-Allow-Headers: Origin, Content-Type, Authorization' );
			@header( 'Control-Type: text/plain' );
			@header( 'Control-Length: 0' );
			http_response_code( 204 );
			die;
		}

		@header( 'Access-Control-Allow-Origin: ' . $URL );
	}

	private function checkAccess( $cont ) {

		foreach ( $this->rules as $method ) {

			if ( $method['name'] == $cont ) {

				return $method['access'] == 'public' ? 1 : - 1;

			}


		}

		return 0;

	}

	private function getAuthorizationHeader() {
		$headers = null;

		if ( isset( $_SERVER['Authorization'] ) ) {
			$headers = trim( $_SERVER["Authorization"] );
		} else if ( isset( $_SERVER['HTTP_AUTHORIZATION'] ) ) { //Nginx or fast CGI
			$headers = trim( $_SERVER["HTTP_AUTHORIZATION"] );
		} elseif ( function_exists( 'apache_request_headers' ) ) {
			$requestHeaders = apache_request_headers();
			// Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
			$requestHeaders = array_combine( array_map( 'ucwords', array_keys( $requestHeaders ) ), array_values( $requestHeaders ) );
			//print_r($requestHeaders);
			if ( isset( $requestHeaders['Authorization'] ) ) {
				$headers = trim( $requestHeaders['Authorization'] );
			}
		}

		return $headers;

	}

	private function authorized( $token ) {

		if ( $token != null && $this->checkToken( $token ) ) {

			return true;

		} else {

			return false;

		}

	}

	private function checkToken( $token ) {

		$token_id = $this->tokenExists( $token );

		if ( $token_id ) {

			$this->updateToken( $token_id );

			return true;

		}

		return false;

	}

	private function tokenExists( $token ) {

		global $wpdb;

		$row = $wpdb->get_row( "SELECT * FROM " . self::APP_TABLE . " WHERE  access_token = '{$token}' AND expiry_time > NOW()" );

		return $row != null ? $row->id : false;

	}

	public function updateToken( $id ) {
		global $wpdb;

		$data = array(
			'expiry_time' => date( 'Y-m-d h:i:s', ( time() + ( 60 * 60 * 24 ) ) )
		);

		$wpdb->update(
			self::APP_TABLE,
			$data,
			array( 'id' => $id )
		);
	}

	public function logout( $token ) {

		$token_id = $this->tokenExists( $token );

		global $wpdb;

		$wpdb->delete(
			self::APP_TABLE,
			array( 'id' => $token_id )
		);

	}

	private function respond( $data, $code, $msg ) {
		echo new responseController( $data, $code, $msg );
	}

	public function getUserIdByToken( $token ) {
		global $wpdb;

		$row = $wpdb->get_row( "SELECT user_id FROM " . self::APP_TABLE . " WHERE  access_token = '{$token}' AND expiry_time > NOW()" );

		return $row->user_id;
	}


	public function getUserSongs( $user_id ) {

		$grps = get_terms( [
			'taxonomy'   => 'audio_mixer_user_group_taxonomy',
			'hide_empty' => false,
		] );

		$ugrps = array();

		foreach ( $grps as $grp ) {
			$options = get_option( "taxonomy_$grp->term_id" );
			$users   = json_decode( $options['users'], true );
			if ( in_array( $user_id, $users ) ) {
				$ugrps[] = $grp->term_id;
			}
		}


		$songs = get_posts( array(
			'post_type'      => 'audio_mixer_files',
			'posts_per_page' => - 1,
			'tax_query'      => array(
				array(
					'taxonomy' => 'audio_mixer_user_group_taxonomy',
					'field'    => 'term_id',
					'terms'    => $ugrps, // Where term_id of Term 1 is "1".
				)
			)
		) );


        $lMarkers = get_option('marker');
        $sMarkers = get_option('marker-short');

		foreach ( $songs as $index => $song ) {
			$songs[ $index ]->meta = get_post_meta( $song->ID );

			$meta = $songs[ $index ]->meta;



			$markers = json_decode($meta['markers'][0], true);

			foreach($markers as $i => $marker) {

			    $content = $marker['content'];

			    foreach ($lMarkers as $ind => $lm)  {
			        if($lm == $content) {

			            $content = $sMarkers[$ind];

                    }
                }

                $marker['content'] = $content;
                $marker['initials'] = $content;

                $markers[$i] = $marker;

            }

            $meta['markers'][0] = json_encode($markers);


			if ( isset( $meta['fileId'] ) ) {

				foreach ( $meta['fileId'] as $file ) {
					$meta[ 'URL' . $file ]    = wp_get_attachment_url( $file );
					$meta[ 'Title_' . $file ] = get_the_title( $file );
					$meta[ 'SONG_META_' . $file ] = get_post( $file );
				}
				$meta['subtitles'] = get_post_meta( $song->ID, 'subtitles' );

			}

			$songs[ $index ]->meta = $meta;

		}


		return $songs;

	}


	public function getSongById( $id ) {

		return get_post( $id );

	}

	public function validateLogin( $email, $pass ) {

		$user = get_user_by( 'email', $email );


		if(!$user) {
			$user = get_user_by('login', $email);
		}

		if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID ) ) {

			return true;

		}

		return false;

	}

	public function login( $email ) {

		$user = get_user_by( 'email', $email );

		if(!$user) {
			$user = get_user_by('login', $email);
		}

		$curr = time();

		$token = array(
			"user_email" => $email,
			"user_id"    => $user->ID,
			"iat"        => $curr,
			"exp"        => ( $curr + ( 60 * 60 * 24 ) )
		);

		$jwt = JWT::encode( $token, self::SECRET_KEY );

		global $wpdb;

		$data = array(
			'user_id'      => $user->ID,
			'access_token' => $jwt,
			'expiry_time'  => date( 'Y-m-d h:i:s', ( $curr + ( 60 * 60 * 24 ) ) ),
			'created_at'   => date( 'Y-m-d h:i:s', $curr ),
			'updated_at'   => date( 'Y-m-d h:i:s', $curr ),
		);

		$wpdb->insert( self::APP_TABLE, $data );
		$data         = array();
		$data['jwt']  = $jwt;
		$user->meta   = get_user_meta( $user->ID );
		$user->avatar = get_avatar_url( $user->ID );
		$data['user'] = $user;

		return $data;
//		$decoded = JWT::decode($jwt, self::SECRET_KEY, array('HS256'));

	}


}