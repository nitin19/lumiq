<?php

class responseController{

	private $data = [];
	private $code = 200;

	public function __construct($data, $code, $message) {
		$this->data = $data;
		$this->data['resp_msg'] = $message;

		$this->code = $code;
		$phpSapiName    = substr(php_sapi_name(), 0, 3);
		if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
			header('Status: '.$this->code.' ');
		} else {
			$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
			header($protocol.' '.$this->code.' ');
		}

	}

	public function __toString() {
		// TODO: Implement __toString() method.

		@header('Content-Type: application/json');
		http_response_code($this->code);


		return json_encode($this->data);

	}


}