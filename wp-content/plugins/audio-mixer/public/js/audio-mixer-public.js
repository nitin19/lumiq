
	 var AJAX_URL = ajax.ajax_url;
	 var plugin_url = ajax.plugin_url;


	 var initializePlayer = function (filesData, uniqueCode, postId, getPresets, getPresetSettings, autoplay, defaultPreset, subtitlesShow, subtitles, loops, markers, offset, please_wait){
		var files, presets, getPresetSettings, selectedPreset="",
			defaultSettings, audioDuration=0, audioCurrentTime=0, updatingRegions = false, updatingTimeout=0,
			/*	Variables for loops	*/
            globalRegionList = [], loop_enabled = false, show_loops = false, custom_update = false,
			/*	Variables for markers	*/
			markersList = [];
		try{
			files = JSON.parse(filesData);
		} catch(e){
			files = [];
		}

		try{
			presets = JSON.parse(getPresets);
		} catch(e){
			presets = [];
		}

		try{
			var presetSettings = JSON.parse(getPresetSettings);
		} catch(e){
			presetSettings = {};
		}

		document.addEventListener("keydown", function(e){
			if (e.keyCode === 0 || e.keyCode === 32) {
				e.preventDefault()
				e.stopImmediatePropagation();
				e.stopPropagation();

				var notReadyFiles = files.filter(function(item){
					if(item.ready!=true){
						return item;
					}
				});

				if(notReadyFiles.length>0){
					console.log(notReadyFiles.length+" files, not ready yet.");
					return;
				}

				try{
					if(files[0] && files[0].wavesurfer && files[0].wavesurfer.isPlaying()==true){
						pause();
					} else{
						play();
					}
				} catch(e){
					console.log(e);
				}


			}
		});

		files = files.map(function(item){
			item.solo = false;
			return item;
		})
		defaultSettings = JSON.parse(JSON.stringify(files));//making a deep copy


		var playBtn = jQuery("#playbtn_"+uniqueCode);
		var pauseBtn = jQuery("#pausebtn_"+uniqueCode);
		var restartBtn = jQuery("#restartbtn_"+uniqueCode);
		var resetBtn = jQuery("#resetbtn_"+uniqueCode);
		var downloadBtn = jQuery("#downloadbtn_"+uniqueCode);
		var notifyBtn = jQuery("#notifybtn_"+uniqueCode);
		var okBtn = jQuery("#ok_"+uniqueCode);
		var stBtn = jQuery("#enableSt_"+uniqueCode);
		

		var timerHolder = jQuery("#timerHolder_"+uniqueCode);

		pauseBtn.hide();

		playBtn.click(play);
		pauseBtn.click(pause);
		restartBtn.click(restart);
		resetBtn.click(resetDefaultSettings);
		downloadBtn.click(showDownloadPopup);
		notifyBtn.click(download);
		stBtn.click(toogleSubs);

		okBtn.click(function(){
			jQuery(".back_wraper_success_"+uniqueCode).hide(10);
		});

		jQuery(".back_wraper_"+uniqueCode).click(function(){
			jQuery(".back_wraper_"+uniqueCode).hide(10);
		});

		jQuery(".email_popup_"+uniqueCode).click(function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
		});

		jQuery(".close_popup").click(function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			jQuery(e.target.parentElement.parentElement).hide()
		});

		function showDownloadPopup(event){
			event.stopImmediatePropagation();
			event.preventDefault();

			jQuery(".back_wraper_"+uniqueCode).show(10);
		}

		function toogleSubs(){
			subtitlesShow = (subtitlesShow == 1) ? 0 : 1;
			if(subtitlesShow){
				stBtn.removeClass('st_mode_off');
				stBtn.addClass('st_mode_on');
				jQuery("#subsHolder_"+uniqueCode).show();
			} else {
				stBtn.removeClass('st_mode_on');
				stBtn.addClass('st_mode_off');
				jQuery("#subsHolder_"+uniqueCode).hide();
			}
		}
		subtitlesShow = (subtitlesShow == 'yes') ? 0 : 1;//toogling to apply toogling first time.
		toogleSubs();

		function download(event){
			event.stopImmediatePropagation();
			event.preventDefault();

			if(jQuery("#email_"+uniqueCode).val()==""){
				jQuery("#errorMsg_"+uniqueCode).html("Please enter your email address");
				return;
			}
			if(validateEmail(jQuery("#email_"+uniqueCode).val())==false){
				jQuery("#errorMsg_"+uniqueCode).html("Please enter a valid email address");
				return;
			}


			email = "&email="+jQuery("#email_"+uniqueCode).val();

			var formData = jQuery("#audioPlayersForm_"+uniqueCode).serialize();
			formData += "&action=download_request";
			formData += "&postId="+postId;
			formData += email;
			jQuery(".dwn_btn").val(please_wait);
			jQuery.post(
				AJAX_URL,
			 	formData,
				function(response){
                    jQuery(".dwn_btn").val('Download');
					if(isNaN(parseInt(response))){
                        jQuery(".email_popup h4").html('<span style="color: red;">' + response  + '</span>');
					} else {
                        jQuery(".email_popup h4").html('');
                        jQuery(".back_wraper_"+uniqueCode).hide(10);
                        jQuery(".back_wraper_success_"+uniqueCode).show(10);
					}
				}
			);


		}

		function resetDefaultSettings(){
			files.forEach(function(item, index){
				item.volume = defaultSettings[index].volume;
				item.muted = defaultSettings[index].muted;
				if(item.solo == true) soloPlay(item.id);
			})
			selectedPreset = "";
			renderPresets();
			applySettings();
		}

		function presetClicked(){
			var value = this.toString();
			if(selectedPreset==value){
				selectedPreset="";
			} else{
				selectedPreset = value;
			}

			files.forEach(function(item){
				if(item.solo == true) soloPlay(item.id);
			})
			renderPresets();
			applySettings();
		}

		function presetModied(){
			files.forEach(function(item){
				item.volume = presetSettings[selectedPreset][item.id].volume;
				item.muted = presetSettings[selectedPreset][item.id].muted;
			});
			selectedPreset = "";
			renderPresets();
		}

		function renderPresets(){
			jQuery("#presets_content_"+uniqueCode).html("");
			presets.forEach(function(item){
				var color = presetSettings[item]["color"];
				color = color || "#666666";
				if(selectedPreset==item){
					var template = jQuery("<div style='color:#fff; background:"+color+"' class='preset'>"+item+"</div>");
				} else{
					var template = jQuery("<div style='color:"+color+"' class='preset'>"+item+"</div>");
				}

				jQuery("#presets_content_"+uniqueCode).append(template);
				template.click(presetClicked.bind(item))
			})
		}

		function applySettings(){
				var soloFiles = files.filter(function(item){
					if(item.solo==true) return item;
				})

				if(soloFiles.length>0){
					console.log("TEST");
				}

				files.forEach(function(item){
					var volume = item.volume;
					var muted = (item.muted==true || item.muted=="true")?true:false;

					if(selectedPreset!=""){
						volume = presetSettings[selectedPreset][item.id].volume;
						muted = presetSettings[selectedPreset][item.id].muted;
						muted = (muted==true || muted=="true")?true:false;
					}

					if(soloFiles.length>0){
						if(item.solo!=true){
							muted = true;
						} else{
							muted = false;
						}
					}

					if(muted==true){
						mute(item.id);
					} else{
						unmute(item.id);
						updateVolume(item.id, volume)
					}
				});
		}

		function updateUI(){
			renderPresets();

			jQuery(".back_wraper_"+uniqueCode).hide(10);
			jQuery(".back_wraper_success_"+uniqueCode).hide(10);
			var playersContainer = jQuery("#audioPlayersForm_"+uniqueCode);
			playersContainer.html("");
			// <img src='"+plugin_url+"/public/images/mute.svg' class='mute20'style='display: none' id='mute_"+uniqueCode+item.id+"' />\
			// <img src='"+plugin_url+"/public/images/volume.svg' class='volume20'style='display: none' id='volume_"+uniqueCode+item.id+"' />\
			files.forEach(function(item, index){
				(function(){
					item.ready = false;
					item.first = false;
					item.last = false;
					if(index==0)item.first = true;
					if(index == (files.length - 1)) item.last = true;
					var template = "\
					<div class='trackName'>"+item.title+"</div>\
                    <div class=\"player\" style='position: relative; background:" + item.color + "' id='player_" + uniqueCode + item.id + "'><div id='player_upper_progress_" + uniqueCode + item.id + "' style='background: white; position: absolute; height: 30px; width: 100%;'><div id='player_lower_progress_" + uniqueCode + item.id + "' style='background: " + item.color + "; position: absolute; height: 30px; -webkit-transition: width 0.1s ease-in-out; -moz-transition: width 0.1s ease-in-out; -o-transition: width 0.1s ease-in-out; transition: width 0.1s ease-in-out;'></div></div></div>\
					<div class=\"playerControls\">\
						<input type='hidden' name='filesId[]' value='"+item.id+"' />\
						<input type='hidden' id='filesMute_"+uniqueCode+item.id+"' name='filesMute"+item.id+"' value='false' />\
						\
						<span class='mute20'style='display: none' id='mute_"+uniqueCode+item.id+"'></span>\
						<span id='volume_"+uniqueCode+item.id+"' class='volume20'></span>\
						<span class='solo' id='solo_"+uniqueCode+item.id+"'>S</span> \
						<input type='range' step='0.01' max='1' min='0' name='filesVolume"+item.id+"' class='volumeControl' id='volumeCtrl_"+uniqueCode+item.id+"'  />\
					</div>\
					<div class=\"clearFloat\"></div>";

					var jqTemplate = jQuery(template);
					item.volumeControl = jqTemplate.find("#volumeCtrl_"+uniqueCode+item.id);
					item.muteControl = jqTemplate.find("#mute_"+uniqueCode+item.id);
					item.unmuteControl = jqTemplate.find("#volume_"+uniqueCode+item.id);
					item.soloControl = jqTemplate.find("#solo_"+uniqueCode+item.id);
					item.container = jqTemplate.find("#player_"+uniqueCode+item.id);


					playersContainer.append(jqTemplate);
					setTimeout(function(){
						initWave(item);
					}, 0)
				})(item)
			});
		}

		function allReady(){
			var notReady = files.filter(function(item){
				if(item.ready==false) return item;
			});

			if(notReady.length==0){
				if(presets.indexOf(defaultPreset)>=0){
					selectedPreset = defaultPreset;
					renderPresets();
					applySettings();
				}
                var waves = document.getElementsByTagName("wave");
                // for(var i=0; i<waves.length; i++){
                //     waves[i].addEventListener('click',function(e){
                //         if(updatingRegions){
                //         	e.stopImmediatePropagation();
				// 		}
                //     }, true);
                // }
				if(autoplay=='yes'){play();}
			}


		}

		function initWave(item){
			item.wavesurfer = WaveSurfer.create({
				container: "#player_"+uniqueCode+item.id,
				barHeight: 1.5,
				height: 30,
				waveColor: "#ffffff",
				progressColor: "#ffffff",
				interact: true,
				pixelRatio: 1
			});

            item.innerProgressBar = jQuery("#player_lower_progress_" + uniqueCode + item.id);
            item.outerProgressBar = jQuery("#player_upper_progress_" + uniqueCode + item.id);
			item.wavesurfer.load(item.url);

			if(item.first){
				item.wavesurfer.on('audioprocess', function () {
					audioCurrentTime = item.wavesurfer.getCurrentTime();
					updateTime();
				});

				item.wavesurfer.on('finish', function () {
					audioCurrentTime = item.wavesurfer.getCurrentTime();
					updateTime();
				});

			}

			/* Subtitles */
			item.wavesurfer.on('audioprocess', function () {

				audioCurrentTime = item.wavesurfer.getCurrentTime();
				if(subtitles != null){
					for(var key in subtitles){
                        var start_time=subtitles[key].start_time;
                        var end_time=subtitles[key].end_time;
                        var subtitle=subtitles[key].subtitle;


						var start_arr=start_time.split(":");
						var end_arr=end_time.split(":");
						var s_time= (parseInt(start_arr[0]) * 3600) + (parseInt(start_arr[1]) * 60) + parseInt(start_arr[2].split(',')[0]) + parseInt(start_arr[2].split(',')[1])/1000;
                        var e_time= (parseInt(end_arr[0]) * 3600) + (parseInt(end_arr[1]) * 60) + parseInt(end_arr[2].split(',')[0]) + parseInt(end_arr[2].split(',')[1])/1000;

                        if(!isNaN(offset) && offset != 0){
                            s_time += parseFloat(offset);
                            e_time += parseFloat(offset);
                        }

                        var div="subtitles_"+s_time.toString().replace('.','_') + "_" + e_time.toString().replace('.','_');
						if(audioCurrentTime>s_time && audioCurrentTime<e_time){
							renderSubtitleDiv(subtitle,div,false);
						}
						else{
							renderSubtitleDiv(subtitle,div,true);
						}
						// if(audio)
					}
				}

			});

			/* ========= */


                        /* Loading event of WaveSurfer */
                        /* Used to render Progress Bar on each track */
			item.wavesurfer.on('loading', function (e) {
				if (e < 100) {
					renderProgressBar(e*0.95, item);
				} else {
					var fixed = 5;
					var pr = 95;
					item.interval = setInterval(function () {
						var perc = fixed / 10;
						fixed = fixed - perc;
						pr = pr + perc;
						renderProgressBar(pr, item);
					}, 500);
				}
			});
			item.wavesurfer.on('ready', function () {
				if (item.first) {
					audioDuration = item.wavesurfer.getDuration();
					updateTime();
                    /*	For Markers as they'll be only rendered one time.	*/
                    if(markers != null && markers !== ''){
                        markers = JSON.parse(markers);
                        if(markers != null){
                            for(var key in markers){
                                var time = markers[key].start_time.split(":");
                                var starter = (parseInt(time[0]) * 60) + parseInt(time[1]);
                                time = markers[key].end_time.split(":");
                                var ender = (parseInt(time[0]) * 60) + parseInt(time[1]);
                                var content = markers[key].content;
                                var color = markers[key].color;

                                var tempMarker = {};
                                tempMarker.start = starter;
                                tempMarker.end = ender;
                                tempMarker.content = content;
                                tempMarker.color = color;
                                markersList.push(tempMarker);
                            }
                        }
                        placeMarkers();
					}
				}
				clearInterval(item.interval);
				item.interval = 0;
				renderProgressBar(100, item);
				item.ready = true;
				if (item.muted == 'false' || item.muted == false) {
					unmute(item.id);
					updateVolume(item.id, item.volume);
				} else {
					mute(item.id);
				}
				jQuery('#filesMute_' + uniqueCode + item.id).val(item.muted);
				allReady();
			});



			item.wavesurfer.on('seek', seekEveryone);

			item.wavesurfer.on('finish', function () {
				jQuery("#playbtn_"+uniqueCode).show();
				jQuery("#pausebtn_"+uniqueCode).hide();
			});

			item.muteControl.on('click', function(){
				if(selectedPreset!="")presetModied();
				unmute(item.id);
			})

			item.unmuteControl.on('click', function(){
				if(selectedPreset!="")presetModied();
				mute(item.id);
			})

			item.soloControl.on('click', function(){
				if(selectedPreset!="")presetModied();
				soloPlay(item.id);
			})

			item.volumeControl.val((item.volume));

			item.volumeControl[0].onchange = function(event){
				if(selectedPreset!="")presetModied();
				updateVolume(item.id, event.target.value);
			}
			testfile = item.wavesurfer;
		}

		/* Function that renders Progress Bar on each track */
		function renderProgressBar(progress, item) {
			if (progress == 100) {
				item.innerProgressBar.css('width', progress + "%");
				setTimeout(function(){
					item.outerProgressBar.hide();
				},0.1);

			} else {
				item.innerProgressBar.css('width', progress + "%");
			}
		}

		/* =============================================== */

		/* Function that renders Subtitle div */
		function renderSubtitleDiv(text,div,flag){
			if(flag==true){
				var subtitles_div=jQuery("#"+div);
				subtitles_div.remove();
			}
			else{
				var sub_div=document.getElementById(div);
				if(!sub_div){
					var subtitles_div=jQuery("#subtitles_div");
					subtitles_div.append("<div id='"+div+"'></div>");
					jQuery("#"+div).append(text.replace('\n', '<br>'));
				}
			}
		}

				/* ================================= */


		function seekEveryone(location){
			files.forEach(function(item){
				if(item.first){
					audioCurrentTime = item.wavesurfer.getCurrentTime();
					updateTime();
				}

				item.wavesurfer.un('seek', seekEveryone);
				item.wavesurfer.seekTo(location);
				item.wavesurfer.on('seek', seekEveryone);
			});

			if(subtitles != null && subtitles.length > 0){
				for(var key in subtitles){
					var start_time=subtitles[key].start_time;
					var end_time=subtitles[key].end_time;
					var subtitle=subtitles[key].subtitle;

					var start_arr=start_time.split(":");
					var end_arr=end_time.split(":");

					var s_time=(parseInt(start_arr[1])/60)+parseInt(start_arr[0]);
					s_time=s_time*60;
					var e_time=(parseInt(end_arr[1])/60)+parseInt(end_arr[0]);
					e_time=e_time*60;

					if(audioCurrentTime>s_time && audioCurrentTime<e_time){
						var div="subtitles_"+s_time+"_"+e_time;
						renderSubtitleDiv(subtitle,div,false);
					}
					else{
						var div="subtitles_"+s_time+"_"+e_time;
						renderSubtitleDiv(subtitle,div,true);
					}
					if(e_time==Math.floor(audioCurrentTime)){
						var div="subtitles_"+s_time+"_"+e_time;
						renderSubtitleDiv(subtitle,div,true);
					}

					// if(audio)
				}
			}
		}

		function soloPlay(id){
			files.forEach(function(item){
				item.solo = item.solo || false;
				if(item.id==id){
					item.solo = !item.solo;
					if(item.solo){
						item.soloControl.attr("style","color:#fff;  background:"+item.color);
					} else{
						item.soloControl.attr("style","");
					}
				}
			})
																																																																																																						applySettings();
		}

		function mute(id){
			try{
				var soloFiles = files.filter(function(item){
					if(item.solo==true) return item;
				})

				var file = files.filter(function(item){
					if(item.id==id){
						return item;
					}
				})
				if(file.length>0){
					if(selectedPreset=="" && soloFiles.length==0){
						file[0].muted = true;
					}

					var height = 5;
					var position = (30-height)/2;
					file[0].wavesurfer.params.height = height; //30px is the time code height, may different in your environment
					file[0].wavesurfer.drawer.setHeight(height);
					file[0].wavesurfer.drawBuffer();

					setTimeout(function(){
						jQuery("#player_"+uniqueCode+file[0].id).css("top", position+"px");
						jQuery("#player_"+uniqueCode+file[0].id).css("position", "relative");
					},0)

					file[0].volumeControl.val(0);
					file[0].wavesurfer.setMute(true);
					file[0].muteControl.show();
					file[0].unmuteControl.hide();
					jQuery('#filesMute_'+uniqueCode+file[0].id).val("true");
				}
			} catch(e){
				console.log(e);
			}

		}

		function getFileVolume(id){

		}

		function unmute(id){
			var soloFiles = files.filter(function(item){
				if(item.solo==true) return item;
			})

			var file = files.filter(function(item){
				if(item.id==id){
					return item;
				}
			})
			if(file.length>0){
				if(selectedPreset=="" && soloFiles.length==0){
					file[0].muted = false;
				}

				var height = 30*file[0].volume;
				if(height<5){
					height = 5;
				}
				var position = (30-height)/2;
				file[0].wavesurfer.params.height = height; //30px is the time code height, may different in your environment
				file[0].wavesurfer.drawer.setHeight(height);
				file[0].wavesurfer.drawBuffer();

				setTimeout(function(){
					jQuery("#player_"+uniqueCode+file[0].id).css("top", position+"px");
					jQuery("#player_"+uniqueCode+file[0].id).css("position", "relative");
				},0)

				file[0].wavesurfer.setMute(false);
				if(file[0].volume>0){
					file[0].volumeControl.val(file[0].volume);
				} else{
					updateVolume(file[0].id, 0.5);
				}
				file[0].muteControl.hide();
				file[0].unmuteControl.show();
				jQuery('#filesMute_'+uniqueCode+file[0].id).val("false");
			}

		}

		function updateVolume(id, volume){
			var file = files.filter(function(item){
				if(item.id==id){
					return item;
				}
			})

			if(file.length>0){
				if(selectedPreset==""){
					file[0].volume = volume;
				}

				if(volume==0 && !file[0].muted){
					mute(file[0].id);
				} else{
					unmute(file[0].id);
				}

				file[0].wavesurfer.setVolume(volume);
				file[0].volumeControl.val(volume);

				var height = 30*volume;
				if(height<5){
					height = 5;
				}
				var position = (30-height)/2;

				file[0].wavesurfer.params.height = height; //30px is the time code height, may different in your environment
				file[0].wavesurfer.drawer.setHeight(height);
				file[0].wavesurfer.drawBuffer();

				setTimeout(function(){
					jQuery("#player_"+uniqueCode+file[0].id).css("top", position+"px");
					jQuery("#player_"+uniqueCode+file[0].id).css("position", "relative");
				},0)

				// file[0].container.attr("style", "position:relative; top: "+position+"px");
			}
		}

		function play(event){

			if(event){
				event.stopImmediatePropagation();
				event.preventDefault();
			}

			var notReadyFiles = files.filter(function(item){
				if(item.ready==false){
					return item;
				}
			});

			if(notReadyFiles.length>0){
				console.log(notReadyFiles.length+" files, not ready yet.");
				return;
			}

			files.forEach(function(item){
				item.wavesurfer.play();
			});

			playBtn.hide();
			pauseBtn.show();
		}

		function pause(event){
			if(event){
				event.stopImmediatePropagation();
				event.preventDefault();
			}

			var notReadyFiles = files.filter(function(item){
				if(item.ready==false){
					return item;
				}
			});

			if(notReadyFiles.length>0){
				console.log(notReadyFiles.length+" files, not ready yet.");
				return;
			}

			files.forEach(function(item){
					item.wavesurfer.pause();
			});

			playBtn.show();
			pauseBtn.hide();

		}

		function restart(event){
			event.stopImmediatePropagation();
			event.preventDefault();

			files.forEach(function(item){
				item.wavesurfer.seekTo(0);
			});

		}

		function convertSecondToTime(time){
			var sec_num = parseInt(time, 10); // don't forget the second param
			// var hours   = Math.floor(sec_num / 3600);
			// var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
			// var seconds = sec_num - (hours * 3600) - (minutes * 60);

			var minutes = Math.floor((sec_num ) / 60);
			var seconds = sec_num - (minutes * 60);

			// if (hours   < 10) {hours   = "0"+hours;}
			if (minutes < 10) {minutes = "0"+minutes;}
			if (seconds < 10) {seconds = "0"+seconds;}
			// return hours+':'+minutes+':'+seconds;

			return minutes+':'+seconds;

		}

		function updateTime(){
			var timeString = convertSecondToTime(audioCurrentTime)+" / "+convertSecondToTime(audioDuration);
			timerHolder.html(timeString);

		}

		function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		updateUI();

		function placeMarkers(){
			markersList.forEach(function(value, index){
				var styles = makeMarkerStyle(value);
				var mrk = "<div data-start='" + value.start + "' id='marker_" + index + "' class='marker' " + styles + ">" + value.content + "</div>";
				jQuery("#marker_content").append(mrk);
                jQuery(".marker").on('click', function(e){
                    var start = jQuery(this).data('start')/audioDuration;
                    files.forEach(function(item, index){
                        item.wavesurfer.seekTo(start);
					});
                })
			});
		}

		function makeMarkerStyle(marker){
			var startPoint = (marker.start/audioDuration) * 100;
            var endPoint = (marker.end/audioDuration) * 100;
            var width = Math.abs(endPoint- startPoint);
            var backgroundColor = marker.color;
            var style = "style='left: " + startPoint +"%; " +
								"right: " + endPoint + "%; " +
								"background: " + backgroundColor + ";" +
								"width: " + width + "%;'";
            return style;
		}
		
		 /*	Loop Code	*/
         jQuery("#enableLoop_"+uniqueCode).on('click', function(e){
			var loopsBtn = jQuery("#enableLoop_"+uniqueCode);
         	if(loop_enabled !== true){				
				loop_enabled = true;
				show_loops = true;
				var starter;
				var ender;
				if(audioCurrentTime < audioDuration - 30) {
                    starter  = audioCurrentTime;
                    ender	= audioCurrentTime + 30;
				} else {
					starter  = parseInt(audioDuration - 30);
					ender	= parseInt(audioDuration);
				}
				
                files.forEach(function(item, index){
                    var region = item.wavesurfer.addRegion({
                        start: starter,
                        end: ender,
                        color: 'rgba(108,122,137,0.5)',
                        loop: true,
						drag: true,
						resize: true
                    });
                    jQuery(".wavesurfer-region").css('border-left','2px solid');
                    jQuery(".wavesurfer-region").css('border-right','2px solid');
                    globalRegionList.push(region);
                    /*  Handling Loop Update */
                    item.wavesurfer.on('region-updated', onRegionUpdate);
				});
				jQuery(".wavesurfer-region").css('border-left','2px solid');
				jQuery(".wavesurfer-region").css('border-right','2px solid');
				
				loopsBtn.removeClass('loop_mode_off');
				loopsBtn.addClass('loop_mode_on');
			} else {	
				loop_enabled = false;	
				globalRegionList.forEach(function(item){
					item.remove();
				});

				files.forEach(function(item, index){
					item.wavesurfer.un('region-updated');
				});
				globalRegionList = [];
				loopsBtn.removeClass('loop_mode_on');
                loopsBtn.addClass('loop_mode_off');
			}
		 });

		 var startAttached = false;
		 var startFixed = 0;
		 var endAttached = false;
		 var endFixed = 0;

		 
		 function onRegionUpdate(e) {
			var update;
			var loopStartRegionStartHit = false;
			var loopEndRegionStartHit = false;
			var offfset = 4;
			var position, marker;
			

			//loop start in range of marker
			var loopStart = markersList.filter(function(marker){
				if(e.start > marker.start - offfset && e.start <  marker.start + offfset) {
					loopStartRegionStartHit = true;
					return marker;
				} else if(e.start > marker.end - offfset && e.start <  marker.end + offfset){
					loopStartRegionStartHit = false;
					return marker;
				}
			});


			//loop end in range of marker
			var loopEnd = markersList.filter(function(marker){
				if(e.end > marker.start - offfset && e.end <  marker.start + offfset) {
					loopEndRegionStartHit = true;
					return marker;
				} else if(e.end > marker.end - offfset && e.end <  marker.end + offfset){
					loopEndRegionStartHit = false;
					return marker;
				}
			});


			update = {
				start : e.start,
				end : e.end,
				loop: e.loop
			};
			
			if(loopStart.length > 0){
				marker = loopStart[loopStart.length-1];
				position = (loopStartRegionStartHit == true) ? marker.start :  marker.end;
				if(startAttached == false ) {
					
					update.start = position;
					update.resize = false;
					update.drag = false;
					
					startFixed = position;
					startAttached = true;

					setTimeout(function(){
						var update = {
							resize: true,
							drag: true,
						};
						updateAllRegions(update);
					}.bind(this), 500);
				} 
			}
			
			if(loopEnd.length > 0){
				marker = loopEnd[loopEnd.length-1];
				position = (loopEndRegionStartHit == true) ? marker.start :  marker.end;
				if(endAttached == false ) {
					
					update.end = position;
					update.resize = false;
					update.drag = false;
					
					endFixed = position;
					endAttached = true;

					setTimeout(function(){
						var update = {
							resize: true,
							drag: true,
						};
						updateAllRegions(update);
					}.bind(this), 500);
				} 
			}
			
			if(startAttached && (e.start > startFixed + offfset || e.start < startFixed - offfset)){
				startAttached = false;
			}

			if(endAttached && (e.end > endFixed + offfset || e.end < endFixed - offfset)){
				endAttached = false;
			}
			
			

			updateAllRegions(update);
			
		 }

		 function updateAllRegions(data){
			files.forEach(function(item){
				item.wavesurfer.un('region-updated');
			});
			globalRegionList.forEach(function(region, index){
					region.update(data);
			});
			files.forEach(function(item){
				item.wavesurfer.on('region-updated',onRegionUpdate);
			});
		 }

		 function hideLoops(){
			var update = {
				loop: false,
				drag: false,
				resize: false,
				color: 'transparent'
			};									
			globalRegionList.forEach(function(region, index){
				region.update(update);
			});
			jQuery(".wavesurfer-region").css('border','none');
		 }
	 }
