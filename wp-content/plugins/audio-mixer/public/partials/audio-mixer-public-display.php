<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/public/partials
 */
 $uniqueCode = rand(10000,999999);
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php
    if(is_user_logged_in()){
        $tmpLink = get_edit_post_link($id);;
?>
<a style="font-weight: bold; font-size: 14px; float:right;" target="_blank" href="<?php echo $tmpLink; ?>"> Edit Song </a>
<?php
    }
?>

<br>
<div class="audioPlayer">
    <div id="presetsHolder_<?php echo $uniqueCode?>" class="presetsHolder">
        <div class="presets_label">
            Presets:
        </div>
        <div class="presets_content" id="presets_content_<?php echo $uniqueCode?>">
        </div>
    </div>
    <div class="timerHolder" id="timerHolder_<?php echo $uniqueCode?>">
        00:00 / 00:00
    </div>
    <div class="markerHolder">
        <div class="markers_label">
            <span style="padding-left:5px;">Markers:</span>
        </div>
        <div class="marker_content" id="marker_content">
        </div>
        <div class="marker_control_label"></div>
    </div>
    <div id="audioPlayersHolder_<?php echo $uniqueCode?>" class="audioPlayersHolder">
        <form id="audioPlayersForm_<?php echo $uniqueCode?>" name="audioPlayersForm_<?php echo $uniqueCode?>">
        </form>
    </div>
    <div class="buttonsHolder" style="display: flex;">
        <div style="width: 87.5%">
            <span title="Play" class="play_btn" id="playbtn_<?php echo $uniqueCode?>"></span>
            <span title="Pause" class="pause_btn" id="pausebtn_<?php echo $uniqueCode?>"></span>
            <span title="Restart" class="restart_btn" id="restartbtn_<?php echo $uniqueCode?>"></span>
            <!-- <span title="Reset Settings" class="reset_btn" id="resetbtn_<?php echo $uniqueCode?>"></span> -->
            <span title="Loop Mode" class="loop_mode_off" id="enableLoop_<?php echo $uniqueCode?>"></span>
            <span title="Sub Titles" class="st_mode_off" id="enableSt_<?php echo $uniqueCode?>"></span>

        </div>
        <div style="width: 10%" class="download_btn" id="downloadbtn_<?php echo $uniqueCode?>">
        </div>
    </div>
    <div class="subsHolder" id="subsHolder_<?php echo $uniqueCode?>">
        <div id="subtitles_div" >
        </div>
    </div>
    
    <div class="back_wraper back_wraper_<?php echo $uniqueCode?>">
        <div class="email_popup email_popup_<?php echo $uniqueCode?>">
            <div class="close_popup pull-right">x</div>
            <h4>[[download_box_heading]]</h4>
            <form method="get">
                <div class="errorMsg" id="errorMsg_<?php echo $uniqueCode?>">&nbsp;</div>
                <input name="emailHolder" id="email_<?php echo $uniqueCode?>" required type="text" placeholder="[[download_box_details]]" />
                <br /><br />
                <input type="submit" value="[[download_box_button]]" class="pull-right dwn_btn" id="notifybtn_<?php echo $uniqueCode?>" />
            </form>
        </div>
    </div>

    <div class="back_wraper back_wraper_success_<?php echo $uniqueCode?>">
        <div class="email_popup email_popup_<?php echo $uniqueCode?>" style="min-height:auto">
            <div class="close_popup pull-right">x</div>
            <h4>[[thank_box_heading]]</h4>
            <center>
                [[thank_box_details]]<br />
                <input type="submit" value="[[thank_box_button]]" id="ok_<?php echo $uniqueCode?>" />
            </center>
        </div>
    </div>
</div>

<script>
    (function( $ ) {
        $(function() {
            var subtitles=[[subtitles]];
            var loops = [[loops]];
            var markers = [[markers]];
            var offset = [[SRT_OFFSET]];
            if(subtitles==""){
                subtitles={};
            }
            initializePlayer('[[filesData]]', <?php echo $uniqueCode;?>, '[[postId]]', '[[presets]]','[[presetSettings]]', "[[autoplay]]", "[[preset]]","[[subtitles_show]]",subtitles, loops, markers, offset, "[[please_wait_text]]");
	    });
    })( jQuery );
</script>