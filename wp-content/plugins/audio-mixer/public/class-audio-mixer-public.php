<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/public
 * @author     Ali Arshad <email@aliarshad.info>
 */
class Audio_Mixer_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Audio_Mixer_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Audio_Mixer_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name . "_audio-mixer-public_css", plugin_dir_url(__FILE__) . 'css/audio-mixer-public.css', array(), filemtime(plugin_dir_path(__FILE__) . 'css/audio-mixer-public.css'), 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Audio_Mixer_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Audio_Mixer_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_register_script($this->plugin_name . "_audio-mixer-public_js", plugin_dir_url(__FILE__) . 'js/audio-mixer-public.js', array('jquery', $this->plugin_name . "_wavesurfer"), filemtime(plugin_dir_path(__FILE__) . 'js/audio-mixer-public.js'), true);
        wp_register_script($this->plugin_name . "_wavesurfer", plugin_dir_url(__FILE__) . 'js/wavesurfer.min.js', array('jquery'), $this->version, true);
        wp_register_script($this->plugin_name . "_wavesurfer_region", plugin_dir_url(__FILE__) . 'js/region.min.js', array('jquery'), $this->version, true);

        wp_enqueue_script($this->plugin_name . "_audio-mixer-public_js");
        wp_enqueue_script($this->plugin_name . "_wavesurfer");
        wp_enqueue_script($this->plugin_name . "_wavesurfer_region");
        $plugin_url = plugin_dir_url(__DIR__);
        wp_localize_script($this->plugin_name . "_audio-mixer-public_js", 'ajax', array('ajax_url' => admin_url('admin-ajax.php'), 'plugin_url' => $plugin_url));
    }

    public function get_post_meta($id, $key, $unique = false) {
        $data = get_post_meta($id, $key, $unique);
        if (is_string($data)) {
            // $data = htmlspecialchars_decode($data);
            $data = $data;
        } else if (is_array($data)) {
            for ($i = 0; $i < COUNT($data); $i++) {
                // $data[$i] = htmlspecialchars_decode($data[$i]);
                $data[$i] = $data[$i];
            }
        }
        return $data;
    }

    function shortcode_audioMixer($attr) {

        if (isset($_GET['preset'])) {
            $preset = $_GET['preset'];
        } else {
            $preset = "";
        }
        $id = 0;
        if ($attr['id']) {
            $id = $attr['id'];
        } else {
            echo "Track id not provided";
            return;
        }
        // $templateData = file_get_contents(__DIR__."/partials/audio-mixer-public-display.php");
        ob_start();
        include "partials/audio-mixer-public-display.php";
        $templateData = ob_get_clean();

        $jsonData = "[";
        $fileIds = $this->get_post_meta($id, 'fileId');

        $order = array();
        $file = array();
        foreach ($fileIds as $fileId) {
            $data = $this->get_post_meta($id, 'order' . $fileId, true);
            $order[$fileId] = $data;
        }
        asort($order);
        foreach ($order as $key => $o) {
            array_push($file, $key);
        }

        foreach ($file as $fileId) {
            $attachmentData = new stdClass();
            $attachmentData->id = $fileId;
            $attachmentData->url = wp_get_attachment_url($fileId);
            $attachment = get_post($fileId);
            $attachmentData->title = str_replace("\"", "", $attachment->post_content); //get_the_title($fileId);

            $attachmentData->volume = $this->get_post_meta($id, 'filesVolume' . $fileId, true);
            $attachmentData->muted = $this->get_post_meta($id, 'filesMute' . $fileId, true);
            $attachmentData->color = $this->get_post_meta($id, 'color' . $fileId, true);

            $jsonData .= json_encode($attachmentData) . ",";
        }
        $jsonData = substr($jsonData, 0, -1);
        $jsonData .= "]";

        $presets = $this->get_post_meta($id, 'presets', true);
        $presetSettings = $this->get_post_meta($id, 'presetSettings', true);
        $autoplay = $this->get_post_meta($id, 'autoplay', true);

        $subtitles_show = $this->get_post_meta($id, 'subtitles_show',true);
        $subtitles = $this->get_post_meta($id, 'subtitles');
        if ($subtitles_show == "") {
            $subtitles_show = "yes";
        }

        $loops = $this->get_post_meta($id, 'loops');

        $markers = $this->get_post_meta($id, 'markers');
        $srtOffset = $this->get_post_meta($id, 'srtOffset');

        $download_box_heading = stripslashes(get_option("download_box_heading", "We are compiling your track"));
        $download_box_details = stripslashes(get_option("download_box_details", "Enter your email address, to get notified once the track is ready."));
        $download_box_button = stripslashes(get_option("download_box_button", "Get Notified"));

        $thank_box_heading = stripslashes(get_option("thank_box_heading", "Great!"));
        $thank_box_details = stripslashes(get_option("thank_box_details", "We will send you an email, once your track is ready."));
        $thank_box_button = stripslashes(get_option("thank_box_button", "Ok"));
        $please_wait_text = stripslashes(get_option("please_wait_text", ""));

        $templateData = str_replace("[[postId]]", $id, $templateData);
        $templateData = str_replace("[[filesData]]", $jsonData, $templateData);
        $templateData = str_replace("[[presets]]", $presets, $templateData);
        $templateData = str_replace("[[presetSettings]]", $presetSettings, $templateData);
        $templateData = str_replace("[[autoplay]]", $autoplay, $templateData);
        $templateData = str_replace("[[preset]]", $preset, $templateData);

        $templateData = str_replace("[[download_box_heading]]", $download_box_heading, $templateData);
        $templateData = str_replace("[[download_box_details]]", $download_box_details, $templateData);
        $templateData = str_replace("[[download_box_button]]", $download_box_button, $templateData);

        $templateData = str_replace("[[thank_box_heading]]", $thank_box_heading, $templateData);
        $templateData = str_replace("[[thank_box_details]]", $thank_box_details, $templateData);
        $templateData = str_replace("[[thank_box_button]]", $thank_box_button, $templateData);

        $templateData = str_replace("[[subtitles_show]]", $subtitles_show, $templateData);
        $templateData = str_replace("[[subtitles]]", json_encode($subtitles[0]), $templateData);
        /*$templateData = str_replace("[[loops]]", json_encode($loops[0]), $templateData);*/
        $templateData = str_replace("[[markers]]", json_encode($markers[0]), $templateData);

        $templateData = str_replace("[[SRT_OFFSET]]", json_encode($srtOffset[0]), $templateData);
        $templateData = str_replace("[[please_wait_text]]", $please_wait_text, $templateData);

        // $templateData = stripslashes($templateData);

        return $templateData;
        // echo $this->version;
    }



	function allowCORS() {
		$URL = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'];
		$URL = $_SERVER['HTTP_ORIGIN'];

		if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ) {

			@header( 'Access-Control-Allow-Origin: ' . $URL );
			@header( "Access-Control-Allow-Credentials: true" );
			@header( 'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS' );
			@header( 'Access-Control-Max-Age: 1000' );
			@header( 'Access-Control-Allow-Headers: Origin, Content-Type, Authorization' );
			@header( 'Control-Type: text/plain' );
			@header( 'Control-Length: 0' );
			http_response_code( 204 );
			die;
		}

		@header( 'Access-Control-Allow-Origin: ' . $URL );
	}


    function  processData($post, $json) {


    	if(count($post) > 0) {
    		$data = $post;

	    } else {

    		$data = $json;

    		foreach($json['presets'] as $index => $song) {
    			$data['filesId'][] = $index;
    			$data['filesMute'.$index] = $song['mute'] ? 'true' : 'false';
    			$data['filesVolume'.$index] = $song['volume'];
		    }

		    unset($data['presets']);


	    }


    	return $data;

    }




    function download_request() {

	    $this->allowCORS();


    	$postData = $_POST;
	    $jsonData = json_decode( file_get_contents( 'php://input' ), true );


	    $clientData = $this->processData($postData, $jsonData);


        $track_post = get_post($clientData['postId']);

        if ($track_post == null) {
            echo "Unable to find track";
            exit;
        }

	    $args = array(
		    "post_type" => "known_people",
		    'meta_query' =>array (
		    	'relation'  => 'AND',
			    array(
				    'key'   => 'known_person_email',
				    'value' =>  $clientData['email'],
				    'compare'   => '='
			    )
		    )
	    );
	    $known_query = new WP_Query($args);
	    if($known_query->post_count == 0){
		    $allowedEveryone = $this->get_post_meta($track_post->ID,'downloadKnown',true);
		    if(strtolower($allowedEveryone) != 'yes'){
                echo get_option("download_not_allowed", "");
		    	// echo "Download feature is available for members only.";
		    	exit;
		    }
	    }



        $download_post = array(
            'post_title' => $track_post->post_title . " | " .$clientData['email']. " | " . date('l jS \of F Y h:i:s A'),
            'post_status' => 'publish',
            'post_type' => 'mixer_downloads',
        );

        $post_Id = wp_insert_post($download_post);

        $fileIds = $clientData['filesId'];

        update_post_meta($post_Id, 'converted', 0);
        update_post_meta($post_Id, 'track', $track_post->ID);
        update_post_meta($post_Id, 'email', $clientData['email']);

        delete_post_meta($post_Id, 'fileId');
        foreach ($fileIds as $fileId) {

            add_post_meta($post_Id, 'fileId', $fileId);
            update_post_meta($post_Id, 'filesVolume' . $fileId, $clientData['filesVolume' . $fileId]);
            update_post_meta($post_Id, 'filesMute' . $fileId, $clientData['filesMute' . $fileId]);
        }

        // Buffer all upcoming output...
        ob_start();

        // Send your response.
        echo $post_Id;
        // Flush all output.
        // Get the size of the output.
        $size = ob_get_length();

        // Disable compression (in case content length is compressed).
        header("Content-Encoding: none");

        // Set the content length of the response.
        header("Content-Length: {$size}");

        // Close the connection.
        header("Connection: close");
        ob_end_flush();
        ob_flush();
        flush();



        set_time_limit(0);
        $totalFiles = count($fileIds);
        //ffmpeg -i 3.mp3 -i 7.mp3 -shortest -filter_complex '[0:a]volume=1[a0]; [1:a]volume=1[a1]; [a0][a1]amix=inputs=2[out]' -map '[out]' -ac 2 -c:a libmp3lame output.mp3
        $ffmpeg_cmd = "ffmpeg";
        $ffmpeg_filter_start = "";
        $ffmpeg_filter_end = "";
        foreach ($fileIds as $index => $fileId) {
            $volume = $clientData['filesVolume' . $fileId];

            if ($clientData['filesMute' . $fileId] == 'true') {
                $volume = 0;
            }

            $volume = $volume * 5;

            $ffmpeg_cmd .= " -i " . wp_get_attachment_url($fileId) . " ";
            $ffmpeg_filter_start .= "[$index:a]volume=" . $volume . "[a$index]; ";

            $ffmpeg_filter_end .= "[a$index]";
        }

        $name = str_replace(" ", "_", $track_post->post_title) . "_$post_Id.mp3";
        $name = preg_replace('/[^a-zA-Z0-9_.]/', '', $name);
        $output = plugin_dir_path(__FILE__) . "../files/$name";
        $log = plugin_dir_path(__FILE__) . "../logs/$post_Id.log";

        $ffmpeg_cmd .= "-shortest -filter_complex '$ffmpeg_filter_start $ffmpeg_filter_end amix=inputs=" . $totalFiles . "[out]'";
        $ffmpeg_cmd .= " -map '[out]' -ac 2 $output"; //-c:a libmp3lame
        $ffmpeg_cmd .= " > $log 2>&1 &";
        // echo $ffmpeg_cmd;

        $dta = shell_exec($ffmpeg_cmd);
        // var_dump($dta);
        update_post_meta($post_Id, 'converted', 1);
        update_post_meta($post_Id, 'fileName', $name);
        $this->send_link_email($clientData['email'], $post_Id, $track_post->ID,$name);
        exit;
    }

    function send_link_email($to, $post_Id, $origionalPostId,$name) {

	    $args = array(
		    "post_type" => "known_people",
		    'meta_query' =>array (
			    'relation'  => 'AND',
			    array(
				    'key'   => 'known_person_email',
				    'value' =>  $to,
				    'compare'   => '='
			    )
		    )
	    );
	    $known_query = new WP_Query($args);
        if ($known_query->have_posts()) {
            while ($known_query->have_posts()) {
                $known_query->the_post();

	            $email = $this->get_post_meta(get_the_ID(), 'known_person_email', true);
	            $fname = $this->get_post_meta(get_the_ID(), 'known_person_fname', true);
	            $lname = $this->get_post_meta(get_the_ID(), 'known_person_lname', true);
	            $addr = $this->get_post_meta(get_the_ID(), 'known_person_addr', true);
	            $gndr = $this->get_post_meta(get_the_ID(), 'known_person_gndr', true);
	            $phone = $this->get_post_meta(get_the_ID(), 'known_person_phone', true);
                $voiceRng = $this->get_post_meta(get_the_ID(), 'known_person_voiceRng', true);
                $voiceRngPl = $this->get_post_meta(get_the_ID(), 'known_person_voiceRngPl', true);


                $postURL    = get_permalink($post_Id);
                $subject    = get_post_meta($origionalPostId, 'subject_known', true);
                $body       = get_post_meta($origionalPostId, 'body_known', true);
                if ($subject == "") {
                    $subject = "Your file is ready";
                }
                if ($body == "") {
                    $body = "[[Link]] to download your file.";
                }
                $body = nl2br($body);
                $body = str_replace("[[Link]]", $postURL, $body);
                $body = str_replace("[[LINK]]", $postURL, $body);
                $body = str_replace("[[FNAME]]", $fname, $body);
                $body = str_replace("[[LNAME]]", $lname, $body);
                $body = str_replace("[[ADDR]]", $addr, $body);
                $body = str_replace("[[GNDR]]", $gndr, $body);
                $body = str_replace("[[EMAIL]]", $email, $body);
                $body = str_replace("[[PHONE]]", $phone, $body);
                $body = str_replace("[[VOICERNG]]", $voiceRng, $body);
                $body = str_replace("[[VOICERNGPL]]", $voiceRngPl, $body);
                $headers = array('Content-Type: text/html; charset=UTF-8');
                wp_mail($to, $subject, $body, $headers);
            }
        }
        else{
            $postURL = get_permalink($post_Id);
	        $subject    = get_post_meta($origionalPostId, 'subject_general', true);
            $body       = get_post_meta($origionalPostId, 'body_general', true);
            if ($subject == "") {
                $subject = "Your file is ready";
            }
            if ($body == "") {
                $body = "[[Link]] to download your file.";
            }
            $body = nl2br($body);
            $body = str_replace("[[Link]]", $postURL, $body);
	        $body = str_replace("[[LINK]]", $postURL, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8');
            wp_mail($to, $subject, $body, $headers);
        }
    }

    function download_files($content) {
        global $post;

        if ($post->post_type == 'mixer_downloads') {
            $fileAvailable = get_post_meta($post->ID, 'converted', true);
            if ($fileAvailable == 1) {

                $name = get_post_meta($post->ID, 'fileName', true);
                $file_url = plugin_dir_url(__DIR__) . "files/$name";
                $content .= '<meta http-equiv="refresh" content="2;url=' . $file_url . '">';
                $content .= 'Your file will be downloaded in a second...';
            } else if ($fileAvailable == 0) {
                $content .= 'Your file is still processing';
            } else if ($fileAvailable == 2) {
                $content .= 'Your file was auto deleted';
            }
        }
        return $content;
    }

}
