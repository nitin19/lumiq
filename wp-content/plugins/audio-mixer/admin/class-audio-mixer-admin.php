<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/admin
 * @author     Ali Arshad <email@aliarshad.info>
 */
class Audio_Mixer_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Audio_Mixer_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Audio_Mixer_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name . "_admin_css", plugin_dir_url(__FILE__) . 'css/audio-mixer-admin.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . "_datatables_css", plugin_dir_url(__FILE__) . 'js/datatables/datatables.min.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . "_chosen_css", plugin_dir_url(__FILE__) . 'css/chosen.min.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Audio_Mixer_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Audio_Mixer_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name . "_admin", plugin_dir_url(__FILE__) . 'js/audio-mixer-admin.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . "_datatables", plugin_dir_url(__FILE__) . 'js/datatables/datatables.min.js', array('jquery'), $this->version, false);
	    wp_enqueue_script($this->plugin_name . "_chosen_js", plugin_dir_url(__FILE__) . 'js/chosen.jquery.min.js', array(), $this->version, 'all');
    }

    public function register_files_post_type() {
        $supports = array("title", "revisions");
        register_post_type("audio_mixer_files", array(
            'labels' => array(
                'name' => __('Audio Player'),
                'singular_name' => __('Song'),
                'all_items' => __('All Songs'),
            ),
            'supports' => $supports,
            'public' => true,
            'map_meta_cap' => true,
            // 'capabilities' => array(
            //     'create_posts' => false,
            // ),
            // 'exclude_from_search' => true,
            'publicly_queryable' => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_icon' => 'dashicons-playlist-audio'
                )
        );
    }

    public function register_downloads_post_type() {
        $supports = array("title", "revisions", "custom-fields");
        register_post_type("mixer_downloads", array(
            'labels' => array(
                'name' => __('Downloads'),
                'singular_name' => __('Download Request'),
            ),
            'supports' => $supports,
            'public' => true,
            'map_meta_cap' => true,
            // 'capabilities' => array(
            //     'create_posts' => false,
            // ),
            // 'exclude_from_search' => true,
            'publicly_queryable' => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => 'edit.php?post_type=audio_mixer_files',
            'show_in_admin_bar' => true
                )
        );
    }

    public function register_known_people_post_type() {
        $supports = array("title", "revisions");
        register_post_type("known_people", array(
            'labels' => array(
                'name' => __('Known People'),
                'singular_name' => __('Known People'),
            ),
            'supports' => $supports,
            'public' => true,
            'map_meta_cap' => true,
            // 'capabilities' => array(
            //     'create_posts' => false,
            // ),
            // 'exclude_from_search' => true,
            'publicly_queryable' => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => 'edit.php?post_type=audio_mixer_files',
            'show_in_admin_bar' => true
                )
        );
    }

    public function register_files_texonomy() {
        register_taxonomy(
                "audio_mixer_texonomy", "audio_mixer_files", array(
            'labels' => array(
                'name' => __('Tracks'),
                'singular_name' => __('Track'),
                'query_var' => true,
                'public' => true,
                'hierarchical' => true,
                'show_ui' => true,
            )
                )
        );
    }


	public function register_user_groups_taxonomy() {
    	register_taxonomy(
    		'audio_mixer_user_group_taxonomy',
		    'audio_mixer_files',
		    array(
			    'labels' => array(
				    'name' => __('User Groups'),
				    'singular_name' => __('User Group'),
			    ),
			    'query_var' => true,
			    'public' => true,
			    'hierarchical'      => true,
			    'show_in_rest'  => true,
			    'show_ui' => true,
			    'show_admin_column' => true,

	        )
	    );
	}


	public function audio_mixer_user_group_taxonomy_add_user_group_field() {

    	include('partials/user-group-field-add.php');

	}
	public function audio_mixer_user_group_taxonomy_edit_user_group_field( $term) {


		include('partials/user-group-field-edit.php');

	}

	public function save_users_group_custom_meta($term_id) {
		if ( isset( $_POST['users'] ) ) {
			$term_meta = get_option( "taxonomy_$term_id" );
			$term_meta['users'] = json_encode($_POST['users']);
			update_option( "taxonomy_$term_id", $term_meta );
		}
	}

    public function restrict_files_by_tracks() {
        global $typenow;
        global $wp_query;
        // echo $typenow;die("wow");
        if ($typenow == 'audio_mixer_files') {

            $taxonomy = 'audio_mixer_texonomy';
            $business_taxonomy = get_taxonomy($taxonomy);
            wp_dropdown_categories(array(
                'show_option_all' => __("Show All {$business_taxonomy->label}"),
                'taxonomy' => $taxonomy,
                'name' => 'audio_mixer_texonomy',
                'orderby' => 'name',
                'selected' => $wp_query->query['audio_mixer_texonomy'],
                'hierarchical' => true,
                'depth' => 3,
                'show_count' => true, // Show # listings in parens
                'hide_empty' => false, // Don't show businesses w/o listings
            ));
        }
    }

    public function convert_track_id_to_taxonomy_term_in_query($query) {
        global $pagenow;
        $qv = &$query->query_vars;
        if ($pagenow == 'edit.php' && isset($qv['audio_mixer_texonomy'])) {
            $term = get_term_by('id', $qv['audio_mixer_texonomy'], 'audio_mixer_texonomy');
            $qv['term'] = $term->slug;
        }
    }

    public function add_tracks_column_to_files_list($posts_columns) {
        $new_posts_columns = array();
        $index = 0;
        foreach ($posts_columns as $key => $posts_column) {
            if ($key == 'author')
                $new_posts_columns['businesses'] = null;
            $new_posts_columns[$key] = $posts_column;
        }

        $new_posts_columns['tracks'] = 'Tracks';
        return $new_posts_columns;
    }

    public function show_tracks_column_for_files_list($column_name, $post_id) {
        global $typenow;
        if ($typenow == 'audio_mixer_files') {
            $taxonomy = 'audio_mixer_texonomy';
            switch ($column_name) {
                case 'tracks':
                    $tracks = get_the_terms($post_id, $taxonomy);
                    if (is_array($tracks)) {
                        foreach ($tracks as $key => $track) {
                            $edit_link = get_term_link($track, $taxonomy);
                            $tracks[$key] = '<a href="' . $edit_link . '">' . $track->name . '</a>';
                        }
                        echo implode(' | ', $tracks);
                    }
                    break;
            }
        }
    }

    public function file_meta_box() {
        add_meta_box(
            'audio_mixer_file_details',
            'Audio Files',
            array(
                $this,
                'display_audio_mixer_file_details'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }

    function known_people_meta_box() {
    	add_meta_box(
    		'audio_mixer_known_people',
		    'Person Information',
		    array($this,
			    'display_audio_mixer_known_people'
		    ),

		    'known_people',
		    'normal',
		    'high'
	    );
    }


    function setting_meta_box() {
    	add_meta_box(
    		'audio_mixer_settings',
		    'Autoplay Setting',
		    array($this,
			    'display_audio_mixer_setting'
		    ),
		    'audio_mixer_files',
		    'normal',
		    'high'
	    );
    }

    public function loop_meta_box() {
        add_meta_box(
            'audio_mixer_loops',
            'Loops',
            array(
                $this,
                'display_audio_mixer_loops'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }

    public function markers_meta_box() {
        add_meta_box(
            'audio_mixer_marker',
            'Markers',
            array(
                $this,
                'display_audio_mixer_marker'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }

    public function subtitles_meta_box() {
        add_meta_box(
            'audio_mixer_subtitle',
            'Subtitles',
            array(
                $this,
                'display_audio_mixer_subtitle'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }

    public function downloads_meta_box() {
        add_meta_box(
            'audio_mixer_download',
            'Downloads',
            array(
                $this,
                'display_audio_mixer_downloads'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }

    public function emails_meta_box() {
        add_meta_box(
            'audio_mixer_emails',
            'Emails',
            array(
                $this,
                'display_audio_mixer_emails'
            ),
            'audio_mixer_files',
            'normal',
            'high'
        );
    }


    public function shortcode_meta_box() {
        add_meta_box('audio_mixer_file_shortcode', 'Shortcode', array($this, 'display_audio_mixer_file_shortcode'), 'audio_mixer_files', 'side', 'high'
        );
    }

    public function display_audio_mixer_file_details() {

        $jsonData = "[";
        $fileIds = $this->get_post_meta(get_the_ID(), 'fileId');
        $audio_mixer_colors=get_option("audio_mixer_colors", "");
        
        $order = array();
        $file = array();
        foreach ($fileIds as $fileId) {
            $data = $this->get_post_meta(get_the_ID(), 'order' . $fileId, true);
            $order[$fileId] = $data;
        }
        asort($order);
        foreach ($order as $key => $o) {
            array_push($file, $key);
        }

        foreach ($file as $fileId) {
            $attachmentData = new stdClass();
            $attachmentData->id = $fileId;
            $attachmentData->url = wp_get_attachment_url($fileId);
            $attachment = get_post($fileId);
            $attachmentData->title = str_replace("\"", "", $attachment->post_content); //get_the_title($fileId);

            $attachmentData->volume = $this->get_post_meta(get_the_ID(), 'filesVolume' . $fileId, true);
            $attachmentData->muted = $this->get_post_meta(get_the_ID(), 'filesMute' . $fileId, true);
            $attachmentData->color = $this->get_post_meta(get_the_ID(), 'color' . $fileId, true);

            $jsonData .= json_encode($attachmentData) . ",";
        }
        $jsonData = substr($jsonData, 0, -1);
        $jsonData .= "]";

        $presets = $this->get_post_meta(get_the_ID(), 'presets', true);
        $presetSettings = $this->get_post_meta(get_the_ID(), 'presetSettings', true);

        $data = file_get_contents(__DIR__ . "/partials/audio-mixer-audio-file-display.php");
        $data = str_replace("[[filesData]]", $jsonData, $data);
        $data = str_replace("[[presets]]", $presets, $data);
        $data = str_replace("[[presetSettings]]", $presetSettings, $data);
        $data = str_replace("[[defaultColors]]", json_encode($audio_mixer_colors), $data);

        echo $data;
    }

    public function display_audio_mixer_known_people() {
	    $data = file_get_contents(__DIR__ . "/partials/known-people.php");

	    $email = $this->get_post_meta(get_the_ID(), 'known_person_email', true);
	    $fname = $this->get_post_meta(get_the_ID(), 'known_person_fname', true);
	    $lname = $this->get_post_meta(get_the_ID(), 'known_person_lname', true);
	    $addr = $this->get_post_meta(get_the_ID(), 'known_person_addr', true);
	    $gndr = $this->get_post_meta(get_the_ID(), 'known_person_gndr', true);
	    $phone = $this->get_post_meta(get_the_ID(), 'known_person_phone', true);
        $voiceRng = $this->get_post_meta(get_the_ID(), 'known_person_voiceRng', true);
        $voiceRngPl = $this->get_post_meta(get_the_ID(), 'known_person_voiceRngPl', true);


	    $data = str_replace('[[FNAME]]', $fname, $data);
	    $data = str_replace('[[LNAME]]', $lname, $data);
	    $data = str_replace('[[EMAIL]]', $email, $data);
	    $data = str_replace('[[GNDR]]', $gndr, $data);
	    $data = str_replace('[[ADDR]]', $addr, $data);
	    $data = str_replace('[[PHONE]]', $phone, $data);
        $data = str_replace('[[VOICERNG]]', $voiceRng, $data);
        $data = str_replace('[[VOICERNGPL]]', $voiceRngPl, $data);

	    echo $data;
    }

	public function display_audio_mixer_setting() {
		$data = file_get_contents(__DIR__ . "/partials/settings.php");

		$autoplay = $this->get_post_meta(get_the_ID(), 'autoplay', true);
		$downloadKnown = $this->get_post_meta(get_the_ID(), 'downloadKnown', true);

		if ($autoplay == "yes") {
			$data = str_replace("[[autoplay_yes]]", "selected", $data);
			$data = str_replace("[[autoplay_no]]", "", $data);
		} else {
			$data = str_replace("[[autoplay_yes]]", "", $data);
			$data = str_replace("[[autoplay_no]]", "selected", $data);
		}

		if ($downloadKnown == "yes") {
			$data = str_replace("[[known_yes]]", "selected", $data);
			$data = str_replace("[[known_no]]", "", $data);
		} else {
			$data = str_replace("[[known_yes]]", "", $data);
			$data = str_replace("[[known_no]]", "selected", $data);
		}

		echo $data;
	}

    public function display_audio_mixer_loops(){
        $data = file_get_contents(__DIR__ . "/partials/loops-meta-box.php");
        $loops = $this->get_post_meta(get_the_ID(), 'loops', true);
        $loops = json_encode($loops);
        $data = str_replace("LOOP_DATA_JSON", $loops, $data);
        echo $data;
    }

    public function display_audio_mixer_marker(){
        $data = file_get_contents(__DIR__ . "/partials/markers-meta-box.php");
        $markers = $this->get_post_meta(get_the_ID(), 'markers', true);
        $data = str_replace("MARKER_DATA_JSON", $markers, $data);
        echo $data;
    }

    public function display_audio_mixer_subtitle(){
        $data = file_get_contents(__DIR__ . "/partials/subtitles-meta-box.php");

        $subtitles_show = $this->get_post_meta(get_the_ID(), 'subtitles_show',true);
        $subtitles = $this->get_post_meta(get_the_ID(), 'subtitles', true);
        $srtOffset = $this->get_post_meta(get_the_ID(), 'srtOffset', true);
        $srt = $this->get_post_meta(get_the_ID(), 'srtFile', true);
        if ($subtitles_show == "") {
            $subtitles_show = "yes";
        }
        if(!$subtitles){
            $subtitles = [];
        }
        $subtitles = json_encode($subtitles);

        $data = str_replace("[[subtitles_show]]", $subtitles_show, $data);
        $data = str_replace("[[subtitles]]", $subtitles, $data);
	    if(isset($srt) && $srt != '') {
		    $data = str_replace("[[SRT_FILE]]", $srt, $data);
	    } else {
		    $data = str_replace("[[SRT_FILE]]", 0, $data);
	    }
	    if(isset($srtOffset) && $srtOffset != '') {
		    $data = str_replace("[[SRT_OFFSET]]", $srtOffset, $data);
	    } else {
		    $data = str_replace("[[SRT_OFFSET]]", 0, $data);
	    }

        echo $data;
    }

    public function display_audio_mixer_downloads(){
        $data = file_get_contents(__DIR__ . "/partials/downloads-meta-box.php");

        global $post;

        $data = str_replace("[[postId]]", $post->ID, $data);

        echo $data;
    }

    public function display_audio_mixer_emails(){
        $data = file_get_contents(__DIR__ . "/partials/emails-meta-box.php");

        $subject_known = htmlentities($this->get_post_meta(get_the_ID(), 'subject_known', true));
        $body_known = htmlentities($this->get_post_meta(get_the_ID(), 'body_known', true));

        if ($subject_known == "") {
	        $subject_known = "Your file is ready";
	    }
	    if ($body_known == "") {
		    $body_known = "[[Link]] to download your file.";
	    }


        $subject_general = htmlentities($this->get_post_meta(get_the_ID(), 'subject_general', true));
	    $body_general = htmlentities($this->get_post_meta(get_the_ID(), 'body_general', true));

	    if ($subject_general == "") {
		    $subject_general = "Your file is ready";
	    }
	    if ($body_general == "") {
		    $body_general = "[[Link]] to download your file.";
	    }


	    $data = str_replace('[[SUBJECT_KNOWN]]', $subject_known, $data);
	    $data = str_replace('[[BODY_KNOWN]]', $body_known, $data);

	    $data = str_replace('[[SUBJECT_GENERAL]]', $subject_general, $data);
	    $data = str_replace('[[BODY_GENERAL]]', $body_general, $data);

        echo $data;
    }



    public function get_post_meta($id, $key, $unique = false) {
        $data = get_post_meta($id, $key, $unique);
        if (is_string($data)) {
            $data = $data;
            //$data = htmlspecialchars_decode($data);
        } else if (is_array($data)) {
            for ($i = 0; $i < COUNT($data); $i++) {
                if($key == 'subtitles' || $key == 'loops'){
                    continue;
                }
                $data[$i] = $data[$i];
            //$data[$i] = htmlspecialchars_decode($data[$i]);
            }
        }
        return $data;
    }

    public function update_post_meta($id, $key, $value) {
        // update_post_meta($id, $key, htmlspecialchars($value));
        update_post_meta($id, $key, $value);
    }

    public function save_audio_mixer_file_details() {
        global $typenow;

	    /*  Known Person Info   */
        if($typenow == 'known_people' && (isset($_POST['save']) || isset($_POST['publish']))){
	        if(isset($_POST['known_person'])) {
		        $this->update_post_meta(get_the_ID(), 'known_person_fname', $_POST['known_person']['fname']);
		        $this->update_post_meta(get_the_ID(), 'known_person_lname', $_POST['known_person']['lname']);
		        $this->update_post_meta(get_the_ID(), 'known_person_gndr', $_POST['known_person']['gndr']);
		        $this->update_post_meta(get_the_ID(), 'known_person_phone', $_POST['known_person']['phone']);
                $this->update_post_meta(get_the_ID(), 'known_person_voiceRng', $_POST['known_person']['voiceRng']);
                $this->update_post_meta(get_the_ID(), 'known_person_voiceRngPl', $_POST['known_person']['voiceRngPl']);
		        $this->update_post_meta(get_the_ID(), 'known_person_addr', $_POST['known_person']['addr']);
		        $this->update_post_meta(get_the_ID(), 'known_person_email', $_POST['known_person']['email']);
		        $this->update_post_meta(get_the_ID(), 'known_person', json_encode($_POST['known_person']));
	        }
        }


        if ($typenow == "audio_mixer_files" && (isset($_POST['save']) || isset($_POST['publish']))) {
            $fileIds = $_POST['filesId'];
            delete_post_meta(get_the_ID(), 'fileId');
            foreach ($fileIds as $fileId) {
                add_post_meta(get_the_ID(), 'fileId', $fileId);
                $this->update_post_meta(get_the_ID(), 'filesVolume' . $fileId, $_POST['filesVolume' . $fileId]);
                $this->update_post_meta(get_the_ID(), 'filesMute' . $fileId, $_POST['filesMute' . $fileId]);
                $this->update_post_meta(get_the_ID(), 'color' . $fileId, $_POST['color' . $fileId]);
                $this->update_post_meta(get_the_ID(), 'order' . $fileId, $_POST['filesOrder' . $fileId]);
            }
            $this->update_post_meta(get_the_ID(), 'presets', $_POST['presets']);
            $this->update_post_meta(get_the_ID(), 'presetSettings', $_POST['presetSettings']);

            $this->update_post_meta(get_the_ID(), 'subject_known', $_POST['subject_known']);
            $this->update_post_meta(get_the_ID(), 'body_known', $_POST['body_known']);

            $this->update_post_meta(get_the_ID(), 'subject_general', $_POST['subject_general']);
            $this->update_post_meta(get_the_ID(), 'body_general', $_POST['body_general']);

            $this->update_post_meta(get_the_ID(), 'subtitles_show', $_POST['subtitle_show']);
            $this->update_post_meta(get_the_ID(), 'subtitles', $_POST['subtitle_data']);

            /*$this->update_post_meta(get_the_ID(), 'loops', $_POST['loop_data']);*/

            $this->update_post_meta(get_the_ID(), 'markers', json_encode($_POST['marker_data']));

            $this->update_post_meta(get_the_ID(), 'autoplay', $_POST['autoplay']); //autoplay_yes
            $this->update_post_meta(get_the_ID(), 'srtFile', $_POST['srtFile']);
            $this->update_post_meta(get_the_ID(), 'srtOffset', $_POST['srtOffset']);

            $this->update_post_meta(get_the_ID(), 'downloadKnown', $_POST['download_known']);
        }
    }

    public function display_audio_mixer_file_shortcode() {
        $id = get_the_ID();
        echo "<center>[audioMixer id=\"" . $id . "\" ]</center>";
    }

    function mixer_downloads_data() {

        $start = $_POST['start'];
        $parent_id = $_POST['parent_id'];
        $post_per_page = $_POST['length'];
        $search = $_POST['search']['value'];

        $returnData = new stdClass;

        $data = [];


        $meta_query = [];

        $parent_post = array(
            "key" => "track",
            "value" => $parent_id
        );
        array_push($meta_query, $parent_post);

        if ($search != "") {
            $search_post = array(
                "key" => "email",
                "value" => $search,
                "compare" => "LIKE"
            );
            array_push($meta_query, $search_post);
        }


        $args = array(
            'post_type' => 'mixer_downloads',
            'posts_per_page' => $post_per_page,
            // 'paged'	=> $page,
            'offset' => $start,
            'meta_query' => array(
                'meta_query' => 'AND',
                $meta_query
            )
        );


        // var_dump($args);
        $the_query = new WP_Query($args);

        $returnData->recordsTotal = $the_query->post_count;
        $returnData->recordsFiltered = intval($the_query->found_posts);

        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $postData = array();
                array_push($postData, get_the_id());
                array_push($postData, get_post_meta(get_the_id(), "email", true));
                array_push($postData, get_the_time('U'));

                array_push($data, $postData);
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            // no posts found
        }
        $returnData->data = $data;



        // $data = '{"draw": 4,"recordsTotal": 57,"recordsFiltered": 57,"data":[[1, "email@aliarshad.info", "Tue, 12 March, 2017"],[2, "ali.arshad@vQode.com", "Wed, 13 March, 2017"],[1, "email@aliarshad.info", "Tue, 12 March, 2017"],[2, "ali.arshad@vQode.com", "Wed, 13 March, 2017"],[1, "email@aliarshad.info", "Tue, 12 March, 2017"],[2, "ali.arshad@vQode.com", "Wed, 13 March, 2017"],[1, "email@aliarshad.info", "Tue, 12 March, 2017"],[2, "ali.arshad@vQode.com", "Wed, 13 March, 2017"],[1, "email@aliarshad.info", "Tue, 12 March, 2017"],[2, "ali.arshad@vQode.com", "Wed, 13 March, 2017"]]}';
        echo json_encode($returnData);
        exit;
    }

    public function add_menu_item() {
        // add_menu_page( 'Mixer Settings', 'Mixer Settings', 'manage_options', 'mixer-plugin', array($this, 'mixer_admin') );
        add_submenu_page("edit.php?post_type=audio_mixer_files", "Mixer settings", "Mixer settings", 'manage_options', 'mixer-plugin', array($this, 'mixer_admin'));
    }

    public function mixer_admin() {
        // $sendFrom = get_option("viralVideoSendFrom", FROM_EMAIL);
        // $sendFrom = get_option("viralVideoSendFrom", FROM_EMAIL);
        if (isset($_POST['save'])) {
            update_option("download_box_heading", $_POST['download_box_heading'], true);
            update_option("download_box_details", $_POST['download_box_details'], true);
            update_option("download_box_button", $_POST['download_box_button'], true);

            update_option("thank_box_heading", $_POST['thank_box_heading'], true);
            update_option("thank_box_details", $_POST['thank_box_details'], true);
            update_option("thank_box_button", $_POST['thank_box_button'], true);
            delete_option("audio_mixer_colors");
            update_option("audio_mixer_colors", $_POST['audio_mixer_colors'],true);
            update_option("marker", $_POST['marker'],true);
            update_option("marker-short", $_POST['marker-short'],true);

            update_option("please_wait_text", $_POST['please_wait_text'],true);
            update_option("download_not_allowed", $_POST['download_not_allowed'],true);
        }

        $download_box_heading = get_option("download_box_heading", "");
        $download_box_details = get_option("download_box_details", "");
        $download_box_button = get_option("download_box_button", "");

        $thank_box_heading = get_option("thank_box_heading", "");
        $thank_box_details = get_option("thank_box_details", "");
        $thank_box_button = get_option("thank_box_button", "");
        
        $audio_mixer_colors=get_option("audio_mixer_colors", "");

        $please_wait_text=get_option("please_wait_text", "");
        $download_not_allowed=get_option("download_not_allowed", "");
        $markers_short = get_option("marker-short", "");
        $markers = get_option("marker", "");


        $download_box_heading = stripslashes($download_box_heading);
        $download_box_details = stripslashes($download_box_details);
        $download_box_button = stripslashes($download_box_button);

        $thank_box_heading = stripslashes($thank_box_heading);
        $download_box_details = stripslashes($download_box_details);
        $download_box_button = stripslashes($download_box_button);

        $please_wait_text = stripslashes($please_wait_text);
        $download_not_allowed = stripslashes($download_not_allowed);

        $htmlData = file_get_contents(__DIR__ . "/partials/admin.php");
        $htmlData = str_replace("[[download_box_heading]]", $download_box_heading, $htmlData);
        $htmlData = str_replace("[[download_box_details]]", $download_box_details, $htmlData);
        $htmlData = str_replace("[[download_box_button]]", $download_box_button, $htmlData);

        $htmlData = str_replace("[[thank_box_heading]]", $thank_box_heading, $htmlData);
        $htmlData = str_replace("[[thank_box_details]]", $thank_box_details, $htmlData);
        $htmlData = str_replace("[[thank_box_button]]", $thank_box_button, $htmlData);

        $htmlData = str_replace("[[please_wait_text]]", $please_wait_text, $htmlData);
        $htmlData = str_replace("[[download_not_allowed]]", $download_not_allowed, $htmlData);
        
        $htmlData = str_replace("[[audio_mixer_colors]]", json_encode($audio_mixer_colors), $htmlData);
        $htmlData = str_replace("[[markers]]", json_encode($markers), $htmlData);
        $htmlData = str_replace("[[markers_short]]", json_encode($markers_short), $htmlData);


        echo $htmlData;
    }

    function clean_files_schedule() {
        if (!wp_next_scheduled('delete_old_files')) {
            wp_schedule_event(time(), 'daily', 'delete_old_files');
        }
    }

    function delete_old_files() {

        $subject = "Files Deleted";
        $body = "";
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $args = array(
            'post_type' => 'mixer_downloads',
            'posts_per_page' => -1,
            'meta_key' => 'converted',
            'meta_value' => 1,
            'date_query' => array(
                'before' => date('Y-m-d', strtotime('-2 days'))
            )
        );

        $the_query = new WP_Query($args);

        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $fileAvailable = get_post_meta(get_the_id(), 'converted', true);
                $body .= get_the_id();
                $body .= " fileAvailable: $fileAvailable ";
                if ($fileAvailable == 1) {
                    $name = get_post_meta(get_the_id(), 'fileName', true);
                    $file_path = plugin_dir_path(__DIR__) . "files/$name";

                    $body .= " file_path: $file_path ";
                    $unlinked = unlink($file_path);
                    $body .= " unlinked: $unlinked ";
                    if ($unlinked) {
                        update_post_meta(get_the_id(), 'converted', 2);
                    }
                }
                /* Restore original Post Data */
                wp_reset_postdata();
            }
        }

        wp_mail("ali.arshad.gem@gmail.com", $subject, $body, $headers);
    }

}
