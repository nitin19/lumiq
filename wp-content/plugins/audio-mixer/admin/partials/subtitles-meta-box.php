<div id="subtitles_div"></div>
<h3>Subtitles</h3>
<label>Show by default: </label>
<select name="subtitle_show" id="subtitle_show">
    <option value="yes">Yes</option>
    <option value="no">No</option>
</select>
<label style="margin-left: 20px;">Offset: <input type="number" step="0.01" name="srtOffset" style="width: 70px;" placeholder="00" value="[[SRT_OFFSET]]"> <small>seconds</small> </label>
<div class="loading-class">
</div>
<div style="margin: 20px 0;" class="srt-parser">
    <label>SRT File: </label>
    <button type="button" id="add-srt">Select SRT</button>
    <input type="hidden" name="srtFile" id="srtHiddenField" value="[[SRT_FILE]]">
</div>
<hr>
<table >
    <thead>
    <tr>
        <th style="width: 20%">Time - From</th>
        <th style="width: 20%">Time - To</th>
        <th style="width: 50%">Subtitles</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><input type="text" id="start_time_subtitle" placeholder="00:00:00,000"><small>Format 00:00:00,000</small></td>
        <td><input type="text" id="end_time_subtitle" placeholder="00:00:00,000"><small>Format 00:00:00,000</small></td>
        <td><textarea  name="" id="subtitle_text"></textarea></td>
        <td><a id="add_subtitle" class="add_btn">Add</a><br>&nbsp;</td>
    </tr>
    </tbody>
</table>
<a class="delete" id="deleteAllSubs" style="padding: 10px; font-weight: bold; cursor: pointer; float: right;">Delete All</a>
<table id="subtitles_table"  width="100%" class="subtitle_table">
    <thead>
    <tr>
        <th style="width: 20%">Time - From</th>
        <th style="width: 20%">Time - To</th>
        <th style="width: 50%">Subtitles</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    <tr>
        <th style="width: 20%">Time - From</th>
        <th style="width: 20%">Time - To</th>
        <th style="width: 50%">Subtitles</th>
        <th>Action</th>
    </tr>
    </tfoot>
</table>

<script>

    (function(){
    
    var subtitles=[[subtitles]];
    var subtitles_show='[[subtitles_show]]';
    var addSubtitleBtn= jQuery("#add_subtitle");
    var subsCounter = 0;

    function validateTimeFormat(time){
        var arr = time.split(':');
        if(isNaN(parseInt(arr[0])) || isNaN(parseInt(arr[1]))){
            return false;
        }
        if(parseInt(arr[0]) < 0  || parseInt(arr[1]) < 0 || parseInt(arr[1]) > 59 )
        {
            return false;
        }
        return true;
    }

    if(addSubtitleBtn){
        addSubtitleBtn.on('click',function(event){
            event.preventDefault();
            var start_time = jQuery("#start_time_subtitle").val();
            var end_time=jQuery("#end_time_subtitle").val();
            if(validateTimeFormat(start_time) && validateTimeFormat(end_time)){
                var subtitle = jQuery("#subtitle_text").val();
                addSubsRow(start_time, end_time, subtitle);
            } else {
                alert("Invalid Time Format Given.");
                return;
            }
            sortTable();
        });
    }

    function deleteSub(){
        jQuery("#subtitle_r_"+this).remove();
        jQuery("#start_input_"+this).remove();
        jQuery("#end_input_"+this).remove();
        jQuery("#subtitle_input_"+this).remove();
    };

    function deleteAllSubs() {
        jQuery("#subtitles_table tbody").html("");
    }

    jQuery("#deleteAllSubs").click(deleteAllSubs);

    jQuery(document).ready(function(){
        jQuery('#subtitle_show option[value="'+subtitles_show+'"]').attr('selected','selected');
        if(subtitles != null && Object.keys(subtitles).length > 0){
            for(var key in subtitles){
                var start_time=subtitles[key].start_time;
                var end_time=subtitles[key].end_time;
                var subtitle=subtitles[key].subtitle;
                addSubsRow(start_time, end_time, subtitle);
            }
            sortTable();
        }

        var mediaUploader;
        jQuery('#add-srt').on('click', function ( e ) {
            e.preventDefault();
            if (mediaUploader) {
                mediaUploader.open();
                return;
            }

            mediaUploader = wp.media.frames.file_frame = wp.media({
                title: 'Select Subtitles',
                button: {
                    text: 'Choose .srt File'
                },
                multiple: false
            });

            mediaUploader.on('select', function () {
                attachment = mediaUploader.state().get('selection').first().toJSON();
                jQuery('#srtHiddenField').val(attachment.url);
                jQuery(".srt-parser").hide();
                jQuery(".loading-class").show();
                parseFile();

            });
            mediaUploader.open();
        });

        function parseFile() {
            var srt = jQuery("#srtHiddenField").val();
            if(srt === '') {
                alert('Please select a file first!');
                return;
            }

            var srtUrl = jQuery("#srtHiddenField").val();
            jQuery.get(srtUrl,  function(response) {
                parseSubs(response);
                jQuery(".srt-parser").show();
                jQuery(".loading-class").hide();
            });
        }
    });

    function addSubsRow(start_time, end_time, subtitle ) {
        var delBtn = jQuery("<a class='deleteSubBtn' data-divInfo='"+subsCounter+"'>Delete</a>");
        jQuery("#subtitles_table tbody").append("<tr id='subtitle_r_"+subsCounter+"'></tr>");
        jQuery("#subtitle_r_"+subsCounter).append("<td><input type='text' id='start_input_"+subsCounter+"' name='subtitle_data["+subsCounter+"][start_time]' value='"+start_time+"'></td>");
        jQuery("#subtitle_r_"+subsCounter).append("<td><input type='text' id='end_input_"+subsCounter+"' name='subtitle_data["+subsCounter+"][end_time]' value='"+end_time+"'></td>");
        jQuery("#subtitle_r_"+subsCounter).append("<td><textarea type='text' id='subtitle_input_"+subsCounter+"' name='subtitle_data["+subsCounter+"][subtitle]'>"+subtitle+"</textarea></td>");
        jQuery("#subtitle_r_"+subsCounter).append("<td id='delbtn"+subsCounter+"'></td>");
        jQuery("#delbtn"+subsCounter).append(delBtn);
        delBtn.click(deleteSub.bind(subsCounter));
        subsCounter++;
    }



    function strip(s) {
        return s.replace(/^\s+|\s+$/g,"");
    }

    function parseSubs(data) {
        var parsedSubs = [];
        srt = data.replace(/\r\n|\r|\n/g, '\n');
        srt = strip(srt);
        var srt_ = srt.split(/\n{2,}/);
        var cont = 0;
        for (s in srt_) {
            st = srt_[s].split('\n');
            if (st.length >= 2) {
                n = st[0];
                i = strip(st[1].split(' --> ')[0]);
                o = strip(st[1].split(' --> ')[1]);
                t = st[2];

                if (st.length > 2) {
                    for (j = 3; j < st.length; j++)
                        t += '\n' + st[j];
                }
                //define variable type as Object
                parsedSubs[cont] = {};
                parsedSubs[cont].number = n;
                parsedSubs[cont].start = i;
                parsedSubs[cont].end = o;
                parsedSubs[cont].text = t;
            }
            cont++;
        }
        populateParsedSubs(parsedSubs);
    }

    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = jQuery("#subtitles_table tbody");
        switching = true;
        while (switching) {
            switching = false;
            rows = table.children("tr");
            for (i = 0; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("input")[0].value;
                y = rows[i + 1].getElementsByTagName("input")[0].value;
                if (x.toLowerCase() > y.toLowerCase()) {
                    shouldSwitch= true;
                    break;
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }

    function populateParsedSubs(subs) {
        jQuery("#subtitles_table tbody").html("");
        subsCounter = 0;
        subs.forEach(function (item, index) {
            addSubsRow(item.start, item.end, item.text);
        });
        sortTable();
    }
    })()
</script>