<h3>Known People: </h3>
<b>Subject:</b> <input type="text" name="subject_known" value="[[SUBJECT_KNOWN]]" /><br />
<br>
<p><b>Body:</b></p>
<textarea name="body_known">[[BODY_KNOWN]]</textarea>

<div><strong>Note: </strong></div>
<ul>
    <li><small> Use [[LINK]] for the download link. </small></li>
    <li><small> Use [[FNAME]] for the First Name. </small></li>
    <li><small> Use [[LNAME]] for the Last Name. </small></li>
    <li><small> Use [[EMAIL]] for the Email. </small></li>
    <li><small> Use [[GNDR]] for the Gender. </small></li>
    <li><small> Use [[ADDR]] for the Address. </small></li>
    <li><small> Use [[PHONE]] for the Phone. </small></li>
    <li><small> Use [[VOICERNG]] for the Voice Range. </small></li>
    <li><small> Use [[VOICERNGPL]] for the Voice Range (Plural). </small></li>
</ul>

<hr>
<h3>Everyone: </h3>
<b>Subject:</b> <input type="text" name="subject_general" value="[[BODY_GENERAL]]" /><br />
<br>
<p><b>Body:</b> <small>[[Link]] keyword will be replaced with actual download link.</small><br /></p>
<br>
<textarea name="body_general">[[BODY_GENERAL]]</textarea><div><strong>Note: </strong></div>
<ul>
    <li><small> Use [[Link]] for the download link. </small></li>
</ul>
