<?php

$t_id = $term->term_id;

// retrieve the existing value(s) for this meta field. This returns an array
$term_meta = get_option( "taxonomy_$t_id" );

$users = get_users();


?>

<tr class="form-field">
	<th scope="row" valign="top"><label for="users"><?php _e( 'Users'); ?></label></th>
	<td>
        <select name="users[]" multiple class="chosen-select">
	        <?php
	        foreach ( $users as $user ) {
		        ?>
                <option
                        <?php if(in_array($user->ID, json_decode($term_meta['users'], true))) echo "selected"; ?>
                        value="<?php echo $user->ID; ?>"><?php echo $user->user_email ?></option>
		        <?php
	        }
	        ?>
        </select>
        <p class="description"><?php _e( 'Select users belonging to this group.'); ?></p>
	</td>
</tr>

<script>
    jQuery(document).ready(function() {
        jQuery(".chosen-select").chosen();
    });
</script>