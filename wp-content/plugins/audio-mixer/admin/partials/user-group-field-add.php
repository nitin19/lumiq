<?php $users = get_users(); ?>
<div class="form-field">
	<label for="users"><?php _e( 'Users'); ?></label>
    <select name="users[]" multiple class="chosen-select">
        <?php
        foreach ( $users as $user ) {
            ?>
            <option value="<?php echo $user->ID; ?>"><?php echo $user->user_email ?></option>
            <?php
        }
        ?>
    </select>
	<p class="description"><?php _e( 'Select users belonging to this group.'); ?></p>
</div>
<script>
    jQuery(document).ready(function() {
        jQuery(".chosen-select").chosen();
    });
</script>