<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<h3>Presets (<a class='pointer' onClick="AddPreset()">Add Preset</a>)</h3>
<table id="allPresetsHolder" style="width: 50%">

</table>
<input type='hidden' id="presets" name='presets' value='' />
<input type='hidden' id="presetSettings" name='presetSettings' value='' />

<hr />
<h3>Tracks</h3>
<button onclick="playAll(event, true)" id="playBtn">Play</button>
<button onclick="playAll(event, false)" id="pauseBtn" style="display:none">Pause</button>
<button onclick="restart(event)">restart</button>
<select onchange="updatePresetSettings()" id="SelectPreset">
    <option value="">Default</option>
</select>
<br /><br />
<div id="fileIds">

</div>
<table id="allFilesHolder" style="width: 100%">

</table>
<br />
<center>
    <button id="uploadAudioBtn">Add a new track (MP3 format)</button>
</center>

<script>
    var files = [];
    var presets = [];
    var selectedPreset = "";
    var presetSettings = {};
    var defaultColors=[[defaultColors]];
    if(defaultColors==""){
        defaultColors=[];
    }


    (function ($) {
        var frame, uploadAudioBtn = $('#uploadAudioBtn');
        var addSubtitleBtn = jQuery("#add_subtitle");

        if (uploadAudioBtn) {
            uploadAudioBtn.on('click', function (event) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if (frame) {
                    frame.close();
                    // return;
                }



                // Create a new media frame
                frame = wp.media({
                    title: 'Select or Upload a track (MP3)',
                    button: {
                        text: 'Use this track'
                    },
                    library: {type: "audio"},
                    multiple: true  // Set to true to allow multiple files to be selected
                });


                // When an image is selected in the media frame...
                frame.on('select', function () {
                    // Get media attachment details from the frame state

                    var attachments = frame.state().get('selection').toJSON();
                    attachments.forEach(function (attachment) {
                        // Send the attachment URL to our custom image input field.
                        var fileData = attachment;

                        fileData.volume = 1;
                        fileData.muted = "false";

                        files.push(fileData);
                    })



                    updateUI();
                });

                // Finally, open the modal on click
                frame.open();
            });
        }

        $(function () {
            init('[[filesData]]', '[[presets]]', '[[presetSettings]]');
        });

    })(jQuery);

    function updatePresetSettings(preset) {
        if (preset) {
            selectedPreset = preset;
        } else {
            selectedPreset = jQuery("#SelectPreset").val();
        }
        if (typeof presetSettings[selectedPreset] == 'object' && presetSettings[selectedPreset] != null) {

        } else {
            presetSettings[selectedPreset] = {};
            files.forEach(function (item) {
                presetSettings[selectedPreset][item.id] = {}
                presetSettings[selectedPreset][item.id].volume = item.volume;
                presetSettings[selectedPreset][item.id].muted = item.muted;
            })
        }
        updateUI();
    }

    function updateItemColor(event, id) {
        files.forEach(function (item) {
            if (item.id == id) {
                item.color = event.target.value;
                var colorSelectId="color"+id;
                document.getElementById(colorSelectId).style.backgroundColor=event.target.value;
            }
        })
    }

    function updatePresetColor(event, item,id) {
        presetSettings[item]["color"] = event.target.value;
        console.log(id);
        document.getElementById('presetColorChange_'+id).style.backgroundColor=event.target.value;
        updatePresetsFields();
    }

    function updateUI() {
        var presetsHolder = jQuery("#allPresetsHolder");
        var selectPreset = jQuery("#SelectPreset").html("");
        presetsHolder.html("");
        selectPreset.html("");
        
        
        selectPreset.append('<option value="">Default</option>');
        var colorCounter=0;
        if (Array.isArray(presets) && presets.length > 0) {
            var template = "\
            <tr>\
                <th>Name</th>\
                <th>Color</th>\
                <th></th>\
            </tr>\
        ";
            presetsHolder.append(jQuery(template));
            presets.forEach(function (item, index) {
                var color = (presetSettings[item]) ? presetSettings[item]["color"] : "#000000";
                
                var colorSelct = "<select style='background-color: "+presetSettings[item]["color"]+"' id='presetColorChange_"+colorCounter+"' onChange=\"updatePresetColor(event, '" + item + "',"+colorCounter+")\">";
                defaultColors.forEach(function(loopItem){
                    colorSelct += "<option value='"+loopItem+"' style='background-color: "+loopItem+"'";
                    if(presetSettings[item]["color"]==loopItem){
                        colorSelct +=" selected";
                    }
                    colorSelct += " >"+loopItem+"</option>"
                });
                   
                colorSelct += "</select>";

               
                
                var template = "\
                <tr>\
                    <td><input type='text' onChange=\"updatePreset(event, " + index + ")\" value='" + item + "'></td>\
                    <td>"+colorSelct+"</td>\
                    <td><a class='pointer' onClick=\"removePreset('" + index + "')\">(remove)</a></td>\
                </tr>\
            ";
                selectPreset.append('<option value="' + item + '">' + item + '</option>');
                presetsHolder.append(jQuery(template));

                colorCounter++;
            })

            
        }

        selectPreset.val(selectedPreset);


        jQuery("#allFilesHolder").html("");
        jQuery("#fileIds").html("");

        if (Array.isArray(files) && files.length > 0) {
            jQuery('#uploadAudioBtn').html("Add a new track (MP3 format)");
            var template = "\
            <tr>\
                <th>Id</th>\
                <th>Name</th>\
                <th>Color</th>\
                <th>File</th>\
                <th>Volume</th>\
                <th></th>\
                <th>Order</th>\
            </tr>\
        ";
            jQuery("#allFilesHolder").append(jQuery(template));
            files.forEach(function (item) {
                item.color = item.color || "#333333";
                var colorSelct = "<select style='background-color: "+item.color+"' id='color" + item.id + "' name='color" + item.id + "' onChange='updateItemColor(event, " + item.id + ")'>";
                defaultColors.forEach(function(loopItem){
                    colorSelct += "<option value='"+loopItem+"' style='background-color: "+loopItem+"'";
                    if(item.color==loopItem){
                        colorSelct +=" selected"
                    }
                    colorSelct += " >"+loopItem+"</option>";
                });
                   
                colorSelct += "</select>";
                var template = "\
                <tr>\
                    <td>" + item.id + "</td>\
                    <td>" + item.title + "</td>\
                    <td>"+colorSelct+"</td>\
                    <td>\
                        <div class='audioHolder'> \
                                <center> \
                                    <audio controls src=" + item.url + "> \
                                    </audio> \
                                </center> \
                        </div>\
                    </td>\
                    <td><input type='number' min='1' max='100' class='volumectrl' style='width: 50px' value='" + (item.volume * 100) + "'>%</td>\
                    <td><a class='pointer' onClick=\"remove('" + item.id + "')\">(remove)</a></td>\\n\
                    <td id='order" + item.id + "'></td\
                </tr>\
            ";
                var jqObj = jQuery(template);
                var audio = jqObj.find('audio');
                var volumeCtrl = jqObj.find('.volumectrl');

                audio[0].onvolumechange = function (event) {
                    if (selectedPreset == "") {
                        this.item.volume = event.target.volume;
                        this.item.muted = event.target.muted;
                        jQuery("#filesVolume" + this.item.id).val(this.item.volume);
                        jQuery("#filesMute" + this.item.id).val(this.item.muted);
                    } else {
                        presetSettings[selectedPreset][this.item.id].muted = event.target.muted;
                        presetSettings[selectedPreset][this.item.id].volume = event.target.volume;
                    }
                    this.volumeCtrl.value = event.target.volume * 100;
                    updatePresetsFields();
                }.bind({item: item, volumeCtrl: volumeCtrl[0]});

                volumeCtrl[0].onchange = function (event) {
                    var max = parseInt(jQuery(this.volumeCtrl).attr('max'));
                    var min = parseInt(jQuery(this.volumeCtrl).attr('min'));

                    if (jQuery(this.volumeCtrl).val() > max)
                    {
                        jQuery(this.volumeCtrl).val(max);
                    } else if (jQuery(this.volumeCtrl).val() < min)
                    {
                        jQuery(this.volumeCtrl).val(min);
                    }
                    this.audio.volume = jQuery(this.volumeCtrl).val() / 100;
                }.bind({volumeCtrl: volumeCtrl[0], audio: audio[0]});

                var muted, volume;

                if (selectedPreset == "") {
                    volume = item.volume;
                    muted = item.muted;
                } else {
                    muted = presetSettings[selectedPreset][item.id].muted;
                    volume = presetSettings[selectedPreset][item.id].volume;
                }

                audio[0].volume = volume;
                volumeCtrl[0].value = volume * 100;
                audio[0].muted = (muted == "false" || muted == false) ? false : true;

                audio[0].load();


                jQuery("#allFilesHolder").append(jqObj);

                var fileIdTemplate = "\
                <input type='hidden' name='filesId[]' value='" + item.id + "' />\
                <input type='hidden' id='filesVolume" + item.id + "' name='filesVolume" + item.id + "' value='" + item.volume + "' />\
                <input type='hidden' id='filesMute" + item.id + "' name='filesMute" + item.id + "' value='" + item.muted + "' />\
            ";
                jQuery("#fileIds").append(jQuery(fileIdTemplate));

            });
        } else {
            jQuery('#uploadAudioBtn').html("Add a new track (MP3 format)");
            jQuery("#allFilesHolder").html("<tr><td colspacing=4><center>No files selected yet.</center></td></tr>");
        }
        updatePresetsFields();
        // audioPlayer.setAttribute("src",audioURL);
        jQuery("#allFilesHolder tbody").sortable({
            items: 'tr:not(:first)',
            create: function (event, ui) {
                jQuery(this).children().each(function (index) {
                    jQuery(this).find('td').last().html(index)
                });
                if (Array.isArray(files) && files.length > 0) {
                    files.forEach(function (item) {
                        var temp = "<input type='hidden' id='filesOrder" + item.id + "' name='filesOrder" + item.id + "' value='" + jQuery("#order" + item.id).text() + "' />";
                        jQuery("#fileIds").append(jQuery(temp));
                    });
                }
                ;

                jQuery("#allFilesHolder").css('cursor', 'pointer');

            },
            update: function (event, ui) {
                jQuery(this).children().each(function (index) {
                    jQuery(this).find('td').last().html(index)
                });
                if (Array.isArray(files) && files.length > 0) {
                    files.forEach(function (item) {
                        var temp = "<input type='hidden' id='filesOrder" + item.id + "' name='filesOrder" + item.id + "' value='" + jQuery("#order" + item.id).text() + "' />";
                        jQuery("#fileIds").append(jQuery(temp));
                    });
                }
            }
        });
    }

    function updatePresetsFields() {
        jQuery("#presetSettings").val(JSON.stringify(presetSettings));
        jQuery("#presets").val(JSON.stringify(presets));
    }

    function removePreset(id) {
        if (Array.isArray(presets) && presets.length > 0) {
            presets = presets.filter(function (item, index) {
                if (index != id) {
                    return item;
                } else {
                    if (item == selectedPreset) {
                        selectedPreset = "";
                    }
                }
            })
        }
        updateUI();
    }

    function updatePreset(event, id) {
        if (Array.isArray(presets) && presets.length > 0) {
            presets = presets.map(function (item, index) {
                if (index == id) {
                    presetSettings[event.target.value] = presetSettings[item];
                    delete presetSettings[item];
                    if (selectedPreset === item) {
                        selectedPreset = event.target.value;
                    }
                    item = event.target.value;

                }
                return item;
            })
        }
        updateUI();
    }

    function AddPreset() {
        name = "new Preset" + presets.length;
        presets.push(name);
        updatePresetSettings(name);
        updateUI();
    }

    function init(data, sets, setSettings) {
        try {
            files = JSON.parse(data);
        } catch (e) {
            files = [];
        }

        try {
            presets = JSON.parse(sets);
        } catch (e) {
            presets = [];
        }

        try {
            presetSettings = JSON.parse(setSettings);
        } catch (e) {
            presetSettings = {};
        }
        
        updateUI();


        /*jQuery('#downloads').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": ajaxurl,
                "type": 'POST',
                "data": function (d) {
                    d.action = "mixer_downloads_data";
                    d.parent_id = [[postId]];
                    return d;
                }
            },
            "columnDefs": [
                {"orderable": false, targets: 0},
                {"orderable": false, targets: 1},
                {
                    "render": function (data, type, row) {
                        var d = new Date(data * 1000);
                        return d.toLocaleString();
                    },
                    "orderable": false,
                    targets: 2
                }
            ]
        });*/
    }

    function remove(id) {
        if (Array.isArray(files) && files.length > 0) {
            var count = 0;
            files = files.filter(function (item) {
                if (count == 1 || item.id != id) {
                    return item;
                } else {
                    count = 1;
                }
            })
        }

        updateUI();
    }

    function playAll(e, play) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var audios = jQuery("audio");
        audios = audios.toArray()
        audios.forEach(function (audio) {
            if (play) {
                audio.play();
                jQuery("#playBtn").hide();
                jQuery("#pauseBtn").show();
            } else {
                audio.pause();
                jQuery("#playBtn").show();
                jQuery("#pauseBtn").hide();
            }
        })
    }

    function restart(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var audios = jQuery("audio");
        audios = audios.toArray()
        audios.forEach(function (audio) {
            audio.currentTime = 0;
        });
    }
</script>