<h3>Settings</h3>
<table width="400">
	<tr>
		<th style="width:70%">Autoplay Track:</th>
		<th>
			<select name="autoplay">
				<option value="yes" [[autoplay_yes]]>Yes</option>
				<option value="no" [[autoplay_no]]>No</option>
			</select>
		</th>
	</tr>
	<tr>
		<th style="width:70%">Allow everyone to download:</th>
		<th>
			<select name="download_known">
				<option value="no"  [[known_no]]>No</option>
				<option value="yes" [[known_yes]] >Yes</option>
			</select>
		</th>
	</tr>
</table>
<hr />