<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the loops meta box in the admin panel
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="loops_div"></div>
<table width="100%"  style="text-align: left;">
    <thead>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><input type="text" id="start_time_loop"><br><small>Format 00:00</small></td>
        <td><input type="text" id="end_time_loop"><br><small>Format 00:00</small></td>
        <td><a id="add_loop" class="add_btn">Add</a></td>
    </tr>
    </tbody>
</table>
<br>
<hr>
<h3>Listing</h3>
<table id="loop_table" width="100%" class="loop_table">
    <thead>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
        <th>Action</th>
    </tr>
    </tfoot>
</table>

<script>
    var loop_increment = 0;

    function validateTimeFormat(time){
        var arr = time.split(':');
        if(!isNaN(parseInt(arr[0])) && !isNaN(parseInt(arr[1]))  && parseInt(arr[0]) >= 0  && parseInt(arr[1]) >= 0 && parseInt(arr[1]) <= 60 )
        {
            return true;
        }
        return false;
    }

    var loops = 'LOOP_DATA_JSON';
    jQuery(document).ready(function(){

        // adding loop data
        var addLoopBtn = jQuery("#add_loop");
        if(addLoopBtn){
            addLoopBtn.on('click',function(event) {
                event.preventDefault();
                var start_time = jQuery("#start_time_loop").val();
                var end_time = jQuery("#end_time_loop").val();
                if(validateTimeFormat(start_time) && validateTimeFormat(end_time) ){
                    key = loop_increment++;
                    addRow(start_time, end_time, key);
                } else {
                    alert("Time Format Not Valid, Please Check");
                }
            });
        }

        // showing loop data
        loops = JSON.parse(loops);

        if(loops != null && loops.length > 0 ){
            for(var key in loops){
                var start_time= loops[key].start_time;
                var end_time = loops[key].end_time;
                addRow(start_time, end_time, key);                
                loop_increment++;
            }
        }

        function addRow(start_time, end_time, counter){
            var row =   "<tr id='loop_" + counter + "'>";
            var row = row +  "<td>" + start_time + "</td>";
            var row = row +  "<td>"+end_time+"</td>";
            var row = row +  "<td> <a class='deleteLoopBtn' data-divInfo='" + counter + "' onclick='deleteLoop(" + counter + ")'>" + "Delete </a> </td>";
            var row = row +  "</tr>";

            jQuery("#loops_div").append("<input type='hidden' id='start_input_"+counter+"' name='loop_data["+counter+"][start_time]' value='" + start_time + "'>");
            jQuery("#loops_div").append("<input type='hidden' id='end_input_"+counter+"' name='loop_data["+counter+"][end_time]' value='"+ end_time + "'>");
            jQuery("#loop_table tbody").append(row);
        }
    });
    function deleteLoop(index){
        jQuery('#start_input_'+index).remove();
        jQuery('#end_input_'+index).remove();
        jQuery('#loop_'+index).remove();
    }
</script>