<div>
<form id="mixer_form" method="post">    
    <h1>Mixer Options</h1>
    <p>Here you can configure all mixer settings</p>
    <h3>Download Box</h3>
    <table>
        <tr>
            <td>Heading:</td>
            <td><input type="text" style="width:500px" name="download_box_heading" value="[[download_box_heading]]"/> </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Text:</td>
            <td><textarea style="width:500px" name="download_box_details" >[[download_box_details]]</textarea> </td>
        </tr>
        <tr>
            <td>Button Text:</td>
            <td><input style="width:500px" type="text" name="download_box_button" value="[[download_box_button]]"/> <br /></td>
        </tr>
    </table>

    <hr />
    <h3>Thank you Box</h3>
    <table>
        <tr>
            <td>Heading:</td>
            <td><input type="text" style="width:500px" name="thank_box_heading" value="[[thank_box_heading]]"/> </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Text:</td>
            <td><textarea style="width:500px" name="thank_box_details" >[[thank_box_details]]</textarea> </td>
        </tr>
        <tr>
            <td>Button Text:</td>
            <td><input style="width:500px" type="text" name="thank_box_button" value="[[thank_box_button]]"/> <br /></td>
        </tr>
    </table>

    <h3>Error & Waiting Messages</h3>
    <table>
        <tr>
            <td>Please wait:</td>
            <td><input type="text" style="width:500px" name="please_wait_text" value="[[please_wait_text]]"/> </td>
        </tr>
        <tr>
            <td>Download not allowed:</td>
            <td><input style="width:500px" type="text" name="download_not_allowed" value="[[download_not_allowed]]"/> <br /></td>
        </tr>
    </table>
    
    
    <hr />
    <h3>Colors</h3>
    <table class="gridtable" id="colors_table">
        <tr>
            <td>Select Color:</td>
            <td><input type="color" id="color_select" name="Color"/> <a id="add_color">Add</a></td>
        </tr>
    </table>

    <div id="mixer_colors">
        
    </div>

    <hr />
    <h3>Markers</h3>
    <table class="gridtable" id="marker_table">
        <tr>
            <th>Marker</th>
            <th>Short</th>
        </tr>
        <tr>
            <td><input type="text" id="marker_long"/></td>
            <td><input type="text" id="marker_short"/></td>
            <td><a id="add_marker_short">Add</a></td>
        </tr>
    </table>

    <table>
        <thead>
        <tr>
            <td>Preset</td>
            <td>Short</td>
        </tr>
        </thead>
        <tbody id="marker_shorts">

        </tbody>
    </table>


    <button name="save">Save</button>
 </form>   

</div>

<script>
    jQuery(document).ready(function(){
       var audio_colors=[[audio_mixer_colors]];
       if(audio_colors==""){
           audio_colors=[];
       }
       console.log(audio_colors);
       
       audio_colors.forEach(function(item){
           var date=new Date().getTime();
           date=date+Math.floor(Math.random());
            var color=item;
            jQuery("#colors_table tbody").append("<tr id='row_"+date+"'></tr>");
            jQuery("#row_"+date).append("<td style='background-color: "+color+"'>"+color+"</td>");
            jQuery("#row_"+date).append("<td><a class='delete-btn' data-id='"+date+"'>Delete</a></td>");

            jQuery('#mixer_form').prepend('<input id="input_'+date+'" type="hidden" name="audio_mixer_colors[]" value="'+color+'" />');

            jQuery(".delete-btn").click(function(){
                var id=jQuery(this).data('id');
                jQuery("#input_"+id).remove();
                jQuery("#row_"+id).remove();
            });
       });
       
        jQuery("#add_color").on('click',function(){
            var date=new Date().getTime();
            date=date+Math.floor(Math.random());
            var color=jQuery("#color_select").val();
            jQuery("#colors_table tbody").append("<tr id='row_"+date+"'></tr>");
            jQuery("#row_"+date).append("<td style='background-color: "+color+"'>"+color+"</td>");
            jQuery("#row_"+date).append("<td><a class='delete-btn' data-id='"+date+"'>Delete</a></td>");

            jQuery('#mixer_form').prepend('<input id="input_'+date+'" type="hidden" name="audio_mixer_colors[]" value="'+color+'" />');

            jQuery(".delete-btn").click(function(){
                var id=jQuery(this).data('id');
                jQuery("#input_"+id).remove();
                jQuery("#row_"+id).remove();
            });
        });


        var marker = [[markers]];
        var marker_short = [[markers_short]];
        if (marker == "") marker = [];
        if (marker_short == "") marker_short = [];

        jQuery("#add_marker_short").on('click', function() {
            var plng = jQuery("#marker_long").val();
            var psht = jQuery("#marker_short").val();

            var row ='<tr> <td> <input type="text" name="marker[]" value="'+ plng +'"> </td> <td> <input type="text" name="marker-short[]" value="'+ psht +'"> </td> <td><a class="del-maker">Delete</a></td>  </tr>';

            jQuery("#marker_shorts").append(row);

            jQuery(".del-maker").on('click', function() {
                jQuery(this).closest("tr").remove();
            });
        });



        marker.forEach( function(mar, index) {
            var plng = marker[index];
            var psht = marker_short[index];
            var row ='<tr> <td> <input type="text" name="marker[]" value="'+ plng +'"> </td> <td> <input type="text" name="marker-short[]" value="'+ psht +'"> </td> <td><a class="del-maker">Delete</a></td>  </tr>';
            jQuery("#marker_shorts").append(row);
        });


        jQuery(".del-maker").on('click', function() {
            jQuery(this).closest("tr").remove();
        });

       
    });
    
    
    
</script>