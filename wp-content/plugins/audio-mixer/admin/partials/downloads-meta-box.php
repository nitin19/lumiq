<h3>Downloads</h3>
<table id="downloads" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Email</th>
        <th>Date</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Id</th>
        <th>Email</th>
        <th>Date</th>
    </tr>
    </tfoot>
</table>

<script>
    jQuery(document).ready(function(){
        jQuery('#downloads').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": ajaxurl,
                "type": 'POST',
                "data": function (d) {
                    d.action = "mixer_downloads_data";
                    d.parent_id = [[postId]];
                    return d;
                }
            },
            "columnDefs": [
                {"orderable": false, targets: 0},
                {"orderable": false, targets: 1},
                {
                    "render": function (data, type, row) {
                        var d = new Date(data * 1000);
                        return d.toLocaleString();
                    },
                    "orderable": false,
                    targets: 2
                }
            ]
        });
    });
</script>