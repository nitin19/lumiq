<div class="info-form">
	<div class="crow">
		<div class="col">
			<label for=""> First Name </label>
			<input type="text" class="field" name="known_person[fname]" id="fname" value="[[FNAME]]"/>
		</div>
		<div class="col">
			<label for=""> Last Name </label>
			<input type="text" class="field" name="known_person[lname]" id="lname" value="[[LNAME]]"/>
		</div>
		<div class="col">
			<label for="">Email</label>
			<input type="email" class="field" name="known_person[email]" id="email" value="[[EMAIL]]"/>
		</div>
	</div>
	<div class="crow">
		<div class="col">
			<label for=""> Gender </label>
			<input type="text" class="field" name="known_person[gndr]" id="gndr" value="[[GNDR]]"/>
		</div>
		<div class="col">
			<label for=""> Phone </label>
			<input type="text" class="field" name="known_person[phone]" id="phone" value="[[PHONE]]"/>
		</div>
		<div class="col">
			<label for="">Voice Range</label>
			<input type="text" class="field" name="known_person[voiceRng]" id="voiceRng" value="[[VOICERNG]]"/>
		</div>
		<div class="col">
			<label for="">Voice Range (Plural)</label>
			<input type="text" class="field" name="known_person[voiceRngPl]" id="voiceRngPl" value="[[VOICERNGPL]]"/>
		</div>
	</div>
	<div class="crow">
		<div class="col">
			<label for=""> Address </label>
			<textarea name="known_person[addr]"  cols="10" rows="3" id="addr">[[ADDR]]</textarea>
		</div>
	</div>
</div>
<script>
</script>