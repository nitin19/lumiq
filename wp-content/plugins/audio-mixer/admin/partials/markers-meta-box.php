<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the markers meta box in the admin panel
 *
 * @link       http://aliarshad.info
 * @since      1.0.0
 *
 * @package    Audio_Mixer
 * @subpackage Audio_Mixer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="markers_div"></div>
<table width="100%" class="marker_data_table">
    <thead>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
        <th>Text</th>
        <th>Initials</th>
        <th>Color</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><input type="text"  id="start_time_marker" placeholder="00:00"><br><small>Format 00:00</small></td>
        <td><input type="text"  id="end_time_marker" placeholder="00:00"><br><small>Format 00:00</small></td>
        <td><input type="text"  id="content_marker" ><br><small>Text to show up</small></td>
        <td><input type="color" id="color_marker" ><br><small>Background</small></td>
        <td valign="top"><a id="add_marker"  class="add_btn">Save</a></td>
    </tr>
    </tbody>
</table>
<br>
<hr>
<h3>Marker Listing</h3>
<table id="marker_table" width="100%" class="marker_table">
    <thead>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
        <th>Content</th>
        <th>Color</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    <tr>
        <th>Time - From</th>
        <th>Time - To</th>
        <th>Content</th>
        <th>Color</th>
        <th>Action</th>
    </tr>
    </tfoot>
</table>

<script>
    var marker_increment = 0;

    var markers = 'MARKER_DATA_JSON';
    var editMode = false;
    var editIndex = -1;

    function validateTimeFormat(time){
        var arr = time.split(':');
        if(isNaN(parseInt(arr[0])) || isNaN(parseInt(arr[1]))){
            return false;
        }
        if (parseInt(arr[0]) < 0  || parseInt(arr[1]) < 0 || parseInt(arr[1]) > 59 )
        {
            return false;
        }
        return true;
    }

    jQuery(document).ready(function(){
        // adding marker data
        var addmarkerBtn = jQuery("#add_marker");
        if(addmarkerBtn){
            addmarkerBtn.on('click',function(event) {
                event.preventDefault();
                var start_time = jQuery("#start_time_marker").val();
                var end_time = jQuery("#end_time_marker").val();
                var content = jQuery("#content_marker").val();
                var color = jQuery("#color_marker").val();
                if(validateTimeFormat(start_time) && validateTimeFormat(end_time)){
                    key = marker_increment++;
                    addRow(start_time, end_time, content, initials, color, key);
                }else {
                    alert('Invalid Time Given');
                }
            });
        }
        if(markers == null || markers === ''){
            return;
        } else {
            markers = JSON.parse(markers);
            if( markers != null || markers !== "" ){
                for(var key in markers){
                    var start_time= markers[key].start_time;
                    var end_time = markers[key].end_time;
                    var content = markers[key].content;
                    if (typeof markers[key].initials != 'undefined') {
                        var initials = markers[key].initials;
                    } else {
                        var initials = '';
                    }
                    var color = markers[key].color;
                    addRow(start_time, end_time, content, initials,color, key);
                    marker_increment++;
                }
            }
        }



        function addRow(start_time, end_time, content, initials, color, counter){
            if(editMode) {
                deleteMarker(editIndex);
                counter = editIndex;
                editMode = false;
            }
            var row =   "<tr id='marker_" + counter + "' data-index="+counter+" data-start="+ start_time +">";
            var row = row +  "<td width='15%'>" + start_time + "</td>";
            var row = row +  "<td width='15%'>"+ end_time + "</td>";
            var row = row +  "<td width='20%'>"+ content + "</td>";
            var row = row +  "<td width='20%'><div class='color_wrapper' style='background: " + color + ";'><span>"+color+"</span></div></td>";
            var row = row +  "<td width='20%'> <a class='deletemarkerBtn' data-divInfo='" + counter + "' onclick='deleteMarker(" + counter + ")'>" + "Delete </a> / <a class='editMarkerBtn' data-divInfo='" + counter + "' onclick='editMarker(" + counter + ")'>" + "Edit </a> </td>";
            var row = row +  "</tr>";
            jQuery("#markers_div").append("<input type='hidden' id='start_"+counter+"' name='marker_data["+counter+"][start_time]' value='" + start_time + "'>");
            jQuery("#markers_div").append("<input type='hidden' id='end_"+counter+"' name='marker_data["+counter+"][end_time]' value='"+ end_time + "'>");
            jQuery("#markers_div").append("<input type='hidden' id='content_"+counter+"' name='marker_data["+counter+"][content]' value='"+ content + "'>");
            jQuery("#markers_div").append("<input type='hidden' id='color_"+counter+"' name='marker_data["+counter+"][color]' value='"+ color + "'>");
            jQuery("#marker_table tbody").append(row);
            sortTable();
        }

    });

    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = jQuery("#marker_table tbody");
        switching = true;
        while (switching) {
            switching = false;
            rows = table.children("tr");
            for (i = 0; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[0];
                y = rows[i + 1].getElementsByTagName("TD")[0];
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch= true;
                    break;
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }

    function deleteMarker(index){
        jQuery('#marker_'+index).remove();
        jQuery('#start_'+index).remove();
        jQuery('#end_'+index).remove();
        jQuery('#content_'+index).remove();
        jQuery('#color_'+index).remove();
    }

    function editMarker(index) {
        editMode = true;
        editIndex = index;
        jQuery("#start_time_marker").val(jQuery("#start_"+index).val());
        jQuery("#end_time_marker").val(jQuery("#end_"+index).val());
        jQuery("#content_marker").val(jQuery("#content_"+index).val());
        jQuery("#color_marker").val(jQuery("#color_"+index).val());
    }
</script>