<?php
/**
 * Plugin Name: Project Api
 * Plugin URI: http://www.mywebsite.com/my-first-plugin
 * Description: Please Don't deactivate this plugin. Because this plugin use for mobile app API.
 * Version: 1.0
 * Author: Nitin Kumar
 * Author URI: http://www.mywebsite.com
 */
/***********All Function *************/
  function user_register($request) {
  	global $wpdb;
    $first_name 	    = $request['first_name'];
  	$last_name 	      = $request['last_name'];
  	$username 		    = $request['user_email'];
    $user_email       = $request['user_email'];
  	$user_password    = md5($request['user_password']);
  	$user_role 	      = $request['user_role'];
    $selected_choir   = $request['selected_choir'];
    $select_choir_cat = $request['select_choir_cat'];
    
    if(email_exists($user_email)){
      return new WP_REST_Response(array('status' => 0, 'msg'=>'Sorry You have already registered this email!') );
    }
    $success = $wpdb->insert('wp_users', array(
   		"user_nicename" => $first_name,
   		"display_name" => $first_name.' '.$last_name,
   		"user_login" => $username,
   		"user_email" => $user_email,
   		"user_pass" => $user_password,
   		"user_registered" => date('Y-m-d h:m:s'),
   		"user_status" => '1',
	  ));
  	if($success){
  		$user_id = $wpdb->insert_id;
    	$user_roles = array( $user_role => 1 );
      if($user_role == 'singer'){
        update_user_meta( $user_id, 'selected_choir', $selected_choir);
        update_user_meta( $user_id, 'selected_choir_cat', $select_choir_cat);
      }
   		update_user_meta( $user_id, 'wp_capabilities', $user_roles);
  		update_user_meta( $user_id, 'first_name', $first_name);
  		update_user_meta( $user_id, 'last_name', $last_name);
      $user_info = get_userdata($user_id);
      $user_image_url = get_user_meta($user_id, 'image_url', true);
      $user_contact_number = get_user_meta($user_id, 'contact_number', true);
      $user_selected_choir = get_user_meta($user_id, 'selected_choir', true);
      $select_choir_cat = get_user_meta($user_id, 'select_choir_cat', true);
  		$response = new WP_REST_Response(array('status' => 1, 'msg' => 'User Created Successfully!', 'data'  => $user_info, 'user_image_url'=>$user_image_url, 'user_contact_number'=>$user_contact_number, 'selected_choir'=>$user_selected_choir, 'select_choir_cat'=>$select_choir_cat));
    	$response->set_status(200);
    	return $response;
  	} else {
      //return new WP_Error( 'Not_Created', 'Sorry user not created!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'Sorry user not created!') );
    }
  }
  function user_login($request){
    $creds = array();
    $creds['user_login'] = $request["email"];
    $creds['user_password'] =  $request["password"];
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );

    if ( is_wp_error($user) ){
      //return new WP_Error( 'Not_login', 'Invalid Credentials!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'Invalid Credentials!') );
    }
    $response = new WP_REST_Response(array('status' => 1, 'msg' => 'User Login Successfully!', 'data'  => $user));
    return $response;
  }
  function get_single_user_data($request) {
   	$user_id = $request['user_id'];
   	$user_info = get_userdata($user_id);
    $user_image_url = get_user_meta($user_id, 'image_url', true);
    if($user_image_url!=''){
        $user_image_url = get_user_meta($user_id, 'image_url', true);
    } else {
        $user_image_url = site_url().'/wp-content/uploads/2019/07/male.jpg';
    }
    $user_contact_number = get_user_meta($user_id, 'contact_number', true);
    $user_selected_choir = get_user_meta($user_id, 'selected_choir', true);
    $select_choir_cat = get_user_meta($user_id, 'select_choir_cat', true);
    $user_address = get_user_meta($user_id, 'address', true);
    $user_city = get_user_meta($user_id, 'city', true);
    $user_state = get_user_meta($user_id, 'state', true);
    $user_country = get_user_meta($user_id, 'country', true);
    $image_base_url = site_url().'/wp-content/uploads/profileimage';
    if($user_info){
   	$response = new WP_REST_Response(array('status' => 1, 'msg' => 'User listing', 'data'=>$user_info, 'user_image_url'=>$user_image_url, 'user_contact_number'=>$user_contact_number, 'selected_choir'=>$user_selected_choir, 'select_choir_cat'=>$select_choir_cat, 'user_address'=>$user_address, 'user_city'=>$user_city, 'user_state'=>$user_state, 'user_country'=>$user_country, 'image_base_url'=>$image_base_url));
   	return $response;
    }
    return new WP_REST_Response(array('status' => 0, 'msg' => 'No Records found!') );
  }
  function forgot_password_reset_link_mail($request){
    global $wpdb;
    $user_email = $request['email'];
    $user_data = $wpdb->get_row("SELECT * from wp_users Where user_email='$user_email'");
    $user = get_user_by('id', $user_data->ID);
    $firstname = $user->first_name;
    $email = $user->user_email;
    $adt_rp_key = get_password_reset_key( $user );
    $user_login = $user->user_login;
    $admin_email = get_option( 'admin_email' );
    $rp_link = '<a href="' .site_url()."/reset-password/?key=$adt_rp_key&email_id=" . rawurlencode($email) . '">' . site_url()."/reset-password/?key=$adt_rp_key&email_id=" . rawurlencode($email) . '</a>';

    $message = "Hi ".$firstname.",<br>";
    $message .= "An account has been created on ".get_bloginfo( 'name' )." for email address ".$email."<br>";
    $message .= "Click here to set the password for your account: <br>";
    $message .= $rp_link.'<br>';

    $subject = __("Your account on ".get_bloginfo( 'name'));
    $headers = array();
    add_filter( 'wp_mail_content_type', function( $content_type ) {return 'text/html';});
    $headers[] = 'From:' .get_bloginfo( 'name' ). '<'.$admin_email.'>'."\r\n";
    $mail_response = wp_mail( $email, $subject, $message, $headers);
    if($mail_response){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Forgot password email sent on your email.Please check your email!'));
      return $response;
    } else {
      //return new WP_Error( 'Not_Forgot', 'Invalid Credentials!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'Invalid Credentials!') );
    }
  }
  function get_all_choir_listing($request) {
    global $wpdb;
    $get_choir_data = $wpdb->get_results("SELECT * FROM groups where status='1' and deleted='0' ORDER BY id DESC");
    if($get_choir_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'All Choir listing', 'data'=>$get_choir_data));
      return $response;
    } else {
      //return new WP_Error( 'No_Choir_Record_Found', 'No Choir Records Found!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Choir Records Found!') );
    }
  }
  function get_user_choir_listing($request) {
    global $wpdb;
    $user_id = $request['user_id'];
    $get_user_choir_data = $wpdb->get_results("SELECT * FROM groups where user_id='$user_id' and status='1' and deleted='0' ORDER BY id DESC");
    if($get_user_choir_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'User Choir listing', 'data'=>$get_user_choir_data));
      return $response;
    } else {
      //return new WP_Error( 'No_Choir_Record_Found', 'No Choir Records Found!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Choir Records Found!') );
    }
  }
  function create_choir($request) {
    global $wpdb;   
    /*if($_FILES["choir_profile"]["name"]!=''){*/
      /*if(isset($_FILES["choir_profile"]["type"])){*/
        global $wpdb;
        $grouptable = 'groups';
        $user_id = $request['user_id']; 
        $group_choir_name = $request['group_choir_name'];
        $group_choir_desc = $request['group_choir_desc'];
        $group_choir_city = $request['group_choir_city'];
        $group_choir_state = $request['group_choir_state'];
        $group_choir_country = $request['group_choir_country'];
        $group_choir_image = $_FILES["choir_profile"]["tmp_name"];

        /*$allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
        $temp = explode(".", $_FILES["choir_profile"]["name"]);
        $extension = end($temp);*/

        /*if ($_FILES["choir_profile"]["error"]) {
            $error .= 'Error opening the file';
            //return new WP_Error( 'No_Open_File', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        /*if ($_FILES["choir_profile"]["type"] != "image/jpeg" &&
            $_FILES["choir_profile"]["type"] != "image/JPEG" &&
            $_FILES["choir_profile"]["type"] != "image/jpg" &&
            $_FILES["choir_profile"]["type"] != "image/JPG" &&
            $_FILES["choir_profile"]["type"] != "image/PNG" &&
            $_FILES["choir_profile"]["type"] != "image/png") { 
            $error .= "Mime type not allowed";
            //return new WP_Error( 'Mime_type', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        /*if (!in_array($extension, $allowedExts)) {
            $error .= 'Extension not allowed';
            //return new WP_Error( 'Extension_not_allowed', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        /*if ($_FILES["choir_profile"]["size"] > 3145728) {
            $error .= 'File size should be less than 2 Mb';
            //return new WP_Error( 'File_size_less_than_2_Mb', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        if ($error == "") {
            $path_array = wp_upload_dir();
            $sourcePath = $_FILES['choir_profile']['tmp_name'];
            $Newfilename = uniqid().'-'.$_FILES['choir_profile']['name'];
            $targetPath = $path_array["basedir"]."/groupchoirimage/".$Newfilename;
            $movefile =  move_uploaded_file($sourcePath, $targetPath);

            if($movefile) {
                $success = $wpdb->insert($grouptable, array(
                  "user_id" => $user_id,
                  "group_name" => $group_choir_name,
                  "group_desc" => $group_choir_desc,
                  "group_city" => $group_choir_city,
                  "group_state" => $group_choir_state,
                  "group_country" => $group_choir_country,
                  "group_img" => $Newfilename,
                  "created_by" => $user_id,
                  "created_at" => date('yyyy-mm-dd h:m:s'),
                ));

                if($success) {
                    $group_id = $wpdb->insert_id;
                    $last_insert_group_data = $wpdb->get_row("SELECT * FROM groups WHERE id='$group_id' and status='1' and deleted='0'");
                    $successsds = $wpdb->insert('wp_terms', array(
                        "name" => $group_choir_name,
                        "slug" => $group_choir_name,
                        "term_group" => '0',
                        "group_id" =>$group_id
                    ));
                    if($successsds){
                        $term_id = $wpdb->insert_id;
                        $successsds = $wpdb->insert('wp_term_taxonomy', array(
                            "term_id" => $term_id,
                            "taxonomy" => 'audio_mixer_user_group_taxonomy',
                            "description" => '',
                            "parent" =>'0',
                            "count"=>'0'
                        ));
                    }
                    $error =  'Your Choir added successfully...!!';
                    $image_base_url = site_url().'/wp-content/uploads/groupchoirimage';
                    $response = new WP_REST_Response(array('status' => 1, 'msg' => $error, 'data'=>$last_insert_group_data, 'image_base_url'=>$image_base_url));
                    return $response;
                } else {
                    $error =  'Sorry fail to add your Choir due to system error. Please try again.';
                    //return new WP_Error( 'added_choir', $error, array('status' => 404) );
                    return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
                }
            } else {
                $error =  'Sorry fail to add your Choir due to system error. Please try again.';
                //return new WP_Error( 'added_choir', $error, array('status' => 404) );
                return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
            }
        } else {
            $error =  'Sorry fail to add your Choir due to system error. Please try again.';
            //return new WP_Error( 'added_choir', $error, array('status' => 404) );
            return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
        }  
/*      } 
    }*/ 
  }
  function edit_choir($request) {
    global $wpdb;   
    /*if($_FILES["edit_choir_profile"]["name"]!=''){
      if(isset($_FILES["edit_choir_profile"]["type"])){*/
        global $wpdb;
        $grouptable = 'groups';
        $user_id = $request['user_id']; 
        $group_id = $request['group_id'];
        $group_choir_name = $request['edit_group_choir_name'];
        $group_choir_desc = $request['edit_group_choir_desc'];
        $group_choir_city = $request['edit_group_choir_city'];
        $group_choir_state = $request['edit_group_choir_state'];
        $group_choir_country = $request['edit_group_choir_country'];
        $group_choir_image = $_FILES["edit_choir_profile"]["tmp_name"];

        /*$allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
        $temp = explode(".", $_FILES["edit_choir_profile"]["name"]);
        $extension = end($temp);*/

        /*if ($_FILES["edit_choir_profile"]["error"]) {
            $error .= 'Error opening the file';
            //return new WP_Error( 'No_Open_File', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }
        if ($_FILES["edit_choir_profile"]["type"] != "image/jpeg" &&
            $_FILES["edit_choir_profile"]["type"] != "image/JPEG" &&
            $_FILES["edit_choir_profile"]["type"] != "image/jpg" &&
            $_FILES["edit_choir_profile"]["type"] != "image/JPG" &&
            $_FILES["edit_choir_profile"]["type"] != "image/PNG" &&
            $_FILES["edit_choir_profile"]["type"] != "image/png") { 
            $error .= "Mime type not allowed";
            //return new WP_Error( 'Mime_type', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        /*if (!in_array($extension, $allowedExts)) {
            $error .= 'Extension not allowed';
            //return new WP_Error( 'Extension_not_allowed', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }
        if ($_FILES["edit_choir_profile"]["size"] > 3145728) {
            $error .= 'File size should be less than 2 Mb';
            //return new WP_Error( 'File_size_less_than_2_Mb', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => $error) );
        }*/
        if ($error == "") {
            $path_array = wp_upload_dir();
            $sourcePath = $_FILES['edit_choir_profile']['tmp_name'];
            $Newfilename = uniqid().'-'.$_FILES['edit_choir_profile']['name'];
            $targetPath = $path_array["basedir"]."/groupchoirimage/".$Newfilename;
            $movefile =  move_uploaded_file($sourcePath, $targetPath);

            if($movefile) {
                $last_insert_group_data = $wpdb->get_row("SELECT * FROM groups WHERE id='$group_id' and status='1' and deleted='0'");
                $where_array = array( 'id' => stripslashes( $group_id ));
                $success = $wpdb->update($grouptable, array(
                    "group_name" => $group_choir_name,
                    "group_desc" => $group_choir_desc,
                    "group_city" => $group_choir_city,
                    "group_state" => $group_choir_state,
                    "group_country" => $group_choir_country,
                    "group_img" => $Newfilename,
                    "updated_by" => $user_id,
                    "updated_at" => date('yyyy-mm-dd h:m:s'),
                ), $where_array);

                if($success) {
                    $successsds = $wpdb->update('wp_terms', array(
                        "name" => $group_choir_name,
                        "slug" => $group_choir_name,
                    ), array('group_id' => $group_id));
                    
                    $error =  'Your Choir Updated successfully...!!';
                    $image_base_url = site_url().'/wp-content/uploads/groupchoirimage';
                    $response = new WP_REST_Response(array('status' => 1, 'msg' => $error, 'data'=>$last_insert_group_data, 'image_base_url'=>$image_base_url));
                    return $response;
                } else {
                    $error =  'Sorry fail to update your Choir due to system error. Please try again.';
                    //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
                    return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
                }
            } else {
                $error =  'Sorry fail to update your Choir due to system error. Please try again.';
                //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
                return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
            }
        } else {
            $error =  'Sorry fail to update your Choir due to system error. Please try again.';
            //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
            return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
        }  
/*      } 
    }*/ 
  }
  function get_single_choir_listing($request) {
    global $wpdb;
    $user_id = $request['user_id'];
    $group_id = $request['group_id'];
    $get_single_choir_data = $wpdb->get_results("SELECT * FROM groups where id='$group_id' and user_id='$user_id' and status='1' and deleted='0' ORDER BY id DESC");
    if($get_single_choir_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Single Choir listing', 'data'=>$get_single_choir_data));
      return $response;
    } else {
      //return new WP_Error( 'No_Choir_Record_Found', 'No Choir Records Found!', array('status' => 404) );
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Choir Records Found!') );
    }
  }
  function edit_user_profile($request) {
    global $wpdb;   
/*    if($_FILES["edit_user_profile"]["name"]!=''){
      if(isset($_FILES["edit_user_profile"]["type"])){*/
        $filename = $_FILES['edit_user_profile']['tmp_name'];
        $id =$request['id'];
        $fname =$request['fname'];
        $lname = $request['lname'];
        $contactnumber =$request['contactnumber'];
        $address = $request['address'];
        $city =$request['city'];
        $state = $request['state'];
        $country = $request['country'];
        $usertable = $wpdb->prefix . 'users';
        $allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
        $temp = explode(".", $_FILES["edit_user_profile"]["name"]);
        $extension = end($temp);
        /*if ($_FILES["edit_user_profile"]["error"]) {
            //$error .= 'Error opening the file';
            //return new WP_Error( 'No_Open_File', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => 'Error opening the file') );
        }*/
        /*if ($_FILES["edit_user_profile"]["type"] != "image/jpeg" &&
            $_FILES["edit_user_profile"]["type"] != "image/JPEG" &&
            $_FILES["edit_user_profile"]["type"] != "image/jpg" &&
            $_FILES["edit_user_profile"]["type"] != "image/JPG" &&
            $_FILES["edit_user_profile"]["type"] != "image/PNG" &&
            $_FILES["edit_user_profile"]["type"] != "image/png") { 
            //$error .= "Mime type not allowed";
            //return new WP_Error( 'Mime_type', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => 'Mime type not allowed') );
        }*/
        /*if (!in_array($extension, $allowedExts)) {
            //$error .= 'Extension not allowed';
            //return new WP_Error( 'Extension_not_allowed', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => 'Extension not allowed') );
        }
        if ($_FILES["edit_user_profile"]["size"] > 3145728) {
            //$error .= 'File size should be less than 2 Mb';
            //return new WP_Error( 'File_size_less_than_2_Mb', $error, array('status' => 404) );
            return new WP_Error(array('status' => 0, 'msg' => 'File size should be less than 2 Mb') );
        }*/
        if ($error == "") {
            $path_array = wp_upload_dir();
            $sourcePath = $_FILES['edit_user_profile']['tmp_name'];
            $Newfilename = uniqid().'-'.$_FILES['edit_user_profile']['name'];
            $targetPath = $path_array["basedir"]."/profileimage/".$Newfilename;
            $movefile =  move_uploaded_file($sourcePath, $targetPath);

            if($movefile) {
                $success = $wpdb->update($usertable, array(
                    'ID'=>$id,
                    "user_nicename" => $fname,
                    "display_name" => $fname.' '.$lname,
                ),array('id'=>$id));
                if($success) {
                    $user_id = $id;
                    $user_role = serialize($role);
                    update_user_meta( $user_id, 'first_name', $fname);  
                    update_user_meta( $user_id, 'last_name', $lname);
                    update_user_meta( $user_id, 'contact_number', $contactnumber);
                    update_user_meta( $user_id, 'address', $address);
                    update_user_meta( $user_id, 'city', $city);
                    update_user_meta( $user_id, 'state', $state);
                    update_user_meta( $user_id, 'country', $country);
                    update_user_meta( $user_id, 'image_url', $Newfilename);
                    $user_info = get_userdata($user_id);
                    $user_image_url = get_user_meta($user_id, 'image_url', true);
                    $user_contact_number = get_user_meta($user_id, 'contact_number', true);
                    $user_selected_choir = get_user_meta($user_id, 'selected_choir', true);
                    $select_choir_cat = get_user_meta($user_id, 'select_choir_cat', true);
                    $address = get_user_meta($user_id, 'address', true);
                    $city = get_user_meta($user_id, 'city', true);
                    $state = get_user_meta($user_id, 'state', true);
                    $country = get_user_meta($user_id, 'country', true);
                    $error =  'Your profile update successfully...!!';
                    $image_base_url = site_url().'/wp-content/uploads/profileimage';
                    $response = new WP_REST_Response(array('status' => 1, 'msg' => $error, 'data'=>$user_info, 'image_base_url'=>$image_base_url, 'user_image_url'=>$user_image_url, 'user_contact_number'=>$user_contact_number, 'selected_choir'=>$user_selected_choir, 'select_choir_cat'=>$select_choir_cat, 'address'=>$address, 'city'=>$city, 'state'=>$state, 'country'=>$country));
                    return $response;
                } else {
                    //$error =  'Sorry fail to update your Choir due to system error. Please try again.';
                    //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
                    return new WP_REST_Response(array('status' => 0, 'msg' => 'Sorry fail to update your Choir due to system error. Please try again.') );
                }
            } else {
                //$error =  'Sorry fail to update your Choir due to system error. Please try again.';
                //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
                return new WP_REST_Response(array('status' => 0, 'msg' => 'Sorry fail to update your Choir due to system error. Please try again.') );
            }
        } else {
            //$error =  'Sorry fail to update your Choir due to system error. Please try again.';
            //return new WP_Error( 'updated_choir', $error, array('status' => 404) );
            return new WP_REST_Response(array('status' => 0, 'msg' => 'Sorry fail to update your Choir due to system error. Please try again.') );
        }  
      /*} */
    /*} else {
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        global $wpdb;
        $usertable = $wpdb->prefix . 'users';
        $id =$request['id'];
        $fname =$request['fname'];
        $lname = $request['lname'];
        $contactnumber =$request['contactnumber'];
        $address = $request['address'];
        $city =$request['city'];
        $state = $request['state'];
        $country = $request['country'];
        $success = $wpdb->update($usertable, array(
            'ID'=>$id,
            "user_nicename" => $fname,
            "display_name" => $fname.' '.$lname,
          
        ),array('id'=>$id));
        $user_id = $id;
        $user_role = serialize($role);
        update_user_meta( $user_id, 'first_name', $fname);  
        update_user_meta( $user_id, 'last_name', $lname);
        update_user_meta( $user_id, 'contact_number', $contactnumber);
        update_user_meta( $user_id, 'address', $address);
        update_user_meta( $user_id, 'city', $city);
        update_user_meta( $user_id, 'state', $state);
        update_user_meta( $user_id, 'country', $country);
        $user_info = get_userdata($user_id);
        $user_image_url = get_user_meta($user_id, 'image_url', true);
        $user_contact_number = get_user_meta($user_id, 'contact_number', true);
        $user_selected_choir = get_user_meta($user_id, 'selected_choir', true);
        $select_choir_cat = get_user_meta($user_id, 'select_choir_cat', true);
        $image_base_url = site_url().'/wp-content/uploads/profileimage';
        $error =  'Your Profile updated successfully...!!';
        $response = new WP_REST_Response(array('status' => 1, 'msg' => $error, 'data'=>$user_info, 'image_base_url'=>$image_base_url, 'image_base_url'=>$image_base_url, 'user_image_url'=>$user_image_url, 'user_contact_number'=>$user_contact_number, 'selected_choir'=>$user_selected_choir, 'select_choir_cat'=>$select_choir_cat));
        return $response;
      } else {
        //$error =  'Sorry fail to update your Profile due to system error. Please try again.';
        return new WP_Error(array('status' => 0, 'msg' => 'Sorry fail to update your Profile due to system error. Please try again.') );
      }
    }*/
  }
  function create_event($request) {
    global $wpdb;
    $items_array = array();
    $insertValuesSQL = '';
      foreach($_FILES['event_img']['name'] as $key=>$val){
        $image_name = $_FILES['event_img']['name'][$key];
        $tmp_name   = $_FILES['event_img']['tmp_name'][$key];
        $size       = $_FILES['event_img']['size'][$key];
        $type       = $_FILES['event_img']['type'][$key];
        $error      = $_FILES['event_img']['error'][$key];
        if($image_name!=''){
          if(isset($type)){
            global $wpdb;
            $eventtable = 'events';
            $user_id = $request['user_id'];
            $events_name = $request['events_name'];
            $events_desc = stripslashes($request['events_desc']);
            $event_start_date = $request['event_start_date'];
            $event_end_date = $request['event_end_date'];
            $event_start_time = $request['event_start_time'];
            $event_end_time = $request['event_end_time'];
            $selectchoir = $request['selectchoir'];
            $events_city = $request['event_city'];
            /*if(!empty($request['events_state'])){*/
            $events_state = $request['event_state'];
            /*} else {
                $events_state ='';
            }*/
            $events_country = $request['event_country'];
            $event_choir_image = $_FILES["event_img"]["tmp_name"];
            $allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
            $temp = explode(".", $image_name);
            $extension = end($temp);
            if ($error) {
                $error .= 'Error opening the file';
                return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
            }
            if ($type != "image/jpeg" &&
              $type != "image/JPEG" &&
              $type != "image/jpg" &&
              $type != "image/JPG" &&
              $type != "image/PNG" &&
              $type != "image/png") { 
              $error .= "Mime type not allowed<br />";
            }
            if (!in_array($extension, $allowedExts)) {
                $error .= 'Extension not allowed';
                return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
            }
            if ($size > 3145728) {
                $error .= 'File size shoud be less than 2 Mb';
                return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
            }
            if ($error == "") {
              $path_array = wp_upload_dir();
              $sourcePath = $tmp_name;
              $filename = uniqid().'-'.$image_name;
              $items_array[] = $filename; 

              $items= implode(',', $items_array);
              $targetPath = $path_array["basedir"]."/eventsimage/".$filename;
              $movefile =  move_uploaded_file($sourcePath, $targetPath);

              if($movefile){
                    $insertValuesSQL .= $filename;
              } else {
                    $errorUpload .= $image_name.', ';
              }
            } else {
              return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
              //echo $error;
            }
          }
        }
      }
      if(!empty($insertValuesSQL)){
        $success = $wpdb->insert($eventtable, array( 
          "user_id" => $user_id,
          "group_id"=> $selectchoir,
          "singer_id" =>'',
          "events_name" => $events_name,
          "events_dec" => $events_desc,
          "events_startdate" => $event_start_date ,
          "events_enddate" => $event_end_date,
          "event_start_time" => $event_start_time,
          "event_end_time" => $event_end_time,
          "events_city" => $events_city,
          "events_state" => $events_state,
          "events_country" => $events_country,
          "join_users" => '',
          "created_by" => $user_id,
          "image" => $items,
          
        ));
        $event_id = $wpdb->insert_id;
        $eventdata = $wpdb->get_row("SELECT * FROM events where id='$event_id' and status='1' and deleted='0'");
        $image_base_url = site_url().'/wp-content/uploads/eventsimage';
        if($success) {
          $error =  'Your Events data Added successfully...!!';
          $response = new WP_REST_Response(array('status' => 1, 'msg' => $error, 'data'=>$eventdata, 'image_base_url'=>$image_base_url));
          return $response;
        } else {
          $error =  'Sorry fail to Update your Events date  due to system error. Please try again.';
          return new WP_REST_Response(array('status' => 0, 'msg' => $error) );
        }
    }
  }
  function view_event($request){
    global $wpdb;
    $event_id = $request['event_id'];
    $group_id = $request['group_id'];
    $event_data = $wpdb->get_results("SELECT * from events where id='$event_id' and status='1' and deleted='0'");
    $image_base_url = site_url().'/wp-content/uploads/eventsimage';
    if($event_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Single event data!', 'data'=>$event_data, 'image_base_url'=>$image_base_url));
      return $response;
    } else {
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Records found of events!') );
    }
  }
  function get_user_event_listing($request){
    global $wpdb;
    $user_id = $request['user_id'];
    $group_id = $request['group_id'];
    $event_data = $wpdb->get_results("SELECT * from events where user_id='$user_id' and status='1' and deleted='0' ORDER BY id DESC");
    $image_base_url = site_url().'/wp-content/uploads/eventsimage';
    if($event_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Event Listing!', 'data'=>$event_data, 'image_base_url'=>$image_base_url));
      return $response;
    } else {
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Records found of events!') );
    }
  }
  function delete_event($request){
    global $wpdb;
    $event_id = $request['event_id'];
    $event_data = $wpdb->update('events', array(
          "deleted" => '1'
    ), array('id' => $event_id));
    if($event_data){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Event data successfully deleted!'));
      return $response;
    } else {
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No data found') );
    }
  }
  function create_albums(){
    global $wpdb;
    $user_id = $request['user_id'];
    $group_id = $request['select_group'];
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $addalbum = wp_insert_post(
          array(
                'post_author'=>$user_id,
                'post_date' => date('Y-m-d h:m:s'),
                'post_date_gmt'=> date('Y-m-d h:m:s'),
                'post_title'=>$request['album_name'],
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name'=>strtolower($request['album_name']),
                'post_modified'=>'',
                'post_modified_gmt'=>'',
                'guid'=>'',
                'post_type'=>'audio_mixer_files' 
          )
        );
        if($addalbum){
            $album_id = $wpdb->insert_id;
            $get_album_data = get_post($album_id);
            add_post_meta($album_id, '_edit_last', '1');
            add_post_meta($album_id, 'presets', true);
            add_post_meta($album_id, 'presetSettings', true);
            add_post_meta($album_id, 'subject_known', 'Your file is ready', true);
            add_post_meta($album_id, 'body_known', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'subject_general', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'body_general', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'subtitles_show', 'yes', true);
            add_post_meta($album_id, 'subtitles', 'NULL', true);
            add_post_meta($album_id, 'markers', '[{"start_time":"04:32","end_time":"05:25","content":"","color":"#ec6712"}]', true);
            add_post_meta($album_id, 'autoplay', 'no', true);
            add_post_meta($album_id, 'srtFile', '0', true);
            add_post_meta($album_id, 'srtOffset', '0', true);
            add_post_meta($album_id, 'downloadKnown', 'no', true);
            add_post_meta($album_id, 'slide_template', 'default', true);
            add_post_meta($album_id, 'eg_sources_html5_mp4', '', true);
            add_post_meta($album_id, 'eg_sources_html5_ogv', '', true);
            add_post_meta($album_id, 'eg_sources_html5_webm', '', true);
            add_post_meta($album_id, 'eg_sources_youtube', '', true);
            add_post_meta($album_id, 'eg_sources_vimeo', '', true);
            add_post_meta($album_id, 'eg_sources_wistia', '', true);
            add_post_meta($album_id, 'eg_sources_image', '', true);
            add_post_meta($album_id, 'eg_sources_iframe', '', true);
            add_post_meta($album_id, 'eg_sources_soundcloud', '', true);
            add_post_meta($album_id, 'eg_vimeo_ratio', '1', true);
            add_post_meta($album_id, 'eg_youtube_ratio', '1', true);
            add_post_meta($album_id, 'eg_wistia_ratio', '1', true);
            add_post_meta($album_id, 'eg_html5_ratio', '1', true);
            add_post_meta($album_id, 'eg_soundcloud_ratio', '1', true);
            add_post_meta($album_id, 'eg_sources_revslider', '', true);
            add_post_meta($album_id, 'eg_sources_essgrid', '', true);
            add_post_meta($album_id, 'eg_featured_grid', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_skin', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_element', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_setting', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_style', '', true);
            add_post_meta($album_id, 'eg_custom_meta_216', 'true', true);
            add_post_meta($album_id, 'eg_votes_count', '0', true);
            $term_data = $wpdb->get_row("select * from wp_terms where group_id='$group_id'");
            $term_id = $term_data->term_id;
            $term_taxonomy_data = $wpdb->get_row("select * from wp_term_taxonomy where term_id='$term_id'");
            $term_taxonomy_id = $term_taxonomy_data->term_taxonomy_id;
            $wpdb->insert('wp_term_relationships', array(
                'object_id'=>$album_id,
                'term_taxonomy_id' => $term_taxonomy_id,
                'term_order'=> '0',
                'group_id'=>$group_id,
            ));

          $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Album created successfully!', 'data'=>$get_album_data));
          return $response;
        } else {
          return new WP_REST_Response(array('status' => 0, 'msg' => 'Sorry fail to add your album due to system error. Please try again.') );
        }
    }
  } 
  function get_albums_listing($request){
    global $wpdb;
    $user_id = $request['user_id'];
    $defaults = array(
        'numberposts'      => -1,
        'category'         => 0,
        'orderby'          => 'id',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'audio_mixer_files',
        'suppress_filters' => true,
        'author'           => $user_id,
    );
 
    $r = wp_parse_args($defaults );
    if ( ! empty( $r['numberposts'] ) && empty( $r['posts_per_page'] ) ) {
        $r['posts_per_page'] = $r['numberposts'];
    }
    $r['ignore_sticky_posts'] = true;
    $r['no_found_rows']       = true;
 
    $get_posts = new WP_Query;
    $getalbumdata = $get_posts->query( $r );
    $all_extra_data_arry = array();
    foreach ($getalbumdata as $getalbumdatas) {
    $album_id = $getalbumdatas->ID;
    $term_relationship_data = $wpdb->get_row("select * from wp_term_relationships where object_id='$album_id'");
    //echo "<pre>"; print_r($term_relationship_data); echo "</pre>";
    $group_id = $term_relationship_data->group_id;
    $group_data = $wpdb->get_row("select * from groups where id='$group_id' and status='1' and deleted='0'");
    $args = array(
      'orderby'          => 'id',
      'order'            => 'DESC',
      'post_type'        => 'attachment',
      'post_mime_type'   => 'audio/mpeg',
      'post_parent'      => $album_id,
      'post_status'      => 'inherit',
      'suppress_filters' => true,
    );
    $getsongsdata = get_posts( $args );

    $allextradata = array(
      'choir_name' => $group_data->group_name,
      'song_count' => count($getsongsdata) 
    );
    array_push($all_extra_data_arry, $allextradata);
    }
    if($getalbumdata){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Album all listing!', 'data'=>$getalbumdata, 'extra_data'=>$all_extra_data_arry));
      return $response;
    } else {
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Records found of albums!') );
    }
  }

  function get_choir_list_check_team_member($request){
    global $wpdb;
    $user_id = $request['user_id'];
    $group_data = $wpdb->get_results("SELECT * from groups where user_id='$user_id' and status='1' and deleted='0' ORDER BY id DESC");
    foreach ($group_data as $group_datas) { 
      $group_id = $group_datas->id;
      $user_query = new WP_User_Query(
            array(
                'meta_key'    => 'selected_choir',
                'meta_value'  => $group_id,
            )
      );  
      $singers = $user_query->get_results(); 
      $singer_total_count = count($singers);
    }
    if($singer_total_count > 0){
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Choir all listing!', 'data'=>$group_data));
      return $response;
    } else {
      return new WP_REST_Response(array('status' => 0, 'msg' => 'No Records found of choir!') );
    }

  }

  function get_allgroups_team_member_list($request){
    global $wpdb;
      $singers = get_users(array(
        'wp_capabilities'     => 'singer',
        ''
      ));
      $allsingerdata = array();
      foreach ($singers as $singer_users) {
        $singer_id=  $singer_users->ID;
        $current_user_id   = $request['user_id'];
        $selected_choir_group_id = get_user_meta($singer_id, 'selected_choir','true' );
        $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id' and user_id='$current_user_id'");
        if($group_data->id==$selected_choir_group_id && $group_data->user_id == $current_user_id){
          $singer_id=  $singer_users->ID;
          $alldata = array();
          $singer_first_name = ucwords(get_user_meta($singer_id, 'first_name','true' ));
          $singer_last_name = ucwords(get_user_meta($singer_id, 'last_name','true' ));
          $singer_category = get_user_meta($singer_id, 'selected_choir_cat','true');
          $singer_choirId = get_user_meta($singer_id, 'selected_choir','true');
          $query = $wpdb->get_results("SELECT group_name from `groups` WHERE `id`='$singer_choirId' AND `status` = '1' AND `deleted`= '0' ");
          $singer_image = get_user_meta($singer_id, 'image_url','true');
          $group_name = $query[0]->group_name;

          $alldata['id'] = $singer_id;
          $alldata['first_name'] = $singer_first_name;
          $alldata['last_name'] = $singer_last_name;
          $alldata['singer_category'] = $singer_category;
          $alldata['group_name'] = $group_name;
          $alldata['singer_image'] = $singer_image; 
        } else {

        }
        array_push($allsingerdata, $alldata); 
      }
      $response = new WP_REST_Response(array('status' => 1, 'msg' => 'Singer all listing!', 'data'=>$allsingerdata));
          return $response;
  }









/***********All Function End************/

/***********All Action *****************/
 add_action('rest_api_init', function(){
	register_rest_route( 'project/api/v1', '/user/register', array(
    	'methods'  => 'POST',
    	'callback' => 'user_register'
	)); 
  register_rest_route( 'project/api/v1', '/user/login', array(
      'methods'  => 'POST',
      'callback' => 'user_login'
  )); 
	register_rest_route( 'project/api/v1', '/user/(?P<user_id>\d+)', array(
    	'methods'  => 'GET',
    	'callback' => 'get_single_user_data'
	));
  register_rest_route( 'project/api/v1', '/user/forgot', array(
      'methods'  => 'POST',
      'callback' => 'forgot_password_reset_link_mail'
  ));
  register_rest_route( 'project/api/v1', '/choir/allchoirlisting', array(
      'methods'  => 'GET',
      'callback' => 'get_all_choir_listing'
  ));
  register_rest_route( 'project/api/v1', '/choir/userchoirlisting', array(
      'methods'  => 'POST',
      'callback' => 'get_user_choir_listing'
  ));
  register_rest_route( 'project/api/v1', '/choir/create-choir', array(
      'methods'  => 'POST',
      'callback' => 'create_choir'
  ));
  register_rest_route( 'project/api/v1', '/choir/edit-choir', array(
      'methods'  => 'POST',
      'callback' => 'edit_choir'
  ));
  register_rest_route( 'project/api/v1', '/choir/single-choir', array(
      'methods'  => 'POST',
      'callback' => 'get_single_choir_listing'
  ));
  register_rest_route( 'project/api/v1', '/user/edit-profile', array(
      'methods'  => 'POST',
      'callback' => 'edit_user_profile'
  ));
  register_rest_route( 'project/api/v1', '/event/create-event', array(
      'methods'  => 'POST',
      'callback' => 'create_event'
  ));
  register_rest_route( 'project/api/v1', '/event/view-event', array(
      'methods'  => 'POST',
      'callback' => 'view_event'
  ));
  register_rest_route( 'project/api/v1', '/event/user-event-listing', array(
      'methods'  => 'POST',
      'callback' => 'get_user_event_listing'
  ));
  register_rest_route( 'project/api/v1', '/event/delete-event', array(
      'methods'  => 'POST',
      'callback' => 'delete_event'
  ));
  register_rest_route( 'project/api/v1', '/album/create-album', array(
      'methods'  => 'POST',
      'callback' => 'create_albums'
  ));
  register_rest_route( 'project/api/v1', '/album/allalbumlisting', array(
      'methods'  => 'POST',
      'callback' => 'get_albums_listing'
  ));
  register_rest_route( 'project/api/v1', '/album/choir-list-with-team-member', array(
      'methods'  => 'POST',
      'callback' => 'get_choir_list_check_team_member'
  ));
  register_rest_route( 'project/api/v1', '/team/allgroups-team-member', array(
      'methods'  => 'POST',
      'callback' => 'get_allgroups_team_member_list'
  ));
 });

/***********All Action End**************/
