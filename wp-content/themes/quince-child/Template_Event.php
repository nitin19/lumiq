<style>
@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
@import url('https://fonts.googleapis.com/css?family=Raleway&display=swap');
.joined {
    cursor: not-allowed !important;
    opacity: .5;
}
.hdr-banner {
    /*background: url(http://www.siing.io/wp-content/uploads/2019/08/04.jpg);*/
    height: 100vh;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    position: relative;
}
p.meet {
    font-size: 24px;
    color: #ff6467;
    font-family: "Roboto";
    font-weight: 500;
    padding-top: 130px;
}
p.mock {
    font-size: 74px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 400;
    margin-bottom: 0px;
}
p.meet {
    margin-bottom: 0px;
    padding-left: 5px;
}
h2.burp {
    font-size: 74px;
    line-height: 78px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 700;
    padding-bottom: 50px;
    text-transform: uppercase;
}
button.cmng {
    border-radius: 28px;
    box-shadow: 0px 25px 38px rgba(15,16,21,0.22);
    background-image: linear-gradient(-26deg, #ff0a0a 0%, #f87a7a 100%);
    border: 1px solid #f87a7a;
    color: #fff;
    padding: 5px 25px 5px 25px;
}
button.nt-cmng {
    background: transparent;
    color: #fff;
    border-radius: 28px;
    border: 1px solid;
    padding: 5px 20px 5px 20px;
}
.abt-evnt {
    text-align: center;
    padding-top: 50px;
}
.abt-evnt h2 {
    font-size: 19px;
    color: #191919;
    font-family: "Roboto" !important;
    font-weight: 600;
    text-align: center;
    background-color: #fff;
    margin-top: -2px !important;
    max-width: 800px;
    margin: 0 auto;
}
.abt-evnt p {
      font-size: 16px;
    line-height: 32px;
    color: #666666;
    font-family: "Roboto" !important;
    font-weight: 400;
    text-align: center;
    padding-top: 30px;
    max-width: 90%;
    margin: 0 auto;
}
.btn-scn {
    text-align: center;
    padding-top: 50px;
}
button.nt-cmngabt {
    background: transparent;
    color: #000;
    border-radius: 28px;
    border: 1px solid;
    padding: 5px 20px 5px 20px;
}
.abt-bdr {
    border: 1px solid #e8e8e8;
}
.cnfrc-evnt {
    padding-top: 00px;
    margin-top: -100px;
}
.evnt-dt img {
    width: 100%;
}
.icon-img {
    width: 132px;
    margin-top: -50px !important;
    margin: 0 auto;
}
.evnt-dt {
    text-align: center;
    padding-bottom: 40px;
    min-height: 440px;
    border: 1px solid #e8e8e8;
}
.evnt-dt h4 {
    font-size: 22px;
    line-height: 32px;
    color: #3d3d3d;
    font-family: "Roboto" !important;
    font-weight: 400;
    margin-top: -20px;
}
.evnt-dt p {
    font-size: 18px;
    color: #666666;
    font-family: "Roboto" !important;
    font-weight: 400;
    margin-bottom: 0px;
}
.evnt-dt span {
    font-size: 17px;
    color: #ff5454;
    font-family: "Roboto" !important;
    font-weight: 400;
    text-align: center;
}
.excul-scn {
    background: #ebeff5;
    padding-top: 50px;
    margin-top: 50px;
    padding-bottom: 50px;
}
.mock h2 {
    font-size: 46px;
    line-height: 57px;
    color: #333333;
    font-family: "Raleway" !important;
    font-weight: 400;
    padding-bottom: 30px;
}
.mock span {
    color: #ff3333;
    font-weight: 600;
}
.bg-mock {
    background: #fff;
    margin-bottom: 10px;
    width: 100%;
}
.bg-mock span {
    font-size: 16px;
}
.bg-mock p {
    margin-bottom: 0px;
    padding-left: 28px;
    padding-top: 15px;
    padding-bottom: 15px;
    font-size: 24px;
    color: #444444;
    font-family: "Roboto" !important;
    font-weight: 500;
    padding-bottom: 30px;
    padding-right: 90px;
}
.bg-mock span.choir-name {
    color: #ff4141;
}
.bg-mock span {
    color: #444444;
}

.flex-caption {
  width: 96%;
  padding: 2%;
  left: 0; 
  bottom: 0;
  background: rgba(0,0,0,.5);
  color: #fff;
  text-shadow: 0 -1px 0 rgba(0,0,0,.3);
  font-size: 14px;
  line-height: 18px;
}
li.css a {
  border-radius: 0;
}
img.demo.cursor {
    width: 100%;
    padding-left: 5px;
}
.column.tmbl {
    width: 20%;
    float: left;
    height: 20%
}

.arrow-img {
    position: relative;
}
a.next {
    float: right;
    top: 40%;
    position: absolute;
    left: 97%;
}
a.prev {
    position: absolute;
    top: 40%;
    right: 97%;
}
h2.slide-hdr {
    font-size: 46px;
    line-height: 57px;
    color: #292929;
    font-family: "Raleway";
    font-weight: 600;
    text-align: center;
    padding-top: 40px;
    padding-bottom: 20px;
}
.demo.cursor {
    min-height: 200px !important;
    padding-bottom: 50px;
}
.col-md-6 {
    display: flex;
}
button.nt-cmng:hover {
    background: linear-gradient(-26deg, #ff0a0a 0%, #f87a7a 100%);
    border: 1px solid #f87a7a;
}
button.cmng:hover {
    background: transparent;
    border: 1px solid;
}
button.cmng.abt:hover {
    color: #000;
}
button.nt-cmngabt:hover {
    background: linear-gradient(-26deg, #ff0a0a 0%, #f87a7a 100%);
    border: 1px solid #f87a7a;
    color: #fff;
}
@media screen  and (min-width: 320px) and (max-width: 768px){
h2.burp {
    font-size: 45px;
}
p.mock {
    font-size: 36px;
}
a.next {
    left: 85%;
}

a.prev {
    right: 85%;
}
.mock h2 {
    font-size: 35px;
}
.bg-mock p {
    padding-left: 19px;
}
}
@media screen  and (min-width: 500px) and (max-width: 768px){
a.prev {
    right: 91%;
}
a.next {
    left: 91%;
}
.abt-evnt h2 {
    max-width: 400px;
}
@media screen  and (min-width: 320px) and (max-width: 500px){
.abt-evnt h2 {
    max-width: 200px;
}
}
@media screen  and (min-width: 600px) and (max-width: 1025px){
h2.burp {
    padding-bottom: 20px;
    line-height: 30px;
    font-size: 30px;
}
}

</style>

<?php

/* Template Name: Events */ 

get_header();
global $wpdb;
$current_user_id   = get_current_user_id();
$events_id = $_GET['events_id'];
$groups_id=$_GET['group_id'];
$users_id=$_GET['user_id'];
$backgroundimage = site_url().'/wp-content/uploads/2019/08/04.jpg';  
$quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id ");
$eventdata = $wpdb->get_row("SELECT * FROM `events` WHERE `id` =  $events_id AND `status`='1' AND `deleted`='0' ");
// echo "<pre>"; print_r($eventdata); echo "</pre>";
$event_id=$eventdata->id;
$group_id=$eventdata->group_id;
 $user_id=$eventdata->user_id;
$user_meta=get_userdata($current_user_id);
$user_roles= $user_meta->roles[0];
?>
<link rel="stylesheet" href="css/style.css">
 <div class="hdr-banner" style="background:  url(<?php echo $backgroundimage; ?>)">
         <div class="container">
            <div class="row">
               <div class="col">
                  <div class="bnr-txt">
                     <p class="meet"></p>
                     <p class="mock"></p>
                     <h2 class="burp"><?php echo $eventdata->events_name;?></h2>
                     <?php   
                      if($user_roles == 'singer'){
                        $join_user= $wpdb->get_row("SELECT * FROM `events` WHERE `singer_id`= '$current_user_id'  AND `join_users` = '1' AND `status`= '1'  AND `deleted` ='0' ");
                        if($join_user){
                         
                       ?>
                     <button class="cmng btn_click joined" coming_id="1" disabled="true">Coming</button>
                     <button class="nt-cmng btn_click" coming_id="0">Not Coming</button>          
                     <?php } else { 
                     ?>
                      <button class="cmng btn_click" coming_id="1">Coming</button>
                      <button class="nt-cmng btn_click" coming_id="0">Not Coming</button> 

                    <?php  }
                     } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="cnfrc-evnt">
         <div class="container">
            <div class="row">
               <div class="col-md-4 date">
                  <div class="evnt-dt">
                     <?php 
                      if($eventdata->image!=''){
                        $beforeDot = explode(",", $eventdata->image);
                        $beforeDot = $beforeDot[0];
                        ?>
                        <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot;?>">
                        <?php 
                      } else {
                    $profile_one= site_url().'/wp-content/uploads/2019/08/7.jpg';?>
                    <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
                    <?php } ?>
                     <div class="icon-img">
                        <img src="http://www.siing.io/wp-content/uploads/2019/08/icon-1.png">
                     </div>
                     <h4>Event Date</h4>
                     <p>Conference Event will be held on</p>
                     <span><strong style="color:#333;">Start Date:</strong><?php echo  $eventdata->events_startdate; ?> - <strong style="color:#333;">End Date:</strong><?php echo $eventdata->events_enddate; ?></span> 
                  </div>
               </div>
               <div class="col-md-4 time">
                  <div class="evnt-dt">
                     <?php 
                      if($eventdata->image!=''){
                        $beforeDot1 = explode(",", $eventdata->image);
                        $beforeDot_one = $beforeDot1[1];
                        ?>
                        <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot_one;?>">
                        <?php 
                      } else {
                    $profile_one= site_url().'/wp-content/uploads/2019/08/7.jpg';?>
                    <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
                    <?php } ?><br>
                     <div class="icon-img">
                        <img src="http://www.siing.io/wp-content/uploads/2019/08/2.png">
                     </div>
                     <h4>Event Time</h4>
                     <p>Conference Event will be held on</p>
                     <span><strong style="color:#333;">Start Time:</strong><?php echo  $eventdata->event_start_time; ?> - <strong style="color:#333;">End Time:</strong><?php echo  $eventdata->event_end_time; ?></span>  
                  </div>
               </div>
               <div class="col-md-4 location">
                  <div class="evnt-dt">
                    <?php 
                    if($eventdata->image!=''){
                      $beforeDot2 = explode(",", $eventdata->image);
                        $beforeDot_two = $beforeDot1[2];
                      ?>
                      <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot_two;?>">
                      <?php 
                    } else {
                  $profile_one= site_url().'/wp-content/uploads/2019/08/7.jpg';?>
                  <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
                  <?php } ?><br>
                     <div class="icon-img">
                        <img src="http://www.siing.io/wp-content/uploads/2019/08/icon-03.png">
                     </div>
                     <h4>Event Location</h4>
                     <p>Conference Event will be held on</p>
                     <span><?php echo  $eventdata->events_city .' '. $eventdata->events_state .' '.  $eventdata->events_country; ?></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col">
               <div class="abt-evnt">
                  <div class="abt-bdr">
                     <h2>About Event</h2>
                     <p><?php echo  $eventdata->events_dec; ?></p>
                  </div>
                  <div class="btn-scn">
                    <!--  <button class="cmng abt">Coming</button>
                     <button class="nt-cmngabt">Not Coming</button>   -->

                      <?php   
                      if($user_roles == 'singer'){
                        $join_user= $wpdb->get_row("SELECT * FROM `events` WHERE `singer_id`= '$current_user_id'  AND `join_users` = '1' AND `status`= '1'  AND `deleted` ='0' ");
                        if($join_user){
                         
                       ?>
                     <button class="cmng abt btn_click joined abt" coming_id="1" disabled="true">Coming</button>
                     <button class="nt-cmngabt btn_click" coming_id="0">Not Coming</button>          
                     <?php } else { 
                     ?>
                      <button class="cmng btn_click abt" coming_id="1">Coming</button>
                      <button class="nt-cmngabt btn_click" coming_id="0">Not Coming</button> 

                    <?php  }
                     } ?>

                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="excul-scn">
         <div class="container">
            <div class="mock">
               <h2>Exculpate, Mockumentary,<br>
                  <span>Burpless, Meetups</span>
               </h2>
            </div>
            <?php 
             $display_name = get_userdata($user_id);
               
            ?>
             <div class="row">
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>See you again<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>Marketing Matters in design area<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>What do you mean?<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>Marketing Matters in design area<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>What do you mean?<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="bg-mock">
                     <p>Marketing Matters in design area<br>
                        <span>by <span class="choir-name"><?php echo $display_name->display_name;?></span> Choir Master</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Place somewhere in the <body> of your page -->
<div class="container">
  <h2 class="slide-hdr">Event Gallery</h2>
  <div class="arrow-img">
    <a class="prev" onclick="plusSlides(-1)"><img src="http://www.siing.io/wp-content/uploads/2019/08/arrow-lft.png"></a>
  <a class="next" onclick="plusSlides(1)"><img src="http://www.siing.io/wp-content/uploads/2019/08/arrow-rgt.png"></a>
      <?php 
      /*if (is_array($eventdata->image) || is_object($eventdata->image))
      { */

      $image_url = explode(",", $eventdata->image);
      foreach ($image_url as $img_value) {
        $img_url = site_url().'/wp-content/uploads/eventsimage/'.$img_value;
       ?>
  <div class="mySlides">
    <div class="numbertext"></div>
    <img src='<?php echo $img_url;?>' style="width:100%">
  </div>
 <?php }  
//}?>

<!--   <div class="mySlides">
    <div class="numbertext"></div>
    <img src="http://www.siing.io/wp-content/uploads/2019/08/22.jpg" style="width:100%" >
  </div>

  <div class="mySlides">
    <div class="numbertext"></div>
    <img src="http://www.siing.io/wp-content/uploads/2019/08/2.jpeg" style="width:100%" >
  </div>
      
  <div class="mySlides">
    <div class="numbertext"></div>
    <img src="http://www.siing.io/wp-content/uploads/2019/08/2.jpeg" style="width:100%" >
  </div>

  <div class="mySlides">
    <div class="numbertext"></div>
    <img src="http://www.siing.io/wp-content/uploads/2019/08/10.png" style="width:100%">
  </div> -->
    
  <div class="caption-container">
    <p id="caption"></p>
  </div>


  <div class="row">
    <?php 
    $image_url = explode(",", $eventdata->image);
    foreach ($image_url as $img_values) {
         $img_url = site_url().'/wp-content/uploads/eventsimage/'.$img_values;
         $i++;

    ?>
    <div class="column tmbl">
      <img class="demo cursor" src='<?php echo $img_url;?>' onclick="currentSlide('<?php echo $i;?>')" alt="">
    </div>
    <?php }
   ?>
    <!-- <div class="column tmbl">
      <img class="demo cursor" src="http://www.siing.io/wp-content/uploads/2019/08/22.jpg"  onclick="currentSlide(2)" alt="">
    </div>
    <div class="column tmbl">
      <img class="demo cursor" src="http://www.siing.io/wp-content/uploads/2019/08/2.jpeg" onclick="currentSlide(3)" alt="">
    </div> 
    <div class="column tmbl">
      <img class="demo cursor" src="http://www.siing.io/wp-content/uploads/2019/08/2.jpeg" onclick="currentSlide(4)" alt="">
    </div>
    <div class="column tmbl">
      <img class="demo cursor" src="http://www.siing.io/wp-content/uploads/2019/08/10.png" onclick="currentSlide(5)" alt="">
    </div>  -->   
  </div>
</div>
</div>
<script src="js/popper.min.js"></script>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  jQuery('.btn_click').click(function(){
  var singer_id= '<?php echo $current_user_id?>';
  var event_id= '<?php echo $event_id?>';
  var dataId = $(this).attr("coming_id");
  // alert(dataId);
     jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_join_events.php',
                data : {coming_id: dataId , user_id: singer_id, event_id: event_id },
                cache: false, 
            success :  function(data) {
              //alert(data);
            // jQuery(".upload-gimg-msg").html(data);
             //setTimeout();
              // function popupPimg() {
              //window.location.assign("<?php echo site_url();?>/");
              location.reload();
              //}
            },
          
          });
  });
  });
</script>

<?php

get_footer();
?>
