<?php 
require_once('../../../wp-load.php');
//function add_group_choir(){
$current_user = wp_get_current_user(); 
$currentUser_email = $current_user->data->user_email;
$admin_email = get_option( 'admin_email' );
$logo=site_url().'/wp-content/uploads/2019/07/logo-1.png';
if($_FILES["photo"]["name"]!=''){
  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["photo"]["type"])){
  //if($_POST['submit']) {
    global $wpdb;
    $grouptable = 'groups';
    $user_id = $_POST['user_id']; 
    $group_choir_name = $_POST['group_choir_name'];
    $group_choir_desc = $_POST['group_choir_desc'];
    $group_choir_city = $_POST['group_choir_city'];
    $group_choir_state = $_POST['group_choir_state'];
    $group_choir_country = $_POST['group_choir_country'];
    $group_choir_image = $_FILES["photo"]["tmp_name"];

    $allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
    $temp = explode(".", $_FILES["photo"]["name"]);
    $extension = end($temp);

    if ($_FILES["photo"]["error"]) {
        $error .= '<div class="alert alert-danger">Error opening the file<br /></div>';
    }
    if ($_FILES["photo"]["type"] != "image/jpeg" &&
        $_FILES["photo"]["type"] != "image/JPEG" &&
        $_FILES["photo"]["type"] != "image/jpg" &&
        $_FILES["photo"]["type"] != "image/JPG" &&
        $_FILES["photo"]["type"] != "image/PNG" &&
        $_FILES["photo"]["type"] != "image/png") { 
        $error .= "Mime type not allowed<br />";
    }
    if (!in_array($extension, $allowedExts)) {
        $error .= '<div class="alert alert-danger">Extension not allowed<br /></div>';
    }
    if ($_FILES["photo"]["size"] > 3145728) {
        $error .= '<div class="alert alert-danger">File size shoud be less than 2 Mb<br /></div>';
    }
    if ($error == "") {
      $path_array = wp_upload_dir();
      $sourcePath = $_FILES['photo']['tmp_name'];
      $Newfilename = uniqid().'-'.$_FILES['photo']['name'];
      $targetPath = $path_array["basedir"]."/groupchoirimage/".$Newfilename;
      $movefile =  move_uploaded_file($sourcePath, $targetPath);

        if($movefile) {
          $success = $wpdb->insert($grouptable, array(
            "user_id" => $user_id,
            "group_name" => $group_choir_name,
            "group_desc" => $group_choir_desc,
            "group_city" => $group_choir_city,
            "group_state" => $group_choir_state,
            "group_country" => $group_choir_country,
            "group_img" => $Newfilename,
            "created_by" => $user_id,
            "created_at" => date('yyyy-mm-dd h:m:s'),
          ));

          if($success) {
                $group_id = $wpdb->insert_id;
                $successsds = $wpdb->insert('wp_terms', array(
                    "name" => $group_choir_name,
                    "slug" => $group_choir_name,
                    "term_group" => '0',
                    "group_id" =>$group_id
                ));
                if($successsds){
                $term_id = $wpdb->insert_id;
                  $successsds = $wpdb->insert('wp_term_taxonomy', array(
                      "term_id" => $term_id,
                      "taxonomy" => 'audio_mixer_user_group_taxonomy',
                      "description" => '',
                      "parent" =>'0',
                      "count"=>'0'
                  ));
                }
                $subject = 'Message from siing.io';
                  $html='';
                  $html = ' <html>
                  <head>
                  <meta charset="utf-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1">
                      <title>Mail template</title>
                  </head>
                  <body>
                  <div class="wrapper" style="width:600px;background-color:#fff;margin:auto;height: auto;">
                  <div class="contentarea" style="position:relative;border: 1px solid #CCCC99;border-radius: 2px; padding: 10px;">
                   
                   <span>Thank For Create Choir Group. </span><br/>
                   <span>Your Group Name: '.$group_choir_name.'</span><br/><br/> 
                   Thanks and Regards,<br/>
                   Lumiiq    
                  </div>
                 <div class="emtmfooter" style="height: 40px; margin-bottom: -12px;  padding: 6px; background-color:#16222e;" ><p style="text-align:center; color:#CCCCCC; font-size:11px;">Copyright © 2019 siing.io - All Rights Reserved</p></div>
                  </div>   
                  </div>
                  </body>
                 </html>';
                $header = 'From: dev1.bdpl@gmail.com' . "\r\n" .
                'Reply-To: '.$admin_email.''. "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                $header .= 'MIME-Version: 1.0' . "\r\n";
                $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $to = $currentUser_email ;
                $headers = "From: ".$admin_email."" . "\r\n" .
                "CC: ".$currentUser_email."";
                //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $mail = mail($to,$subject,$html,$header);
            echo $error =  '<div class="alert alert-success">Your Group Choir added successfully...!!</div>';
          } else {
            echo $error =  '<div class="alert alert-danger">Sorry fail to add your Group Choir due to system error. Please try again.</div>';
          }
        } else {
            echo $error =  '<div class="alert alert-danger">Sorry fail to add your Group Choir due to system error. Please try again.</div>';
        }
    } else {
        echo $error;
    }
  } 
}else {
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    global $wpdb;
    $grouptable = 'groups';
    $user_id = $_POST['user_id'];
    $group_choir_name = $_POST['group_choir_name'];
    $group_choir_desc = $_POST['group_choir_desc'];
    $group_choir_city = $_POST['group_choir_city'];
    $group_choir_state = $_POST['group_choir_state'];
    $group_choir_country = $_POST['group_choir_country'];

    $success = $wpdb->insert($grouptable, array(
        "user_id" => $user_id,
        "group_name" => $group_choir_name,
        "group_desc" => $group_choir_desc,
        "group_city" => $group_choir_city,
        "group_state" => $group_choir_state,
        "group_country" => $group_choir_country,
        "created_by" => $user_id,
        "created_at" => date('yyyy-mm-dd h:m:s'),
    ));
    if($success) {
        $group_id = $wpdb->insert_id;
        $successsds = $wpdb->insert('wp_terms', array(
            "name" => $group_choir_name,
            "slug" => $group_choir_name,
            "term_group" => '0',
            "group_id" =>$group_id
        ));
        if($successsds){
          $term_id = $wpdb->insert_id;
          $successsds = $wpdb->insert('wp_term_taxonomy', array(
              "term_id" => $term_id,
              "taxonomy" => 'audio_mixer_user_group_taxonomy',
              "description" => '',
              "parent" =>'0',
              "count"=>'0'
          ));
        }
        $subject = 'Message from siing.io';
                  $html='';
                  $html = ' <html>
                  <head>
                  <meta charset="utf-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1">
                      <title>Mail template</title>
                  </head>
                  <body>
                  <div class="wrapper" style="width:600px;background-color:#fff;margin:auto;height: auto;">
                  <div class="contentarea" style="position:relative;border: 1px solid #CCCC99;border-radius: 2px; padding: 10px;">
                   
                   <span>Thank For Create Choir Group. </span><br/>
                   <span>Your Group Name: '.$group_choir_name.'</span><br/><br/> 
                   Thanks and Regards,<br/>
                   Lumiiq    
                  </div>
                 <div class="emtmfooter" style="height: 40px; margin-bottom: -12px;  padding: 6px; background-color:#16222e;" ><p style="text-align:center; color:#CCCCCC; font-size:11px;">Copyright © 2019 siing.io - All Rights Reserved</p></div>
                  </div>   
                  </div>
                  </body>
                 </html>';
                $header = 'From: dev1.bdpl@gmail.com' . "\r\n" .
                'Reply-To: '.$admin_email.''. "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                $header .= 'MIME-Version: 1.0' . "\r\n";
                $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $to = $currentUser_email ;
                $headers = "From: ".$admin_email."" . "\r\n" .
                "CC: ".$currentUser_email."";
                //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $mail = mail($to,$subject,$html,$header);
      echo $error =  '<div class="alert alert-success">Your Group Choir added successfully...!!</div>';
    } else {
      echo $error =  '<div class="alert alert-danger">Sorry fail to add your Group Choir due to system error. Please try again.</div>';
    }
  }
}
?>