<?php 
require_once('../../../wp-load.php');
//function add_group_choir(){
if($_FILES["photo"]["name"]!=''){
  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["photo"]["type"])){
  //if($_POST['submit']) {
    global $wpdb;
    $grouptable = 'groups';
    $user_id = $_POST['user_id'];
    $group_id = $_POST['group_id'];
    $group_choir_name = $_POST['group_choir_name'];
    $group_choir_desc = $_POST['group_choir_desc'];
    $group_choir_city = $_POST['group_choir_city'];
    $group_choir_state = $_POST['group_choir_state'];
    $group_choir_country = $_POST['group_choir_country'];
    $group_choir_image = $_FILES["photo"]["tmp_name"];

    $allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
    $temp = explode(".", $_FILES["photo"]["name"]);
    $extension = end($temp);

    if ($_FILES["photo"]["error"]) {
        $error .= '<div class="alert alert-danger">Error opening the file<br /></div>';
    }
    if ($_FILES["photo"]["type"] != "image/jpeg" &&
        $_FILES["photo"]["type"] != "image/JPEG" &&
        $_FILES["photo"]["type"] != "image/jpg" &&
        $_FILES["photo"]["type"] != "image/JPG" &&
        $_FILES["photo"]["type"] != "image/PNG" &&
        $_FILES["photo"]["type"] != "image/png") { 
        $error .= "Mime type not allowed<br />";
    }
    if (!in_array($extension, $allowedExts)) {
        $error .= '<div class="alert alert-danger">Extension not allowed<br /></div>';
    }
    if ($_FILES["photo"]["size"] > 3145728) {
        $error .= '<div class="alert alert-danger">File size shoud be less than 2 Mb<br /></div>';
    }
    if ($error == "") {
      $path_array = wp_upload_dir();
      $sourcePath = $_FILES['photo']['tmp_name'];
      $Newfilename = uniqid().'-'.$_FILES['photo']['name'];
      $targetPath = $path_array["basedir"]."/groupchoirimage/".$Newfilename;
      $movefile =  move_uploaded_file($sourcePath, $targetPath);

        /*$path_array = wp_upload_dir();
        $sourcePath = $_FILES['photo']['tmp_name'];
        $Newfilename = uniqid().'-'.$_FILES['photo']['name'];
        $targetPath = $path_array["basedir"]."/groupchoirimage".$file_name;
        $movefile =  move_uploaded_file($sourcePath,$targetPath);*/
        
        if($movefile) {
          $where_array = array( 'id' => stripslashes( $_POST[ 'group_id' ] ));
          $success = $wpdb->update($grouptable, array(
            "group_name" => $group_choir_name,
            "group_desc" => $group_choir_desc,
            "group_city" => $group_choir_city,
            "group_state" => $group_choir_state,
            "group_country" => $group_choir_country,
            "group_img" => $Newfilename,
            "updated_by" => $user_id,
            "updated_at" => date('yyyy-mm-dd h:m:s'),
          ), $where_array);

          if($success) {
            echo $error =  '<div class="alert alert-success">Your product updated successfully...!!</div>';
          } else {
            echo $error =  '<div class="alert alert-danger">Sorry fail to add your product due to system error. Please try again.</div>';
          }
        } else {
            echo $error =  '<div class="alert alert-danger">Sorry fail to add your product due to system error. Please try again.</div>';
        }
    } else {
        echo $error;
    }
  } 
}else {
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    global $wpdb;
    $grouptable = 'groups';
    $user_id = $_POST['user_id'];
    $group_id = $_POST['group_id'];
    $group_choir_name = $_POST['group_choir_name'];
    $group_choir_desc = $_POST['group_choir_desc'];
    $group_choir_city = $_POST['group_choir_city'];
    $group_choir_state = $_POST['group_choir_state'];
    $group_choir_country = $_POST['group_choir_country'];
    $where_array = array( 'id' => stripslashes( $_POST[ 'group_id' ] ));
    $success = $wpdb->update($grouptable, array(
        "user_id" => $user_id,
        "group_name" => $group_choir_name,
        "group_desc" => $group_choir_desc,
        "group_city" => $group_choir_city,
        "group_state" => $group_choir_state,
        "group_country" => $group_choir_country,
        "updated_by" => $user_id,
        "updated_at" => date('yyyy-mm-dd h:m:s'),
    ), $where_array);
    if($success) {
      echo $error =  '<div class="alert alert-success">Your product updated successfully...!!</div>';
    } else {
      echo $error =  '<div class="alert alert-danger">Sorry fail to add your product due to system error. Please try again.</div>';
    }
  }
}
?>