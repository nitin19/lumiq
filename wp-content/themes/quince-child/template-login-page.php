<?php
/*Template Name: Login Page
*/
get_header();
?>
<?php $varable_holding_img_url = site_url().'/wp-content/uploads/2019/07/06_choir_signup.jpg'; ?>
  <div class="choir_backimg" style="background-image: url(<?php echo $varable_holding_img_url; ?>);">
      <div class="container">
        <div class="choir_main">
            <div class="row">
                <div class="col-lg-6">
                  <h6>Your Personal</h6>
                  <h1>Soundtrack Everywhere</h1>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="col-lg-6">
                  <div class="sign_up">
                      <h2>Login</h2>
                      <div class="wp_login_error">
    <?php if( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) { ?>
        <p>The password you entered is incorrect, Please try again.</p>
    <?php } 
    else if( isset( $_GET['login'] ) && $_GET['login'] == 'empty' ) { ?>
        <p>Please enter both username and password.</p>
    <?php } ?>
</div>  
                       <?php 
                          global $user_login;
                          if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
                                <div class="aa_error">
                                  <p><?php _e( 'FAILED: Try again!', 'AA' ); ?></p>
                                </div>
                          <?php 
                          endif;
                          ?>
                          <?php 
                          $args = array(
                              'echo'           => true,
                              'redirect'       => home_url( '/wp-admin/' ), 
                              'form_id'        => 'loginform',
                              'label_username' => __( 'Email' ),
                              'label_password' => __( 'Password' ),
                              'label_remember' => __( 'Remember Me' ),
                              'label_log_in'   => __( 'Log In' ),
                              'id_username'    => 'user_email',
                              'id_password'    => 'user_pass',
                              'id_remember'    => 'rememberme',
                              'id_submit'      => 'wp-submit',
                              'class_submit'   => 'btn btn-default',
                              'remember'       => true,
                              'value_username' => NULL,
                              'value_remember' => true
                          ); 
                          wp_login_form( $args );
                       ?>
                       <a class="forgot-password" href="<?php echo site_url(); ?>/forgot-password/">
                          <?php _e( 'Forgot your password?', 'personalize-login' ); ?>
                       </a> 
                  </div>
                </div>
            </div>
        </div>  
      </div>
  </div>
<style type="text/css">
  html{
    margin-top: 0px !important;
  }
  a.forgot-password {
    color: #f6b12c;
  }
</style>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){
      jQuery.noConflict();
      jQuery.validator.addMethod("alphaUname", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
       });
      jQuery.validator.addMethod("pwcheck", function(value) {
       return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
       });
      jQuery("#loginform").validate({
          rules: {  
            log: 
            {
              required: true,
              email: true
            },           
            pwd: 
            {
              required: true,             
            },    
          }, 
          messages: {       
            log: 
            {
              required: "Please Enter Email",
              email: "Please Enter Valid Email",
            },
            pwd: 
            {
              required: "Please Enter Password"
            },                                 
          },         
      });
    });
</script>