<?php 
require_once('../../../wp-load.php');
$current_user = wp_get_current_user(); 
$currentUser_email = $current_user->data->user_email;
$admin_email = get_option( 'admin_email' );
$logo=site_url().'/wp-content/uploads/2019/07/logo-1.png';
$error='';
//function add_group_choir(){
$items_array = array();
$insertValuesSQL = '';
//if($_FILES["photo"]["name"]!='') ;
if($_SERVER['REQUEST_METHOD'] == "POST"){
  foreach($_FILES['photo']['name'] as $key=>$val){
    $image_name = $_FILES['photo']['name'][$key];
    $tmp_name   = $_FILES['photo']['tmp_name'][$key];
    $size       = $_FILES['photo']['size'][$key];
    $type       = $_FILES['photo']['type'][$key];
    $error      = $_FILES['photo']['error'][$key];
    if($image_name!=''){
    if(isset($type)){
      //if($_POST['submit']) {
        global $wpdb;
        $grouptable = 'events';
        $user_id = $_POST['user_id'];
        $events_name = $_POST['events_name'];
        $events_desc = stripslashes($_POST['events_desc']);
        $event_start_date = $_POST['event_start_date'];
        $event_end_date = $_POST['event_end_date'];
        $event_start_time = $_POST['event_start_time'];
        $event_end_time = $_POST['event_end_time'];
        $selectchoir = $_POST['selectchoir'];
        $events_city = $_POST['events_city'];
        if(!empty($_POST['events_state'])){
        $events_state = $_POST['events_state'];
        } else {
            $events_state ='';
        }

        $events_country = $_POST['events_country'];
        $group_choir_image = $_FILES["photo"]["tmp_name"];
        $allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
        $temp = explode(".", $image_name);
        $extension = end($temp);
        if ($error) {
            $error .= '<div class="alert alert-danger">Error opening the file<br /></div>';
        }
        if ($type != "image/jpeg" &&
          $type != "image/JPEG" &&
          $type != "image/jpg" &&
          $type != "image/JPG" &&
          $type != "image/PNG" &&
          $type != "image/png") { 
          $error .= "Mime type not allowed<br />";
        }
        if (!in_array($extension, $allowedExts)) {
            $error .= '<div class="alert alert-danger">Extension not allowed<br /></div>';
        }
        if ($size > 3145728) {
            $error .= '<div class="alert alert-danger">File size shoud be less than 2 Mb<br /></div>';
        }
        if ($error == "") {
          $path_array = wp_upload_dir();
          $sourcePath = $tmp_name;
          $filename = uniqid().'-'.$image_name;
          $items_array[] = $filename; 

          $items= implode(',', $items_array);
          $targetPath = $path_array["basedir"]."/eventsimage/".$filename;
          $movefile =  move_uploaded_file($sourcePath, $targetPath);

          if($movefile){
                $insertValuesSQL .= $filename;
          } else {
                $errorUpload .= $image_name.', ';
          }
        } else {
            echo $error;
        }
      }
    }
  }
    if(!empty($insertValuesSQL)){
      $success = $wpdb->insert($grouptable, array( 
        "user_id" => $user_id,
        "group_id"=> $selectchoir,
        "singer_id" =>'',
        "events_name" => $events_name,
        "events_dec" => $events_desc,
        "events_startdate" => $event_start_date ,
        "events_enddate" => $event_end_date,
        "event_start_time" => $event_start_time,
        "event_end_time" => $event_end_time,
        "events_city" => $events_city,
        "events_state" => $events_state,
        "events_country" => $events_country,
        "join_users" => '',
        "created_by" => $user_id,
        "image" => $items,
        
      ));
      if($success) {
      $event_id = $wpdb->insert_id;
      $user_id = $_POST['user_id'];
      $notificationtable='notification';
      $groupdata = $wpdb->get_row("SELECT * FROM `events` WHERE `id`= '$event_id' AND status= '1' AND deleted= '0' ");
      $noti_group_id=$groupdata->group_id;
      $noti_events_name=$groupdata->events_name;
      $notificationsuccess = $wpdb->insert($notificationtable, array( 
          "choirmaster_id" =>$user_id,
          "singer_id" => '',
          "group_id" =>$noti_group_id,
          "events_id" =>$event_id,
          "album_id" =>'',
          "singer_message" => $noti_events_name .' '. 'event is created',
          "is_readby_choirmaster" => '1',
          "created_by" => $user_id,
          "updated_by" => $user_id,
      ));
        $subject = 'Message from siing.io';
              $html='';
              $html = ' <html>
              <head>
              <meta charset="utf-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1">
                  <title>Mail template</title>
              </head>
              <body>
              <div class="wrapper" style="width:600px;background-color:#fff;margin:auto;height: auto;">
              <div class="contentarea" style="position:relative;border: 1px solid #CCCC99;border-radius: 2px; padding: 10px;">
               
               <span>Thank For Create Event. </span><br/>
               <span>Your Event Name: '.$events_name.'</span><br/><br/> 
               Thanks and Regards,<br/>
               Lumiiq    
              </div>
             <div class="emtmfooter" style="height: 40px; margin-bottom: -12px;  padding: 6px; background-color:#16222e;" ><p style="text-align:center; color:#CCCCCC; font-size:11px;">Copyright © 2019 siing.io - All Rights Reserved</p></div>
              </div>   
              </div>
              </body>
             </html>';
            $header = 'From: dev1.bdpl@gmail.com' . "\r\n" .
            'Reply-To: '.$admin_email.''. "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            $header .= 'MIME-Version: 1.0' . "\r\n";
            $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $to = $currentUser_email;
            $headers = "From: ".$admin_email."" . "\r\n" .
            "CC: ".$currentUser_email."";
            //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $mail = mail($to,$subject,$html,$header);
        echo $error =  '<div class="alert alert-success">Your Events data Added successfully...!!</div>';
      } else {

        echo $error =  '<div class="alert alert-danger">Sorry fail to Update your Events date  due to system error. Please try again. </div>';
      }
    } else {
        global $wpdb;
        $grouptable = 'events';
        $user_id = $_POST['user_id'];
        $events_name = $_POST['events_name'];
        $events_desc = stripslashes($_POST['events_desc']);
        $event_start_date = $_POST['event_start_date'];
        $event_end_date = $_POST['event_end_date'];
        $event_start_time = $_POST['event_start_time'];
        $event_end_time = $_POST['event_end_time'];
        $selectchoir = $_POST['selectchoir'];
        $events_city = $_POST['events_city'];
        $events_state = $_POST['events_state'];
        $events_country = $_POST['events_country'];
        $success = $wpdb->insert($grouptable, array(

        "user_id" => $user_id,
        "group_id"=> $selectchoir,
        "singer_id" =>'',
        "events_name" => $events_name,
        "events_dec" => $events_desc,
        "events_startdate" => $event_start_date ,
        "events_enddate" => $event_end_date,
        "event_start_time" => $event_start_time,
        "event_end_time" => $event_end_time,
        "events_city" => $events_city,
        "events_state" => $events_state,
        "events_country" => $events_country,
        "join_users" => '',
        "created_by" => $user_id,
        "image" => $filename,
      ));
      if($success) {
         $event_id = $wpdb->insert_id;
          $user_id = $_POST['user_id'];
          $notificationtable='notification';
          $groupdata = $wpdb->get_row("SELECT * FROM `events` WHERE `id`= '$event_id' AND status= '1' AND deleted= '0' ");
          $noti_group_id=$groupdata->group_id;
          $noti_events_name=$groupdata->events_name;
          $notificationsuccess = $wpdb->insert($notificationtable, array( 
                "choirmaster_id" =>$user_id,
                "singer_id" => '',
                "group_id" =>$noti_group_id,
                "events_id" =>$event_id,
                "album_id" =>'',
                "singer_message" => $noti_events_name . 'event is created',
                "created_by" => $user_id,
                "updated_by" => $user_id,
            ));
          $subject = 'Message from siing.io';
                    $html='';
                    $html = ' <html>
                    <head>
                    <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Mail template</title>
                    </head>
                    <body>
                    <div class="wrapper" style="width:600px;background-color:#fff;margin:auto;height: auto;">
                    <div class="contentarea" style="position:relative;border: 1px solid #CCCC99;border-radius: 2px; padding: 10px;">
                     
                     <span>Thank For Create Event. </span><br/>
                     <span>Your Group Name: '.$events_name.'</span><br/><br/> 
                     Thanks and Regards,<br/>
                     Lumiiq    
                    </div>
                   <div class="emtmfooter" style="height: 40px; margin-bottom: -12px;  padding: 6px; background-color:#16222e;" ><p style="text-align:center; color:#CCCCCC; font-size:11px;">Copyright © 2019 siing.io - All Rights Reserved</p></div>
                    </div>   
                    </div>
                    </body>
                   </html>';
                  $header = 'From: dev1.bdpl@gmail.com' . "\r\n" .
                  'Reply-To: '.$admin_email.''. "\r\n" .
                  'X-Mailer: PHP/' . phpversion();
                  $header .= 'MIME-Version: 1.0' . "\r\n";
                  $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                  $to = $currentUser_email;
                  $headers = "From: ".$admin_email."" . "\r\n" .
                  "CC: ".$currentUser_email."";
                  //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                  $mail = mail($to,$subject,$html,$header);
        echo $error =  '<div class="alert alert-success">Your Events data Added successfully...!!</div>';
      } else {
        echo $error =  '<div class="alert alert-danger">Sorry fail to Update your Events date due to system error. Please try again.</div>';
      }
    }
}
?>