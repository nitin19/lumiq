<!DOCTYPE html>
<!--[if lt IE 7]><html class="ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]><html class="ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->


<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
	<!-- <script src="<?php //echo get_template_directory_uri(); ?>-child/js/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/eskju.jquery.lazyloading.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
	<?php if (ot_get_option('favicon')){
		echo '<link rel="shortcut icon" href="'. esc_url(ot_get_option('favicon')) .'" />';
	} 

	if (ot_get_option('ipad_favicon_retina')){
		echo '<link rel="apple-touch-icon" sizes="152x152" href="'. esc_url(ot_get_option('ipad_favicon_retina')) .'" >';
	} 

	if (ot_get_option('iphone_favicon_retina')){
		echo '<link rel="apple-touch-icon" sizes="120x120" href="'. esc_url(ot_get_option('iphone_favicon_retina')) .'" >';
	}

	if (ot_get_option('ipad_favicon')){
		echo '<link rel="apple-touch-icon" sizes="76x76" href="'. esc_url(ot_get_option('ipad_favicon')) .'" >';
	} 

	if (ot_get_option('iphone_favicon')){
		echo '<link rel="apple-touch-icon" href="'. esc_url(ot_get_option('iphone_favicon')) .'" >';
	} ?>
		
	<?php echo ot_get_option('tracking_code'); ?>
	<?php wp_head(); ?>
<script>
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
function notimyFunction() {
  document.getElementById("notimyDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.notilogin_link')) {
    var dropdowns = document.getElementsByClassName("noti");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
function notimyFunction() {
  document.getElementById("eventnotimyDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.notilogin_link')) {
    var dropdowns = document.getElementsByClassName("event_join_noti");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper">
		
		<?php 
			$header_class = '';
			
			// Check if full width header selected
			if ( ot_get_option('layout_style') != 'boxed' && ot_get_option('header_style') == 'full-width' ){ 
				$header_class .= 'full-width';
			}
			
			// Check if overlay header selected
			if ( is_page() && ot_get_option('overlay_header') != 'off' && ( in_array( get_the_ID(), ot_get_option('overlay_header_pages', array()) ) ) ){ 
				$header_class .= ' overlay-header';
			}
		?>
		
		<?php if( ot_get_option('top_bar', 'off') != 'off' ) {
			get_sidebar('top');
		} ?>
		<?php if( is_page( array( 'Login', 'Choir Register', 'Singer Register', 'Forgot Password', 'Reset Password') ) ) { ?>
		<header id="site-header" class="<?php echo trim($header_class); ?> login_register_header" role="banner">
			<div id="header-wrapper">
				<div id="header-container" class="clearfix">
					<div id="site-logo" class="logo_img">
						<?php get_template_part( 'logo' ); // Include logo.php ?>
					</div>
					<nav class="navbar navbar-expand-lg navbar-light">
            			<a class="navbar-brand" href="<?php echo site_url();?>"><img src="<?php echo site_url();?>/wp-content/uploads/2019/07/logo-1.png"></a>
            			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              				<span class="navbar-toggler-icon"></span>
            			</button>
            			<div class="collapse navbar-collapse" id="navbarSupportedContent">
              				<ul class="navbar-nav mr-auto">  
              				</ul>
			              	<form class="form-inline my-2 my-lg-0">
			                  	<ul class="right-nav">
			                  		<?php if(is_page( array( 'Choir Register', 'Singer Register'))){ ?>
							        <li><a href="<?php echo site_url();?>/login">Login</a><a href="<?php echo site_url();?>/?page_id=2307">Already Have an Account</a></li>
							        <?php } else { ?>
			                    	<li><a href="<?php echo site_url();?>/choir-register">SignUp</a></li>
			                    	<?php } ?>
			                  	</ul>
			              	</form>
            			</div>
        			</nav>			
					<a href="#mobile-site-navigation" class="toggle-mobile-menu"><i class="fa fa-bars"></i></a>
				</div><!-- #header-container -->	
			</div><!-- #header-wrapper -->	
		</header><!-- #site-header -->	
		<?php } else { ?>
			<header id="site-header" class="<?php echo trim($header_class); ?>" role="banner">
			<div id="header-wrapper">
				<div id="header-container" class="clearfix">
					<div id="site-logo" class="logo_img_main">
						<?php get_template_part( 'logo' ); // Include logo.php ?>
					</div>
					
					<nav id="site-navigation" role="navigation">
						<?php 
						    $current_user = get_current_user_id();
						    $user_meta=get_userdata($current_user);
				        	$user_roles= $user_meta->roles[0];
				        ?>
                        <?php if(!is_user_logged_in()){ ?>
                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'menu-container', 'fallback_cb' => 'mnky_no_menu') ); ?>
							<li class="login_btn"><a class="login_link" href="/login">Login</a></li>
						<?php } elseif($user_roles == 'choirmaster'){ ?>
						    <?php wp_nav_menu( array( 'theme_location' => 'Choirmaster', 'container_class' => 'menu-container', 'fallback_cb' => 'mnky_no_menu') ); ?>
						<?php } elseif($user_roles == 'singer'){ ?>
							<?php wp_nav_menu( array( 'theme_location' => 'Singer', 'container_class' => 'menu-container', 'fallback_cb' => 'mnky_no_menu') ); ?>
						<?php } ?>
                                                
						<?php if(is_user_logged_in()){ 
						$current_user = wp_get_current_user();
						$img_name = get_user_meta($current_user->id, image_url, true);
						?>
						<div class="dropdown">	
								<i onclick="notimyFunction()" class="fa fa-bell notilogin_link" aria-hidden="true"></i>
								<?php
									$user_meta=get_userdata($current_user->id);
				        			$user_roles= $user_meta->roles[0];
				        			if($user_roles == 'singer') {
								      $group_data =get_user_meta($current_user->id,'selected_choir','true');
								      $noti_data = $wpdb->get_results("SELECT * FROM `notification` WHERE `group_id`= '$group_data' AND is_readby_singer= '0'  AND status= '1' AND deleted= '0' ");
					       			  $count_noti_data = count($noti_data);
						       		  if($count_noti_data > 0){ ?>
						       		    <span class="notification_count"><?php echo $count_noti_data;?></span>
								       	<div id="notimyDropdown" class="dropdown-content noti">
								       	   
					       				  	<?php foreach ($noti_data as $notificationvalue) { ?>
					       					    <li><span id="event_id<?php echo $notificationvalue->id;?>" class="read_id">
					       					    	<p id="<?php echo $notificationvalue->events_id;?>" class="<?php echo $notificationvalue->id;?>"><?php echo $notificationvalue->singer_message;?> </p></span></li>
					        			    <?php } ?>
					        			</div>
						           	  <?php }
					     	        }  elseif($user_roles == 'choirmaster') {
								      $noti_data_choir = $wpdb->get_results("SELECT * FROM `notification` WHERE `choirmaster_id`= '$current_user->id' AND is_readby_choirmaster= '0'  AND status= '1' AND deleted= '0' ");

								    $count_noti_choir_data = count($noti_data_choir);
						       		  if($count_noti_choir_data > 0){ ?>
						       		    <span class="notification_count"><?php echo $count_noti_choir_data;?></span>
								       	<div id="notimyDropdown" class="dropdown-content noti">
								       	   
					       				  	<?php foreach ($noti_data_choir as $choirvalue) {

					       				  		 ?>
					       					    <li><span id="noti_id<?php echo $choirvalue->id;?>" class="choirread_id">
					       					    	<p id="<?php echo $choirvalue->singer_id;?>" class="<?php echo $choirvalue->id;?>"><?php echo $choirvalue->choir_message;?> </p></span></li>
					        			    <?php } ?>
					        			</div>
						           	  <?php }

						           	 $event_noti_data = $wpdb->get_results("SELECT * FROM `notification` WHERE `choirmaster_id`= '$current_user->id' AND is_readby_choirmaster = '0'  AND status= '1' AND deleted= '0' ");
						            $singer_id = $event_noti_data[0]->singer_id;
						           	 $image_url =get_user_meta($singer_id,'image_url', 'true');
						           	 $count_noti_event_data = count($event_noti_data);
						       		  if($count_noti_event_data > 0){ ?>
						       		    <span class="notification_count"><?php echo $count_noti_event_data;?></span>
								       	<div id="eventnotimyDropdown" class="dropdown-content event_join_noti">
								       	   
					       				  	<?php foreach ($event_noti_data as $eventvalue) {
					       				  		 ?>
					       					    <li><span id="noti_id<?php echo $eventvalue->id;?>" class="choirread_id">
					       					    	<p id="<?php echo $eventvalue->singer_id;?>" class="<?php echo $eventvalue->id;?>"><?php 
					       					    		  if($image_url != ''){ ?> 
											              <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $image_url; ?>" alt="profile_img" width="100%">
											          <?php } else {
											              $profile_one= site_url().'/wp-content/uploads/2019/07/male_new.png';?>
											              <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
											          <?php } 
					       					    	echo $eventvalue->choir_message;?> </p></span></li>
					        			    <?php } ?>
					        			</div>
						           	  <?php }
								              }
								   
					       			 
					     	    ?>
						</div>
							<div class="dropdown">
								
							<?php if($img_name) { ?>
								<img onclick="myFunction()" class="dropbtn" src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $img_name; ?>">
							<?php } else { ?>
							  <img onclick="myFunction()" class="dropbtn" src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png">
							<?php } ?>
							  <span class="user_name"><?php printf(esc_html( $current_user->display_name ) ); ?></span>
							  <div id="myDropdown" class="dropdown-content">
							  	<?php 
							  	$user_meta=get_userdata($current_user->id);
         						$user_roles= $user_meta->roles[0];
         						if($user_roles == 'choirmaster'){ ?>
							    <a href="<?php echo site_url();?>/choir-profile"> <i class="fa fa-user" aria-hidden="true"></i> My Profile</a>
							    <?php } else if($user_roles == 'singer') { ?>
							    <a href="<?php echo site_url();?>/singer-profile"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a>
							    <?php } else { ?>
							    <a href="<?php echo admin_url();?>">My Profile</a>
							    <?php } ?>
							    <!--<a href="<?php echo wp_logout_url( home_url() );?>" id="wp-submit">Logout</a>-->
							    	<button type="button" name="logout" value="<?php echo $current_user->id; ?>" id="logout-btn"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</button>
							  </div>
							</div>
					    <?php } ?>
						<?php if( class_exists( 'WooCommerce' ) && ot_get_option('cart_button') != 'off' ) : ?>
							<div class="header_cart_wrapper">
								<?php global $woocommerce; ?>
								<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'quince' ); ?>" class="header_cart_link" >
									<?php woocommerce_cart_button(); ?>
								</a>	
								<?php if( ot_get_option('cart_widget') != 'off' ) {
									woocommerce_cart_widget();
								} ?>
							</div>
						<?php endif; ?>
						
						<?php if( ot_get_option('search_button') != 'off' ) : ?>
							<button id="trigger-header-search" class="search_button" type="button">
								<i class="fa fa-search"></i>
							</button>
						<?php endif; ?>					
					</nav><!-- #site-navigation -->
					
					<?php if( ot_get_option('search_button') != 'off' ) : ?>
						<div class="header-search">
							<?php get_search_form(); ?>
						</div>
					<?php endif; ?>
								
					<a href="#mobile-site-navigation" class="toggle-mobile-menu"><i class="fa fa-bars"></i></a>
				</div><!-- #header-container -->	
			</div><!-- #header-wrapper -->	
		</header><!-- #site-header -->	
		<?php } ?>
		<style>
			#logout-btn {
			    width: 100%;
			    height: auto;
			    background-color: transparent;
			    text-align: left;
			    padding-left: 18px;
			    color: #000;
			    font-weight: 400;
			    text-transform: capitalize;
			    font-size: 14px;
			    padding-top: 8px;
				padding-bottom: 9px;
			}
			#logout-btn:hover {
			    background-color: #f8f6f6;
			    color: #ed2939;
			}
			.header-sticked nav#site-navigation .dropdown {
			    margin-top: -18px;
			}
			.header-sticked nav#site-navigation .login_btn {
			    top: 16px;
			}
		</style>
		<script type="text/javascript">
			$('document').ready(function(){
				$('#logout-btn').on('click',function(){
					var logoutId = $('#logout-btn').val();
					$.ajax({
						type: "POST",
						url: "<?php echo get_template_directory_uri(); ?>-child/logout_ajax.php",
						data: {
							logoutid:logoutId
						},
						cache: false,
						success: function(data){
							if(data == "True")
							{
								//alert("<?php echo wp_logout_url( home_url() );?>");
								window.location.href = "<?php echo wp_logout_url( home_url() );?>";
							}
							else
							{
								window.location.href = "<?php echo wp_logout_url( home_url() );?>";
							}
						}
					});
				});
			});

			$(function() {
			    var header = $('#feature_menu');
			    var menu = $('#feature_menu');
			    $(window).scroll(function() {
			        var scroll = $(window).scrollTop();

			        if (scroll >= 50) {
			            header.addClass('stick');
			            menu.addClass('stick');
			        } else {
			            header.removeClass('stick');
			            menu.removeClass('stick');
			        } 
			    });
			}); 

			
		</script>
		<script type="text/javascript">
    jQuery(document).ready(function(){
  jQuery('.read_id').click(function(){
  var read_id=jQuery(this).find("p").attr('id');
  var read_class=jQuery(this).find("p").attr('class');
  jQuery.ajax({
                type : 'GET',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_read_singer_msg.php',
                data : {read_id: read_class},
                cache: false, 
            success :  function(data) {
              //alert(data);
             //jQuery(".upload-gimg-msg").html(data);
            // setTimeout(3000);
             //function popupPimg() {
                //alert("<?php echo site_url();?>/events/?events_id="+read_id);
               window.location.assign("<?php echo site_url();?>/events/?events_id="+read_id);
              //}
            },
          
          });
});
});
</script>
		<script type="text/javascript">
    jQuery(document).ready(function(){
  jQuery('.choirread_id').click(function(){
  var read_id=jQuery(this).find("p").attr('id');
  var read_class=jQuery(this).find("p").attr('class');
  jQuery.ajax({
                type : 'GET',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_read_choirmast_msg.php',
                data : {read_id: read_class},
                cache: false, 
            success :  function(data) {
              //alert(data);
             //jQuery(".upload-gimg-msg").html(data);
            // setTimeout(3000);
             //function popupPimg() {
                //alert("<?php echo site_url();?>/events/?events_id="+read_id);
               window.location.assign("<?php echo site_url();?>/singer-profile/?singer_id="+read_id);
              //}
            },
          
          });
});
});
</script>
		<?php get_template_part( 'title' ); // Include title.php ?>
		
		<div id="main" class="clearfix">