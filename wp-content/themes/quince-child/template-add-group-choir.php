<?php

/*Template Name: Add Group Choir
*/
get_header();
?>
<?php 
$current_user = wp_get_current_user();
if(is_user_logged_in()){
?>

<div class="add_choir">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="add_choir_hd">
          <h1>Add Choir</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
      </div>
    </div>
    <div class="row">
     <div class="col-sm-12">
      <div id="response" class="response-message"></div>
      <form action="" name="add_group_choir" id="add_group_choir" method="post" enctype="multipart/form-data">
        <div class="image_uploader_sec">
          <div class="image_uploader_inersec">
            <div class="row">
              <div class="col-sm-5">
                <div id="image_preview"><img id="previewing" src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" /></div>
                <!-- <form action="/file-upload" class="dropzone" id="myId"> -->
                 <!--  <div class="fallback">
                    <input name="file" type="file"  multiple />
                  </div> -->
                <!-- </form> -->
              </div>  
              <div class="col-sm-7">
                <h2>Upload Choir Image</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <!-- <input id="file" type="file" name="file" value="" /> -->
                <input type="file" name="photo" id="photo" value="" class="ss-field-width" />
                <!-- <span title="attach file" class="attachFileSpan" onclick="document.getElementById('myInput').click()">
                Attach file
                </span> -->
                <p class="img_size">(jpeg, png, less than 2 MB)</p>
              </div>
            </div>
            <div class="row mb-4 mt-4">
              <div class="col-sm-12">
                  <input class="red_btn" type="hidden" name="user_id" value="<?php echo $current_user->id;?>" />
                  <div class="form-group">
                    <label> Name <span class="required_icon">*</span> </label>
                    <input type="text" name="group_choir_name" id="group_choir_name" value="" placeholder="Choir Name" class="custom_input">
                  </div>
                  <div class="form-group">
                    <label> Description <!--<span class="required_icon">*</span>--> </label>
                    <textarea name="group_choir_desc" id="group_choir_desc" value="" class="custom_input" placeholder=""></textarea>
                  </div>
                  <div class="form-group">
                    <label> City <span class="required_icon">*</span> </label>
                    <input type="text" name="group_choir_city" id="group_choir_city" value="" placeholder="Enter your city here" class="custom_input">
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> State </label>
                        <input type="text" name="group_choir_state" id="group_choir_state" value="" placeholder="Enter State" class="custom_input">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Country <span class="required_icon">*</span> </label>  
                        <input type="text" name="group_choir_country" id="group_choir_country" value="" placeholder="Enter Country Name" class="custom_input">
                      </div>
                    </div>
                  </div>
                  <div class="upload-gimg-msg"></div>

                  <input class="red_btn" type="submit" name="submit" value="Submit" id="post_submit" /><div id="loading"><img src="<?php echo site_url();?>/wp-content/uploads/2019/08/loading_new_cart_img.gif"></div>
                  <!-- <button type="submit" class="red_btn">Submit</button> -->
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>  
<script type="text/javascript" language="javascript">
  jQuery(document).ready(function(e){
    jQuery(document).ajaxStart(function() {
      jQuery("#loading").css('display','inline-block');
    }).ajaxStop(function() {
      jQuery("#loading").hide();
    });
    jQuery("#add_group_choir").on('submit',(function(e) {
      e.preventDefault();
      var group_choir_name = jQuery('#group_choir_name').val();
      var group_choir_desc = jQuery('#group_choir_desc').val();
      var group_choir_city = jQuery('#group_choir_city').val();
      var group_choir_state = jQuery('#group_choir_state').val();
      var group_choir_country = jQuery('#group_choir_country').val();
      var photo = jQuery('#photo').val();
    
       if(group_choir_name!='' && group_choir_city!='' && group_choir_state!='', group_choir_country!='') { 
        jQuery.ajax({
          url: "<?php echo get_template_directory_uri(); ?>-child/aj_add_group_choir.php",        
          type: "POST",            
          data: new FormData(this), 
          contentType: false,       
          cache: false,             
          processData:false,       
          success: function(data) {
            //alert(data);
            jQuery(".upload-gimg-msg").html(data);
            setTimeout(popupPimg, 2000);
              function popupPimg() {
                window.location.assign("<?php //echo site_url();?>/choir-profile");
              }
            }
        });
     } 
     else {
        if(group_choir_name ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Group Choir Name!</div>');
        } /* else if (group_choir_desc ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Group Choir Description!</div>');
        }*/ else if(group_choir_city ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Group Choir City Name!</div>');
        } else if(group_choir_country ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Group Choir Country Name!</div>');
        } else { 
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please fill the all required (*) fields first !</div>');
        }
      }
    }
   ));
    /*jQuery.noConflict();
    jQuery.validator.addMethod("alphaUname", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
    jQuery("#add_group_choir").validate({
      rules: {
        group_choir_name: {
          required: true,
          //alphaUname: true
        },
        group_choir_desc: {
          required: true,
          //alphaUname: true
        },
        group_choir_city: {
          required: true,
          //alphaUname: true
        },
        group_choir_country: {
          required: true,
          //alphaUname: true
        }       
      }, 
      messages: {
        group_choir_name: {
          required: "Please Enter Group Choir name",
          //alphaUname: "Please Enter Latters only"
        },
        group_choir_desc: {
          required: "Please Enter Group Choir Description",
          //alphaUname: "Please Enter Latters only"
        },
        group_choir_city: {
          required: "Please Enter Group Choir City",
          //alphaUname: "Please Enter Latters only"
        },
        group_choir_country: {
          required: "Please Enter Group Choir Country",
          //alphaUname: "Please Enter Latters only"
        }                                 
      },
      submitHandler: add_group_choir        
    });*/
    /*function add_group_choir(){
      var group_choir_name = jQuery('#group_choir_name').val();
      var group_choir_desc = jQuery('#group_choir_desc').val();
      var group_choir_city = jQuery('#group_choir_city').val();
      var group_choir_state = jQuery('#group_choir_state').val();
      var group_choir_country = jQuery('#group_choir_country').val();
      var photo = jQuery('#photo').val();

      jQuery.ajax({
        type : 'POST',
        url  : '<?php //echo get_template_directory_uri(); ?>-child/aj_add_group_choir.php',
        data : new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function()
        {
          jQuery("#errorReg").fadeOut();
          jQuery(".red_btn").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sende ...');
        },
        success :  function(data) {
          jQuery("#response").html('<p>Thank you for filling in the form.We have sent you an email with information</p>');
        },
      });
    }*/
  });
</script>


<script type="text/javascript">
jQuery(function() {
jQuery("#photo").change(function() {
//$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
jQuery('#previewing').attr('src','noimage.png');
//$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
jQuery("#photo").css("color","green");
jQuery('#image_preview').css("display", "block");
jQuery('#previewing').attr('src', e.target.result);
jQuery('#previewing').attr('width', '250px');
jQuery('#previewing').attr('height', '230px');
};
</script>>
<style type="text/css">
  #group_choir_desc {
    height: 150px;
  }
  #loading img {
    width: 25px;
    position: relative;
    top: 10px;
    left: -4%;
  }
  #post_submit {
    float: left;
    margin-right: 10px !important;
  }
  #loading { display: none; }
</style>
<?php get_footer();?>