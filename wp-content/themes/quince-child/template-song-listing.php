<?php
/*Template Name: Songs Listing
*/
get_header();
$post_id = $_GET['post_id'];
$user_id = $_GET['user_id'];
$current_user = wp_get_current_user();
$user_id = $current_user->id;
$args = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'cat'         => '',
  'category_name'    => '',
  'orderby'          => 'id',
  'order'            => 'DESC',
  'include'          => '',
  'exclude'          => '',
  'meta_key'         => '',
  'meta_value'       => '',
  'post_type'        => 'attachment',
  'post_mime_type'   => 'audio/mpeg',
  'post_parent'      => $post_id,
  'author'     => '',
  'author_name'    => '',
  'post_status'      => 'inherit',
  'suppress_filters' => true,
  'fields'           => '',
);
$getsongsdata = get_posts( $args );
?>
 <div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Songs
          <div class="serch_btn">
            <!--<input type="search" name="" placeholder="Search songs...">-->
            <a href="<?php //echo site_url();?>/add-songs/?post_id=<?php echo $post_id;?>&user_id=<?php echo $user_id;?>" class="pink_link">Upload New Track</a>
            <button onclick="playAll(event, true)" id="playBtn"><i class="fa fa-play" aria-hidden="true"></i> Play</button>
            <button onclick="playAll(event, false)" id="pauseBtn" style="display:none"><i class="fa fa-pause" aria-hidden="true"></i> Pause</button>
            <button onclick="restart(event)" id="restart_btn"><i class="fa fa-refresh" aria-hidden="true"></i> restart</button>
          </div>
        </h2>
        <table class="table mem_tbl display dataTable no-footer" id="song-listing">
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Id</h6>
              </th>
              <!-- <th>
                <h6 class="table_hd">Track Name</h6>
              </th> -->
               <th>
                <h6 class="table_hd">Track</h6>
              </th>
              <!-- <th>
                <h6 class="table_hd">Action</h6>
              </th> -->
             </tr>
          </thead>
          <tbody>
            <?php foreach ($getsongsdata as $getpaymentdatas) { ?>
            <tr>
              <td>
                <p>#<?php echo $getpaymentdatas->ID;?><p>
              </td> 
              <!-- <td>
                <p><?php //echo $getpaymentdatas->post_title;?></p>
              </td> -->
               <td>

                    <div class='audioHolder'>
                            <audio controls src="<?php echo $getpaymentdatas->guid;?>"></audio>
                    </div>
                    <p class="track_name"><?php echo $getpaymentdatas->post_title;?></p>
              </td>
              <!-- <td><a href="<?php //echo site_url();?>/?page_id=2349&post_id=<?php //echo $getpaymentdatas->ID;?>&user_id=<?php //echo $getpaymentdatas->post_author;?>" class="table_link">Edit </a></td> -->
             </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php echo do_shortcode('[audioMixer id="3048" ]');?>
<!--<div class="audioPlayer">
    <div id="presetsHolder_783183" class="presetsHolder">
        <div class="presets_label">
            Presets:
        </div>
        <div class="presets_content" id="presets_content_783183">
        </div>
    </div>
    <div class="timerHolder" id="timerHolder_783183">
        00:00 / 00:00
    </div>
    <div class="markerHolder">
        <div class="markers_label">
            <span style="padding-left:5px;">Markers:</span>
        </div>
        <div class="marker_content" id="marker_content">
        </div>
        <div class="marker_control_label"></div>
    </div>
    <div id="audioPlayersHolder_783183" class="audioPlayersHolder">
        <form id="audioPlayersForm_783183" name="audioPlayersForm_783183">
        </form>
    </div>
    <div class="buttonsHolder" style="display: flex;">
        <div style="width: 87.5%">
            <span title="Play" class="play_btn" id="playbtn_783183"></span>
            <span title="Pause" class="pause_btn" id="pausebtn_783183"></span>
            <span title="Restart" class="restart_btn" id="restartbtn_783183"></span>-->
            <!-- <span title="Reset Settings" class="reset_btn" id="resetbtn_783183"></span> -->
            <!--<span title="Loop Mode" class="loop_mode_off" id="enableLoop_783183"></span>
            <span title="Sub Titles" class="st_mode_off" id="enableSt_783183"></span>

        </div>
        <div style="width: 10%" class="download_btn" id="downloadbtn_783183">
        </div>
    </div>
    <div class="subsHolder" id="subsHolder_783183">
        <div id="subtitles_div" >
        </div>
    </div>
    
    <div class="back_wraper back_wraper_783183">
        <div class="email_popup email_popup_783183">
            <div class="close_popup pull-right">x</div>
            <h4>T'es prêt à télécharger ta propre version ?</h4>
            <form method="get">
                <div class="errorMsg" id="errorMsg_783183">&nbsp;</div>
                <input name="emailHolder" id="email_783183" required type="text" placeholder="Entre ton email ici pour qu'on t'envoie le MP3" />
                <br /><br />
                <input type="submit" value="C'EST PARTI" class="pull-right dwn_btn" id="notifybtn_783183" />
            </form>
        </div>
    </div>

    <div class="back_wraper back_wraper_success_783183">
        <div class="email_popup email_popup_783183" style="min-height:auto">
            <div class="close_popup pull-right">x</div>
            <h4>Va voir tes emails, ça arrive de suite</h4>
            <center>
                Dès que le fichier est prêt, tu le reçois en MP3<br />
                <input type="submit" value="OK" id="ok_783183" />
            </center>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type='text/javascript' src='https://www.lumiiq.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
<script type='text/javascript' src='https://www.lumiiq.com/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=4.9.9'></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery('#song-listing').dataTable( {
    "pagingType": "full_numbers"
  });
} );
</script>
 <script>
    (function( $ ) {
        $(function() {
            var subtitles=null;
            var loops = [[loops]];
            var markers = "null";
            var offset = "0";
            if(subtitles==""){
                subtitles={};
            }
            initializePlayer('[{"id":5674,"url":"http://www.siing.io/wp-content/uploads/2019/08/Homicide%20Ft%20Sunny%20Malton%20-%20Sidhu%20Moose%20Wala%20%20Big%20Boi%20Deep%20(DJJOhAL.Com).mp3","title":"Guitare","volume":"1","muted":"false","color":"#ff8040"},{"id":5676,"url":"http://www.siing.io/wp-content/uploads/2019/08/Homicide%20Ft%20Sunny%20Malton%20-%20Sidhu%20Moose%20Wala%20%20Big%20Boi%20Deep%20(DJJOhAL.Com).mp3","title":"Soprano","volume":"1","muted":"false","color":"#ee31ba"},{"id":5673,"url":"http://www.siing.io/wp-content/uploads/2019/08/Homicide%20Ft%20Sunny%20Malton%20-%20Sidhu%20Moose%20Wala%20%20Big%20Boi%20Deep%20(DJJOhAL.Com).mp3","title":"Alto","volume":"1","muted":"false","color":"#cc33c8"},{"id":5677,"url":"http://www.siing.io/wp-content/uploads/2019/08/Homicide%20Ft%20Sunny%20Malton%20-%20Sidhu%20Moose%20Wala%20%20Big%20Boi%20Deep%20(DJJOhAL.Com).mp3","title":"T\u00e9nor","volume":"1","muted":"false","color":"#00a3f0"},{"id":5675,"url":"http://www.siing.io/wp-content/uploads/2019/08/Homicide%20Ft%20Sunny%20Malton%20-%20Sidhu%20Moose%20Wala%20%20Big%20Boi%20Deep%20(DJJOhAL.Com).mp3","title":"Lead","volume":"1","muted":"false","color":"#ff4848"}]', 783183, '5692', '["Soprano","Alto","Ténor"]','{"Soprano":{"5673":{"volume":0.2,"muted":false},"5674":{"volume":1,"muted":"false"},"5675":{"volume":0.3,"muted":false},"5676":{"volume":1,"muted":"false"},"5677":{"volume":0.2,"muted":false},"color":"#ee31ba"},"Alto":{"5673":{"volume":1,"muted":"false"},"5674":{"volume":1,"muted":"false"},"5675":{"volume":0.3,"muted":false},"5676":{"volume":0.2,"muted":false},"5677":{"volume":0.2,"muted":false},"color":"#cc33c8"},"Ténor":{"5673":{"volume":0.2,"muted":false},"5674":{"volume":1,"muted":"false"},"5675":{"volume":0.3,"muted":false},"5676":{"volume":0.2,"muted":false},"5677":{"volume":1,"muted":"false"},"color":"#00a3f0"}}', "yes", "","yes",subtitles, loops, markers, offset, "Un instant ...");
      });
    })( jQuery );
</script>-->
<script type="text/javascript">
 function playAll(e, play) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var audios = jQuery("audio");
        audios = audios.toArray()
        audios.forEach(function (audio) {
            if (play) {
                audio.play();
                jQuery("#playBtn").hide();
                jQuery("#pauseBtn").show();
            } else {
                audio.pause();
                jQuery("#playBtn").show();
                jQuery("#pauseBtn").hide();
            }
        })
    }

    function restart(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var audios = jQuery("audio");
        audios = audios.toArray()
        audios.forEach(function (audio) {
            audio.currentTime = 0;
        });
    }
</script>
<style type="text/css">
.audioHolder audio {
    width: 100%;
}
.track_name{
    font-size: 12px;
}
#playBtn, #pauseBtn, #restart_btn {
    background: #176FC8;
    color: #fff !important;
    padding: 10px;
    margin-left:10px;
    margin-right:10px;
    -webkit-text-fill-color: #fff !important;
}
#song-listing_filter label input[aria-controls="song-listing"] {
    border: 1px solid #ccc;
    border-radius: 23px;
    height: 35px;
}
#song-listing_filter {
    margin-bottom: 20px;
}
#song-listing_length label select {
    height: 33px;
    width: 69px;
    border: 1px solid #ccc;
}

</style>
<?php get_footer();?>