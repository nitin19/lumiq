<?php
/*Template Name: Add Albums Choir
*/
get_header();
?>
<?php 
$current_user = wp_get_current_user();
$current_user_id = $current_user->id;
$group_data = $wpdb->get_results("select * from groups where user_id='$current_user_id' and status='1' and deleted='0'");

?>
<div class="add_choir">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="add_choir_hd">
          <h1>Add Albums</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
      </div>
    </div>
    <div class="row">
     <div class="col-sm-12">
      <div id="response" class="response-message"></div>
        <form action="" name="add_group_choir" id="add_group_choir" method="post" enctype="multipart/form-data">
            <div class="upload-gimg-msg"></div>
            <div class="image_uploader_sec">
              <div class="image_uploader_inersec">
                <div class="row mb-4 mt-4">
                  <div class="col-sm-12">
                    <!-- <input class="red_btn" type="hidden" name="post_type" id="post_type" value="" /> -->
                    <div class="form-group">
                      <label> Name <span class="required_icon">*</span> </label>
                      <input type="text" name="post_title" id="post_title" value="" class="custom_input">
                    </div>
                    <div class="form-group">
                      <label> Choir <span class="required_icon">*</span> </label>
                      <select name="group_select" id="group_select" class="custom_input">
                        <option value="">Select Your Choir</option>
                        <?php foreach ($group_data as $group_datas) { 
                          $group_id = $group_datas->id;
                          $user_query = new WP_User_Query(
                                array(
                                    'meta_key'    => 'selected_choir',
                                    'meta_value'  => $group_id,
                                )
                          );  
                          $singers = $user_query->get_results(); 
                          $singer_total_count = count($singers);
                          if($singer_total_count > 0){
                        ?>
                          <option value="<?php echo $group_datas->id;?>"><?php echo $group_datas->group_name;?></option>
                        <?php } else { ?>

                        <?php }} ?>
                      </select>  
                    </div>
                    <input class="red_btn" type="submit" name="post_submit" id="post_submit" value="Submit" />
                  </div>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>  
<script type="text/javascript" language="javascript">
  jQuery(document).ready(function(e){
    jQuery("#add_group_choir").on('submit',(function(e) {
      e.preventDefault();
      var post_title = jQuery('#post_title').val();
      var group_select = jQuery('#group_select').val();
      var form = $('#add_group_choir').get(0);
      if(post_title!='' && group_select!='') { 
      jQuery.ajax({
        url: "<?php echo get_template_directory_uri(); ?>-child/aj_add_albums_choir.php",        
        type: "POST",            
        data: new FormData(form), 
        contentType: false,       
        cache: false,             
        processData:false,       
        success: function(data) {
            jQuery(".upload-gimg-msg").html(data);
            setTimeout(popupPimg, 2000);
            function popupPimg() {
              window.location.assign("<?php echo site_url();?>/albums-listing");
            }
          }
      });
      } else {
        if(post_title ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Albums Name!</div>');
        } else if (group_select ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Select Your Group Choir!</div>');
        } else { 
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please fill the all required (*) fields first !</div>');
        }
      }
    }
   ));
  });
</script>
<style type="text/css">
  #group_choir_desc {
    height: 150px;
  }
</style>
<?php get_footer();?>