<?php

/* Template Name: choir-profile */ 
if ( is_user_logged_in() ) 
{
  require_once('stripe/config.php');
  require_once("stripe/vendor/autoload.php");
  get_header();
  if(isset($_GET['login']))
  {
      global $wpdb;
      $current_userId = get_current_user_id();
      $updateQuery = $wpdb->query("UPDATE wp_users SET login_status = '1' WHERE ID = ".$current_userId."");
  }

  $backgroundimage = site_url().'/wp-content/uploads/2019/07/profile_banner.jpg';
          if ( is_user_logged_in() ) {
           $current_user_id   = get_current_user_id();
           $user_meta=get_userdata($current_user_id);
           $user_roles= $user_meta->roles[0];
           if($user_roles == 'choirmaster'){
            $userdata    =  get_userdata( $current_user_id );
            $user_email= $userdata->user_email;
            $display_name = ucwords($userdata->display_name);
            $firstname = get_user_meta( $current_user_id, 'first_name','true' );
            $lastname = get_user_meta( $current_user_id, 'last_name','true' );
            $contact_number = get_user_meta( $current_user_id, 'contact_number','true' );
            $address = get_user_meta( $current_user_id, 'address','true' );
            $city = get_user_meta( $current_user_id, 'city','true' );
            $state = get_user_meta( $current_user_id, 'state','true' );
            $country = get_user_meta( $current_user_id, 'country','true' );
            $profile_image= get_user_meta( $current_user_id, 'image_url','true' );

  $quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ORDER BY id DESC LIMIT 5");
  //echo "<pre>"; print_r($quary); echo "</pre>";
  //}
  $events_data = $wpdb->get_results("SELECT * FROM `events` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ORDER BY id DESC LIMIT 5");

  $getpaymentdata = $wpdb->get_results("Select * from payment where user_id='$current_user_id' and status='1' and deleted='0' ORDER BY id desc");
      /*$defaults = array(
          'numberposts'      => 5,
          'post_type'        => 'audio_mixer_files',
          'author'      => $current_user_id,
          'post_status'      => 'publish'
      );
      $r = wp_parse_args($defaults);
      $get_posts = new WP_Query;
      $getalbums = $get_posts->query( $r ); 

      $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'cat'         => '',
        'category_name'    => '',
        'orderby'          => 'id',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'attachment',
        'post_mime_type'   => 'audio/mpeg',
        'author'      => $current_user_id,
        'author_name'    => '',
        'post_status'      => 'inherit',
        'suppress_filters' => true,
        'fields'           => '',
      );
      $gettrackdata = get_posts( $args );
*/
  /*    $query_args = array(
      'numberposts'      => 5,
      'post_type'  => 'audio_mixer_files',
      'author'      => $current_user_id,
      'post_status'      => 'publish',
      'orderby'          => 'ID',
      'order'            => 'DESC',
      'meta_query' => array(
          array(
              'key'   => 'fileId',
          ),
      )
  );
   
  $getalbums = new WP_Query( $query_args );*/
  $defaults = array(
        'numberposts' => 5,
        'category'         => 0,
        'orderby'          => 'id',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'audio_mixer_files',
        'suppress_filters' => true,
        'author'           => $current_user_id,
    );
 
    $r = wp_parse_args( $args, $defaults );
    if ( ! empty( $r['numberposts'] ) && empty( $r['posts_per_page'] ) ) {
        $r['posts_per_page'] = $r['numberposts'];
    }
 
    $r['ignore_sticky_posts'] = true;
    $r['no_found_rows']       = true;
 
    $get_posts = new WP_Query;
    $getalbumdata = $get_posts->query( $r );
  ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
  <div class="profile_sec">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="profileimage">
          <?php 
            if($profile_image){ ?> 
              <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $profile_image; ?>" alt="profile_img" width="100%">
              <?php 
            } else {

          $profile_one= site_url().'/wp-content/uploads/2019/07/male.jpg';?>
          <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
          <?php } ?>
          </div>
          <div class="choir_master">
          <div class="choir_master_iner">
          <!--<small>Choir Master</small>-->
          <a href="<?php echo site_url();?>/edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a> 
            <h1><?php echo  $display_name; ?> </h1>
            <p><?php echo  $address .' '. $city .' '. $state .' '.  $country; ?></p>
            <div class="row">
              <div class="col-sm">
                <!-- <h3>Contact Number</h3> -->
                <span class="phone_no"><i class="fa fa-phone" aria-hidden="true"></i><?php echo  $contact_number; ?></span>
              </div>
              <div class="col-sm">  
                <!-- <h3>Email Address</h3> -->
                <span class="phone_no"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $user_email ;?></span>
              </div>
              <div class="col-sm">
                <!-- <h3>My Choirs</h3> -->
                <span class="phone_no"><i class="fa fa-user" aria-hidden="true"></i>
                    <?php 
                 $count=$wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  '$current_user_id' AND status='1' AND deleted='0' ");
                    $count_group=count($count);
                  echo $count_group .'&nbsp'. 'Choirs';
                    ?>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  <div class="container" id="choir-list-section">
    <div class="choir_list">
      <div class="row">
        <div class="col-sm-12">
          <h2>Choir List 
            <div class="serch_btn">
             <!--  <form action="" name="search_form"> -->
              <!--<input type="search" id="search-choir" name="search_choir" placeholder="Search choir" value="">-->
              <a href="<?php echo site_url();?>/add-group-choir" class="pink_link">Add Choir</a> 
              <a href="<?php echo site_url();?>/choirs-listing" class="pink_link">View All</a>
              <!--  <input type="submit"> -->
  <!-- </form>
   -->          </div>
          </h2>
          <table class="table choir_tbl">
            <tbody>
        <?php 
        foreach ($quary as $value) {
            $choir_group_id = $value->id;
            $group_choir_name = $value->group_name;
            $group_choir_desc = $value->group_desc;
            $string = strip_tags($group_choir_desc);
            if (strlen($string) > 150) { 
            // truncate string
            $stringCut = substr($string, 0, 150);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...'; 
            }
            $group_choir_city = $value->group_city;
            $group_choir_state = $value->group_state;
            $group_choir_country = $value->group_country;
            $group_choir_image = $value->group_img;
        ?>
                <tr>
                <td class="tbl-td-eight">

                  <?php
                  if($group_choir_image){ ?>
                  <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $group_choir_image ;?>" alt="choir_list" class="ch_list">
                  <?php 
                  } else {
                    $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
                  <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
                  <?php } ?>
                </td>
                <td class="tbl-td-two">
                  <h6 class="table_hd"><?php echo $group_choir_name;?></h6>
                  <p class="table_text"><?php echo $string;?></p>
                </td>
                <?php 
                  $user_query = new WP_User_Query(
                  array(
                  'meta_key'    => 'selected_choir',
                  'meta_value'  => $value->id,
                  )
                );
                $singers = $user_query->get_results(); 
                $singer_total_count = count($singers);
                ?>
                <td class="tbl-td-seven">
                  <h6 class="table_loctn">Members</h6>
                  <p class="table_text"><?php echo $singer_total_count;?></p>
                </td>
                <td class="tbl-td-three">
                  <h6 class="table_loctn">Location</h6>
                  <p class="table_text"><?php echo $group_choir_city .' , '. $group_choir_state .' , '. $group_choir_country; ?></p>
                </td>
                <td class="tbl-td-four tblone"><a title="View Choir" href="<?php echo site_url().'/group?'?>group_id=<?php echo $value->id;?>" class="table_link"><i class="fa fas fa-eye" aria-hidden="true"></i> </a></td>
                <td class="tbl-td-four tbltwo"><a href="<?php echo site_url().'/edit-group-choir?'?>group_id=<?php echo $value->id;?>" data-toggle="tooltip" data-placement="top" title="Edit Choir" class="table_link"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>  </a></td>
                <?php
                if($singer_total_count >0){
                    ?>
                     <td class="tbl-td-four tblthree"><a title="Group Chat" href="<?php echo site_url().'/group-chat?'?>group_id=<?php echo $value->id;?>" class="table_link" style="float: right;"><i class="fa fa-users" aria-hidden="true"></i></a></td>
                    <?php } else{ ?> 
                    <td class="tbl-td-four tblthree"><a title="Group Chat" disabled = "true" href="javascript:void(0)" class="table_link" style="float: right; cursor: not-allowed;opacity: .5;"><i class="fa fa-users" aria-hidden="true"></i></a></td>
                    <?php } ?>
                <td class="tbl-td-six">
                  <?php 
                  $user_query = new WP_User_Query(
                  array(
                  'meta_key'    => 'selected_choir',
                  'meta_value'  => $choir_group_id,
                  )
                );
                $singers = $user_query->get_results(); 
                $singer_total_count = count($singers);
                $chat_group = $wpdb->get_results("SELECT * FROM `wp_group_chat` WHERE `group_id` = '$choir_group_id'  ");
                 $chat_group_count = count($chat_group);
                 if($singer_total_count >0 &&  $chat_group_count >0){
                ?>
  <button disabled="true" class="table_link disble-btn remove_events_<?php echo $value->id;?> remove_btn" id="<?php echo $value->id;?>" style="cursor: not-allowed;"><i class="fa fa-times" aria-hidden="true"></i></button>
                  <?php } else { ?>
          <span id="<?php echo $value->id;?>" class="rem_grp">
          <span class="table_link remove_events_<?php echo $value->id;?>" id="<?php echo $value->id;?>" title="Remove Choir"><i class="fa fa-times" aria-hidden="true"></i></span></span></td>
                    <?php  } ?>
              </tr>
             <? } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Latest Uploaded Albums
         <div class="serch_btn">
            <a href="<?php echo site_url();?>/add-albums" class="pink_link">Add New Albums</a>
            <a href="<?php echo site_url();?>/albums-listing/" class="pink_link">View All</a>
          </div>
        </h2>
        <table class="table mem_tbl display dataTable" id="albums_listing">
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Id</h6>
              </th>
              <th>
                <h6 class="table_hd">Albums Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Choir Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Songs Count</h6>
              </th>
              <th>
                <h6 class="table_hd">Action</h6>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($getalbumdata as $getalbumdatas) { 
              $album_id = $getalbumdatas->ID;
              $term_relationship_data = $wpdb->get_row("select * from wp_term_relationships where object_id='$album_id'");
              //echo "<pre>"; print_r($term_relationship_data); echo "</pre>";
              $group_id = $term_relationship_data->group_id;
              $group_data = $wpdb->get_row("select * from groups where id='$group_id' and status='1' and deleted='0'");
              $args = array(
                'orderby'          => 'id',
                'order'            => 'DESC',
                'post_type'        => 'attachment',
                'post_mime_type'   => 'audio/mpeg',
                'post_parent'      => $album_id,
                'post_status'      => 'inherit',
                'suppress_filters' => true,
              );
              $getsongsdata = get_posts( $args );
            ?>
            <tr>
              <td>
                <p>#<?php echo $getalbumdatas->ID;?><p>
              </td>
              <td>
                <p><?php echo $getalbumdatas->post_title;?></p>
              </td>
              <td>
                <p><?php echo $group_data->group_name;?></p>
              </td>
              <td>
                <p><?php echo count($getsongsdata);?></p>
              </td>
              <td class="album-action-btn"><a href="<?php echo site_url();?>/songs-listings/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="View Album" class="album_btn"><i class="fa fas fa-eye" aria-hidden="true"></i></a>
                <a href="<?php echo site_url();?>/edit-album/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="Edit Album" class="album_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>




  <!--<div class="container">
    <div class="latest_song">
      <div class="">
        <h2>Latest Uploaded Songs
          <div class="serch_btn">
            <a href="<?php //echo site_url();?>/albums-listing" class="pink_link">My Albums</a> 
          </div>
        </h2>
        <hr>
        <table class="table mem_tbl">
            <thead>
              <tr>
                <th>
                  <h6 class="table_hd">ID</h6>
                </th>
                <th>
                  <h6 class="table_hd">TRACK NAME</h6>
                </th>
                <!-- <th>
                  <h6 class="table_hd">GROUP NAME</h6>
                </th> -->
                <!-- <th>
                  <h6 class="table_hd">SONG COUNT</h6>
                </th> -->
                <!-- <th>
                  <h6 class="table_hd">ACTION</h6>
                </th> -->
              <!--</tr>
            </thead>
            <tbody>
              <?php //foreach ($gettrackdata as $get_tracks) { 
              ?>
              <tr>
                <td>
                  <p class="track_id">#<?php //echo $get_tracks->ID;?><p>
                </td>
                <td>
                  <div class='audioHolder'>
                    <audio controls src="<?php //echo $get_tracks->guid;?>"></audio>
                  </div>
                  <p class="track_name"><?php //echo $get_tracks->post_title;?></p>
                </td>-->
                <!-- <td>
                  <p><?php //echo $get_albums->post_title;?></p>
                  </p>
                </td>
                <td>
                  <p><?php //echo $get_albums->post_title;?></p>
                  </p>
                </td> -->
                <!-- <td><a href="<?php //echo site_url();?>/single-payment/?id=<?php //echo $getpaymentdatas->id;?>&user_id=<?php //echo $current_user_id;?>" class="table_link">View </a></td> -->
              <!--</tr>
              <?php //} ?>
            </tbody>
          </table>-->
        <!-- <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/826090043&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/823902110&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/646576953&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/643347699&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe> -->
      <!--</div>
    </div>
  </div>-->
  <div class="container" id="event-list-section">
    <div class="choir_list">
      <div class="row">
        <div class="col-sm-12">
          <h2>Events List 
            <div class="serch_btn">
              <!--<input type="search" name="" placeholder="Search choir">-->
              <?php 
              $isgroup_data= $wpdb->get_results("SELECT * from `groups` WHERE `user_id`='$current_user_id' AND `status` = '1' AND `deleted`= '0' ");
              $isgroup_total_count = count($isgroup_data);
                if($isgroup_total_count >0){
                ?>

              <a href="<?php echo site_url();?>/add-events" class="pink_link">Add New </a> 
              <a href="<?php echo site_url();?>/events-listing" class="pink_link">View All</a>
              <?php } else { ?>

  <!--              <a href="<?php echo site_url();?>/add-events" class="pink_link">Add New </a> 
   -->             <button class="pink_link" data-toggle="modal" data-target="#myModal"> Add New </button>

                <?php } ?>
            </div>
          </h2>
          <table class="table event_tbl">
            <tbody>
               <div class="upload-gimg-msg"></div>
        <?php 
        foreach ($events_data as $events_value) {
           //echo "<pre>";
           //print_r($events_value);
           //echo "</pre>";
      $events_id=$events_value->id;
      $group_id=$events_value->group_id;
      $events_name = $events_value->events_name;
      $events_dec = $events_value->events_dec;
        $string = strip_tags($events_dec);
            if (strlen($string) > 150) { 
            // truncate string
            $stringCut = substr($string, 0, 150);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...'; 
            }
      $events_city = $events_value->events_city;
      $events_state = $events_value->events_state;
      $events_country = $events_value->events_country;
      $events_image = $events_value->image;
        ?>
                <tr>
                  
                <td class="tbl-td-one">

              <?php
              if($events_image !=''){ 
                $beforeDot = explode(",", $events_image);
                $beforeDot = $beforeDot[0];
                ?>
            <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot ;?>" alt="choir_list" class="ch_list">
              <?php 
              } else {
                $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
              <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
              <?php } ?>
            </td>
            <td class="tbl-td-two">
              <h6 class="table_hd"><?php echo $events_name;?></h6>
              <p class="table_text"><?php echo $string;?></p>
            </td>
            <td class="tbl-td-date-event">
            <h6 class="table_loctn">Date of event</h6>
            <p class="table_text"><?php echo $events_value->events_startdate .' - '. $events_value->events_enddate; ?></p>
          </td>
            <td class="tbl-td-three">
            <h6 class="table_loctn">Location</h6>
            <p class="table_text"><?php echo $events_city .' , '. $events_state .' , '. $events_country; ?></p>
          </td>
                <td class="tbl-td-four tfourone"><a href="<?php echo site_url().'/events?'?>events_id=<?php echo $events_id;?>&group_id=<?php echo $group_id;?>" title="View Event" class="table_link"><i class="fa fas fa-eye" aria-hidden="true"></i> </a></td>
                <td class="tbl-td-four tfourtwo"><a href="<?php echo site_url().'/edit-events?'?>events_id=<?php echo $events_id;?>" class="table_link" title="Edit Event"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                <td class="tbl-td-six"> 
                  <span id="<?php echo $events_id;?>" class="rem_even">
                  <span class="table_link remove_events_<?php echo $events_id;?>" id="<?php echo $events_id;?>" title="Remove Event"><i class="fa fa-times" aria-hidden="true"></i></span></span></td>
              </tr>
             <? } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php 
  $backgroundmemimage = site_url().'/wp-content/uploads/2019/07/member_banner.jpg';
  ?>
  <div class="team_member_sec" style="background:  url(<?php echo $backgroundmemimage; ?>)">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
         <h2>Team Members</h2>
        </div>
      </div>
      <div class="row">
        <?php 
  $singers = get_users(array(
      'wp_capabilities'     => 'singer',
      'limit' => 4,
  ));
               
   foreach ($singers as $singer_users) {
  //   echo "<pre>";
  // print_r($singer_users);
  // echo "</pre>";
      $singer_id=  $singer_users->ID;
      $current_user_id   = get_current_user_id();
      $selected_choir_group_id = get_user_meta($singer_id, 'selected_choir','true' );
      $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id' and user_id='$current_user_id'");
      if($group_data->id==$selected_choir_group_id && $group_data->user_id == $current_user_id){
          $singer_id=  $singer_users->ID;
          $singer_first_name = ucwords(get_user_meta($singer_id, 'first_name','true' ));
          $singer_last_name = ucwords(get_user_meta($singer_id, 'last_name','true' ));
          $singer_category = get_user_meta($singer_id, 'selected_choir_cat','true');
          $singer_choirId = get_user_meta($singer_id, 'selected_choir','true');
          $query = $wpdb->get_results("SELECT group_name from `groups` WHERE `id`='$singer_choirId' AND `status` = '1' AND `deleted`= '0' ");
          //echo "<pre>"; print_r($query); echo "</pre>";
          $singer_image = get_user_meta($singer_id, 'image_url','true');

      ?> 
        <div class="col-md-3">
          <div class="member-profile">
            <?php 
             if($singer_image) { ?> 
            <a class="text-center" href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $singer_image; ?>" align="team_member" width="100%"></a>
              <?php } else { ?>
            <a class="text-center"  href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><img src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" alt="profile_img" width="100%"></a>
             <?php  } ?>
            <a href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><h4><?php echo $singer_first_name .' '. $singer_last_name; ?></h4></a>
            <p><?php echo $singer_category ?></p>
            <p><?php echo $query[0]->group_name; ?></p>
            <!--<a href="<?php //echo site_url().'/singer-profile?'?>singer_id=<?php //echo $singer_id;?>">View </a>-->
          </div>
        </div>
        <?php }  else {

        }
      }
  ?>
    </div>
  </div>
  <?php
  }
   else {
  }  
  } else {
  } 
}
else
{
  wp_redirect('http://www.siing.io/?login');
}
?>
<style>
  .serch_btn input[type="search"] {
    border: 1px solid #000;
  }
  .latest_song h2 {
    width: 100%;
    text-align: left;
    margin-bottom: 10px;
  }
  .serch_btn{
      padding-top: 20px;
  }
  .track_name{
    font-size: 12px;
  }
  .audioHolder audio {
    width: 100%;
  }
  .track_id{
    margin-bottom: 0px !important;
    padding-top: 20px;
  }
  a.pink_link {
    padding-right: 20px;
  }
  .member-profile {
    margin-bottom: 30px;
    min-height: 320px;
  }
  .phone_no:hover, .phone_no:active, .phone_no:focus {
    font-size: 14px;
    color: #666666 !important;
  }
  td.album-action-btn {
    vertical-align: middle;
  }
  a.album_btn {
    padding-left: 20px;
  }
  i.fa.fas.fa-eye {
    font-size: 16px;
    color: #1a7648;
  }
  i.fa.fa-pencil-square-o {
    font-size: 16px;
    color: #176fc8;
  }
</style>

<!--********************************** The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <!-- <h4 class="modal-title">Modal Heading</h4> -->
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         You do not create any Event utill you have not any Group. First create the Choir as well as create Event.
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
          <button id="ok_event" class="table_link" data-dismiss="modal">ok</button>
          
        </div>
        
      </div>
    </div>
  </div>
<!--********************************** The Modal  ENDS HERE -->
<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.rem_even').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_events.php',
                data : {events_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.rem_grp').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_group.php',
                data : {group_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>
<script type="text/javascript">
jQuery(window).load(function() {
  var sub_id =  
  jQuery.ajax({
    type: "POST",
    url: url,
    data: 
    dataType: "json",
      success: function(data) {
        alert('success');
         //$('#termoneError').text('The page has been successfully loaded');
      },
      error: function() {
        alert('failed');
         //$('#termoneError').text('An error occurred');
      }
  });//ajax request   
});
</script>

<?php

get_footer();
?>
