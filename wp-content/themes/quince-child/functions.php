<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'jquery.mmenu' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );
// END ENQUEUE PARENT ACTION 
add_action( 'wp_enqueue_scripts', function() {
  wp_enqueue_style( 'child-style', get_template_directory_uri() . '/style.css' );
});
/**********add custom user role **************/
add_role(
    'choirmaster',
    __( 'Choir Master' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
    )
);
add_role(
    'singer',
    __( 'Singer' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
    )
);
/**********add custom user role end**************/
/**********after login rediect*******************/
function my_login_redirect( $url, $request, $user ){
	if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
		if( $user->has_cap( 'administrator') or $user->has_cap( 'author')) {
		 $url = admin_url();
		} elseif ($user->has_cap( 'choirmaster')) {
            global $wpdb;
            $today_obj      = new DateTime( date( 'Y-m-d', strtotime( 'today' ) ) );
            $register_date  = date('Y-m-d', strtotime($user->user_registered));
            $registered_obj = new DateTime( date( 'Y-m-d', strtotime( $register_date ) ) );
            $interval_obj   = $today_obj->diff( $registered_obj );
            $paymentdata = $wpdb->get_row("select * from payment where user_id='$user->id' and status='1' and deleted='0'");
            if( $interval_obj->days > 30 && $paymentdata =='') {
                $url = site_url().'/choir-expire-plan';
            } else {                               
                $url = site_url().'/choir-profile/?login="choir-login"';
            }
		 
		} elseif ($user->has_cap( 'singer')) {
            global $wpdb;
            $today_obj      = new DateTime( date( 'Y-m-d', strtotime( 'today' ) ) );
            $register_date  = date('Y-m-d', strtotime($user->user_registered));
            $registered_obj = new DateTime( date( 'Y-m-d', strtotime( $register_date ) ) );
            $interval_obj   = $today_obj->diff( $registered_obj );
            $paymentdata = $wpdb->get_row("select * from payment where user_id='$user->id' and status='1' and deleted='0'");
            if( $interval_obj->days > 7 && $paymentdata =='') {
                $url = site_url().'/singer-expire-plan';
            } else {                               
                $url = site_url().'/singer-profile/?login="singer-login"';
            }
		} else {
		 $url = home_url();
		}
	}
	return $url;
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );
/**********after login rediect end*******************/
function js_hide_admin_bar( $show ) {
	if ( ! current_user_can( 'administrator' ) ) {
		return false;
	}
	return $show;
}
add_filter( 'show_admin_bar', 'js_hide_admin_bar' );



/**
 * Function Name: front_end_login_fail.
 * Description: This redirects the failed login to the custom login page instead of default login page with a modified url
**/
add_action( 'wp_login_failed', 'front_end_login_fail' );
function front_end_login_fail( $username ) {

// Getting URL of the login page
$referrer = $_SERVER['HTTP_REFERER'];    
// if there's a valid referrer, and it's not the default log-in screen
if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) {
    wp_redirect( get_permalink( 2307 ) . "?login=failed" ); 
    exit;
}

}

/**
 * Function Name: check_username_password.
 * Description: This redirects to the custom login page if user name or password is   empty with a modified url
**/
add_action( 'authenticate', 'check_username_password', 1, 3);
function check_username_password( $login, $username, $password ) {

// Getting URL of the login page
$referrer = $_SERVER['HTTP_REFERER'];

// if there's a valid referrer, and it's not the default log-in screen
if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) { 
    if( $username == "" || $password == "" ){
        wp_redirect( get_permalink( 2307 ) . "?login=empty" );
        exit;
    }
}

}
// Replace my constant 'LOGIN_PAGE_ID' with your custom login page id.



//  Redirection after logout

add_action('wp_logout','ps_redirect_after_logout');
function ps_redirect_after_logout(){
         wp_redirect( '/' );
         exit();
}
	function before_login_ready_section() {

		register_sidebar( array(
			'name'          => 'before Login Ready Section',
			'id'            => 'ready-section-widget',
			'before_title'  => '<h1 class="title-heading">',
			'after_title'   => '</h1>',
		) );

	}
	add_action( 'widgets_init', 'before_login_ready_section' );


// Logout without confirmation.
add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result)
{
    /**
     * Allow logout without confirmation
     */
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '/';
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
        header("Location: $location");
        die;
    }
}

function register_my_menus() {
    register_nav_menus(
        array(
          'Choirmaster' => __( 'Under Choirmaster Navigation' ), 'Singer' => __( 'Under Singer Navigation' )
        )
    );
}
add_action( 'init', 'register_my_menus' );

function choir_master_plans_section() {

    register_sidebar( array(
        'name'          => 'Choir Master Plans Section Widget',
        'id'            => 'choir-plans-section-widget',
        'before_widget' => '<div class="plans-section">',
        'after_widget'  => '</div>',
        'before_title'  => '<h1 class="plans-title-heading">',
        'after_title'   => '</h1>',
    ) );
}
add_action( 'widgets_init', 'choir_master_plans_section' );
/***********not deactivate project api plugin **************/
add_filter( 'plugin_action_links', 'disable_plugin_deactivation', 10, 4 );
function disable_plugin_deactivation( $actions, $plugin_file, $plugin_data, $context ) {
if ( array_key_exists( 'edit', $actions ) )
unset( $actions['edit'] );
// Remove deactivate link for important plugins
if ( array_key_exists( 'deactivate', $actions ) && in_array( $plugin_file, array(
'project-api/project-api.php',
)))
unset( $actions['deactivate'] );
return $actions;
}