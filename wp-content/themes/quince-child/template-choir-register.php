<?php /* Template Name: Choir Register */ 
get_header();
?>
<?php $varable_holding_img_url = site_url().'/wp-content/uploads/2019/07/06_choir_signup.jpg'; ?>
<div class="choir_backimg" style="background-image: url(<?php echo $varable_holding_img_url; ?>);">
  <div class="container">
    <div class="choir_main">
        <div class="row">
          <div class="col-lg-6">
            <h6>Your Personal</h6>
            <h1>Soundtrack Everywhere</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
          </div>
          <div class="col-lg-6">
            <div class="sign_up">
              <h2>Sign Up</h2>
              <p>Create your account for free</p>
                <form name="choir-register" id="choir-register">
                  <input type="text" name="fname" placeholder="First Name">
                  <input type="text" name="lname" placeholder="Last Name">
                  <!-- <input type="text" name="uname" placeholder="UserName"> -->
                  <input type="email" name="email" placeholder="Email Address">
                  <input type="password"  name="password" placeholder="Enter Password">
                  <!--<input type="text" name="organization" placeholder="Enter Your organization Name">-->
                  <input type="hidden" name="role" value="choirmaster">
                  <input class="btn btn-default" type="submit" name="submit" value="Create Account" />
                </form>
                <div  style="background:green; color:#fff;" id="response" class="response-message">
                </div>
            </div>
          </div>
        </div>
    </div>  
  </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){
      jQuery.noConflict();
      jQuery.validator.addMethod("alphaUname", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
       });
      jQuery.validator.addMethod("pwcheck", function(value) {
       return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
       });
      jQuery("#choir-register").validate({
          rules: {
            fname: {
                        required: true,
                        alphaUname: true
                   },
            lname: {
                        required: true,
                        alphaUname: true
                   },
            /*uname: {
                        required: true,
                        remote: {
                        url: '<?php echo get_template_directory_uri(); ?>-child/aj_check_uname.php',
                        type: "POST",
                        }
                   },*/      
            email: {
                        required: true,
                        remote: {
                        url: '<?php echo get_template_directory_uri(); ?>-child/aj_check_email.php',
                        type: "POST",
                        }
                    },             
            password: {
                        required: true,
                        pwcheck: true,
                        minlength: 6              
                   },
                   /*
            organization: {
                        required: true             
                   },  
                   */     
                  }, 
          messages: {
            fname: {
                        required: "Please Enter First name",
                        alphaUname: "Please Enter Latters only"
                     },
            lname: {
                        required: "Please Enter Last name",
                        alphaUname: "Please Enter Latters only"
                     },
            // uname: {
            //             required: "Please Enter UserName",
            //             remote: "UserName Allready Exist "
            //          },        
            email: {
                        required: "Please Enter Email",
                        email: "Please Enter Valid Email",
                        remote: "E-mail already Exist"
                     },
            password: {
                        required: "Please Enter Password",
                        pwcheck: "Bring alphabet (A-Z / a-z), tal (0-9) or at specialcharcter (!,%, &, @, #, $, ^, *,?, _, ~).",
                        minlength: "Please Enter Minimum 6 Character"
                     },
                     /*
            organization: {
                        required: "Please Enter organization"
                     },  
                     */                                  
                    },
          submitHandler: function(form) {
            var data = jQuery("#choir-register").serialize();
            jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_choir-register.php',
                data : data,
            beforeSend: function()
            {
                jQuery("#errorReg").fadeOut();
                jQuery(".btn-default").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sende ...');
            },
            success :  function(data) {
              //alert(data);
              jQuery("#response").html('<p>Thank you for filling in the form.We have sent you an email with information</p>');           
              setTimeout(4000);
                window.location.assign("<?php echo site_url();?>/login"); 
            },
          });
        },         
      });
    });
</script>
<style>
  html{
    margin-top: 0px !important;
  }
</style>