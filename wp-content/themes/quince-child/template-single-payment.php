<?php 
/*Template Name: Single Payment*/
get_header();
$payment_id = $_GET['id'];
$user_id = $_GET['user_id'];
$status = $_GET['status'];
$getpaymentdata = $wpdb->get_row("Select * from payment where id='$payment_id' and status='1' and deleted='0'");
/*global $wpdb;
$user = wp_get_current_user(); 
echo $today_obj = date('h:i:s', strtotime( 'today' ));
date_default_timezone_set("Europe/Paris");
date("h:i:s");*/

//echo $register_date  = date('h:i:s', strtotime($user->user_registered));
?>
<div class="container">
  <div class="choir_list">
    <div class="row">
    	<div class="col-sm-12 single_payment_sec">
			<table>
				<tr>
	    			<th>Status:</th>
	    			<td><?php echo $getpaymentdata->payment_status;?></td>
	  			</tr>
			    <tr>
			        <th>Start Date:</th>
			    	<td>
			    		<?php echo $start_date = date("jS F, Y",$getpaymentdata->start_data);?>
			    	</td>
			  	</tr>
	  			<tr>
	    			<th>Next Payment Date:</th>
					<td>
						<?php 
                        	$next_payment_date = date("jS F, Y",$getpaymentdata->sub_current_period_end);
                   			echo $next_payment_date;
                  		?>
                   </td>
	  			</tr>
	  			<tr>
	    			<th>Trial End Date:</th>
					<td>
						<?php echo $trial_end_date = date("jS F, Y",$getpaymentdata->trial_end);?>
					</td>
	  			</tr>
	  			<!-- <tr>
	    			<th>Payment:</th>
					<td>555 77 855</td>
	  			</tr> -->
	  			<tr>
	    			<th>Action:</th>
					<td>
					    <?php if($getpaymentdata->payment_status ==  'active'){ ?>
							<form action="<?php echo get_template_directory_uri(); ?>-child/stripe/cancel-subscription.php" id="cancel_btn" name="cancel_btn" method="post">
								<input type="hidden" name="subscription_id" id="subscription_id" value="<?php echo $getpaymentdata->subscription_id;?>">
								<input type="hidden" name="payment_id" id="payment_id" value="<?php echo $payment_id;?>">
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
								<button type="submit" value="canceled">Canceled</button>
							<!-- <a href="" class="cancel_btn">Cancel</a> -->
						    </form>
						<?php } elseif($status == 'true') { ?>
							<form action="<?php echo get_template_directory_uri(); ?>-child/stripe/cancel-subscription.php" id="cancel_btn" name="cancel_btn" method="post">
								<input type="hidden" name="subscription_id" id="subscription_id" value="<?php echo $getpaymentdata->subscription_id;?>">
								<input type="hidden" name="payment_id" id="payment_id" value="<?php echo $payment_id;?>">
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
								<button type="submit" value="canceled" readOnly>Canceled</button>
							<!-- <a href="" class="cancel_btn">Cancel</a> -->
						    </form>
						<?php } else { ?>
						<?php } ?>
					</td>
	  			</tr>
			</table>
		</div>
	</div>
  </div>
</div>
<style type="text/css">
.single_payment_sec table tr th, .single_payment_sec table tr td {
 text-align: left;
}
.cancel_btn {
    background: #FF0004;
    color: #fff;
    padding: 8px 25px 8px 25px;
    font-size: 14px;
    font-weight: 600;
    border-radius: 5px;
}
</style>
<?php get_footer();?>