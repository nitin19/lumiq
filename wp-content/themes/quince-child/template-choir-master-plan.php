<?php 
/* Template Name: Choir Master Plan */
get_header();
$backgroundimage = site_url().'/wp-content/uploads/2019/07/price_banner.jpg';
require_once('stripe/config.php');
$currentuser = get_current_user_id();
$payment_status = $_GET['status'];
?>
<div class="price_banner" style="background: url(<?php echo $backgroundimage;?>)"> 
  <div class="container"> 
    <div class="row">
      <div class="col-sm-12"> 
        <h1>Pricing & Plans</h1>
        <p>Toutes les chansons mènent chez nous !</p>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="price_sec">
    <div class="price_left">
      <h2>Choir Master</h2>
      <img src="<?php echo site_url();?>/wp-content/uploads/2019/07/choir_master.png" class="price_img">
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
    </div>
    <div class="price_right">
      <h3>50/year</h3>
      <p>30 days trial, then 50€ per year, </p>
      <ul>
        <li>Advanced use of the Audio Player (can also upload, edit and categorize songs)</li>
        <li>Choir Messenger</li>
        <li>Direct Link with Singers (via push notifications and emails)</li>
        <li>Invite singers to join the choir</li>
      </ul>
      <h6>During the trial, singer doesn’t have access to the “<span style="color: #ff4c4f;">Download</span>”  features of the player.</h6>
      <?php if ( is_user_logged_in() ) { ?>
      <?php if($payment_status == 'active'){ ?>
          <h2 class="payment_status_message">Thanks! You've subscribed to this plan.</h2>
      <?php } elseif ($payment_status == 'trialing') { ?>
          <h2 class="payment_status_message">Thanks! You've subscribed to this plan.But your trial period days is Pending. After trial period your plan will be activated.</h2>
      <?php } else { ?>
          <form action="<?php echo get_template_directory_uri(); ?>-child/stripe/choircharge.php" method="post" class="frmStripePayment">
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $currentuser;?>">
            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="<?php echo $stripe['publishable_key']; ?>"
              data-name="€50/year Plan"
              data-description="30 days trial, then €50/year"
              data-panel-label="Buy Now"
              data-label="Get Started"
              data-locale="auto">
            </script>
          </form>
      <?php } ?>
      <?php } else { ?>
          <a href="<?php echo site_url();?>/login" class="stripe-button-el">Get Started</a>
      <?php } ?>
      <!-- <a href="#" class="gradient_btn">Select Plan</a> -->
    </div>
  </div>
  <div class="priceftr">
    <h4 class="priceftr_hd">Free from 10 paid registered singers in the choir</h4>
    <p class="priceftr_contant">Choir Master will be able to send simple invitation to singers to join his choir, and let them pay for their subscription, or prepaid invitation which will be free for the singer, and discounted when bought by the choir master (20€ per year)</p>
  </div>
</div>
<style type="text/css">
  .price_banner {
  background-image: url("../images/price_banner.jpg");
  background-size: cover !important;
  background-position: center !important;;
  background-repeat: no-repeat !important;;
  height:60vh;
  display: flex;
  align-items: center;
}
.price_sec {
    background-color: #ffffff;
    box-shadow: 0px 10px 20px #ccc;
    padding: 50px 0px 0px 0px;
    margin-top: -100px;
    float: left;
    width: 100%;
    display: flex;
    flex-wrap: wrap;  
    border-radius: 30px;
}
.price_banner h1 {
    font-size: 60px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 600;
    text-align: center;
}
.price_banner p {
    font-size: 23px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 600;
    text-align: center;
}
.priceftr {
    padding: 30px 0px;
    clear: both;
}
.priceftr_contant {
    font-size: 20px;
    color: #878787;
    font-family: "Roboto";
    font-weight: 400;
    text-align: center;
    max-width: 800px;
    margin:0 auto;
}
.priceftr_hd {
    font-size: 30px;
    color: #168bb1;
    font-family: "Raleway";
    font-weight: 500;
    text-align: center;
}
.price_left h2 {
    font-size: 40px;
    color: #fe5558;
    font-family: "Raleway";
    font-weight: 700;
}
.price_left p {
  font-size: 14px;
  color: #999999;
  font-family: "Roboto";
  font-weight: 400; 
  margin-top: 30px;
}
.price_left {
    width: 300px;
    float: left;
    padding: 20px;
}
.price_right {
    width: calc(100% - 300px);
    padding-left: 20px;
    background-color: #303030;
    padding: 80px;
    border-radius: 10px 0px 30px 0px;
}
.price_right h3 {
    font-size: 81px;
    color: #ffffff;
    font-family: 'Maven Pro', sans-serif;
    position: relative;
}
.price_right h3:before {
    content: "€";
    font-size: 50px;
    position: absolute;
    left: -30px;
    font-weight: lighter;
}
.price_right p {
    font-size: 22px;
    color: #ff4c4f;
    font-family: "Roboto";
    font-weight: 500;
}
.price_right ul {
    list-style: none;
    padding: 0px;
    margin-left: 0px;
}
.price_right ul li {
    font-size: 17px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 400;
    padding: 5px 0px;
}
.price_right h6 {
    font-size: 17px;
    color: #20c0ee;
    font-family: "Roboto";
    font-weight: 400;
    max-width: 540px;
}
.stripe-button-el {
    background-image: linear-gradient(to right , #176fc8, #176fc8 39%, #ff5558 66%, #ff5558);
    padding: 8px 20px;
    font-size: 18px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 500;
    text-align: center;
    margin-top: 30px;
    display: inline-block;
}
.price_right ul li:before {
    content: "";
    background-image: url("<?php echo site_url();?>/wp-content/uploads/2019/07/tick.png");
    height: 20px;
    width: 30px;
    display: inline-block;
    background-repeat: no-repeat;
    vertical-align: middle;
}
.price_img {width: 100%;}
@media (max-width:1024px) {
.price_right {padding: 40px;}
.price_right h3 {font-size: 50px;}
}
@media (max-width:768px) {
  .price_left {width: 100%;}
  .price_right {width: 100%;}
  .price_left {text-align: center;}
  .price_img {width: auto;}
  .price_sec {padding: 20px 0px 0px 0px;}
}
@media (max-width:767px) {
  .price_banner h1 {font-size: 35px;}  
  .price_banner p {font-size: 16px;}
  .price_right h3 {font-size: 50px;}
  .price_right {padding: 20px;}
  .price_right h3:before {font-size: 30px;left: -15px;}
  .price_left h2 {font-size: 30px;}
  .priceftr_hd {font-size: 22px;}
  .priceftr_contant {font-size: 14px;}
  .price_right p {font-size: 18px;}
  .price_right ul li {font-size: 14px;}
  .price_right h6 {font-size: 14px;}
  .gradient_btn {font-size: 14px;margin-top: 20px;}
}
@media (max-width:425px) {
  .price_img {width: 100%;}
  .priceftr_hd {font-size: 20px;}
}
.frmStripePayment {
    /*border: #E0E0E0 1px solid;*/
    /*padding: 20px 30px;*/
    width: 180px;
    text-align: center;
    /*background: #ececec;*/
    /*margin: 60px auto;*/
    font-family: Arial;
}
.stripe-button-el span {
    background: none;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.plan-caption {
    margin-bottom: 30px;
    font-size: 1.2em;
    width: 180px;
}
.payment_status_message {
    color: #fff;
    padding-top: 20px;
}
</style>
<?php get_footer();?>