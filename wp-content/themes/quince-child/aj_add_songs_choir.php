<?php
require_once('../../../wp-load.php');
$current_user_id = get_current_user_id();
$error='';
global $wpdb;
$post_id = $_POST['post_id'];
$user_id = $_POST['user_id'];
if($_SERVER['REQUEST_METHOD'] == "POST"){
    $wp_upload_dir = wp_upload_dir();
    $filename = $wp_upload_dir['subdir'] . '/' . $_FILES["upload_songs"]["name"];
    $parent_post_id = $post_id;

    $allowedExts = array("mpeg");
    $temp = explode(".", $_FILES["upload_songs"]["name"]);
    $extension = end($temp);

    $filetype = wp_check_filetype( basename( $filename ), null );
    /*if ($_FILES["upload_songs"]["type"] != "audio/mpeg") { 
        $error .= "Mime type not allowed<br />";
    }*/
    if (!in_array($extension, $allowedExts)) {
        $error .= '<div class="alert alert-danger">Only mp3 Extension is allowed<br /></div>';
    }

    if ($error == "") {
    $sourcePath = $_FILES['upload_songs']['tmp_name'];
    $Newfilename = uniqid().'-'.$filename;
    $targetPath = $wp_upload_dir['path'] . '/' . basename( $filename );
    $movefile =  move_uploaded_file($_FILES['upload_songs']['tmp_name'], $targetPath);
    if($movefile){
        $attachment = array(
            'post_author'=>$user_id,
            'post_date' => date('Y-m-d h:m:s'),
            'post_date_gmt'=> date('Y-m-d h:m:s'),
            'comment_status' => 'open',
            'ping_status' => 'closed',
            'post_modified'=>'',
            'post_modified_gmt'=>'',
            'post_name'=>strtolower(preg_replace( '/\.[^.]+$/', '', basename( $filename ) )),
            'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
            'post_content'   => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
            'post_status'    => 'inherit',
            'post_type'      => 'attachment'
        );

        $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
        if ( ! is_wp_error( $attach_id ) ) {
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            $newfilepath = ($wp_upload_dir['path'] . '/' . basename( $filename ));
            $audio_file_path = get_attached_file($attach_id);
            wp_read_audio_metadata( $audio_file_path );
            $attachment_data = wp_generate_attachment_metadata( $attach_id, $newfilepath);
            wp_update_attachment_metadata( $attach_id,  $attachment_data );
            set_post_thumbnail( $parent_post_id, $attach_id );
        }
        $filesVolume = 'filesVolume'.$attach_id;
        $filesMute = 'filesMute'.$attach_id;
        $color = 'color'.$attach_id;
        $order = 'order'.$attach_id;
        add_post_meta($post_id,'fileId', $attach_id);
        add_post_meta($post_id,$filesVolume, '1');
        add_post_meta($post_id,$filesMute, 'false');
        add_post_meta($post_id,$color, '');
        add_post_meta($post_id,$order, '');
        echo $error =  '<div class="alert alert-success">Your Songs added successfully...!!</div>';
    } else {
        echo $error =  '<div class="alert alert-danger">Sorry fail to add your Songs due to system error. Please try again.</div>';
    }
  } else {
     echo $error;
  }
}