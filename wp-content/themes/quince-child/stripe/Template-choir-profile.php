<?php

/* Template Name: choir-profile */ 
require_once('stripe/config.php');
require_once("stripe/vendor/autoload.php");
get_header();
if(isset($_GET['login']))
{
    global $wpdb;
    $current_userId = get_current_user_id();
    $updateQuery = $wpdb->query("UPDATE wp_users SET login_status = '1' WHERE ID = ".$current_userId."");
}

$backgroundimage = site_url().'/wp-content/uploads/2019/07/profile_banner.jpg';
        if ( is_user_logged_in() ) {
         $current_user_id   = get_current_user_id();
         $user_meta=get_userdata($current_user_id);
         $user_roles= $user_meta->roles[0];
         if($user_roles == 'choirmaster'){
          $userdata    =  get_userdata( $current_user_id );
          $user_email= $userdata->user_email;
          $display_name = ucwords($userdata->display_name);
          $firstname = get_user_meta( $current_user_id, 'first_name','true' );
          $lastname = get_user_meta( $current_user_id, 'last_name','true' );
          $contact_number = get_user_meta( $current_user_id, 'contact_number','true' );
          $address = get_user_meta( $current_user_id, 'address','true' );
          $city = get_user_meta( $current_user_id, 'city','true' );
          $state = get_user_meta( $current_user_id, 'state','true' );
          $country = get_user_meta( $current_user_id, 'country','true' );
          $profile_image= get_user_meta( $current_user_id, 'image_url','true' );

//     if(isset($_GET['search_choir'])){
//       $search_choir=$_GET['search_choir'];
//        $quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `group_name` LIKE '%".$search_choir."%' or `group_city` LIKE '%".$search_choir."%' or `group_state` LIKE '%".$search_choir."%' or `group_country` LIKE '%".$search_choir."%' AND `user_id` =  '$current_user_id' AND `status` = '1' AND `deleted` = '0'
// ");
//       echo '<pre>';
//       print_r($quary);
//       echo '</pre>';

//     }  else {    

$quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ");
//}

$events_data = $wpdb->get_results("SELECT * FROM `events` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ");

$getpaymentdata = $wpdb->get_results("Select * from payment where user_id='$current_user_id' and status='1' and deleted='0' ORDER BY id desc");

//print_r($quary); 

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
<div class="profile_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3 profileimage">
        <?php 
          if($profile_image){ ?> 
            <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $profile_image; ?>" alt="profile_img" width="100%">
            <?php 
          } else {

        $profile_one= site_url().'/wp-content/uploads/2019/07/male.jpg';?>
        <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
        <?php } ?>
        </div>
        <div class="col-md-9 choir_master">
        <div class="choir_master_iner">
        <small>Choir Master</small>
        <a href="<?php echo site_url();?>/edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a> 
          <h1><?php echo  $display_name; ?> </h1>
          <p><?php echo  $address .' '. $city .' '. $state .' '.  $country; ?></p>
          <div class="row">
            <div class="col-sm">
              <h3>Contact Number</h3>
              <p><a href="tell:<?php echo  $contact_number; ?>" class="phone_no"><?php echo  $contact_number; ?></a></p>
            </div>
            <div class="col-sm">
              <h3>Email Address</h3>
              <p><a href="mailto:<?php echo $user_email ;?>" class="phone_no"><?php echo $user_email ;?></a></p>
            </div>
            <div class="col-sm">
              <h3>Choirs Group</h3>
              <p><a href="#" class="phone_no">
                  <?php 
               $count=$wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  '$current_user_id' AND status='1' AND deleted='0' ");
                  $count_group=count($count);
                echo $count_group .'&nbsp'. 'Groups';
                  ?>
               </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Choir List 
          <div class="serch_btn">
           <!--  <form action="" name="search_form"> -->
            <input type="search" id="search-choir" name="search_choir" placeholder="Search choir" value="">
            <a href="<?php echo site_url();?>/add-group-choir" class="pink_link">Join New</a> 
            <!--  <input type="submit"> -->
<!-- </form>
 -->          </div>
        </h2>
        <table class="table choir_tbl">
          <tbody>
      <?php 
      foreach ($quary as $value) {
          $choir_group_id = $value->id;
          $group_choir_name = $value->group_name;
          $group_choir_desc = $value->group_desc;
          $string = strip_tags($group_choir_desc);
          if (strlen($string) > 150) { 
          // truncate string
          $stringCut = substr($string, 0, 150);
          $endPoint = strrpos($stringCut, ' ');

          //if the string doesn't contain any space then it will cut without word basis.
          $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
          $string .= '...'; 
          }
          $group_choir_city = $value->group_city;
          $group_choir_state = $value->group_state;
          $group_choir_country = $value->group_country;
          $group_choir_image = $value->group_img;
      ?>
              <tr>
              <td class="tbl-td-one">

                <?php
                if($group_choir_image){ ?>
                <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $group_choir_image ;?>" alt="choir_list" class="ch_list">
                <?php 
                } else {
                  $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
                <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
                <?php } ?>
              </td>
              <td class="tbl-td-two">
                <h6 class="table_hd"><?php echo $group_choir_name;?></h6>
                <p class="table_text"><?php echo $string;?></p>
              </td>
              <td class="tbl-td-three">
                <h6 class="table_loctn">Location</h6>
                <p class="table_text"><?php echo $group_choir_city .' '. $group_choir_state .' '. $group_choir_country; ?></p>
              </td>
              <td class="tbl-td-four"><a href="<?php echo site_url().'/group?'?>group_id=<?php echo $value->id;?>" class="table_link">View </a></td>
              <td class="tbl-td-five"><a href="<?php echo site_url().'/edit-group-choir?'?>group_id=<?php echo $value->id;?>" class="table_link"> <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> --> Edit  </a></td>
              <td class="tbl-td-six">
              <span id="<?php echo $value->id;?>" class="rem_grp">
                <?php 
                $user_query = new WP_User_Query(
                array(
                'meta_key'    => 'selected_choir',
                'meta_value'  => $choir_group_id,
                )
              );
              $singers = $user_query->get_results(); 
              $singer_total_count = count($singers);
              $chat_group = $wpdb->get_results("SELECT * FROM `wp_group_chat` WHERE `group_id` = '$choir_group_id'  ");
               $chat_group_count = count($chat_group);
               if($singer_total_count >0 &&  $chat_group_count >0){
              ?>

        <!--         <span class="table_link remove_events_<?php //echo $value->id;?>" id="<?php //echo $value->id;?>"> --> <!-- <i class="fa fa-trash-o remove_events" aria-hidden="true"></i> --> <!-- Remove</span> -->
<button disabled="true" class="table_link remove_events_<?php echo $value->id;?> remove_btn" id="<?php echo $value->id;?>"> Remove </button>
                <?php } else { ?>
        <span class="table_link remove_events_<?php echo $value->id;?>" id="<?php echo $value->id;?>"> <!-- <i class="fa fa-trash-o remove_events" aria-hidden="true"></i> --> Remove</span></span></td>
                  <?php  } ?>
            </tr>
           <? } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="latest_song">
  <div class="container">
    <div class="row">
      <h2>Latest Uploaded Songs</h2>
      <hr>
      <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/826090043&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
      <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/823902110&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
      <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/646576953&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
      <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/643347699&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
    </div>
  </div>
</div>
<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Events List 
          <div class="serch_btn">
            <input type="search" name="" placeholder="Search choir">
            <?php 
            $isgroup_data= $wpdb->get_results("SELECT * from `groups` WHERE `user_id`='$current_user_id' AND `status` = '1' AND `deleted`= '0' ");
            $isgroup_total_count = count($isgroup_data);
              if($isgroup_total_count >0){
              ?>

            <a href="<?php echo site_url();?>/add-events" class="pink_link">Add New </a> 
            <?php } else { ?>

<!--              <a href="<?php echo site_url();?>/add-events" class="pink_link">Add New </a> 
 -->             <button class="pink_link" data-toggle="modal" data-target="#myModal"> Add New </button>

              <?php } ?>
          </div>
        </h2>
        <table class="table event_tbl">
          <tbody>
             <div class="upload-gimg-msg"></div>
      <?php 
      foreach ($events_data as $events_value) {
        // echo "<pre>";
        // print_r($events_value);
        // echo "</pre>";
    $events_id=$events_value->id;
    $group_id=$events_value->group_id;
    $events_name = $events_value->events_name;
    $events_dec = $events_value->events_dec;
      $string = strip_tags($events_dec);
          if (strlen($string) > 150) { 
          // truncate string
          $stringCut = substr($string, 0, 150);
          $endPoint = strrpos($stringCut, ' ');

          //if the string doesn't contain any space then it will cut without word basis.
          $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
          $string .= '...'; 
          }
    $events_city = $events_value->events_city;
    $events_state = $events_value->events_state;
    $events_country = $events_value->events_country;
    $events_image = $events_value->image;
      ?>
              <tr>
                
              <td class="tbl-td-one">

            <?php
            if($events_image !=''){ 
              $beforeDot = explode(",", $events_image);
              $beforeDot = $beforeDot[0];
              ?>
          <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot ;?>" alt="choir_list" class="ch_list">
            <?php 
            } else {
              $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
            <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
            <?php } ?>
          </td>
          <td class="tbl-td-two">
            <h6 class="table_hd"><?php echo $events_name;?></h6>
            <p class="table_text"><?php echo $string;?></p>
          </td>
          <td class="tbl-td-three">
          <h6 class="table_loctn">Location</h6>
          <p class="table_text"><?php echo $events_city .' '. $events_state .' '. $events_country; ?></p>
        </td>
              <td class="tbl-td-four"><a href="<?php echo site_url().'/events?'?>events_id=<?php echo $events_id;?>&group_id=<?php echo $group_id;?>" class="table_link">View </a></td>
              <td class="tbl-td-five"><a href="<?php echo site_url().'/edit-events?'?>events_id=<?php echo $events_id;?>" class="table_link"> <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> --> Edit  </a></td>
              <td class="tbl-td-six">
                <span id="<?php echo $events_id;?>" class="rem_even">
                <span class="table_link remove_events_<?php echo $events_id;?>" id="<?php echo $events_id;?>"> <!-- <i class="fa fa-trash-o remove_events" aria-hidden="true"></i>  -->Remove</span></span></td>
            </tr>
           <? } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Membership transaction
          <div class="serch_btn">
            <input type="search" name="" placeholder="Search choir">
            <!--<a href="#" class="pink_link">Join New</a> -->
          </div>
        </h2>
        <table class="table mem_tbl">
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">SUBSCRIPTION</h6>
              </th>
              <th>
                <h6 class="table_hd">STATUS</h6>
              </th>
              <th>
                <h6 class="table_hd">NEXT PAYMENT</h6>
              </th>
              <th>
                <h6 class="table_hd">TOTAL</h6>
              </th>
              <th>
                <h6 class="table_hd"></h6>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($getpaymentdata as $getpaymentdatas) { ?>
            <tr>
              <td>
                <p><?php echo $getpaymentdatas->subscription_id;?><p>
              </td>
              <td>
                <p><?php echo $getpaymentdatas->payment_status;?></p>
              </td>
              <td>
                <p>
                  <?php 
                   $next_payment_date = date("jS F, Y",$getpaymentdatas->sub_current_period_end);
                   echo $next_payment_date;
                  ?>
                </p>
              </td>
              <td>
                <p>
                  <?php setlocale(LC_MONETARY, 'en_EUR');
                        $payment_price = money_format('%i', $getpaymentdatas->amount/100);
                    echo '€'.$payment_price;?>
                </p>
              </td>
              <td><a href="<?php echo site_url();?>/single-payment/?id=<?php echo $getpaymentdatas->id;?>&user_id=<?php echo $current_user_id;?>" class="table_link">View </a></td>
            </tr>
            <script type="text/javascript">
              jQuery( document ).ready(function() {
                var sub_id = <?php echo $getpaymentdatas->subscription_id;?>;
                jQuery.ajax({
                  type: 'POST',
                  url: '<?php echo get_template_directory_uri(); ?>-child/stripe/get-subscription-data.php',
                  data: {subscription_id: sub_id},
                  success: function(data) {
                    alert('success');
                     //$('#termoneError').text('The page has been successfully loaded');
                  },
                  error: function() {
                    alert('failed');
                     //$('#termoneError').text('An error occurred');
                  }
                });  
              });
            </script>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php 
$backgroundmemimage = site_url().'/wp-content/uploads/2019/07/member_banner.jpg';
?>
<div class="team_member_sec" style="background:  url(<?php echo $backgroundmemimage; ?>)">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
       <h2>Team Members</h2>
      </div>
    </div>
    <div class="row">
      <?php 
$singers = get_users(array(
    'wp_capabilities'     => 'singer',
    'limit' => 4,
));
             
 foreach ($singers as $singer_users) {
//   echo "<pre>";
// print_r($singer_users);
// echo "</pre>";
    $singer_id=  $singer_users->ID;
    $current_user_id   = get_current_user_id();
    $selected_choir_group_id = get_user_meta($singer_id, 'selected_choir','true' );
    $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id' and user_id='$current_user_id'");
    if($group_data->id==$selected_choir_group_id && $group_data->user_id == $current_user_id){
        $singer_id=  $singer_users->ID;
        $singer_first_name = ucwords(get_user_meta($singer_id, 'first_name','true' ));
        $singer_last_name = ucwords(get_user_meta($singer_id, 'last_name','true' ));
        $singer_category = get_user_meta($singer_id, 'selected_choir_cat','true');
        $singer_image = get_user_meta($singer_id, 'image_url','true');

    ?> 
      <div class="col-sm-2">
        <div class="member-profile">
          <?php 
           if($singer_image) { ?> 
          <a href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $singer_image; ?>" align="team_member" width="100%"></a>
            <?php } else { ?>
          <a href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><img src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" alt="profile_img" width="100%"></a>
           <?php  } ?>
          <a href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>"><h4><?php echo $singer_first_name .' '. $singer_last_name; ?></h4></a>
          <p><?php echo $singer_category ?></p>
          <!--<a href="<?php //echo site_url().'/singer-profile?'?>singer_id=<?php //echo $singer_id;?>">View </a>-->
        </div>
      </div>
      <?php }  else {

      }
    }
?>
  </div>
</div>
<?php
}
 else {
}  
} else {
} 
?>
<style>
  .serch_btn input[type="search"] {
    border: 1px solid #000;
  }
</style>

<!--********************************** The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <!-- <h4 class="modal-title">Modal Heading</h4> -->
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         You do not create any Event utill you have not any Group. First create the Choir as well as create Event.
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
          <button id="ok_event" class="table_link" data-dismiss="modal">ok</button>
          
        </div>
        
      </div>
    </div>
  </div>
<!--********************************** The Modal  ENDS HERE -->
<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.rem_even').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_events.php',
                data : {events_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.rem_grp').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_group.php',
                data : {group_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>
<script type="text/javascript">
jQuery(window).load(function() {
  var sub_id =  
  jQuery.ajax({
    type: "POST",
    url: url,
    data: 
    dataType: "json",
      success: function(data) {
        alert('success');
         //$('#termoneError').text('The page has been successfully loaded');
      },
      error: function() {
        alert('failed');
         //$('#termoneError').text('An error occurred');
      }
  });//ajax request   
});
</script>

<?php

get_footer();
?>
