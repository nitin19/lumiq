<?php
  require_once('config.php');
  require_once('../../../../wp-load.php');
  require_once("vendor/autoload.php");
  \Stripe\Stripe::setApiKey($stripe['secret_key']);

  $subscription_id  = $_POST['subscription_id'];

  $single_subscription_data = \Stripe\Subscription::retrieve($subscription_id);
  print_r($single_subscription_data);

?>
