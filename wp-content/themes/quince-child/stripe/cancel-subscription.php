<?php
require_once('config.php');
require_once('../../../../wp-load.php');
require_once("vendor/autoload.php");
\Stripe\Stripe::setApiKey($stripe['secret_key']);

$subscription_id = $_POST['subscription_id'];
$user_id = $_POST['user_id'];
$payment_id = $_POST['payment_id'];

$cancelNow = array('at_period_end' => true); // false will cancel now

//cancel
$stripe = \Stripe\Subscription::retrieve($subscription_id);
$result = $stripe->cancel($cancelNow);
if($result){
		global $wpdb;
		if ($result->canceled_at) {
		    $canceled_at = date('Y-m-d H:i:s', $result->canceled_at);
		}
		$cancel_at_period_end = ($result->cancel_at_period_end) ? 'true' : 'false';
		$status = $result->status;
    	$success = $wpdb->update('payment', array(
    		"canceled_at"          => $canceled_at,
    		"cancel_at_period_end" => $cancel_at_period_end,
		), array('id'=>$payment_id, 'user_id' =>$user_id));
		if($success){
			if($cancel_at_period_end == 'true'){
				wp_redirect('/single-payment/?status='.$cancel_at_period_end);
			} else {
				wp_redirect('/single-payment/?status=error');
			}
		}
} else {
	wp_redirect('/single-payment/?status=error');
}

//update user
// $user = User::find(Auth::id());
// $user->current_period_end = date('Y-m-d H:i:s', $result->current_period_end);
// if ($result->canceled_at) {
//     $user->canceled_at = date('Y-m-d H:i:s', $result->canceled_at);
// }
// $user->cancel_at_period_end = ($result->cancel_at_period_end) ? 'true' : 'false';
// $user->status = $result->status;
// $user->save();

?>