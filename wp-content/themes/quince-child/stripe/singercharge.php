<?php
  require_once('config.php');
  require_once('../../../../wp-load.php');
  require_once("vendor/autoload.php");
  \Stripe\Stripe::setApiKey($stripe['secret_key']);

  $token  = $_POST['stripeToken'];
  $email  = $_POST['stripeEmail'];
  $user_id  = $_POST['user_id'];

  global $wpdb;
  $user = wp_get_current_user();
  $today_obj      = new DateTime( date( 'Y-m-d', strtotime( 'today' ) ) );
  $register_date  = date('Y-m-d', strtotime($user->user_registered));
  $registered_obj = new DateTime( date( 'Y-m-d', strtotime( $register_date ) ) );
  $interval_obj   = $today_obj->diff( $registered_obj );
  $paymentdata = $wpdb->get_row("select * from payment where user_id='$user->id' and status='1' and deleted='0'");
  //$newDate = date('Y-m-d', strtotime('+30 days', $register_date));
  $date = strtotime(date("Y-m-d", strtotime($register_date)) . " +7 days");

  $plan = \Stripe\Plan::create(array(
      "product" => [
          "name" => "Singer $25/Year",
          "type" => "service"
      ],
      "nickname" => "Singer $25/Year",
      "interval" => "year",
      "interval_count" => "1",
      "currency" => "EUR",
      "amount" => "2500",
  ));

  $customer = \Stripe\Customer::create([
      'email' => $email,
      'source'  => $token,
  ]);

  if( $interval_obj->days < 7 || $paymentdata !='') {
      $subscription = \Stripe\Subscription::create(array(
        "customer" => $customer->id,
        "items" => array(
            array(
                "plan" => $plan->id,
            ),
        ),
        'trial_end' => $date,
      ));
  } else {
      $subscription = \Stripe\Subscription::create(array(
        "customer" => $customer->id,
        "items" => array(
            array(
                "plan" => $plan->id,
            ),
        ),
      ));
  }
  if($subscription){
    if($subscription->trial_end!=''){
       $trial_end_date = $subscription->trial_end;
    } else {
      $trial_end_date = '';
    }
    if($subscription->trial_start!=''){
      $trial_start_date = $subscription->trial_start;
    } else {
      $trial_start_date ='';
    }
    if($subscription->canceled_at!=''){
      $trial_canceled_at_date = $subscription->canceled_at;
    } else {
      $trial_canceled_at_date = '';
    }
    global $wpdb;
    $success = $wpdb->insert('payment', array(
      "user_id"                   => $user_id,
      "user_email"                => $email,
      "subscription_id"           => $subscription->id,
      "customer_id"               => $subscription->customer,
      "subscription_item_id"      => $subscription->items->data[0]->id,
      "plan_id"                   => $subscription->plan->id,
      "amount"                    => $subscription->plan->amount,
      "currency"                  => $subscription->plan->currency,
      "pay_interval"              => $subscription->plan->interval,
      "interval_count"            => $subscription->plan->interval_count,
      "plan_name"                 => $subscription->plan->name,
      "quantity"                  => $subscription->quantity,
      "subscription_url"          => $subscription->items->url,
      "start_data"                => $subscription->start_date,
      "payment_status"            => $subscription->status,
      "latest_invoice"            => $subscription->latest_invoice,
      "sub_current_period_start"  => $subscription->current_period_start,
      "sub_current_period_end"    => $subscription->current_period_end,
      "trial_start"               => $subscription->trial_start,
      "trial_end"                 => $subscription->trial_end,
      "created_by"                => $user_id,
    ));
    if($success){
      if($subscription->status == 'active'){
        //echo "Thanks! You've subscribed to the " . $subscription->plan->name .  " plan.";
        wp_redirect('/singer-plan/?status='.$subscription->status);
      } elseif ($subscription->status == 'trialing') {
        wp_redirect('/singer-plan/?status='.$subscription->status);
      } else {
        wp_redirect('/singer-plan/?status=error');
        //echo "Sorry! Your Payment has been Failed!1";
      }
    }
  } else {
        wp_redirect('/singer-plan/?status=error');
        //echo "Sorry! Your Payment has been Failed!2";
  }
  


?>