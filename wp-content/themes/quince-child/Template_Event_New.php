<?php

/* Template Name: Events */ 

get_header();

$current_user_id   = get_current_user_id();
$events_id = $_GET['events_id'];
$groups_id=$_GET['group_id'];
$users_id=$_GET['user_id'];


$backgroundimage = site_url().'/wp-content/uploads/2019/07/profile_banner.jpg';  
$quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id ");
$eventdata = $wpdb->get_row("SELECT * FROM `events` WHERE `id` =  $events_id AND `status`='1' AND `deleted`='0' ");

$event_id=$eventdata->id;
$group_id=$eventdata->group_id;
$user_id=$eventdata->$user_id;
$user_meta=get_userdata($current_user_id);
$user_roles= $user_meta->roles[0];
?>
<div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
<div class="profile_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3 profileimage">
        <?php 
          if($eventdata->image!=''){
            ?>
            <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $eventdata->image;?>">
            <?php 
          } else {
        $profile_one= site_url().'/wp-content/uploads/2019/07/male.jpg';?>
        <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
        <?php } ?>
        </div>
        <div class="col-md-9 choir_master">
        <div class="choir_master_iner">
        <small> Events </small>
        <?php 
        if($user_roles == 'choirmaster' && $groups_id== $group_id){ ?>
          <a href="<?php echo site_url();?>/edit-events?events_id=<?php echo $events_id; ?>" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Event </a>
          <?php } else {

          }
          ?>
       <!--  <a href="<?php echo site_url();?>/choir-edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a>  -->
          <h1><?php echo $eventdata->events_name;?> </h1>
          <p><?php echo  $eventdata->events_city .' '. $eventdata->events_state .' '.  $eventdata->events_country; ?></p>
          <div class="row">
            <div class="col-sm">
              <h3>Start Date</h3>
              <p><?php echo  $eventdata->events_startdate; ?></p>
          </div>
          <div class="col-sm">
              <h3>End Date</h3>
              <p><?php echo $eventdata->events_enddate; ?></p>
            </div>
            <div class="col-sm">
              <h3>Start Time</h3>
              <p><?php echo  $eventdata->event_start_time; ?></p></div>
              <div class="col-sm">
              <h3>End Time</h3>
              <p><?php echo  $eventdata->event_end_time; ?></p>
            </div>
            <!-- <div class="col-sm">
              <h3>Email Address</h3>
              <p><a href="mailto:<?php echo $user_email ;?>" class="phone_no"><?php echo $user_email ;?></a></p>
            </div> -->
            <div class="col-sm">
              <?php
              $groupdata = $wpdb->get_results('SELECT * FROM `groups` WHERE `id`= "'.$eventdata->group_id.'" ');
?>
              <h3>Choirs Group</h3>
               <?php foreach ( $groupdata as $group ) { ?>
              <p><a href="#" class="phone_no"><?php echo $group->group_name;?></a></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="team_member_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
       <h2>Team Members</h2>
      </div>
    </div>
  </div></div>
}
       
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.hhh').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_events.php',
                data : {events_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/?page_id=2312");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>

<?php

get_footer();
?>