<?php
/*Template Name: Add Songs Choir
*/
get_header();
?>
<?php 
$current_user = wp_get_current_user();
$post_id = $_GET['post_id'];
$user_id = $_GET['user_id'];
?>
<div class="add_choir">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="add_choir_hd">
          <h1>Add Songs</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
      </div>
    </div>
    <div class="row">
     <div class="col-sm-12">
      <div id="response" class="response-message"></div>
        <form action="" name="add_group_choir" id="add_group_choir" method="post" enctype="multipart/form-data">
            <div class="upload-gimg-msg"></div>
            <div class="image_uploader_sec">
              <div class="image_uploader_inersec">
                <div class="row mb-4 mt-4">
                  <div class="col-sm-12">
                    <!-- <input class="red_btn" type="hidden" name="post_type" id="post_type" value="attachment" /> -->
                    <input class="red_btn" type="hidden" name="post_id" id="post_id" value="<?php echo $post_id;?>" />
                    <input class="red_btn" type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>" />
                    <div class="form-group">
                      <label> Upload Your Songs <span class="required_icon">*</span> </label>
                      <input type="file" name="upload_songs[]" id="upload_songs" value="" class="custom_input upload_songs" multiple="true">
                    </div>
                    <input class="red_btn" type="submit" name="post_submit" id="post_submit" value="Submit" /><div id="loading"><img src="<?php echo site_url();?>/wp-content/uploads/2019/08/loading_new_cart_img.gif"></div>
                    <input class="red_btn cancel_btn" type="button"  name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url();?>/albums-listing/' " />
                  </div>
                  <!-- <button type="submit" class="red_btn">Submit</button> -->
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>  
<script type="text/javascript" language="javascript">
  jQuery(document).ready(function(e){
     jQuery(document).ajaxStart(function() {
    jQuery("#loading").css('display','inline-block');
    }).ajaxStop(function() {
      jQuery("#loading").hide();
    });
    jQuery("#add_group_choir").on('submit',(function(e) {
      e.preventDefault();
      var post_id = jQuery('#post_id').val();
      var user_id = jQuery('#user_id').val();
      var upload_songs = jQuery('#upload_songs').val();
      alert(upload_songs);
      var form = $('#add_group_choir').get(0);
      if(upload_songs!='') { 
      jQuery.ajax({
        url: "<?php echo get_template_directory_uri(); ?>-child/aj_add_songs_choir.php",        
        type: "POST",            
        data: new FormData(form), 
        contentType: false,       
        cache: false,             
        processData:false,       
        success: function(data) {
            jQuery(".upload-gimg-msg").html(data);
            setTimeout(popupPimg, 2000);
            function popupPimg() {
              window.location.assign("<?php echo site_url();?>/songs-listings");
            }
          }
      });
      } else {
        if(upload_songs ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Select Your Song!</div>');
        } else { 
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please fill the all required (*) fields first !</div>');
        }
      }
    }
   ));
  });
</script>
<style type="text/css">
  #group_choir_desc {
    height: 150px;
  }
  #loading img {
    width: 25px;
    position: relative;
    top: 10px;
    left: -4%;
  }
  #post_submit {
    float: left;
    margin-right: 10px !important;
  }
  #loading { display: none; }
</style>
<?php get_footer();?>