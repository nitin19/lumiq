<?php 
require_once('../../../wp-load.php');

 
 if($_FILES["file"]["name"]!=''){
 if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["file"]["type"])){
 	global $wpdb;
   	$filename = $_FILES['file']['tmp_name'];
  	$id =$_POST['id'];
	$fname =$_POST['fname'];
	$lname = $_POST['lname'];
	$contactnumber =$_POST['contactnumber'];
	$address = $_POST['address'];
	$city =$_POST['city'];
	$state = $_POST['state'];
	$country = $_POST['country'];
	$usertable = $wpdb->prefix . 'users';
	$allowedExts = array("jpeg","jpg","png","JPEG","JPG","PNG");
    $temp = explode(".", $_FILES["file"]["name"]);
    $extension = end($temp);
    if ($_FILES["file"]["error"]) {
        $error .= '<div class="alert alert-danger">Error opening the file<br /></div>';
    }
    if ($_FILES["file"]["type"] != "image/jpeg" &&
        $_FILES["file"]["type"] != "image/JPEG" &&
        $_FILES["file"]["type"] != "image/jpg" &&
        $_FILES["file"]["type"] != "image/JPG" &&
        $_FILES["file"]["type"] != "image/PNG" &&
        $_FILES["file"]["type"] != "image/png") { 
        $error .= "Mime type not allowed<br />";
    }
    if (!in_array($extension, $allowedExts)) {
        $error .= '<div class="alert alert-danger">Extension not allowed<br /></div>';
    }
    if ($_FILES["file"]["size"] > 2000000) {
    	
       echo $error .= '<div class="alert alert-danger">File size shoud be less than 2 Mb<br /></div>';
    }
    if ($error == "") {
      $path_array = wp_upload_dir();
      $sourcePath = $_FILES['file']['tmp_name'];
      $Newfilename = uniqid().'-'.$_FILES['file']['name'];
      $targetPath = $path_array["basedir"]."/profileimage/".$Newfilename;
      $movefile =  move_uploaded_file($sourcePath, $targetPath);
      if($movefile) {
			$success = $wpdb->update($usertable, array(
		  	'ID'=>$id,
		   	"user_nicename" => $fname,
		   	"display_name" => $fname.' '.$lname,
		  
		 ),array('id'=>$id));
 
		  $user_id = $id;
		  $user_role = serialize($role);
		  update_user_meta( $user_id, 'first_name', $fname);  
		  update_user_meta( $user_id, 'last_name', $lname);
		  update_user_meta( $user_id, 'contact_number', $contactnumber);
		  update_user_meta( $user_id, 'address', $address);
		  update_user_meta( $user_id, 'city', $city);
		  update_user_meta( $user_id, 'state', $state);
		  update_user_meta( $user_id, 'country', $country);
		  update_user_meta( $user_id, 'image_url', $Newfilename);

		 echo $error =  '<div class="alert alert-success">Your profile update successfully...!!</div>';

		 } else {
		   echo $error =  '<div class="alert alert-danger">Sorry fail to update your profile due to system error. Please try again.</div>';

		 }
    }
  }
} else {
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    global $wpdb;
    $usertable = $wpdb->prefix . 'users';
    $id =$_POST['id'];
	$fname =$_POST['fname'];
	$lname = $_POST['lname'];
	$contactnumber =$_POST['contactnumber'];
	$address = $_POST['address'];
	$city =$_POST['city'];
	$state = $_POST['state'];
	$country = $_POST['country'];
    $success = $wpdb->update($usertable, array(
		  	'ID'=>$id,
		   	"user_nicename" => $fname,
		   	"display_name" => $fname.' '.$lname,
		  
	),array('id'=>$id));
	  $user_id = $id;
	  $user_role = serialize($role);
	  update_user_meta( $user_id, 'first_name', $fname);  
	  update_user_meta( $user_id, 'last_name', $lname);
	  update_user_meta( $user_id, 'contact_number', $contactnumber);
	  update_user_meta( $user_id, 'address', $address);
	  update_user_meta( $user_id, 'city', $city);
	  update_user_meta( $user_id, 'state', $state);
	  update_user_meta( $user_id, 'country', $country);

	  echo $error =  '<div class="alert alert-success">Your Profile updated successfully...!!</div>';
    } else {
   	  echo $error =  '<div class="alert alert-danger">Sorry fail to update your Profile due to system error. Please try again.</div>';
    }
}
?>
