<?php
/*Template Name: Choirs Listing
*/
get_header();
?>
<?php 
if ( is_user_logged_in() ) {
  $current_user_id   = get_current_user_id();
  $CurrentUserChoirData = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ORDER BY id DESC");

  ?>
<div class="container">
  <div class="choir_list mb-5">
    <div class="row">
      <div class="col-sm-12">
        <h2>Choirs
        </h2>
        <table class="table mem_tbl display dataTable" id="albums_listing">
          <div class="serch_btn">
          </div>
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Choir Image</h6>
              </th>
              <th>
                <h6 class="table_hd">Choir Title/Description</h6>
              </th>
              <th>
                <h6 class="table_hd">Members</h6>
              </th>
              <th>
                <h6 class="table_hd">Location</h6>
              </th>
              <th>
                <h6 class="table_hd">Action</h6>
              </th>
            </tr>
          </thead>
          <tbody>
      <?php 
      foreach ($CurrentUserChoirData as $value) {
          $choir_group_id = $value->id;
          $group_choir_name = $value->group_name;
          $group_choir_desc = $value->group_desc;
          $string = strip_tags($group_choir_desc);
          if (strlen($string) > 150) { 
          // truncate string 
          $stringCut = substr($string, 0, 150);
          $endPoint = strrpos($stringCut, ' ');

          //if the string doesn't contain any space then it will cut without word basis.
          $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
          $string .= '...'; 
          }
          $group_choir_city = $value->group_city;
          $group_choir_state = $value->group_state;
          $group_choir_country = $value->group_country;
          $group_choir_image = $value->group_img;
      ?>
              <tr>
              <td class="tbl-td-eight">

                <?php
                if($group_choir_image){ ?>
                <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $group_choir_image ;?>" alt="choir_list" class="ch_list">
                <?php 
                } else {
                  $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
                <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
                <?php } ?>
              </td>
              <td class="tbl-td-two">
                <h6 class="table_heading"><?php echo $group_choir_name;?></h6>
                <p class="table_text"><?php echo $string;?></p>
              </td>
              <?php 
                $user_query = new WP_User_Query(
                array(
                'meta_key'    => 'selected_choir',
                'meta_value'  => $value->id,
                )
              );
              $singers = $user_query->get_results(); 
              $singer_total_count = count($singers);
              ?>
              <td class="tbl-td-seven">
                <h6 class="table_loctn">Members</h6>
                <p class="table_text"><?php echo $singer_total_count;?></p>
              </td>
              <td class="tbl-td-three">
                <h6 class="table_loctn">Location</h6>
                <p class="table_text"><?php echo $group_choir_city .' , '. $group_choir_state .' , '. $group_choir_country; ?></p>
              </td>
              <td>
                <a href="<?php echo site_url().'/group?'?>group_id=<?php echo $value->id;?>" title="View Choir" class=""><i class="fa fas fa-eye" aria-hidden="true"></i> </a>
                <a href="<?php echo site_url().'/edit-group-choir?'?>group_id=<?php echo $value->id;?>" class="" title="Edit Choir"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <?php 
                $user_query = new WP_User_Query(
                  array(
                  'meta_key'    => 'selected_choir',
                  'meta_value'  => $value->id,
                  )
                );
                $singers = $user_query->get_results(); 
                $singer_total_count = count($singers);
                if($singer_total_count >0) { ?>
                  <a href="<?php echo site_url().'/group-chat?'?>group_id=<?php echo $value->id;?>" class="" title="Group Chat" style="margin-top:4px;"><i class="fa fa-users" aria-hidden="true"></i></a>
                  <?php } else { ?> 
                  <a title="Group Chat" disabled ="true" href="javascript:void(0)" class="" style="cursor: not-allowed;opacity:0.5;"><i class="fa fa-users" aria-hidden="true"></i></a>
                <?php } ?>
                <?php 
                $user_query = new WP_User_Query(
                  array(
                  'meta_key'    => 'selected_choir',
                  'meta_value'  => $choir_group_id,
                  )
                );
                $singers = $user_query->get_results(); 
                $singer_total_count = count($singers);
                $chat_group = $wpdb->get_results("SELECT * FROM `wp_group_chat` WHERE `group_id` = '$choir_group_id'  ");
               $chat_group_count = count($chat_group);
               if($singer_total_count > 0 &&  $chat_group_count > 0){
               ?>
               <a title="Remove Choir" disabled ="true" href="javascript:void(0)" class=" remove_events_<?php echo $value->id;?>" id="<?php echo $value->id;?>" style="cursor: not-allowed;opacity: 0.5;margin-top:4px;"><i class="fa fa-times" aria-hidden="true"></i></a>
              <?php } else { ?>
              <span id="<?php echo $value->id;?>" class="rem_grp">
              <span class=" remove_events_<?php echo $events_id;?>" id="<?php echo $value->id;?>" title="Remove Choir"><i class="fa fa-times" aria-hidden="true"></i></span></span>
              <?php  } ?>
              </td>
            </tr>
           <? } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery('#albums_listing').dataTable( {
    "pagingType": "full_numbers"
  } );
} );
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
  jQuery('.rem_grp').click(function(){
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_group.php',
                data : {group_id: remove_id},
                cache: false, 
            success :  function(data) {
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          });
}else{
return false;
}
});
});
</script>
<style type="text/css">
#albums_listing_filter label input[aria-controls="albums_listing"] {
    border: 1px solid #ccc;
    border-radius: 23px;
    height: 35px;
}
span.table_link {
  background: #ff2424 !important;
  padding: 5px 11px 5px 11px !important;
  border-radius: 3px !important;
}
.tbl-td-two {
    width: 40%;
}
#albums_listing_filter {
    margin-bottom: 20px;
}
#albums_listing_length label select {
    height: 33px;
    width: 69px;
    border: 1px solid #ccc;
}
.edit_btn {
    margin-right: 10px;
}
.table_heading {
  font-size: 14px;
  color: #333333;
  font-family: "Raleway";
  font-weight: 700;
  text-align: left;
  margin-bottom: 2px;
}
table.dataTable tbody td a {
    padding-right: 5px;
}

</style>
<?php get_footer();?>