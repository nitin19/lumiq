<?php

/* Template Name: Edit-choir-profile */ 
if ( is_user_logged_in() ) {
  get_header();?> 
  <?php 

         $current_user_id   = get_current_user_id();
        $user_meta=get_userdata($current_user_id);
         $user_roles= $user_meta->roles[0];
         $userdata    =  get_userdata($current_user_id);
         $user_email= $userdata->user_email;
         $firstname = get_user_meta( $current_user_id, 'first_name','true' );
         $lastname = get_user_meta( $current_user_id, 'last_name','true' );
         $contact_number = get_user_meta( $current_user_id, 'contact_number','true' );
         $address = get_user_meta( $current_user_id, 'address','true' );
         $city = get_user_meta( $current_user_id, 'city','true' );
         $state = get_user_meta( $current_user_id, 'state','true' );
         $country = get_user_meta( $current_user_id, 'country','true' );
         $profile_image= get_user_meta( $current_user_id, 'image_url','true' );
?>
<div class="add_choir">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="add_choir_hd">
          <h1>Edit Profile </h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
      <form action="" name="choir-update" id="choir-update" method="post" enctype="multipart/form-data">
        <div class="image_uploader_sec">
        <div class="image_uploader_inersec">
          <div class="row">
            <div class="col-sm-5">
              <?php if($profile_image){ ?> 
                <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $profile_image; ?>" alt="profile_img" width="100%">
              <?php } else { ?>
              <div id="image_preview"><img id="previewing" src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" /></div>
              <?php } ?>
            </div>  
            <div class="col-sm-7">
              <h2>Upload Choir Image</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <input id="myInput" type="file" name="file" style="display: none;"/>
                <span title="attach file" class="attachFileSpan" onclick="document.getElementById('myInput').click()">
                Attach file
                </span>
                <p class="img_size">(jpeg, png less than 2 MB)</p>
            </div>
          </div>
          <div class="row mb-4 mt-4">
            <div class="col-sm-12">
                <input type="hidden" name="id" value="<?php echo $current_user_id;?>" id="id">
                <div class="form-group">
                  <label> First name * </label>
                  <input type="text" name="fname" value="<?php echo $firstname;?>" id="firstname" placeholder="First Name" class="custom_input">
                </div>
                <div class="form-group">
                  <label> Last name * </label>
                  <input type="text" name="lname" value="<?php echo $lastname;?>" id="lastname" placeholder="Last Name" class="custom_input">
                </div>
                <div class="form-group">
                  <label> Contact Number </label>
                  <input type="text" name="contactnumber" value="<?php echo $contact_number;?>" id="contact_number"  placeholder="1-888-652-2588" class="custom_input">
                </div>

                <div class="form-group">
                  <label> Email Address * </label>
                  <input type="email" name="email" value="<?php echo $user_email;?>" id="user_email"  class="custom_input" readonly>
                </div>
                <div class="form-group">
                  <label> Address </label>
                  <input type="text" name="address" value="<?php echo $address;?>" id="contact_number" placeholder="Enter your address" class="custom_input">
                </div>
                <div class="form-group">
                  <label> City </label>
                  <input type="text" name="city" value="<?php echo $city;?>" id="city" placeholder="Enter your city here" class="custom_input">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label> State  </label>
                      <input type="text" name="state" value="<?php echo $state;?>" id="state" placeholder="Enter State" class="custom_input">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label> Country </label>  
                      <input type="text" name="country" value="<?php echo $country;?>" id="country" placeholder="Enter Country Name" class="custom_input">
                    </div>
                  </div>
                </div>
                <div class="upload-gimg-msg"></div>
                <input class="red_btn" type="submit" name="submit" value="Update" id="post_submit" /><div id="loading"><img src="<?php echo site_url();?>/wp-content/uploads/2019/08/loading_new_cart_img.gif"></div>
                <input class="red_btn cancel_btn" type="button"  name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url();?>/choir-profile' " />
            </div>
          </div>
        </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- <script src="js/dropzone.js"></script>
 --><!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
 -->
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery(document).ajaxStart(function() {
      jQuery("#loading").css('display','inline-block');
    }).ajaxStop(function() {
      jQuery("#loading").hide();
    });
    jQuery.noConflict();
      jQuery.validator.addMethod("alphaUname", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
       });
      jQuery.validator.addMethod("pwcheck", function(value) {
       return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
       });
    //alert("one");
     jQuery("#choir-update").validate({
          rules: {
            fname: {
                        required: true,
                       // alphaUname: true
                   },
            lname: {
                        required: true,
                        //alphaUname: true
                   },
            /*
            contactnumber: {
                        required: true,
                        number: true,
                       maxlength: 11,
                       minlength: 11

                   }, 
            address: {
                        required: true
                        
                   },
            city: {
                        required: true,
                        alphaUname: true
                   },
            country: {
                        required: true,
                        alphaUname: true

                   }, 
            */                     
                  }, 
          messages: {
            fname: {
                        required: "Please Enter First name",
                       // alphaUname: "Please Enter Latters only"
                     },
            lname: {
                        required: "Please Enter Last name",
                       // alphaUname: "Please Enter Latters only"
                     }, 
            /*
            contactnumber: {
                        required: "Please Enter Contact Number",
                        number: "Please Enter Number only",
                        maxlength: "Please Enter  only 11 Numbers",
                        minlength: "Please Enter  only 11 Numbers"
                     }, 
              address: {
                        required: "Please Enter address ",
                        alphaUname: "Please Enter Latters only"
                     },
              city: {
                        required: "Please Enter City Name",
                        alphaUname: "Please Enter Latters only"
                     },  
              country: {
                        required: "Please Enter Country Name",
                        alphaUname: "Please Enter Latters only"
                     },  
              */                                 
                    },
          submitHandler: function(form){
             //form.submit();
            // var data = jQuery("#choir-update").serialize();
            var user_roles=  '<?php echo $user_roles;?>';
            var id = jQuery('#id').val();
            var firstname = jQuery('#firstname').val();
            var lastname = jQuery('#lastname').val();
            var contact_number = jQuery('#contact_number').val();
            var city = jQuery('#city').val();
            var state = jQuery('#state').val();
            var country = jQuery('#country').val();
            var image = jQuery('#myInput').val();
            var form = $('#choir-update').get(0);
            var fd = new FormData(form);
            jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_choir-edit.php',
                data : fd,
                processData: false,
                contentType: false, 

            beforeSend: function()
            {
               
                jQuery("#errorReg").fadeOut();
                jQuery(".btn-default").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sendin ...');
            },
            success :  function(data) {
              //alert(data);
            if(user_roles == 'choirmaster'){
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 1000);
              function popupPimg() {
                //window.location.assign("<?php //echo site_url();?>/choir-profile/");
              }
            } else {
              jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 1000);
              function popupPimg() {
                //window.location.assign("<?php //echo site_url();?>/singer-profile/");
              }
            }
              
              // else{
              //   alert('file not uploaded');
              //   jQuery("#response").html('<p>Sorry</p>');
              // }
            },
            // error: function(data){
            //     console.log("error");
            //     console.log(data);
            // },
          });
          }, 
      });
    });
</script>
<script type="text/javascript">
jQuery(function() {
jQuery("#myInput").change(function() {
//$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
jQuery('#previewing').attr('src','<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png');
//$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
jQuery("#myInput").css("color","green");
jQuery('#image_preview').css("display", "block");
jQuery('#previewing').attr('src', e.target.result);
jQuery('#previewing').attr('width', '250px');
jQuery('#previewing').attr('height', '230px');
};
</script>
<style type="text/css">
  #loading img {
    width: 25px;
    position: relative;
    top: 10px;
    left: -4%;
  }
  #post_submit {
    float: left;
    margin-right: 10px !important;
  }
  #loading { display: none; }
</style>
<?php
}
else
{
  wp_redirect('http://www.siing.io/?login');
}
get_footer();
?>
        