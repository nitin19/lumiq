<?php
/*Template Name: Albums Listing
*/
get_header();
?>
<?php 
$current_user = wp_get_current_user();
$current_user_id = $current_user->id;
    $defaults = array(
        'numberposts' => -1,
        'category'         => 0,
        'orderby'          => 'id',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'audio_mixer_files',
        'suppress_filters' => true,
        'author'           => $current_user_id,
    );
 
    $r = wp_parse_args( $args, $defaults );
    /*if ( empty( $r['post_status'] ) ) {
        $r['post_status'] = ( 'attachment' == $r['post_type'] ) ? 'inherit' : 'publish';
    }*/
    if ( ! empty( $r['numberposts'] ) && empty( $r['posts_per_page'] ) ) {
        $r['posts_per_page'] = $r['numberposts'];
    }
    /*if ( ! empty( $r['category'] ) ) {
        $r['cat'] = $r['category'];
    }*/
    /*if ( ! empty( $r['include'] ) ) {
        $incposts            = wp_parse_id_list( $r['include'] );
        $r['posts_per_page'] = count( $incposts );  // only the number of posts included
        $r['post__in']       = $incposts;
    } elseif ( ! empty( $r['exclude'] ) ) {
        $r['post__not_in'] = wp_parse_id_list( $r['exclude'] );
    }*/
 
    $r['ignore_sticky_posts'] = true;
    $r['no_found_rows']       = true;
 
    $get_posts = new WP_Query;
    $getalbumdata = $get_posts->query( $r );
?>
<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Albums
          <!-- <div class="serch_btn">
            <input type="search" name="" placeholder="Search albums...">
            <a href="<?php //echo site_url();?>/add-albums" class="pink_link">Add New Albums</a>
          </div> -->
        </h2>
        <table class="table mem_tbl display dataTable" id="albums_listing">
          <div class="serch_btn">
            <!-- <input type="search" name="" placeholder="Search albums..."> -->
            <a href="<?php echo site_url();?>/add-albums" class="pink_link">Add New Albums</a>
          </div>
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Id</h6>
              </th>
              <th>
                <h6 class="table_hd">Albums Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Choir Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Songs Count</h6>
              </th>
              <th>
                <h6 class="table_hd">Action</h6>
              </th>
              <!-- <th>
                <h6 class="table_hd">TOTAL</h6>
              </th>
              <th>
                <h6 class="table_hd"></h6>
              </th> -->
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($getalbumdata as $getalbumdatas) { 
              $album_id = $getalbumdatas->ID;
              $term_relationship_data = $wpdb->get_row("select * from wp_term_relationships where object_id='$album_id'");
              //echo "<pre>"; print_r($term_relationship_data); echo "</pre>";
              $group_id = $term_relationship_data->group_id;
              $group_data = $wpdb->get_row("select * from groups where id='$group_id' and status='1' and deleted='0'");
              $args = array(
                'orderby'          => 'id',
                'order'            => 'DESC',
                'post_type'        => 'attachment',
                'post_mime_type'   => 'audio/mpeg',
                'post_parent'      => $album_id,
                'post_status'      => 'inherit',
                'suppress_filters' => true,
              );
              $getsongsdata = get_posts( $args );
            ?>
            <tr>
              <td>
                <p>#<?php echo $getalbumdatas->ID;?><p>
              </td>
              <td>
                <p><?php echo $getalbumdatas->post_title;?></p>
              </td>
              <td>
                <p><?php echo $group_data->group_name;?></p>
              </td>
              <td>
                <p><?php echo count($getsongsdata);?></p>
              </td>
              <td><a href="<?php echo site_url();?>/songs-listings/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="View Album" class=""><i class="fa fas fa-eye" aria-hidden="true"></i></a>
                <a href="<?php echo site_url();?>/edit-album/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="Edit Album" class=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                <!--<a title="View Album" class="table_link view_btn"><i class="fa fa-times" aria-hidden="true"></i></a>--></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery('#albums_listing').dataTable( {
    "pagingType": "full_numbers"
  } );
} );
</script>
<style type="text/css">
#albums_listing_filter label input[aria-controls="albums_listing"] {
    border: 1px solid #ccc;
    border-radius: 23px;
    height: 35px;
}
table.dataTable tbody td a {
    padding-right: 20px;
}
#albums_listing_filter {
    margin-bottom: 20px;
}
#albums_listing_length label select {
    height: 33px;
    width: 69px;
    border: 1px solid #ccc;
}
.edit_btn {
    margin-right: 10px;
}
.no-footer i.fa.fa-pencil-square-o {
    color: #176fc8 !important; 
}
.no-footer i.fa.fas.fa-eye {
    color: #1a7648 !important;
}
</style>
<?php get_footer();?>