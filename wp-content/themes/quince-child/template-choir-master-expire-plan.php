<?php 
/* Template Name: Choir Master Expire Plan */
if(is_user_logged_in()) {
$backgroundimage = site_url().'/wp-content/uploads/2019/07/price_banner.jpg';
require_once('stripe/config.php');
$currentuser = get_current_user_id();
$payment_status = $_GET['status'];
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/style.css">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>-child/js/eskju.jquery.lazyloading.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<?php echo ot_get_option('tracking_code'); ?>
  <?php wp_head(); ?>

  <?php 
      $header_class = '';
      
      // Check if full width header selected
      if ( ot_get_option('layout_style') != 'boxed' && ot_get_option('header_style') == 'full-width' ){ 
        $header_class .= 'full-width';
      }
      
      // Check if overlay header selected
      if ( is_page() && ot_get_option('overlay_header') != 'off' && ( in_array( get_the_ID(), ot_get_option('overlay_header_pages', array()) ) ) ){ 
        $header_class .= ' overlay-header';
      }
    ?>
    
    <?php if( ot_get_option('top_bar', 'off') != 'off' ) {
      get_sidebar('top');
    } ?>
    <script>
      function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
      }
      window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
          var dropdowns = document.getElementsByClassName("dropdown-content");
          var i;
          for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
              openDropdown.classList.remove('show');
            }
          }
        }
      }
    </script>
    <header id="site-header" class="<?php echo trim($header_class); ?>" role="banner">
      <div id="header-wrapper">
        <div id="header-container" class="clearfix">
          <div id="site-logo" class="logo_img_main">
            <a href="javascript:void(0)"><img src="<?php echo site_url();?>/wp-content/uploads/2019/07/logo.png" alt="Lumiiq" style="max-height: 90px; width: auto;"></a>
          </div>
          
          <nav id="site-navigation" role="navigation">
            <?php 
                $current_user = wp_get_current_user();
                //echo "<pre>"; print_r($current_user); echo "</pre>";
                $user_meta=get_userdata($current_user);
                $user_roles= $user_meta->roles[0];
                $img_name = get_user_meta($current_user->id, image_url, true);
               // echo "<pre>"; print_r($current_user->data->ID); echo "</pre>";
                ?>
              <div class="dropdown">
                
              <?php if($img_name) { ?>
                <img onclick="myFunction()" class="dropbtn" src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $img_name; ?>">
              <?php } else { ?>
                <img onclick="myFunction()" class="dropbtn" src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png">
              <?php } ?>
                <span class="user_name"><?php printf(esc_html( $current_user->display_name ) ); ?></span>
                <div id="myDropdown" class="dropdown-content">
                  <!--<a href="<?php //echo wp_logout_url( home_url() );?>" id="wp-submit">Logout</a>-->
                    <button type="button" name="logout" value="<?php echo $current_user->id; ?>" id="logout-btn"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</button>
                </div>
              </div>
            <?php if( class_exists( 'WooCommerce' ) && ot_get_option('cart_button') != 'off' ) : ?>
              <div class="header_cart_wrapper">
                <?php global $woocommerce; ?>
                <a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'quince' ); ?>" class="header_cart_link" >
                  <?php woocommerce_cart_button(); ?>
                </a>  
                <?php if( ot_get_option('cart_widget') != 'off' ) {
                  woocommerce_cart_widget();
                } ?>
              </div>
            <?php endif; ?>
            
            <?php if( ot_get_option('search_button') != 'off' ) : ?>
              <button id="trigger-header-search" class="search_button" type="button">
                <i class="fa fa-search"></i>
              </button>
            <?php endif; ?>         
          </nav><!-- #site-navigation -->
          
          <?php if( ot_get_option('search_button') != 'off' ) : ?>
            <div class="header-search">
              <?php get_search_form(); ?>
            </div>
          <?php endif; ?>
                
          <a href="#mobile-site-navigation" class="toggle-mobile-menu"><i class="fa fa-bars"></i></a>
        </div><!-- #header-container -->  
      </div><!-- #header-wrapper -->  
    </header>
<div class="price_banner" style="background: url(<?php echo $backgroundimage;?>)"> 
  <div class="container"> 
    <div class="row">
      <div class="col-sm-12"> 
        <h1>Pricing & Plans</h1>
        <p>Toutes les chansons mènent chez nous !</p>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="price_sec">
    <div class="price_left">
      <h2>Choir Master</h2>
      <img src="<?php echo site_url();?>/wp-content/uploads/2019/07/choir_master.png" class="price_img">
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
    </div>
    <div class="price_right">
      <h3>50/year</h3>
      <p>30 days trial, then 50€ per year, </p>
      <ul>
        <li>Advanced use of the Audio Player (can also upload, edit and categorize songs)</li>
        <li>Choir Messenger</li>
        <li>Direct Link with Singers (via push notifications and emails)</li>
        <li>Invite singers to join the choir</li>
      </ul>
      <h6>During the trial, singer doesn’t have access to the “<span style="color: #ff4c4f;">Download</span>”  features of the player.</h6>
      <?php if($payment_status == 'active'){ ?>
          <h2 class="payment_status_message">Thanks! You've subscribed to this plan.</h2>
      <?php } else { ?>
          <form action="<?php echo get_template_directory_uri(); ?>-child/stripe/choircharge.php" method="post" class="frmStripePayment">
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $currentuser;?>">
            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="<?php echo $stripe['publishable_key']; ?>"
              data-name="€50/year Plan"
              data-description="30 days trial, then €50/year"
              data-panel-label="Buy Now"
              data-label="Get Started"
              data-locale="auto">
            </script>
          </form>
      <?php } ?>
      <!-- <a href="#" class="gradient_btn">Select Plan</a> -->
    </div>
  </div>
  <div class="priceftr">
    <h4 class="priceftr_hd">Free from 10 paid registered singers in the choir</h4>
    <p class="priceftr_contant">Choir Master will be able to send simple invitation to singers to join his choir, and let them pay for their subscription, or prepaid invitation which will be free for the singer, and discounted when bought by the choir master (20€ per year)</p>
  </div>
</div>
<?php  } else {
  wp_redirect( site_url() );
}
?>
<style type="text/css">
  .price_banner {
  background-image: url("../images/price_banner.jpg");
  background-size: cover !important;
  background-position: center !important;;
  background-repeat: no-repeat !important;;
  height:60vh;
  display: flex;
  align-items: center;
}
.price_sec {
    background-color: #ffffff;
    box-shadow: 0px 10px 20px #ccc;
    padding: 50px 0px 0px 0px;
    margin-top: -100px;
    float: left;
    width: 100%;
    display: flex;
    flex-wrap: wrap;  
    border-radius: 30px;
}
.price_banner h1 {
    font-size: 60px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 600;
    text-align: center;
}
.price_banner p {
    font-size: 23px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 600;
    text-align: center;
}
.priceftr {
    padding: 30px 0px;
    clear: both;
}
.priceftr_contant {
    font-size: 20px;
    color: #878787;
    font-family: "Roboto";
    font-weight: 400;
    text-align: center;
    max-width: 800px;
    margin:0 auto;
}
.priceftr_hd {
    font-size: 30px;
    color: #168bb1;
    font-family: "Raleway";
    font-weight: 500;
    text-align: center;
}
.price_left h2 {
    font-size: 40px;
    color: #fe5558;
    font-family: "Raleway";
    font-weight: 700;
}
.price_left p {
  font-size: 14px;
  color: #999999;
  font-family: "Roboto";
  font-weight: 400; 
  margin-top: 30px;
}
.price_left {
    width: 300px;
    float: left;
    padding: 20px;
}
.price_right {
    width: calc(100% - 300px);
    padding-left: 20px;
    background-color: #303030;
    padding: 80px;
    border-radius: 10px 0px 30px 0px;
}
.price_right h3 {
    font-size: 81px;
    color: #ffffff;
    font-family: 'Maven Pro', sans-serif;
    position: relative;
}
.price_right h3:before {
    content: "€";
    font-size: 50px;
    position: absolute;
    left: -30px;
    font-weight: lighter;
}
.price_right p {
    font-size: 22px;
    color: #ff4c4f;
    font-family: "Roboto";
    font-weight: 500;
}
.price_right ul {
    list-style: none;
    padding: 0px;
    margin-left: 0px;
}
.price_right ul li {
    font-size: 17px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 400;
    padding: 5px 0px;
}
.price_right h6 {
    font-size: 17px;
    color: #20c0ee;
    font-family: "Roboto";
    font-weight: 400;
    max-width: 540px;
}
.stripe-button-el {
    background-image: linear-gradient(to right , #176fc8, #176fc8 39%, #ff5558 66%, #ff5558);
    padding: 8px 20px;
    font-size: 18px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 500;
    text-align: center;
    margin-top: 30px;
    display: inline-block;
}
.price_right ul li:before {
    content: "";
    background-image: url("<?php echo site_url();?>/wp-content/uploads/2019/07/tick.png");
    height: 20px;
    width: 30px;
    display: inline-block;
    background-repeat: no-repeat;
    vertical-align: middle;
}
.price_img {width: 100%;}
@media (max-width:1024px) {
.price_right {padding: 40px;}
.price_right h3 {font-size: 50px;}
}
@media (max-width:768px) {
  .price_left {width: 100%;}
  .price_right {width: 100%;}
  .price_left {text-align: center;}
  .price_img {width: auto;}
  .price_sec {padding: 20px 0px 0px 0px;}
}
@media (max-width:767px) {
  .price_banner h1 {font-size: 35px;}  
  .price_banner p {font-size: 16px;}
  .price_right h3 {font-size: 50px;}
  .price_right {padding: 20px;}
  .price_right h3:before {font-size: 30px;left: -15px;}
  .price_left h2 {font-size: 30px;}
  .priceftr_hd {font-size: 22px;}
  .priceftr_contant {font-size: 14px;}
  .price_right p {font-size: 18px;}
  .price_right ul li {font-size: 14px;}
  .price_right h6 {font-size: 14px;}
  .gradient_btn {font-size: 14px;margin-top: 20px;}
}
@media (max-width:425px) {
  .price_img {width: 100%;}
  .priceftr_hd {font-size: 20px;}
}
.frmStripePayment {
    /*border: #E0E0E0 1px solid;*/
    /*padding: 20px 30px;*/
    width: 180px;
    text-align: center;
    /*background: #ececec;*/
    /*margin: 60px auto;*/
    font-family: Arial;
}
.stripe-button-el span {
    background: none;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.plan-caption {
    margin-bottom: 30px;
    font-size: 1.2em;
    width: 180px;
}
.payment_status_message {
    color: #fff;
    padding-top: 20px;
}
#logout-btn {
    width: 100%;
    height: auto;
    background-color: transparent;
    text-align: left;
    padding-left: 18px;
    color: #000;
    font-weight: 400;
    text-transform: capitalize;
    font-size: 14px;
    padding-top: 8px;
  padding-bottom: 9px;
}
#logout-btn:hover {
    background-color: #f8f6f6;
    color: #ed2939;
}
.header-sticked nav#site-navigation .dropdown {
    margin-top: -18px;
}
.header-sticked nav#site-navigation .login_btn {
    top: 16px;
}
</style>