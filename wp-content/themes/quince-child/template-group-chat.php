<style class="cp-pen-styles">
.page-header {
    display: none;
}
#frame {
  width: 100%;
  min-width: 360px;
  max-width: 100%;
  height: 85vh;
  min-height: 300px;
  max-height: 720px;
  background: #e0f5fe;
}
/*div#chat-section {
  margin-bottom: 50px;
}*/
.footer_bot {
    position: relative;
    float: left;
    width: 100%;
}
i.fa.fa-file-image-o {
    background-image: linear-gradient(90deg, #1dc8e3 15%, #1dd2d5 45%, #1de5ba 75%);
    border-radius: 50%;
    width: 40px;
    height: 40px;
    line-height: 40px;
    color: #fff;
    font-size: 20px;
}
.gallery_attachment {
    position: absolute;
    width: 15%;
    right: 0;
    text-align: center;
    top:-43px;
}
li.replies span {
    float: right;
    width: 70px;
    overflow: hidden;
}
.person-msg {
  max-width: 600px;
  float: right;
}

@media screen and (max-width: 360px) {
  #frame {
    width: 100%;
    height: 100vh;
  }
}
#frame #sidepanel {
  float: left;
  min-width: 280px;
  max-width: 340px;
  width: 40%;
  height: 100%;
  background: #fdfdfd;
  color: #f5f5f5;
  overflow: hidden;
  position: relative;
}
@media screen and (max-width: 767px) {
  #frame #sidepanel {
    width: 100%;
    min-width: 100%;
    height: auto;
  }
}
#frame #sidepanel #profile .wrap #status-options.active {
  opacity: 1;
  visibility: visible;
  margin: 75px 0 0 0;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options.active {
    margin-top: 62px;
  }
}
#frame #sidepanel #profile .wrap #status-options:before {
  content: '';
  position: absolute;
  width: 0;
  height: 0;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;
  border-bottom: 8px solid #435f7a;
  margin: -8px 0 0 24px;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options:before {
    margin-left: 23px;
  }
}
#frame #sidepanel #profile .wrap #status-options ul {
  overflow: hidden;
  border-radius: 6px;
}
#frame #sidepanel #profile .wrap #status-options ul li {
  padding: 15px 0 30px 18px;
  display: block;
  cursor: pointer;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options ul li {
    padding: 15px 0 35px 22px;
  }
}
#frame #sidepanel #profile .wrap #status-options ul li:hover {
  background: #496886;
}
#frame #sidepanel #profile .wrap #status-options ul li span.status-circle {
  position: absolute;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  margin: 5px 0 0 0;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options ul li span.status-circle {
    width: 14px;
    height: 14px;
  }
}
#frame #sidepanel #profile .wrap #status-options ul li span.status-circle:before {
  content: '';
  position: absolute;
  width: 14px;
  height: 14px;
  margin: -3px 0 0 -3px;
  background: transparent;
  border-radius: 50%;
  z-index: 0;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options ul li span.status-circle:before {
    height: 18px;
    width: 18px;
  }
}
#frame #sidepanel #profile .wrap #status-options ul li p {
  padding-left: 12px;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #profile .wrap #status-options ul li p {
    display: none;
  }
}
#frame #sidepanel #profile .wrap #status-options ul li#status-online span.status-circle {
  background: #2ecc71;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-online.active span.status-circle:before {
  border: 1px solid #2ecc71;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-away span.status-circle {
  background: #f1c40f;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-away.active span.status-circle:before {
  border: 1px solid #f1c40f;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-busy span.status-circle {
  background: #e74c3c;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-busy.active span.status-circle:before {
  border: 1px solid #e74c3c;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-offline span.status-circle {
  background: #95a5a6;
}
#frame #sidepanel #profile .wrap #status-options ul li#status-offline.active span.status-circle:before {
  border: 1px solid #95a5a6;
}
#frame #sidepanel #profile .wrap #expanded {
  padding: 100px 0 0 0;
  display: block;
  line-height: initial !important;
}
#frame #sidepanel #profile .wrap #expanded label {
  float: left;
  clear: both;
  margin: 0 8px 5px 0;
  padding: 5px 0;
}
#frame #sidepanel #profile .wrap #expanded input {
  border: none;
  margin-bottom: 6px;
  background: #32465a;
  border-radius: 3px;
  color: #f5f5f5;
  padding: 7px;
  width: calc(100% - 43px);
}
#frame #sidepanel #profile .wrap #expanded input:focus {
  outline: none;
  background: #435f7a;
}
#frame #sidepanel #search {
  border-top: 1px solid #32465a;
  border-bottom: 1px solid #32465a;
  font-weight: 300;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #search {
    display: none;
  }
}
#frame #sidepanel #search label {
  position: absolute;
  margin: 10px 0 0 20px;
}
#frame #sidepanel #search input {
  font-family: "proxima-nova",  "Source Sans Pro", sans-serif;
  padding: 10px 0 10px 46px;
  width: 100%;
  border: none;
  background: #32465a;
  color: #f5f5f5;
}
#frame #sidepanel #search input:focus {
  outline: none;
  background: #435f7a;
}
#frame #sidepanel #search input::-webkit-input-placeholder {
  color: #f5f5f5;
}
#frame #sidepanel #search input::-moz-placeholder {
  color: #f5f5f5;
}
#frame #sidepanel #search input:-ms-input-placeholder {
  color: #f5f5f5;
}
#frame #sidepanel #search input:-moz-placeholder {
  color: #f5f5f5;
}
#frame #sidepanel #contacts {
  height: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  padding: 0px 0px;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #contacts {
    height: calc(100% - 149px);
    overflow-y: scroll;
    overflow-x: hidden;
  }
  #frame #sidepanel #contacts::-webkit-scrollbar {
    display: none;
  }
}
#frame #sidepanel #contacts.expanded {
  height: calc(100% - 334px);
}
#frame #sidepanel #contacts::-webkit-scrollbar {
  width: 8px;
  background: #2c3e50;
}
#frame #sidepanel #contacts::-webkit-scrollbar-thumb {
  background-color: #243140;
}
#frame #sidepanel #contacts ul li.contact {
  position: relative;
  padding: 10px 0 15px 0;
  font-size: 0.9em;
  cursor: pointer;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #contacts ul li.contact {
    padding: 6px 0 46px 8px;
  }
}
#frame #sidepanel #contacts ul li.contact:hover {
  background: #32465a;
}
#frame #sidepanel #contacts ul li.contact.active {
  background: #32465a;
  border-right: 5px solid #435f7a;
}
#frame #sidepanel #contacts ul li.contact.active span.contact-status {
  border: 2px solid #32465a !important;
}
#frame #sidepanel #contacts ul li.contact .wrap {
  width: 88%;
  margin: 0 auto;
  position: relative;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #contacts ul li.contact .wrap {
    width: 100%;
  }
}
#frame #sidepanel #contacts ul li.contact .wrap span {
  position: absolute;
  left: 0;
  margin: -2px 0 0 -2px;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  border: 2px solid #2c3e50;
  background: #95a5a6;
}
#frame #sidepanel #contacts ul li.contact .wrap span.online {
  background: #2ecc71;
}
#frame #sidepanel #contacts ul li.contact .wrap span.away {
  background: #f1c40f;
}
#frame #sidepanel #contacts ul li.contact .wrap span.busy {
  background: #e74c3c;
}
#frame #sidepanel #contacts ul li.contact .wrap img {
  width: 40px;
  border-radius: 50%;
  float: left;
  margin-right: 10px;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #contacts ul li.contact .wrap img {
    margin-right: 0px;
  }
}
#frame #sidepanel #contacts ul li.contact .wrap .meta {
  padding: 5px 0 0 0;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #contacts ul li.contact .wrap .meta {
    display: none;
  }
}
#frame #sidepanel #contacts ul li.contact .wrap .meta .name {
  font-weight: 600;
}
#frame #sidepanel #contacts ul li.contact .wrap .meta .preview {
  margin: 5px 0 0 0;
  padding: 0 0 1px;
  font-weight: 400;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -moz-transition: 1s all ease;
  -o-transition: 1s all ease;
  -webkit-transition: 1s all ease;
  transition: 1s all ease;
}
#frame #sidepanel #contacts ul li.contact .wrap .meta .preview span {
  position: initial;
  border-radius: initial;
  background: none;
  border: none;
  padding: 0 2px 0 0;
  margin: 0 0 0 1px;
  opacity: .5;
}
#frame #sidepanel #bottom-bar {
  position: absolute;
  width: 100%;
  bottom: 0;
}
#frame #sidepanel #bottom-bar button {
  float: left;
  border: none;
  width: 50%;
  padding: 10px 0;
  background: #32465a;
  color: #f5f5f5;
  cursor: pointer;
  font-size: 0.85em;
  font-family: "proxima-nova",  "Source Sans Pro", sans-serif;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #bottom-bar button {
    float: none;
    width: 100%;
    padding: 15px 0;
  }
}
#frame #sidepanel #bottom-bar button:focus {
  outline: none;
}
#frame #sidepanel #bottom-bar button:nth-child(1) {
  border-right: 1px solid #2c3e50;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #bottom-bar button:nth-child(1) {
    border-right: none;
    border-bottom: 1px solid #2c3e50;
  }
}
#frame #sidepanel #bottom-bar button:hover {
  background: #435f7a;
}
#frame #sidepanel #bottom-bar button i {
  margin-right: 3px;
  font-size: 1em;
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #bottom-bar button i {
    font-size: 1.3em;
  }
}
@media screen and (max-width: 735px) {
  #frame #sidepanel #bottom-bar button span {
    display: none;
  }
}
#frame .content {
  float: right;
  width: 60%;
  height: 100%;
  overflow: hidden;
  position: relative;
}
@media screen and (max-width: 767px) {
  #frame .content {
    width: 100%;
    min-width: 100% !important;
  }
}
@media screen and (min-width: 900px) {
  #frame .content {
    width: calc(100% - 340px);
  }
}
#frame .content .contact-profile {
  width: 100%;
  height: 60px;
  line-height: 60px;
  background: #21c4e9;
    background-image: linear-gradient(90deg, #1dc8e3 15%, #1dd2d5 45%, #1de5ba 75%);
}
.display-name {color: #333333 !important;font-family: Raleway;font-weight: 600;letter-spacing: 1px;}
.login-detail img {
    border-radius: 50%;
    border:2px solid #ffffff;
    margin-right: 10px;
}
#frame .content .contact-profile img {
  width: 40px; 
  border-radius: 50%; 
  float: left;
  margin: 9px 12px 0 9px;
}

.online-status {
    left: 25px !important;
    bottom: 15px !important;
    border-radius: 50%;
    background-color: #15b315;
    width: 12px;
    height:12px;
}
.offline-status {
    left: 25px !important;
    bottom: 15px !important;
    border-radius: 50%;
    background-color: #ff0002;
    width: 12px;
    height:12px;
}
img.user_persnal_img {
  border-radius: 50%;
  height:45px;
  width: 45px;
}
.login-detail {
    
    border-top:1px solid #eeeeee;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 10px;
    
}
#frame .content .contact-profile p {
  float: left;
}
#frame .content .contact-profile .social-media {
  float: right;
}
#frame .content .contact-profile .social-media i {
  margin-left: 14px;
  cursor: pointer;
}
#frame .content .contact-profile .social-media i:nth-last-child(1) {
  margin-right: 20px;
}
#frame .content .contact-profile .social-media i:hover {
  color: #435f7a;
}
#frame .content .messages {
  height: auto;
  min-height: calc(100% - 110px);
  max-height: calc(100% - 120px);
  overflow-y: scroll;
  overflow-x: hidden;
  width: 100%;
}
div#contacts ul li {
    list-style: none;
}
@media screen and (max-width: 735px) {
  #frame .content .messages {
    max-height: calc(100% - 105px);
  }
}
#frame .content .messages::-webkit-scrollbar {
  width: 0px;
  background: transparent;
}
#frame .content .messages::-webkit-scrollbar-thumb {
  background-color: rgba(255, 255, 255, 1);
}
#frame .content .messages ul li {
  display: inline-block;
  clear: both; 
  float: left;
  width: calc(100% - 25px);
  font-size: 0.9em;
  margin-bottom: 20px;
}
#frame .content .messages ul li:nth-last-child(1) {
  margin-bottom: 20px;
}
/*#frame .content .messages ul li.sent img {
  margin: 6px 8px 0 0;
}*/
#frame .content .messages ul li.sent p {
    background: #ffffff;
    color: #333333;
    font-family: Raleway;
    letter-spacing: 1px;
    font-size: 13px;  
    font-weight: 500; 
    box-shadow: 0px 10px 10px #e4e4e4;
    margin-bottom: 0px;
    display: inline-block;
    padding: 17px 15px;
    border-radius: 0px 20px 0px 20px;
    line-height: 150%;
}
img.chat_user_persnal_img {
    border-radius: 50%;
    height: 45px;
    width: 45px;
    border: 2px solid #ffffff;
    margin-right: 10px;
}
#frame .content .messages ul li.replies img {
  float: right;
  margin: 6px 0 0 8px;
}
#frame .content .messages ul li.replies p {
    background: #f5f5f5;
    float: right;
    clear: both;
    background-image: linear-gradient(to bottom right, #1dc9e2 ,#1de5bd);
    color: #ffffff;
    font-family: Raleway;
    letter-spacing: 1px;
    font-size: 13px;
    font-weight: 500;
    box-shadow: 0px 10px 10px #e4e4e4;
    margin-bottom: 0px;
    display: inline-block;
    padding: 17px 15px;
    border-radius: 0px 20px 0px 20px;
    line-height: 150%;
    background-image: linear-gradient(90deg, #1dc8e3 15%, #1dd2d5 45%, #1de5ba 75%);
}
#frame .content .messages ul li img {
  /*width: 22px;
  border-radius: 50%;*/
  float: left;
}
#frame .content .messages ul li .sent-person-msg {
  max-width: 600px;
  float: left;
}
span.chat_time1 {
  display: block;
  color: #333333;
  font-family: Raleway;
  letter-spacing: 1px;
  font-size: 14px;
  font-weight: 400;
  margin-top: 15px;
  width: 100% !important;
  text-align: left;
}
span.chat_time {
  display: block;
  color: #333333;
  font-family: Raleway;
  letter-spacing: 1px;
  font-size: 14px;
  font-weight: 400;
  margin-top: 15px;
  width: 100% !important;
  text-align: right;
}
.sent-person-name {
  width: 70px;
  overflow: hidden;
  float: left;
}
@media screen and (min-width: 767px) {
  #frame .content .messages ul li .sent-person-msg {
    max-width: 600px;
  }
}
#frame .content .message-input {
  position: absolute;
  bottom: 0;
  width: 85%;
  z-index: 99;
  margin: 0px 20px;
}
#frame .content .message-input .wrap {
  position: relative;
}
#frame .content .message-input .wrap input {
  font-family: "proxima-nova",  "Source Sans Pro", sans-serif;
  float: left;
  border: none;
  width: 100%;
  padding: 11px 32px 10px 25px;
  font-size: 1em;
  color: #32465a;
  border-radius: 40px;
  letter-spacing: 1px;
}
@media screen and (max-width: 735px) {
  #frame .content .message-input .wrap input {
    padding: 15px 32px 16px 8px;
  }
}
#frame .content .message-input .wrap input:focus {
  outline: none;
}
#frame .content .message-input .wrap .attachment {
  position: absolute;
  right: 60px;
  z-index: 4;
  margin-top: 10px;
  font-size: 1.1em;
  color: #435f7a;
  opacity: .5;
  cursor: pointer;
}
@media screen and (max-width: 767px) {
  #frame .content .message-input .wrap .attachment {
    margin-top: 17px;
    right: 65px;
  }
}
#frame .content .message-input .wrap .attachment:hover {
  opacity: 1;
}
#frame .content .message-input .wrap button {
      float: right;
    border: none;
    width: 50px;
    padding: 12px 0;
    cursor: pointer;
    background: transparent;
    color: #1dc8e3;
    height: 43px;
    position: absolute;
    right: 0;
}
@media screen and (max-width: 767px) {
  #frame .content .message-input .wrap button {
    padding: 16px 0;
  }
}
/*#frame .content .message-input .wrap button:hover {
  background: #435f7a;
}*/
#frame .content .message-input .wrap button:focus {
  outline: none;
}
p.group-name {
    font-family: Raleway;
    font-weight: 600;
    letter-spacing: 1px;
    color: #ffffff !important;
    font-size: 22px;
}

</style>
<?php 
/* Template Name: Group Chat */
if ( is_user_logged_in() ) 
{
	get_header();
	$group_id = $_GET['group_id'];
  $getGroupdata = $wpdb->get_results("SELECT * FROM `groups` WHERE `id` =  $group_id AND `status`='1' AND `deleted`='0'");
  //echo "<pre>"; print_r($getGroupdata[0]->group_name); echo "</pre>";
  $groupName = $getGroupdata[0]->group_name;
  //echo "<pre>"; print_r($groupName); echo "</pre>";
  $groupImg = $getGroupdata[0]->group_img;
	$current_user = wp_get_current_user();
  //echo "<pre>"; print_r($current_user->data->login_status); echo "</pre>";
  $current_userId = get_current_user_id();
	$userRole = $current_user->roles['0'];
	if($userRole == "singer" || $userRole == "choirmaster")
	{
		?>
		<script>
			function getText()
			{
				var $msg_text = document.getElementById('text').value;
				var $current_groupId = document.getElementById('current-groupId').value;	
				var $current_userid = document.getElementById('current-userid').value;
				var $current_username = document.getElementById('current-username').value;
				xhr = new XMLHttpRequest();
				xhr.open('POST' , '<?php echo get_template_directory_uri(); ?>-child/chat/chatdb.php',true);
				xhr.setRequestHeader('content-type','application/x-www-form-urlencoded');
			xhr.send('chat='+$msg_text+'&current_userid='+$current_userid+'&current_username='+$current_username+'&current_groupId='+$current_groupId);
				xhr.onreadystatechange = function(){
					if (xhr.responseText)
					{
						// document.getElementById('chatarea').innerHTML = xhr.responseText;
					}
					document.getElementById('my-form').reset();
				}
			}
			function setText()
			{
				var $current_userid = document.getElementById('current-userid').value;
				var $current_groupId = document.getElementById('current-groupId').value;
				xhr = new XMLHttpRequest();
				xhr.open('POST' , '<?php echo get_template_directory_uri(); ?>-child/chat/chatFetch.php' , true);
				xhr.setRequestHeader('content-type','application/x-www-form-urlencoded');
				xhr.send('current_groupId='+$current_groupId+'&current_userid='+$current_userid);
				xhr.onreadystatechange = function(){
					//alert(xhr.responseText);
					document.getElementById('chat-message').innerHTML = xhr.responseText;	
				}	
			}
			setInterval("setText()",2000);
			//setText();
			setInterval("users()",2000);
			//users(); 
			function users()
			{
				var $current_groupId = document.getElementById('current-groupId').value;
				xhr1 = new XMLHttpRequest();
				xhr1.open('POST' , '<?php echo get_template_directory_uri(); ?>-child/chat/userFetch.php' , true);
				xhr1.setRequestHeader('content-type','application/x-www-form-urlencoded');
				xhr1.send('current_groupId='+$current_groupId);
				xhr1.onreadystatechange = function(){
					//alert(xhr.responseText);
					document.getElementById('loginperson').innerHTML = xhr1.responseText;
				}
			}
		</script>
    <div class="container">
		<div class="" id="chat-section">
			<div id="frame">
				<div id="sidepanel">
					<div id="contacts">
						<div id="loginperson">
              <div class=""><img src="" class="user_img"> Gourav Sharma <span class="online_dot"></span></div>      
            </div>
					</div>
				</div>
				<div class="content">
					<div class="contact-profile">
            <?php 
              if($groupImg) { ?>
                  <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $groupImg; ?>" alt="" style="height:45px;width:45px;">
              <?php }
              else { ?>
                  <img src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" alt="">
              <?php  } ?>
  						<p class="group-name" style="color: #ffffff;"><?= $groupName; ?></p>
					</div>
					<div class="messages" id="chatarea">
						<ul id="chat-message"></ul>
					</div>
          <div class=" footer_bot">
  					<div class="message-input">
  						<div class="wrap">
  						<form id="my-form" method="post">
  							<input type="hidden" name="group_id" id="current-groupId" value="<?php echo $group_id; ?>">
  							<input type="hidden" name="current_user" id="current-userid" value="<?php echo $current_user->id; ?>">
  							<input type="hidden" name="current_username" id="current-username" value="<?php echo $current_user->display_name; ?>">
  							<input twidth: 100%; type="text" id="text" placeholder="Write your message...">
  							<button type="button" id="send-msg" class="submit" onclick="getText()"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
  						</form>
  						</div>
  					</div>
            <div class="gallery_attachment">
              <i class="fa fa-file-image-o" aria-hidden="true"></i>
            </div>
          </div>
				</div>
			</div>
		</div>
  </div>
		<script>
      var log = $('#chatarea');
      log.animate({ scrollTop: log.prop('scrollHeight')}, 0);
      window.addEventListener('load',function(){
         //jump to bottom of the div
         var div = document.querySelector('#chatarea');
         div.scrollTop = div.scrollHeight;
      });
			$('html').keydown(function(e){
			  if(e.which==13){
				var $msg_text = document.getElementById('text').value;
			  	if($msg_text != '') {
			    	getText();
			  	}
			  }
			});
		</script>
		<?php
	}
	else
	{
		echo '<div class="alert alert-danger">Sorry You Do not have permissions to access this page.. </div>';
	}
get_footer();
} 
else 
{
   wp_redirect('http://www.siing.io/?login'); 
}
?>