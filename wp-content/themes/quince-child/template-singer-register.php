<?php 
/* Template Name: singer Register */ 
get_header();
$users = get_users(array(
    'meta_key'     => 'user_role',
    'meta_value'   => 'choir',
    'orderby' => 'ID',
    'order'   => 'ASC'

));
$varable_holding_img_url = site_url().'/wp-content/uploads/2019/07/06_choir_signup.jpg';
?>
<?php
  global $wpdb;
  $groupdata = $wpdb->get_results("select * from groups WHERE status= '1' AND deleted='0' " );
?>
<div class="choir_backimg" style="background-image: url(<?php echo $varable_holding_img_url; ?>);">
  <div class="container">
    <div class="choir_main">
        <div class="row">
          <div class="col-lg-6">
            <h6>Your Personal</h6>
            <h1>Soundtrack Everywhere</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
          </div>
          <div class="col-lg-6">
            <div class="sign_up">
              <h2>Sign Up</h2>
              <p>Create your account for free</p>
                <form name="singer-register" id="singer-register">
                  <input type="text" name="fname" placeholder="First Name">
                  <input type="text" name="lname" placeholder="Last Name">
                  <!--<input type="text" name="uname" placeholder="Username">-->
                  <input type="email" name="email" placeholder="Email Address">
                  <input type="password"  name="password" placeholder="Enter Password">
                  <select name="selectchoir" id="selectchoir">
                    <option value="default">Select Your Choir Group</option>
                    <?php foreach ( $groupdata as $group ) { ?>
                    <option value="<?php echo $group->id; ?>">
                      <?php echo $group->group_name;  ?> 
                      (<?php if($group->group_city){echo $group->group_city.',';} ?>
                      <?php if($group->group_state){echo $group->group_state.',';} ?>
                      <?php if($group->group_country){echo $group->group_country;} ?>)
                    </option>
                    <?php } ?>
                  </select> 
                  <select name="select_choir_cat" id="select_choir_cat">
                    <option value="default">Select Your Group Role</option>
                    <option value="musician">Musician</option>
                    <option value="sing">Sing</option>
                    <option value="drum">Drum</option>
                    <option value="piyano">Piyano</option>
                  </select> 
                  <input type="hidden" name="role" value="singer">
                  <input class="btn btn-default" type="submit" name="submit" value="Create Account" />
                </form>
                <div  style="background:green; color:#fff;" id="response" class="response-message">
                </div>
            </div>
          </div>
        </div>
    </div>  
  </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){
      jQuery.validator.addMethod("alphaUname", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
       });
      jQuery.validator.addMethod("pwcheck", function(value) {
       return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
       });
      jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
      }, "Value must not equal arg.");

      jQuery("#singer-register").validate({
          rules: {
            fname: {
                        required: true,
                        alphaUname: true
                   },
            lname: {
                        required: true,
                        alphaUname: true
                   },
           /* uname: {
                        required: true,
                        remote: {
                        url: '<?php //echo get_template_directory_uri(); ?>-child/aj_check_uname.php',
                        type: "POST",
                        }
                   },*/       
            email: {
                        required: true,
                        remote: {
                        url: '<?php echo get_template_directory_uri(); ?>-child/aj_check_email.php',
                        type: "POST",
                        }             
                   },
            password: {
                        required: true,
                        pwcheck: true,
                        minlength: 6              
                   },
            selectchoir: { 
                        valueNotEquals: "default" 
                      },
            select_choir_cat: { 
                        valueNotEquals: "default" 
                      }            
                    }, 
          messages: {
            fname: {
                        required: "Please Enter First name",
                        alphaUname: "Please Enter Latters only"
                     },
            lname: {
                        required: "Please Enter Last name",
                        alphaUname: "Please Enter Latters only"
                     },
            /*uname: {
                        required: "Please Enter Last name",
                        remote: "Username Allready Exist"
                    },*/         
            email: {
                        required: "Please Enter Email",
                        email: "Please Enter Valid Email",
                        remote: "Email Allready Exist"                     },
            password: {
                        required: "Please Enter Password",
                        pwcheck: "Bring alphabet (A-Z / a-z), tal (0-9) or at specialcharcter (!,%, &, @, #, $, ^, *,?, _, ~).",
                        minlength: "Please Enter Minimum 6 Character"
                     },
            selectchoir: {
                        valueNotEquals: "Please Select Your Choir"
                     },
            select_choir_cat: {
                        valueNotEquals: "Please Select Your Choir Category"
                     }                                             
                    },
          submitHandler: function(form) {
            var data = jQuery("#singer-register").serialize();
            jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_singer-register.php',
                data : data,
            beforeSend: function()
            {
                jQuery("#errorReg").fadeOut();
                jQuery(".btn-default").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sende ...');
            },
            success :  function(data) {
              //alert(data);
              jQuery("#response").html('<p>Thank you for filling in the form.We have sent you an email with information</p>');           
              setTimeout(4000);
                window.location.assign("<?php echo site_url();?>/login"); 
            },
          });
        },         
      });
    });
</script>
<style>
body {font-family: 'Raleway', sans-serif;}
.choir_backimg {
	background-size: cover;
	background-position: center;
	height: 100vh;
}
select.error {
    background-color: #fff !important;
}
.choir_backimg {
	padding-bottom: 40px;
}
.right-nav li {
    list-style: none;
}
.right-nav li a {
    font-size: 15px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 400;
    display: block;
}
.right-nav li a:first-child {
	font-size: 22px;
	color: #ffffff;
	font-family: "Roboto";
	font-weight: 500;
}
.choir_main h6 {
    font-size: 30px;
    color: #f6b12c;
    font-family: "Raleway";
    font-weight: 700;
    margin-bottom: 0px;
}
.choir_main h1 {
    font-size: 70px;
    color: #ffffff;
    font-family: "Raleway";
    font-weight: 700;
    line-height: 65px;
    padding-bottom: 20px;
}
.choir_main p {
    font-size: 16px;
	color: #ffffff;
	font-family: "Roboto";
	font-weight: 500;
}
.sign_up {
    background-color: #4b5073;
    opacity: 0.7;
    padding: 20px 30px 25px 30px;
}
.choir_main h2 {
    font-size: 30px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 500;
}
input.btn.btn-default {
font-size: 20px;
    color: #ffffff;
    font-family: "Roboto";
    font-weight: 500;
    background-color: #ff9933;
    width: 100%;
    border: 1px solid #ff9933;
    border-radius: 5px;
    box-shadow: 0px 5px 0px #e8790a;
}
.choir_main input {
    width: 100%;
    background-color: #fff;
    border: 1px solid #fff;
    border-radius: 5px;
    margin-bottom: 10px;
    height: 40px;
    padding: 0px 10px;
}
.choir_main select {
    width: 100%;
    background-color: #fff;
    border: 1px solid #fff;
    border-radius: 5px;
    margin-bottom: 10px;
    height: 40px;
    padding: 0px 10px;
}
.choir_main {
    padding-top: 35px;
    max-width: 1000px;
    margin:0 auto;
}
@media screen and (max-width: 1200px) {
	.choir_backimg {
	height:auto;
}
}
.tooltip {
    opacity: 1 !important;
}
</style>