<?php
/*Template Name: Singer Profile */

if ( is_user_logged_in() ) 
{
  get_header();
  if(isset($_GET['login']))

  {
      global $wpdb;
      $current_userId = get_current_user_id();
      $updateQuery = $wpdb->query("UPDATE wp_users SET login_status = '1' WHERE ID = ".$current_userId."");
  }
  ?>  
  <?php 
  $backgroundimage = site_url().'/wp-content/uploads/2019/07/profile_banner.jpg'; 
    $current_user_id   = get_current_user_id();
    $user_meta=get_userdata($current_user_id);
    $user_roles= $user_meta->roles[0];
    if($user_roles == 'singer'){
        global $wpdb;
        $userdata    =  get_userdata( $current_user_id );
        $user_email= $userdata->user_email;
        $display_name = ucwords($userdata->display_name);
        $firstname = get_user_meta( $current_user_id, 'first_name','true' );
        $lastname = get_user_meta( $current_user_id, 'last_name','true' );
        $contact_number = get_user_meta( $current_user_id, 'contact_number','true' );
        $address = get_user_meta( $current_user_id, 'address','true' );
        $city = get_user_meta( $current_user_id, 'city','true' );
        $state = get_user_meta( $current_user_id, 'state','true' );
        $country = get_user_meta( $current_user_id, 'country','true' );
        $profile_image= get_user_meta( $current_user_id, 'image_url','true' );
        $selected_choir_group_id = get_user_meta( $current_user_id, 'selected_choir','true' );

        $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id'");
        $group_all_data = $wpdb->get_results("select * from groups where id='$selected_choir_group_id'");
        $getpaymentdata = $wpdb->get_results("Select * from payment where user_id='$current_user_id' and status='1' and deleted='0' ORDER BY id desc");

        /*$args = array(
          'posts_per_page'   => 5,
          'offset'           => 0,
          'cat'         => '',
          'category_name'    => '',
          'orderby'          => 'id',
          'order'            => 'DESC',
          'include'          => '',
          'exclude'          => '',
          'meta_key'         => '',
          'meta_value'       => '',
          'post_type'        => 'attachment',
          'post_mime_type'   => 'audio/mpeg',
          'author'     => '',
          'author_name'    => '',
          'post_status'      => 'inherit',
          'suppress_filters' => true,
          'fields'           => '',
        );
        $gettrackdata = get_posts( $args );*/

        $defaults = array(
        'numberposts' => 5,
        'category'         => 0,
        'orderby'          => 'id',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'audio_mixer_files',
        'suppress_filters' => true,
        'author'           => 49,
    );
 
    $r = wp_parse_args( $args, $defaults );
    if ( ! empty( $r['numberposts'] ) && empty( $r['posts_per_page'] ) ) {
        $r['posts_per_page'] = $r['numberposts'];
    }
 
    $r['ignore_sticky_posts'] = true;
    $r['no_found_rows']       = true;
 
    $get_posts = new WP_Query;
    $getalbumdata = $get_posts->query( $r );
//echo "<pre>"; print_r($getalbumdata); echo "</pre>";
  ?>
  <div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
  <div class="profile_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12" style="display: flex;">
        <div class="profileimage">
            <?php if($profile_image){ ?> 
                <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $profile_image; ?>" alt="profile_img" width="100%">
            <?php } else {
                $profile_one= site_url().'/wp-content/uploads/2019/07/profile_one.png';?>
                <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
            <?php } ?>
          <!-- <img src="<?php //echo site_url();?>/wp-content/uploads/2019/07/profile_one.png" alt="profile_img" width="100%"> -->
        </div>
        <div class="choir_master">
          <div class="choir_master_iner">
            <a href="<?php echo site_url();?>/edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a>

            <h1><?php echo  $display_name; ?></h1>
            <p><?php echo  $address .' '. $city .' '. $state .' '.  $country; ?></p>
            <div class="row">
              <div class="col-sm"> 
                <!-- <h3>Contact Number</h3>  -->
                <span class="phone_no"><i class="fa fa-phone" aria-hidden="true"></i><?php echo $contact_number; ?></span>
              </div>
              <div class="col-sm">
                <!-- <h3>Email Address</h3> -->
                <span class="phone_no"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo $user_email;?></span>
              </div>
              <div class="col-sm">
                <!-- <h3>My Choirs</h3> -->
                <span class="phone_no"><i class="fa fa-user" aria-hidden="true"></i><?php echo $group_data->group_name;?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="choir_list">
      <div class="row">
        <div class="col-sm-12">
          <h2>Choirs List 
            <!--<div class="serch_btn">
              <input type="search" name="" placeholder="Search choir"> 
              <a href="#" class="pink_link">Add Choir</a> 
            </div>-->
          </h2>
          <table class="table choir_tbl">
            <tbody>
              <?php foreach ($group_all_data as $group_all_datas) {

            $string = strip_tags($group_all_datas->group_desc);
            if (strlen($string) > 150) { 
            // truncate string
            $stringCut = substr($string, 0, 150);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...'; 
            }
               ?>
              <tr>
                <td class="tbl-td-one">
                  <?php if($group_all_datas->group_img!=''){ ?>
                  <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $group_all_datas->group_img;?>" alt="choir_list" class="ch_list" style="width: 400px;"> 
                  <?php } else { ?>
                  <img src="images/choir_list1.png" alt="choir_list" class="ch_list">
                  <?php } ?>
                </td>
                <td class="tbl-td-two">
                  <h6 class="table_hd"><?php echo $group_all_datas->group_name;?></h6>
                  <p class="table_text"><?php echo $string;?></p>
                </td>
                <td class="tbl-td-three">
                  <h6 class="table_loctn">Location</h6>
                  <p class="table_text"><?php echo $group_all_datas->group_city;?>, <?php echo $group_all_datas->group_state;?>, <?php echo $group_all_datas->group_country;?></p>
                </td>
                <td class="tbl-td-four"><a title="View Choir" href="<?php echo site_url().'/group?'?>group_id=<?php echo $group_all_datas->id;?>" class="table_link"><i class="fa fas fa-eye" aria-hidden="true"></i> </a>
                </td>
                <!-- <td><a href="#" class="table_link"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Detail </a></td> -->
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Latest Uploaded Albums
        </h2>
        <table class="table mem_tbl display dataTable" id="albums_listing">
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Id</h6>
              </th>
              <th>
                <h6 class="table_hd">Albums Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Choir Name</h6>
              </th>
              <th>
                <h6 class="table_hd">Songs Count</h6>
              </th>
              <th>
                <h6 class="table_hd">Action</h6>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($getalbumdata as $getalbumdatas) { 
              $album_id = $getalbumdatas->ID;
              $term_relationship_data = $wpdb->get_row("select * from wp_term_relationships where object_id='$album_id'");
              //echo "<pre>"; print_r($term_relationship_data); echo "</pre>";
              $group_id = $term_relationship_data->group_id;
              $group_data = $wpdb->get_row("select * from groups where id='$group_id' and status='1' and deleted='0'");
              $args = array(
                'orderby'          => 'id',
                'order'            => 'DESC',
                'post_type'        => 'attachment',
                'post_mime_type'   => 'audio/mpeg',
                'post_parent'      => $album_id,
                'post_status'      => 'inherit',
                'suppress_filters' => true,
              );
              $getsongsdata = get_posts( $args );
            ?>
            <tr>
              <td>
                <p>#<?php echo $getalbumdatas->ID;?><p>
              </td>
              <td>
                <p><?php echo $getalbumdatas->post_title;?></p>
              </td>
              <td>
                <p><?php echo $group_data->group_name;?></p>
              </td>
              <td>
                <p><?php echo count($getsongsdata);?></p>
              </td>
              <td class="album-action-btn"><a href="<?php echo site_url();?>/songs-listings/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="View Album" class="album_btn"><i class="fa fas fa-eye" aria-hidden="true"></i></a>
                <a href="<?php echo site_url();?>/edit-album/?post_id=<?php echo $getalbumdatas->ID;?>&user_id=<?php echo $getalbumdatas->post_author;?>" title="Edit Album" class="album_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
  <?php 
  $events_data = $wpdb->get_results("SELECT * FROM `events` WHERE group_id = '$selected_choir_group_id' AND `status`='1' AND `deleted`='0' ");
  ?>
  <div class="container">
    <div class="choir_list">
      <div class="row">
        <div class="col-sm-12">
          <h2>Events List</h2>
          <table class="table event_tbl">
            <tbody>
               <div class="upload-gimg-msg"></div>
        <?php 
        foreach ($events_data as $events_value) {
        $events_id=$events_value->id;
        $group_id=$events_value->group_id;
        $events_name = $events_value->events_name;
        $events_dec = $events_value->events_dec;
        $string = strip_tags($events_dec);
        if (strlen($string) > 150) { 
        // truncate string
        $stringCut = substr($string, 0, 150);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...'; 
        }
        $events_city = $events_value->events_city;
        $events_state = $events_value->events_state;
        $events_country = $events_value->events_country;
        $events_image = explode(',',$events_value->image);
      //echo "<pre>"; print_r($events_image); echo "</pre>";
        ?>
                <tr>
                  
                <td class="tbl-td-one">

              <?php
              if($events_image){ ?>
            <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $events_image['0'] ;?>" alt="choir_list" class="ch_list">
              <?php 
              } else {
                $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
              <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
              <?php } ?>
            </td>
            <td class="tbl-td-two">
              <h6 class="table_hd"><?php echo $events_name;?></h6>
              <p class="table_text"><?php echo $string;?></p>
            </td>
            <td class="tbl-td-three">
            <h6 class="table_loctn">Location</h6>
            <p class="table_text"><?php echo $events_city .' '. $events_state .' '. $events_country; ?></p>
          </td>
                <td class="tbl-td-four"><a href="<?php echo site_url().'/events?'?>events_id=<?php echo $events_id;?>&group_id=<?php echo $group_id;?>" class="table_link"><i class="fa fas fa-eye" aria-hidden="true"></i> </a></td> 
              </tr>
             <? } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


  <!-- <div class="container">
    <div class="choir_list mb-5">
      <div class="row">
        <div class="col-sm-12">
          <h2>Latest Uploaded Songs
            <div class="serch_btn">
              <!-- <input type="search" name="" placeholder="Search choir"> -->
              <!--<a href="#" class="pink_link">Join New</a>-->
            <!-- </div>
          </h2>
          <table class="table mem_tbl">
            <thead>
              <tr>
                <th>
                  <h6 class="table_hd">ID</h6>
                </th>
                <th>
                  <h6 class="table_hd">TRACK</h6>
                </th> -->
                <!-- <th>
                  <h6 class="table_hd">NEXT PAYMENT</h6>
                </th>
                <th>
                  <h6 class="table_hd">TOTAL</h6>
                </th>
                <th>
                  <h6 class="table_hd"></h6>
                </th> -->
              <!-- </tr>
            </thead>
            <tbody>
              <?php //foreach ($gettrackdata as $gettrackdatas) { ?>
              <tr>
                <td>
                  <p class="track_id">#<?php //echo $gettrackdatas->ID;?><p>
                </td>
                <td>
                  <div class='audioHolder'>
                    <audio controls src="<?php //echo $gettrackdatas->guid;?>"></audio>
                  </div>
                  <p class="track_name"><?php //echo $gettrackdatas->post_title;?></p>
                </td> -->
                <!-- <td>
                  <p>
                    <?php 
                     //echo $next_payment_date = date("jS F, Y",$getpaymentdatas->sub_current_period_end);
                     //echo $next_payment_data;
                    ?>
                  </p>
                </td>
                <td>
                  <p>
                    <?php //setlocale(LC_MONETARY, 'en_EUR');
                          //$payment_price = money_format('%i', $getpaymentdatas->amount/100);
                      //echo '€'.$payment_price;?>
                  </p>
                </td>
                <td><a href="<?php //echo site_url();?>/single-payment/?id=<?php //echo $getpaymentdatas->id;?>" class="table_link">View</a></td> -->
              <!--</tr>
              <?php// } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div> -->
  <?php } else {
        $singer_id= $_GET['singer_id'];
        global $wpdb;
        $userdata    =  get_userdata( $singer_id  );
        $user_email= $userdata->user_email;
        $display_name = ucwords($userdata->display_name);
        $firstname = get_user_meta( $singer_id , 'first_name','true' );
        $lastname = get_user_meta( $singer_id , 'last_name','true' );
        $contact_number = get_user_meta( $singer_id , 'contact_number','true' );
        $address = get_user_meta( $singer_id , 'address','true' );
        $city = get_user_meta( $singer_id , 'city','true' );
        $state = get_user_meta( $singer_id , 'state','true' );
        $country = get_user_meta( $singer_id , 'country','true' );
        $profile_image= get_user_meta( $singer_id , 'image_url','true' );
        $selected_choir_group_id = get_user_meta( $singer_id , 'selected_choir','true' );

        $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id'");
        $group_all_data = $wpdb->get_results("select * from groups where id='$selected_choir_group_id'");
  ?>
  <div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
  <div class="profile_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 profileimage">
            <?php if($profile_image){ ?> 
                <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $profile_image; ?>" alt="profile_img" width="100%">
            <?php } else {
                $profile_one= site_url().'/wp-content/uploads/2019/07/profile_one.png';?>
                <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
            <?php } ?>
          <!-- <img src="<?php //echo site_url();?>/wp-content/uploads/2019/07/profile_one.png" alt="profile_img" width="100%"> -->
        </div>
        <div class="col-sm-9 choir_master">
          <div class="choir_master_iner">
            <!-- <a href="<?php echo site_url();?>/choir-edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a> -->

            <h1><?php echo  $display_name; ?></h1>
            <p><?php echo  $address .' '. $city .' '. $state .' '.  $country; ?></p> 
            <div class="row">
              <div class="col-sm">
                <!-- <h3>Contact Number</h3> -->
                <div><span><i class="fa fa-phone" aria-hidden="true"></i></span> <a href="tel:<?php echo  $contact_number; ?>" class="phone_no"><?php echo $contact_number; ?></a></div>
              </div>
              <div class="col-sm">
                <!-- <h3>Email Address</h3> -->
                <div><span><i class="fa fa-envelope" aria-hidden="true"></i> </span> <a href="mailto:<?php echo $user_email;?>" class="phone_no"><?php echo $user_email;?></a></div>
              </div>
              <div class="col-sm">
                <!-- <h3>Choirs Group</h3> -->
                <div><span><i class="fa fa-user" aria-hidden="true"></i></span><?php echo $group_data->group_name;?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="choir_list">
      <div class="row">
        <div class="col-sm-12">
          <h2>Choirs List 
            <!--<div class="serch_btn">
              <input type="search" name="" placeholder="Search choir"> 
              <a href="#" class="pink_link">Add Choir</a> 
            </div>-->
          </h2>
          <table class="table">
            <tbody>
              <?php foreach ($group_all_data as $group_all_datas) { 
                    $string = strip_tags($group_all_datas->group_desc);
            if (strlen($string) > 150) { 
            // truncate string
            $stringCut = substr($string, 0, 150);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...'; 
            }

                ?>
              <tr>
                <td class="tbl-td-one">
                  <?php if($group_all_datas->group_img!=''){ ?>
                  <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $group_all_datas->group_img;?>" alt="choir_list" class="ch_list" style="width: 400px;"> 
                  <?php } else { ?>
                  <img src="images/choir_list1.png" alt="choir_list" class="ch_list">
                  <?php } ?>
                </td>
                <td class="tbl-td-two">
                  <h6 class="table_hd"><?php echo $group_all_datas->group_name;?></h6>
                  <p class="table_text"><?php echo $string?></p>
                </td>
                <td class="tbl-td-three">
                  <h6 class="table_loctn">Location</h6>
                  <p class="table_text"><?php echo $group_all_datas->group_city;?>, <?php echo $group_all_datas->group_state;?>, <?php echo $group_all_datas->group_country;?></p>
                </td>
               <!--  <td><a href="<?php echo site_url().'/group?'?>group_id=<?php echo $group_all_datas->id;?>" class="table_link" class="table_link">View </a></td> -->
                <!-- <td><a href="#" class="table_link"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Detail </a></td> -->
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php 
  } 
}
else {
  wp_redirect('http://www.siing.io/?login');
}
       ?>


<style type="text/css">
.profile_banner {
  background-size: cover !important;
  background-position: center !important;
  background-repeat: no-repeat !important;
  height:50vh;
}
.profile_sec {margin-top: -60px;}

.choir_master_iner {width: 100%;padding: 20px;}
.col-sm-9.choir_master {
    max-width: 73% !important;
    margin-left: 1% !important;
}
.ch_list {
    border: 5px solid #ffffff;
    box-shadow: 0px 0px 20px #ccc;
    border-radius: 3px;
}
.table_hd {
    font-size: 20px;
    color: #333333;
    font-family: "Raleway";
    font-weight: 700;
}

.table_loctn {
    font-size: 20px;
    color: #333333;
    font-family: "Raleway";
    font-weight: 700;
}
.table_link {
    font-size: 14px;
    color: #2a2a2a;
    font-family: "Roboto";
    font-weight: 500;
    text-decoration: none;
}
.table_link:hover {color:#2a2a2a;}
.pink_link {
    font-size: 18px;
    background: -webkit-linear-gradient(#ff2424 , #ff2424 );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-family: "Roboto";
    font-weight: 500;
}
.phone_no:hover, .phone_no:active, .phone_no:focus {
  font-size: 14px;
  color: #666666 !important;
}
.serch_btn input[type="search"] {
    width: 150px;
    border: 1px solid #fff;
    border-radius: 20px;
    height: 32px;
    margin-right: 10px;
}
.serch_btn {
    display: inline-flex;
    float: right;
    align-items: center;
}
.serch_btn input[type="search"]::-webkit-input-placeholder { 
    background: -webkit-linear-gradient(#9f9f9f , #9f9f9f );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-size: 16px;
    color: #9f9f9f;
    font-family: "Roboto";
    font-weight: 400;
    height:32px;
}

.serch_btn input[type="search"]:-ms-input-placeholder { 
    background: -webkit-linear-gradient(#9f9f9f , #9f9f9f );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-size: 16px;
    color: #9f9f9f;
    font-family: "Roboto";
    font-weight: 400;
    height:32px;
}

.serch_btn input[type="search"]::placeholder {
    background: -webkit-linear-gradient(#9f9f9f , #9f9f9f );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-size: 16px;
    color: #9f9f9f;
    font-family: "Roboto";
    font-weight: 400;
    height:32px;
}
.team_member_sec {
    background-image: url("../images/member_banner.jpg");
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    padding: 80px 0px;
}
.team_member_sec h2 {
    font-size: 40px;
    font-family: "Raleway";
    font-weight: 700;
    margin-bottom: 40px;
    text-align: center;
}
.team_member_sec h2 {
    background-image: linear-gradient(to right , #176fc8, #176fc8 33%, #ff5558 66%, gold);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-size: 40px;
    font-family: "Raleway";
    font-weight: 700;
    margin-bottom: 40px;
    text-align: center;
}
.team_member_sec h4 {
    font-size: 18px;
    color: #333333;
    font-family: "Raleway";
    font-weight: 700;
    margin-top: 15px;
}
.team_member_sec p {
    font-size: 14px;
    color: #797979;
    font-family: "Roboto";
    font-weight: 400;
    margin-bottom: 10px;
}
.team_member_sec a {
    font-size: 15px;
    color: #ff3333;
    font-family: "Roboto";
    font-weight: 500;
    display: block;
}
.latest_song {
    padding: 80px 0px;
}
.latest_song iframe {margin-bottom: 20px;}
.latest_song h2 {
    background-image: linear-gradient(to right , #176fc8, #176fc8 39%, #ff5558 66%, #ff5558);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-size: 40px;
    font-family: "Raleway"; 
    font-weight: 700;
    margin-bottom: 40px;
    text-align: center;
}
.col-md-3.profileimage img {
    min-height: 250px !important;
}
.serch_btn input[type="search"] {
    border: 1px solid #000;
}
.track_name{
    font-size: 12px;
}
.audioHolder audio {
  width: 100%;
}
.track_id{
  margin-bottom: 0px !important;
  padding-top: 20px;
}
.fa-eye {color: #1a7648;}
.choir_tbl a.table_link {
    padding-right: 15px;
}
</style>
<?php get_footer();?>