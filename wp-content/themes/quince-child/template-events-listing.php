<?php
/*Template Name: Events Listing
*/
get_header();
?>
<?php 
if ( is_user_logged_in() ) {
  $current_user_id   = get_current_user_id();
  $events_data = $wpdb->get_results("SELECT * FROM `events` WHERE `user_id` =  $current_user_id AND `status`='1' AND `deleted`='0' ORDER BY id DESC");

  ?>
<div class="container">
  <div class="choir_list">
    <div class="row">
      <div class="col-sm-12">
        <h2>Events
          <!-- <div class="serch_btn">
            <input type="search" name="" placeholder="Search albums...">
            <a href="<?php //echo site_url();?>/add-albums" class="pink_link">Add New Albums</a>
          </div> -->
        </h2>
        <table class="table mem_tbl display dataTable" id="albums_listing">
          <div class="serch_btn">
            <!-- <input type="search" name="" placeholder="Search albums..."> -->
            <!--<a href="<?php //echo site_url();?>/add-group-choir" class="pink_link">Add New Choir</a>-->
          </div>
          <thead>
            <tr>
              <th>
                <h6 class="table_hd">Event Image</h6>
              </th>
              <th>
                <h6 class="table_hd">Event Title / Description</h6>
              </th>
              <th>
                <h6 class="table_hd">Date Of Event</h6>
              </th>
              <th>
                <h6 class="table_hd">Location</h6>
              </th>
              <th>
                <h6 class="table_hd">Action</h6>
              </th>
            </tr>
          </thead>
          <tbody>
             <div class="upload-gimg-msg"></div>
      <?php 
          foreach ($events_data as $events_value) {
          $events_id=$events_value->id;
          $group_id=$events_value->group_id;
          $events_name = $events_value->events_name;
          $events_dec = $events_value->events_dec;
          $string = strip_tags($events_dec);
          if (strlen($string) > 150) { 
          // truncate string
          $stringCut = substr($string, 0, 150);
          $endPoint = strrpos($stringCut, ' ');

          //if the string doesn't contain any space then it will cut without word basis.
          $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
          $string .= '...'; 
          }
        $events_city = $events_value->events_city;
        $events_state = $events_value->events_state;
        $events_country = $events_value->events_country;
        $events_image = $events_value->image;
      ?>
              <tr>
                
              <td class="tbl-td-one">

            <?php
            if($events_image !=''){ 
              $beforeDot = explode(",", $events_image);
              $beforeDot = $beforeDot[0];
              ?>
          <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot ;?>" alt="choir_list" class="ch_list">
            <?php 
            } else {
              $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
            <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
            <?php } ?>
          </td>
          <td class="tbl-td-two">
            <h6 class="table_heading"><?php echo $events_name;?></h6>
            <p class="table_text"><?php echo $string;?></p>
          </td>
          <td class="tbl-td-date-event">
          <p class="table_text"><?php echo $events_value->events_startdate .' - '. $events_value->events_enddate; ?></p>
        </td>
          <td class="tbl-td-three">
          <p class="table_text"><?php echo $events_city .' , '. $events_state .' , '. $events_country; ?></p>
        </td>
        <td>
          <a href="<?php echo site_url().'/events?'?>events_id=<?php echo $events_id;?>&group_id=<?php echo $group_id;?>" title="View Event" class=""><i class="fa fas fa-eye" aria-hidden="true"></i> </a>
          <a href="<?php echo site_url().'/edit-events?'?>events_id=<?php echo $events_id;?>" class="" title="Edit Event"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
          <span id="<?php echo $events_id;?>" class="rem_even">
          <span class="remove_events_<?php echo $events_id;?>" id="<?php echo $events_id;?>" title="Remove Event"><i class="fa fa-times" aria-hidden="true"></i></span></span>
        </td>
            </tr>
           <? } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery('#albums_listing').dataTable( {
    "pagingType": "full_numbers"
  } );
} );
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
  jQuery('.rem_even').click(function(){
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){
  var remove_id = jQuery(this).find("span").attr('id');
  alert(remove_id);
  jQuery.ajax({
      type : 'POST',
      url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_events.php',
      data : {events_id: remove_id},
      cache: false, 
      success :  function(data) {
      jQuery(".upload-gimg-msg").html(data);
      setTimeout(popupPimg, 3000);
      function popupPimg() {
        window.location.assign("<?php echo site_url();?>/choir-profile");
      }
    },
  });
}else{
return false;
}
});
});
</script>
<style type="text/css">
#albums_listing_filter label input[aria-controls="albums_listing"] {
    border: 1px solid #ccc;
    border-radius: 23px;
    height: 35px;
}
table.dataTable tbody td a {
    padding-right: 5px;
}
.no-footer i.fa.fa-pencil-square-o {
    color: #176fc8 !important; 
}
i.fa.fa-times {
    color: red !important;
}
a.btn-danger i {
    color: #fff;
}
a.btn-danger {
    margin-top: 3px;
}
.table_heading {
  font-size: 16px;
  color: #333333;
  font-family: "Raleway";
  font-weight: 700;
  text-align: left;
}
#albums_listing_filter {
    margin-bottom: 20px;
}
#albums_listing_length label select {
    height: 33px;
    width: 69px;
    border: 1px solid #ccc;
}
.no-footer i.fa.fas.fa-eye {
    color: #1a7648 !important;
}
.edit_btn {
    margin-right: 10px;
}
.ch_list {
    width: 100%;
    height: auto;
}
.tbl-td-two {
    width: 36%;
}
span.table_link {
    background: #ff2424 !important;
    padding: 5px 11px 5px 11px !important;
    margin-top: 4px;
    border-radius: 3px !important;
}
.fa.fa-pencil-square-o, .fa.fas.fa-eye, .fa.fa-times{
  color: #fff !important;
}
</style>
<?php get_footer();?>