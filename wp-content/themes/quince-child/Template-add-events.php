<?php
/*Template Name: Add Events Choir
*/
get_header();
?>
<?php 
  if(is_user_logged_in()){
    $current_user = wp_get_current_user();
    $current_user_id=$current_user->ID;
    $user_meta=get_userdata($current_user_id);
         $user_roles= $user_meta->roles[0];
         if($user_roles == 'choirmaster'){
  global $wpdb;
  $groupdata = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id`= '$current_user_id' AND status= '1' AND deleted= '0' ");
?>
<div class="add_choir">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="add_choir_hd">
          <h1>Add Events</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
      </div>
    </div>
    <div class="row">
     <div class="col-sm-12">
      <div id="response" class="response-message"></div>
      <form action="" name="add_events" id="add_events" method="post" enctype="multipart/form-data">
        <div class="image_uploader_sec">
          <div class="image_uploader_inersec">
            <div class="row">
              <div class="col-sm-5">
                <?php $noimage= site_url().'/wp-content/uploads/2019/07/male.jpg';?>
                <div id="image_preview"><img id="previewing" src="<?php echo $noimage ;?>" /></div>
                <!-- <form action="/file-upload" class="dropzone" id="myId"> -->
                 <!--  <div class="fallback">
                    <input name="file" type="file"  multiple />
                  </div> -->
                <!-- </form> -->
              </div>  
              <div class="col-sm-7">
                <h2>Upload Event Image <span class="required_icon">*</span> </h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <!-- <input id="file" type="file" name="file" value="" /> -->
                <input type="file" name="photo[]" id="photo" value="" class="ss-field-width" multiple="true"/>
                <!-- <span title="attach file" class="attachFileSpan" onclick="document.getElementById('myInput').click()">
                Attach file
                </span> -->
                <p class="img_size">(jpeg, png, less than 2 MB)</p>
              </div>
            </div>
            <div class="row mb-4 mt-4">
              <div class="col-sm-12">
                  <input class="red_btn" type="hidden" name="user_id" value="<?php echo $current_user_id;?>" />
                  <div class="form-group">
                    <label> Event Name <span class="required_icon">*</span> </label>
                    <input type="text" name="events_name" id="events_name" value="" placeholder="Event Name" class="custom_input">
                  </div>
                  <div class="form-group">
                    <label> Description <span class="required_icon">*</span> </label>
                    <textarea name="events_desc" id="events_desc"  class="custom_input ckeditor" ></textarea>
                  </div>
                   <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Start Date </label>
                        <input type="text" name="event_start_date" id="event_start_date" class="custom_input" >
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> End Date </label>
                        <input type="text" name="event_end_date" id="event_end_date" class="custom_input">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                             <div class='col-sm-6'>
            <div class="form-group">
               <label> Start Time </label>
                <div class='input-group date' id='datetimepicker3'>
                   <input placeholder="Start time" type="text" name="event_start_time" id="event_start_time" class="custom_input" value="<?php echo $eventdata->event_start_time;?>">
                    <span class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </span>
                </div>
            </div>
        </div>
         <div class='col-sm-6'>
         <div class="form-group">
               <label> End Time </label>
                <div class='input-group date' id='datetimepicker4'>
                   <input placeholder="End time" type="text" name="event_end_time" id="event_end_time" class="custom_input" value="<?php echo $eventdata->event_end_time;?>">
                    <span class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </span>
                </div>
            </div>
        </div>
                
                  </div>
                  <div class="form-group">
                  <select name="selectchoir" id="selectchoir" class="custom_input">
                    <option>Select your Choir</option>
                    <?php foreach ( $groupdata as $group ) { 
                      $group_id = $group->id;
                      $user_query = new WP_User_Query(
                            array(
                                'meta_key'    => 'selected_choir',
                                'meta_value'  => $group_id,
                            )
                      );  
                      $singers = $user_query->get_results(); 
                      $singer_total_count = count($singers);
                      if($singer_total_count > 0){
                    ?>
                    <option value="<?php echo $group->id; ?>"><?php echo $group->group_name;  ?> (<?php echo $group->group_city;?>, <?php echo $group->group_state;?>, <?php echo $group->group_country;?>)</option>
                  <?php } else { ?>
                    <?php } } ?>
                  
                  </select> 
                </div>
                  <div class="form-group">
                    <label>City <span class="required_icon">*</span> </label>
                    <input type="text" name="events_city" id="events_city" value="" placeholder="Enter your city here" class="custom_input">
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> State </label>
                        <input type="text" name="events_state" id="events_state" value="" placeholder="Enter State" class="custom_input">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Country <span class="required_icon">*</span> </label>  
                        <input type="text" name="events_country" id="events_country" value="" placeholder="Enter Country Name" class="custom_input">
                      </div>
                    </div>
                  </div>
                  <div class="upload-gimg-msg"></div>

                  <input class="red_btn" type="submit" name="submit" value="Submit" id="post_submit"/><div id="loading"><img src="<?php echo site_url();?>/wp-content/uploads/2019/08/loading_new_cart_img.gif"></div>
                  <input class="red_btn cancel_btn" type="button"  name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url();?>/choir-profile' " />
                  <!-- <button type="submit" class="red_btn">Submit</button> -->
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php 
} 
else{
  echo '<div class="alert alert-danger">Sorry You Do not have permissions to access this page.. </div>';
}
}
else{
  echo '<div class="alert alert-danger">Sorry You Do not have permissions to access this page.. </div>';
}


?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script src="<?php echo get_template_directory_uri(); ?>-child/js/moment-with-locales.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/css/font-awesome.css">
        <link href="<?php echo get_template_directory_uri(); ?>-child/css/prettify-1.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>-child/css/base.css" rel="stylesheet">
<link rel="stylesheet"  href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap-clockpicker.css">
<link rel="stylesheet"  href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap-clockpicker.min.css">.
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap-clockpicker.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap-clockpicker.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/js/bootstrap.min.js"></script>  
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script> 
<script type="text/javascript" language="javascript">
  jQuery(document).ready(function(e){
    jQuery(document).ajaxStart(function() {
      jQuery("#loading").css('display','inline-block');
    }).ajaxStop(function() {
      jQuery("#loading").hide();
    });
  jQuery("#add_events").on('submit',(function(e) {
      e.preventDefault();
      var events_name = jQuery('#events_name').val();
      var events_desc = jQuery('#events_desc').val();
      var event_start_date = jQuery('#event_start_date').val();
      var event_end_date = jQuery('#event_end_date').val();
      var event_start_time = jQuery('#event_start_time').val();
      var event_end_time= jQuery('#event_end_time').val();
      var event_end_time= jQuery('#event_end_time').val();
      var selectchoir= jQuery('#selectchoir').val();
      var events_city = jQuery('#events_city').val();
      var events_state = jQuery('#events_state').val();
      var events_country = jQuery('#events_country').val();
      var photo = jQuery('#photo').val();
    
      if(events_name!='' && events_desc!='' && event_start_date!='' && event_end_date!='', event_start_time!='' && event_end_time!='' && selectchoir!='', events_city!='', events_state!='', events_country!='', photo!='') { 
      
        jQuery(document).ready(function(){    
        for(var events_desc in CKEDITOR.instances)
        CKEDITOR.instances[events_desc].updateElement();
          });
        jQuery.ajax({
          url: "<?php echo get_template_directory_uri(); ?>-child/aj_add_events.php",        
          type: "POST",            
          data: new FormData(this), 
          contentType: false,       
          cache: false,             
          processData:false,       
          success: function(data) {
            //alert(data);
            jQuery(".upload-gimg-msg").html(data);
            setTimeout(popupPimg, 2000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            }
        });
      } else {
        if(events_name ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events  Name!</div>');
        } else if (events_desc ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events Description!</div>');
        } 
        else if (event_start_date ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events Start Date !</div>');
        }
        else if (event_end_date ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events End Date !</div>');
        }
        else if (event_start_time ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events Start Time!</div>');
        }
        else if (event_end_time ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events End Time!</div>');

        }else if(events_state =''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events City Name!</div>');
        }
        else if(selectchoir =''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Groups!</div>');
        }
        else if(events_city ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events City Name!</div>');
        } else if(events_country ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Events Country Name!</div>');
        } else { 
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please fill the all required (*) fields first !</div>');
        }
      }
    }
   ));
  });
</script>


<script type="text/javascript">
jQuery(function() {
jQuery("#photo").change(function() {
//$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
var source='<?php echo $noimage?>';
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
jQuery('#previewing').attr('src', source);
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
jQuery("#photo").css("color","green");
jQuery('#image_preview').css("display", "block");
jQuery('#previewing').attr('src', e.target.result);
jQuery('#previewing').attr('width', '250px');
jQuery('#previewing').attr('height', '230px');
};
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
jQuery('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            jQuery('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
        <script>
   jQuery(document).ready(function(){
   jQuery( function() {
   jQuery( "#event_end_date" ).datepicker({ minDate: 0 , dateFormat: 'yy-mm-dd' });
   jQuery( "#event_start_date").datepicker({ minDate: 0 , dateFormat: 'yy-mm-dd' });
  });
});
  </script>
<style type="text/css">

  #group_choir_desc {
    height: 150px;
  }
  #loading img {
    width: 25px;
    position: relative;
    top: 10px;
    left: -4%;
  }
  #post_submit {
    float: left;
    margin-right: 10px !important;
  }
  #loading { display: none; }
</style>
<?php get_footer();?>