<?php
require_once('../../../wp-load.php');
$current_user_id = get_current_user_id();
global $wpdb;
$group_id = $_POST['group_select'];
if($_SERVER['REQUEST_METHOD'] == "POST"){
        $addalbum = wp_insert_post(
            array(
                'post_author'=>$current_user_id,
                'post_date' => date('Y-m-d h:m:s'),
                'post_date_gmt'=> date('Y-m-d h:m:s'),
                'post_title'=>$_POST['post_title'],
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name'=>strtolower($_POST['post_title']),
                'post_modified'=>'',
                'post_modified_gmt'=>'',
                'guid'=>'',
                'post_type'=>'audio_mixer_files' 
            )
        );
        if($addalbum){
            $album_id = $wpdb->insert_id;
            add_post_meta($album_id, '_edit_last', '1');
            add_post_meta($album_id, 'presets', true);
            add_post_meta($album_id, 'presetSettings', true);
            add_post_meta($album_id, 'subject_known', 'Your file is ready', true);
            add_post_meta($album_id, 'body_known', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'subject_general', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'body_general', '[[Link]] to download your file.', true);
            add_post_meta($album_id, 'subtitles_show', 'yes', true);
            add_post_meta($album_id, 'subtitles', 'NULL', true);
            add_post_meta($album_id, 'markers', '[{"start_time":"04:32","end_time":"05:25","content":"fdgfghfdg","color":"#ec6712"}]', true);
            add_post_meta($album_id, 'autoplay', 'no', true);
            add_post_meta($album_id, 'srtFile', '0', true);
            add_post_meta($album_id, 'srtOffset', '0', true);
            add_post_meta($album_id, 'downloadKnown', 'no', true);
            add_post_meta($album_id, 'slide_template', 'default', true);
            add_post_meta($album_id, 'eg_sources_html5_mp4', '', true);
            add_post_meta($album_id, 'eg_sources_html5_ogv', '', true);
            add_post_meta($album_id, 'eg_sources_html5_webm', '', true);
            add_post_meta($album_id, 'eg_sources_youtube', '', true);
            add_post_meta($album_id, 'eg_sources_vimeo', '', true);
            add_post_meta($album_id, 'eg_sources_wistia', '', true);
            add_post_meta($album_id, 'eg_sources_image', '', true);
            add_post_meta($album_id, 'eg_sources_iframe', '', true);
            add_post_meta($album_id, 'eg_sources_soundcloud', '', true);
            add_post_meta($album_id, 'eg_vimeo_ratio', '1', true);
            add_post_meta($album_id, 'eg_youtube_ratio', '1', true);
            add_post_meta($album_id, 'eg_wistia_ratio', '1', true);
            add_post_meta($album_id, 'eg_html5_ratio', '1', true);
            add_post_meta($album_id, 'eg_soundcloud_ratio', '1', true);
            add_post_meta($album_id, 'eg_sources_revslider', '', true);
            add_post_meta($album_id, 'eg_sources_essgrid', '', true);
            add_post_meta($album_id, 'eg_featured_grid', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_skin', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_element', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_setting', '', true);
            add_post_meta($album_id, 'eg_settings_custom_meta_style', '', true);
            add_post_meta($album_id, 'eg_custom_meta_216', 'true', true);
            add_post_meta($album_id, 'eg_votes_count', '0', true);
            $term_data = $wpdb->get_row("select * from wp_terms where group_id='$group_id'");
            $term_id = $term_data->term_id;
            $term_taxonomy_data = $wpdb->get_row("select * from wp_term_taxonomy where term_id='$term_id'");
            $term_taxonomy_id = $term_taxonomy_data->term_taxonomy_id;
            $wpdb->insert('wp_term_relationships', array(
                'object_id'=>$album_id,
                'term_taxonomy_id' => $term_taxonomy_id,
                'term_order'=> '0',
                'group_id'=>$group_id,
            ));
            echo $error =  '<div class="alert alert-success">Your Group Choir added successfully...!!</div>';
        } else {
            echo $error =  '<div class="alert alert-danger">Sorry fail to add your Album due to system error. Please try again.</div>';
        }
}