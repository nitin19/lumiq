<?php

/* Template Name: Group */ 

if ( is_user_logged_in() ) {
  get_header();
  $group_id=$_GET['group_id'];
  $current_user_id   = get_current_user_id();
  $user_meta=get_userdata($current_user_id);
  $user_roles= $user_meta->roles[0];
          

$backgroundimage = site_url().'/wp-content/uploads/2019/07/profile_banner.jpg';  
//$quary = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` =  $current_user_id ");
$groupdata = $wpdb->get_row("SELECT * FROM `groups` WHERE `id` =  $group_id AND `status`='1' AND `deleted`='0' ");
      $string = $groupdata->group_desc;
      if (strlen($string) > 190) { 
        // truncate string
        $stringCut = substr($string, 0, 190);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '<span id="dots">...</span>'; 
      }

      $events_data = $wpdb->get_results("SELECT * FROM `events` WHERE `group_id` =  $group_id AND `status`='1' AND `deleted`='0' ORDER BY id DESC");
?>
<div class="profile_banner" style="background:  url(<?php echo $backgroundimage; ?>)">  </div>
<div class="profile_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-3 profileimage">
        <?php 
          if($groupdata->group_img!=''){
          	?>
            <img src="<?php echo site_url();?>/wp-content/uploads/groupchoirimage/<?php echo $groupdata->group_img;?>">
            <?php 
          } else {
        $profile_one= site_url().'/wp-content/uploads/2019/07/male.jpg';?>
        <img src="<?php echo $profile_one; ?>" alt="profile_img" width="100%">
        <?php } ?>
        </div>
        <div class="col-md-9 choir_master">
        <div class="choir_master_iner">
        <small> Choir Name </small>
        <?php 
        if($user_roles == 'choirmaster'){ ?>
                  <a href="<?php echo site_url();?>/edit-group-choir/?group_id=<?php echo $group_id; ?>" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Choir </a>
                  <?php } ?><br>
                  <a href="<?php echo site_url().'/group-chat?'?>group_id=<?php echo $group_id;?>" class="table_link" style="float: right;"><i class="fa fa-users" aria-hidden="true"></i> Chat</a>
       <!--  <a href="<?php //echo site_url();?>/choir-edit-profile" class="table_link" style="float: right;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile </a>  -->
          <h1><?php echo $groupdata->group_name;?> </h1>
          <p><?php echo  $groupdata->group_city .' '. $groupdata->group_state .' '.  $groupdata->group_country; ?></p>
          <p><?php echo $string; ?></p>
          <!-- <div class="row">
            <div class="col-sm">
              <h3>Start Date</h3>
              <p><?php //echo  $groupdata->events_startdate; ?></p>
          </div>
          <div class="col-sm">
              <h3>End Date</h3>
              <p><?php //echo $groupdata->events_enddate; ?></p>
            </div>
            <div class="col-sm">
              <h3>Start Time</h3>
              <p><?php //echo  $groupdata->event_start_time; ?></p></div>
              <div class="col-sm">
              <h3>End Time</h3>
              <p><?php //echo  $groupdata->event_end_time; ?></p>
            </div>
            <!-- <div class="col-sm">
              <h3>Email Address</h3>
              <p><a href="mailto:<?php //echo $user_email ;?>" class="phone_no"><?php //echo $user_email ;?></a></p>
            </div> -->
            <div class="row">
            <div class="col-sm">
             <!--  <h3>Choirs Group</h3>
             <p><a href="#" class="phone_no"><?php //echo $groupdata->group_name;?></a></p> -->
 <!--<p><a href="<?php //echo site_url().'/group-chat?'?>group_id=<?php //echo $group_id;?>" class="table_link">Chat</a>
    </p>-->
            </div>
          </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  if(!empty($events_data)) { ?>
      <div class="container" id="event-list-section">
      <div class="choir_list">
        <div class="row">
          <div class="col-sm-12">
            <h2>Events List </h2>
            <table class="table event_tbl">
              <tbody>
                 <div class="upload-gimg-msg"></div>
          <?php 
          foreach ($events_data as $events_value) {
             //echo "<pre>";
             //print_r($events_value);
             //echo "</pre>";
              $events_id=$events_value->id;
              $group_id=$events_value->group_id;
              $events_name = $events_value->events_name;
              $events_dec = $events_value->events_dec;
              $string = strip_tags($events_dec);
              if (strlen($string) > 150) { 
              // truncate string
              $stringCut = substr($string, 0, 150);
              $endPoint = strrpos($stringCut, ' ');

              //if the string doesn't contain any space then it will cut without word basis.
              $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
              $string .= '...'; 
              }
              $events_city = $events_value->events_city;
              $events_state = $events_value->events_state;
              $events_country = $events_value->events_country;
              $events_image = $events_value->image;
              ?>
                  <tr>  
                  <td class="tbl-td-one">
                <?php
                if($events_image !=''){ 
                  $beforeDot = explode(",", $events_image);
                  $beforeDot = $beforeDot[0];
                  ?>
              <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot ;?>" alt="choir_list" class="ch_list">
                <?php 
                } else {
                  $choir_list1= site_url().'/wp-content/uploads/2019/07/choir_list1.png';?>
                <img src="<?php echo $choir_list1 ;?>" alt="choir_list" class="ch_list">
                <?php } ?>
              </td>
              <td class="tbl-td-two">
                <h6 class="table_hd"><?php echo $events_name;?></h6>
                <p class="table_text"><?php echo $string;?></p>
              </td>
              <td class="tbl-td-seven">
              <h6 class="table_loctn">Date of event</h6>
              <p class="table_text"><?php echo $events_value->events_startdate .' - '. $events_value->events_enddate; ?></p>
            </td>
              <td class="tbl-td-three">
              <h6 class="table_loctn">Location</h6>
              <p class="table_text"><?php echo $events_city .' , '. $events_state .' , '. $events_country; ?></p>
            </td>
                  <td class="tbl-td-four"><a href="<?php echo site_url().'/events?'?>events_id=<?php echo $events_id;?>&group_id=<?php echo $group_id;?>" title="View Event" class="table_link"><i class="fa fas fa-eye" aria-hidden="true"></i> </a></td>
                  <!--<td class="tbl-td-four"><a href="<?php //echo site_url().'/edit-events?'?>events_id=<?php //echo $events_id;?>" class="table_link" title="Edit Event"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                  <td class="tbl-td-six">-->
                    <!--<span id="<?php //echo $events_id;?>" class="rem_even">
                    <span class="table_link remove_events_<?php //echo $events_id;?>" id="<?php //echo $events_id;?>" title="Remove Event"><i class="fa fa-times" aria-hidden="true"></i></span></span></td>-->
                </tr>
               <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php } 
$user_query = new WP_User_Query(
    array(
      'meta_key'    => 'selected_choir',
      'meta_value'  => $group_id
    )
  );
$singers = $user_query->get_results();
//echo "<pre>"; print_r($singers); echo "</pre>";
if(!empty($singers)) { 
$backgroundmemimage = site_url().'/wp-content/uploads/2019/07/member_banner.jpg';
  ?>
    <div class="team_member_sec" style="background:  url(<?php echo $backgroundmemimage; ?>)">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
           <h2>Team Members</h2>
          </div>
        </div>
    <div class="row">
      <?php          
      foreach ($singers as $singer_users) {
        //echo "<pre>"; print_r($singer_users); echo "</pre>";
      $singer_id=  $singer_users->ID;
      $current_user_id   = get_current_user_id();
      $selected_choir_group_id = get_user_meta($singer_id, 'selected_choir','true' );
      $group_data = $wpdb->get_row("select * from groups where id='$selected_choir_group_id' and user_id='$current_user_id'");
      if($group_data->id==$selected_choir_group_id && $group_data->user_id == $current_user_id){
      $singer_id=  $singer_users->ID;
      $singer_first_name = ucwords(get_user_meta($singer_id, 'first_name','true' ));
      $singer_last_name = ucwords(get_user_meta($singer_id, 'last_name','true' ));
      $singer_category = get_user_meta($singer_id, 'selected_choir_cat','true');
      $singer_image = get_user_meta($singer_id, 'image_url','true');
      ?> 
      <div class="col-sm-2">
        <?php 
         if($singer_image) { ?> 
        <img src="<?php echo site_url();?>/wp-content/uploads/profileimage/<?php echo $singer_image; ?>" align="team_member" width="100%">
          <?php } else { ?>
        <img src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" alt="profile_img" width="100%">
         <?php  } ?>
        <h4><?php echo $singer_first_name .' '. $singer_last_name; ?></h4>
        <p><?php echo $singer_category ?></p>
        <a href="<?php echo site_url().'/singer-profile?'?>singer_id=<?php echo $singer_id;?>">View Detail</a>

      </div>
      <?php }  else {

      }
    }
?>
    </div>
  </div>
</div>
<?php } ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
       //alert("jquary working ");
  jQuery('.rem_even').click(function(){
    //alert("click working");
 var checkstr =  confirm('are you sure you want to delete this?');
if(checkstr == true){

  var remove_id=jQuery(this).find("span").attr('id');
  //var remove_id=jQuery(".remove_events_<?php //echo $events_id;?>").attr("id");
  //alert(remove_id);
  jQuery.ajax({
                type : 'POST',
                url  : '<?php echo get_template_directory_uri(); ?>-child/aj_remove_events.php',
                data : {events_id: remove_id},
                cache: false, 
            success :  function(data) {
              //alert(data);
             jQuery(".upload-gimg-msg").html(data);
             setTimeout(popupPimg, 3000);
              function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile");
              }
            },
          
          });
}else{
return false;
}
});
});
</script>
<style type="text/css">
  .profileimage {
    max-width: 24%; 
  }
  .member-profile {
    margin-bottom: 30px;
    min-height: 320px;
  }
  .profile_sec {
    padding-bottom: 60px;
  }
</style>
<?php
}
else
{
  wp_redirect('http://www.siing.io/?login');
}
get_footer();
?>