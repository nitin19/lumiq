<?php
/* 
	Template Name: Forgot Password 
*/
get_header();
?>
<?php $varable_holding_img_url = site_url().'/wp-content/uploads/2019/07/06_choir_signup.jpg'; ?>
  <div class="choir_backimg" style="background-image: url(<?php echo $varable_holding_img_url; ?>);">
      <div class="container">
        <div class="choir_main">
            <div class="row">
                <div class="col-lg-6">
                  <h6>Your Personal</h6>
                  <h1>Soundtrack Everywhere</h1>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="col-lg-6">
                  <div class="sign_up">
                      <h2 class="text-center">Forgot Password</h2>
						<div id="password-lost-form" class="widecolumn">
							<?php if ( $attributes['show_title'] ) : ?>
							<h3><?php _e( 'Forgot Your Password?', 'personalize-login' ); ?></h3>
							<?php endif; ?>

							<p class="text-center">
							<?php
							_e(
							"Enter your email address and we'll send you a link you can use to pick a new password.",
							'personalize_login'
							);
							?>
							</p>
							<form id="lostpasswordform" action="" method="post">
								<div class="upload-gimg-msg"></div>
								<p class="form-row">
								<label class="form-label" for="user_login"><?php _e( 'Email', 'personalize-login' ); ?>
								<input type="email" name="user_login" id="user_login">
								</p>

								<p class="lostpassword-submit">
								<input type="submit" name="submit" class="lostpassword-button"
								value="<?php _e( 'Submit', 'personalize-login' ); ?>"/>
								</p>
							</form>
						</div>
                  </div>
                </div>
            </div>
        </div>  
      </div>
  </div>
  <script type="text/javascript" language="javascript">
  jQuery(document).ready(function(e){
    jQuery("#lostpasswordform").on('submit',(function(e) {
      e.preventDefault();
      var post_email = jQuery('#user_login').val();
    //alert(post_email);
       if(post_email!='') { 
        jQuery.ajax({
          url: "<?php echo get_template_directory_uri(); ?>-child/aj_forgot_password.php",        
          type: "POST",            
          data: new FormData(this), 
          contentType: false,       
          cache: false,             
          processData:false,       
          success: function(data) {
            //alert(data);
            jQuery(".upload-gimg-msg").html(data);
            }
        });
     } 
     else {
        if(post_email ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your Email Address!</div>');
        }  
      }
    }
   ));
});
</script>
<style type="text/css">
	html{
	margin-top: 0px !important;
	}
	input.lostpassword-button {
		background-color: #ff5558 !important;
	}
	label.form-label {
	    width: 100%;
	}
</style>