

<?php
   /* Template Name: Edit-Events */ 
   get_header();?> 

<?php 
   if(is_user_logged_in()){
    $current_user = wp_get_current_user();
    $current_user_id=$current_user->ID;
    $user_meta=get_userdata($current_user_id);
    $user_roles= $user_meta->roles[0];
    if($user_roles == 'choirmaster'){
   global $wpdb;
   $grouptable = 'events';
   $current_user_id   = get_current_user_id();
   $events_id = $_GET['events_id'];
   
   $eventdata = $wpdb->get_row("SELECT * FROM `events` WHERE `id`= '$events_id' AND `status`='1' AND `deleted`='0' 
   ");
   
   ?>
<div class="add_choir">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="add_choir_hd">
               <h1>Edit Events </h1>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <form action="" name="add_events" id="add_events" method="post" enctype="multipart/form-data">
               <div class="image_uploader_sec">
                  <div class="image_uploader_inersec">
                     <div class="row">
                        <div class="col-sm-5">
                           <?php if ($eventdata->image!='') { 
                              $beforeDot = explode(",", $eventdata->image);
                              $beforeDot = $beforeDot[0];
                                ?>
                           <img src="<?php echo site_url();?>/wp-content/uploads/eventsimage/<?php echo $beforeDot;?>">
                           <?php } else { ?>
                           <div id="image_preview"><img id="previewing" src="<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png" /></div>
                           <?php } ?>
                        </div>
                        <div class="col-sm-7">
                           <h2>Upload Event Image <span class="required_icon">*</span> </h2>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                           <!-- <input id="file" type="file" name="file" value="" /> -->
                           <input type="file" name="photo[]" id="photo" value="" class="ss-field-width" multiple="true" />
                           <!-- <span title="attach file" class="attachFileSpan" onclick="document.getElementById('myInput').click()">
                              Attach file
                              </span> -->
                           <p class="img_size">(jpeg, png, less than 2 MB)</p>
                           <div class="field" align="left">              
                           </div>
                        </div>
                     </div>
                     <div class="row">
                      <div class="col-sm-12">
                     <?php 
                        $image_url = explode(",", $eventdata->image);
                        foreach ($image_url as $img_values) {
                             $img_url = site_url().'/wp-content/uploads/eventsimage/'.$img_values;
                             $i++;
                        
                        ?>
                     <div class="column tmbl">
                        <img class="demo cursor" src='<?php echo $img_url;?>' onclick="currentSlide('<?php echo $i;?>')" alt="">
                     </div>
                     <?php }
                        ?>
                      </div>
                      </div>
                     <div class="row mb-4 mt-4">
                        <div class="col-sm-12">
                           <input class="red_btn" type="hidden" name="user_id" value="<?php echo $current_user_id;?>" />
                           <input class="red_btn" type="hidden" name="event_id" value="<?php echo $eventdata->id;?>" />
                           <div class="form-group">
                              <label>Event Name <span class="required_icon">*</span> </label>
                              <input type="text" name="events_name" id="events_name" value="<?php echo $eventdata->events_name;?>" placeholder="Event Name" class="custom_input">
                           </div>
                           <div class="form-group">
                              <label> Description <span class="required_icon">*</span> </label>
                              <textarea name="events_desc" class="custom_input ckeditor" id="events_desc"><?php echo $eventdata->events_dec;?></textarea>

                           </div>
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <label> Start Date </label>
                                    <input type="text" name="event_start_date" id="event_start_date" value="<?php echo $eventdata->events_startdate;?>" class="custom_input" >
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <label> End Date </label>
                                    <input type="text" name="event_end_date" value="<?php echo $eventdata->events_enddate;?>" id="event_end_date" class="custom_input" >
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class='col-sm-6'>
                                 <div class="form-group">
                                    <label> Start Time </label>
                                    <div class='input-group date' id='datetimepicker3'>
                                       <input placeholder="Start time" type="text" name="event_start_time" id="event_start_time" class="custom_input" value="<?php echo $eventdata->event_start_time;?>">
                                       <span class="input-group-addon">
                                       <i class="fa fa-clock-o"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class='col-sm-6'>
                                 <div class="form-group">
                                    <label> End Time </label>
                                    <div class='input-group date' id='datetimepicker4'>
                                       <input placeholder="End time" type="text" name="event_end_time" id="event_end_time" class="custom_input" value="<?php echo $eventdata->event_end_time;?>">
                                       <span class="input-group-addon">
                                       <i class="fa fa-clock-o"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                             <label> Select a Choir </label>
                              <?php 
                                 $groupdata = $wpdb->get_results("SELECT * FROM `groups` WHERE `user_id` ='$eventdata->user_id' AND status= '1' AND deleted= '0' ");
                                             ?>
                              <select name="selectchoir" id="selectchoir" class="custom_input">
                                <option>Select a choir</option>
                                 <?php 
                                    foreach ( $groupdata as $group ) {
                                      $group_id = $group->id;
                      $user_query = new WP_User_Query(
                            array(
                                'meta_key'    => 'selected_choir',
                                'meta_value'  => $group_id,
                            )
                      );  
                      $singers = $user_query->get_results(); 
                      $singer_total_count = count($singers);
                      if($singer_total_count > 0){
                                      ?>
                              <option value="<?php echo $group->id;?>" <?php 
                                 if ($eventdata->group_id==$group->id){ 
                                  echo 'selected';} else { }?>><?php echo $group->group_name;?> (<?php echo $group->group_city;?>, <?php echo $group->group_state;?>, <?php echo $group->group_country;?>)</option>
                                 <?php 
                                    } else {

                                    }
                                    }
                                    ?>
                              </select>
                           </div>
                           <div class="form-group">
                              <label>City <span class="required_icon">*</span> </label>
                              <input type="text" name="events_city" id="events_city" value="<?php echo $eventdata->events_city;?>" placeholder="Enter your city here" class="custom_input">
                           </div>
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <label> State </label>
                                    <input type="text" name="events_state" id="events_state" value="<?php echo $eventdata->events_state;?>" placeholder="Enter State" class="custom_input">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <label> Country <span class="required_icon">*</span> </label>  
                                    <input type="text" name="events_country" id="events_country" value="<?php echo $eventdata->events_country;?>" placeholder="Enter Country Name" class="custom_input">
                                 </div>
                              </div>
                           </div>
                           <div class="upload-gimg-msg"></div>
                           <input class="red_btn" type="submit" name="submit" value="Update" id="post_submit"/><div id="loading"><img src="<?php echo site_url();?>/wp-content/uploads/2019/08/loading_new_cart_img.gif"></div>
                           <input class="red_btn cancel_btn" type="button" name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url();?>/choir-profile' " />
                           <!-- <button type="submit" class="red_btn">Submit</button> -->
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<?php
   } 
   else{
     echo '<div class="alert alert-danger">Sorry You Do not have permissions to access this page.. </div>';
   }
   }
   else{
     echo '<div class="alert alert-danger">Sorry You Do not have permissions to access this page.. </div>';
   }
   ?>
<!-- <script src="js/dropzone.js"></script>
   --><!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
   -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo get_template_directory_uri(); ?>-child/js/moment-with-locales.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/css/font-awesome.css">
<link href="<?php echo get_template_directory_uri(); ?>-child/css/prettify-1.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>-child/css/base.css" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script> 
<script type="text/javascript">
   jQuery(document).ready(function(){
    jQuery(document).ajaxStart(function() {
      jQuery("#loading").css('display','inline-block');
    }).ajaxStop(function() {
      jQuery("#loading").hide();
    });
     jQuery.noConflict();
       jQuery.validator.addMethod("alphaUname", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
        });
       jQuery.validator.addMethod("pwcheck", function(value) {
        return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
        });
     //alert("one");
     
      jQuery("#add_events").validate({
           rules: {
             fname: {
                         required: true,
                         alphaUname: true
                    },
             lname: {
                         required: true,
                         alphaUname: true
                    },
             contactnumber: {
                         required: true,
                         number: true,
                        maxlength: 11,
                        minlength: 11
   
                    },  
             address: {
                         required: true
                         
                    },
             city: {
                         required: true,
                         alphaUname: true
                    },
             country: {
                         required: true,
                         alphaUname: true
   
                    },                      
                   }, 
           messages: {
             fname: {
                         required: "Please Enter First name",
                         alphaUname: "Please Enter Latters only"
                      },
             lname: {
                         required: "Please Enter Last name",
                         alphaUname: "Please Enter Latters only"
                      }, 
             contactnumber: {
                         required: "Please Enter Contact Number",
                         number: "Please Enter Number only",
                         maxlength: "Please Enter  only 11 Numbers",
                         minlength: "Please Enter  only 11 Numbers"
                      }, 
               address: {
                         required: "Please Enter address ",
                         alphaUname: "Please Enter Latters only"
                      },
               city: {
                         required: "Please Enter City Name",
                         alphaUname: "Please Enter Latters only"
                      },  
               country: {
                         required: "Please Enter Country Name",
                         alphaUname: "Please Enter Latters only"
                      },                                   
                     },
           submitHandler: function(form){
        jQuery(document).ready(function(){    
        for(var events_desc in CKEDITOR.instances)
        CKEDITOR.instances[events_desc].updateElement();
          });

              //form.submit();
             // var data = jQuery("#choir-update").serialize();
             var id = jQuery('#id').val();
             var firstname = jQuery('#events_name').val();
             var lastname = jQuery('#lastname').val();
             var events_desc = jQuery('#events_desc').val();
             var contact_number = jQuery('#contact_number').val();
             var city = jQuery('#city').val();
             var state = jQuery('#state').val();
             var country = jQuery('#country').val();
             var image = jQuery('#photo').val();
             var form = $('#add_events').get(0);
             var fd = new FormData(form);
             jQuery.ajax({
                 type : 'POST',
                 url  : '<?php echo get_template_directory_uri(); ?>-child/aj_edit_events.php',
                 data : fd,
                 processData: false,
                 contentType: false, 
             beforeSend: function()
             {
                
                 jQuery("#errorReg").fadeOut();
                 jQuery(".btn-default").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sendin ...');
             },
             success :  function(data) {
              // alert(data);
              jQuery(".upload-gimg-msg").html(data);
              setTimeout(popupPimg, 1000);
               function popupPimg() {
                window.location.assign("<?php echo site_url();?>/choir-profile/#event-list-section");
   
               }
               
               // else{
               //   alert('file not uploaded');
               //   jQuery("#response").html('<p>Sorry</p>');
               // }
             },
             // error: function(data){
             //     console.log("error");
             //     console.log(data);
             // },
           });
           },  
       });
     });
</script>
<script type="text/javascript">
   jQuery(function() {
   jQuery("#photo").change(function() {
   //$("#message").empty(); // To remove the previous error message
   var file = this.files[0];
   var imagefile = file.type;
   var match= ["image/jpeg","image/png","image/jpg"];
   if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
   {
   jQuery('#previewing').attr('src','<?php echo site_url();?>/wp-content/uploads/2019/07/male_new.png');
   //$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
   return false;
   }
   else
   {
   var reader = new FileReader();
   reader.onload = imageIsLoaded;
   reader.readAsDataURL(this.files[0]);
   }
   });
   });
   function imageIsLoaded(e) {
   jQuery("#photo").css("color","green");
   jQuery('#image_preview').css("display", "block");
   jQuery('#previewing').attr('src', e.target.result);
   jQuery('#previewing').attr('width', '250px');
   jQuery('#previewing').attr('height', '230px');
   };
</script>
<script type="text/javascript">
   jQuery(document).ready(function(){
   jQuery('#datetimepicker3').datetimepicker({
                       format: 'LT'
                   });
               jQuery('#datetimepicker4').datetimepicker({
                       format: 'LT'
                   });
               });
           
</script>
<script>
   jQuery(document).ready(function(){
   jQuery( function() {
    jQuery( "#event_end_date" ).datepicker({ minDate: 0 , dateFormat: 'yy-mm-dd' });
    jQuery( "#event_start_date").datepicker({ minDate: 0 , dateFormat: 'yy-mm-dd' });
   });
   });
</script>
<style type="text/css">
   img.demo.cursor {
   width: 100%;
   padding-left: 5px;
   min-height: 100px;
   }
   .column.tmbl {
   width: 20%;
   float: left;
   height: 20%
   }  #loading img {
    width: 25px;
    position: relative;
    left: -4%;
  }
  #post_submit {
    float: left;
    margin-right: 10px !important;
  }
  #loading { display: none; }
</style>
<?php
   get_footer();
   
   ?>

