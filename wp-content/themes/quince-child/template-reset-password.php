<?php
/* 
	Template Name: Reset Password 
*/
get_header();
$email_id = $_GET['email_id'];
?>
<?php $varable_holding_img_url = site_url().'/wp-content/uploads/2019/07/06_choir_signup.jpg'; ?>
  <div class="choir_backimg" style="background-image: url(<?php echo $varable_holding_img_url; ?>);">
      <div class="container">
        <div class="choir_main">
            <div class="row">
                <div class="col-lg-6">
                  <h6>Your Personal</h6>
                  <h1>Soundtrack Everywhere</h1>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="col-lg-6">
                  <div class="sign_up">
                      <h2 class="text-center">Reset Password</h2>
        						<div id="reset-lost-password" class="widecolumn">
        							</p>
        							<form id="resetpasswordform" action="" method="post">
        								<div class="upload-gimg-msg"></div>
                        <p class="form-row">
                        <label class="form-label" for="useremail_login">Email
                        <input type="text" name="useremail_login" id="useremail_login" value="<?php echo $email_id; ?>" readonly="">
                        </p>
        								<p class="form-row">
        								<label class="form-label" for="reset-password">Password
        								<input type="password" name="user_password" id="user_password" placeholder="Enter New Password">
        								</p>
        								<p class="resetlostpassword-submit">
        								<input type="submit" name="submit" class="resetlostpassword-button"
        								value="Reset Password"/>
        								</p>
        							</form>
        						</div>
                  </div>
                </div>
            </div>
        </div>  
      </div>
  </div>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-validate.bootstrap-tooltip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>-child/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){
      jQuery.validator.addMethod("alphaUname", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
       });
      jQuery.validator.addMethod("pwcheck", function(value) {
       return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
       });
      jQuery("#resetpasswordform").validate({
          rules: {
          user_password: {
                      required: true,
                      pwcheck: true,
                      minlength: 6              
                 },            
                }, 
          messages: {
          user_password: {
                      required: "Please Enter Password",
                      pwcheck: "Bring alphabet (A-Z / a-z), tal (0-9) or at specialcharcter (!,%, &, @, #, $, ^, *,?, _, ~).",
                      minlength: "Please Enter Minimum 6 Character"
                   },                                            
                  },
                submitHandler: function(form) {
                var data = jQuery("#resetpasswordform").serialize();
                jQuery.ajax({
                    type : 'POST',
                    url  : '<?php echo get_template_directory_uri(); ?>-child/aj_reset_password.php',
                    data : data,
                success :  function(data) {
                  //alert(data);
                  jQuery(".upload-gimg-msg").html(data);
                  setTimeout(popupPimg, 3000);
                    function popupPimg() {
                      window.location.assign("<?php echo site_url();?>/login");
                    }
                },
              });
            }, 
      });
    });
</script>
<script type="text/javascript" language="javascript">
/*  jQuery(document).ready(function(e){
    jQuery("#resetpasswordform").on('submit',(function(e) {
      //alert('gfdgfdgd');
      e.preventDefault();
      var post_email = jQuery('#useremail_login').val();
      var post_new_password = jQuery('#user_password').val();
    //alert(post_email);
       if(post_email!='' && post_new_password!='') { 
        jQuery.ajax({
          url: "<?php //echo get_template_directory_uri(); ?>-child/aj_reset_password.php",        
          type: "POST",            
          data: new FormData(this), 
          contentType: false,       
          cache: false,             
          processData:false,       
          success: function(data) {
            //alert(data);
            jQuery(".upload-gimg-msg").html(data);
            }
        });
     } 
     else {
        if(post_new_password ==''){
          jQuery(".upload-gimg-msg").html('<div class="alert alert-danger">ERROR: Please Enter Your New Password!</div>');
        }  
      }
    }
  ));
});*/
</script>
<style type="text/css">
	html{
	margin-top: 0px !important;
	}
	input.resetlostpassword-button {
		background-color: #ff5558 !important;
	}
	label.form-label {
	    width: 100%;
	}
</style>