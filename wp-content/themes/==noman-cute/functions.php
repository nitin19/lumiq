<?php
	
	//Child Theme Functions File
	
	add_action( 'wp_enqueue_scripts', 'noman_cute_enqueue_wp_child_theme' );
	
	function noman_cute_enqueue_wp_child_theme() 
	{
		wp_enqueue_style('noman-cute-parent-styles', get_template_directory_uri().'/style.css' );
	
		wp_enqueue_style('noman-cute-child-styles', get_stylesheet_uri());
		
		wp_enqueue_style('noman-cute-basic-styles', get_stylesheet_directory_uri() . '/css/basic.css', array(), time());
		
		wp_enqueue_style('noman-cute-default-styles', get_stylesheet_directory_uri() . '/css/default.css', array(), time());
	
		wp_enqueue_script('noman-cute-child-scripts', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ), '1.0', true );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}		
	}
	
	if ( ! function_exists( 'noman_cute_setup' ) ) :
		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		function noman_cute_setup() {
			/*
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on Twenty Nineteen, use a find and replace
			 * to change 'noman-cute' to the name of your theme in all the template files.
			 */
			load_child_theme_textdomain( 'noman-cute', get_stylesheet_directory() . '/languages' );
	
			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );
	
			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
	
			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );
	
			// This theme uses wp_nav_menu() in two locations.
			register_nav_menus(
				array(
					'menu-1' => __( 'Primary', 'noman-cute' ),
					'footer' => __( 'Footer Menu', 'noman-cute' ),
					'social' => __( 'Social Links Menu', 'noman-cute' ),
				)
			);
	
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support(
				'html5',
				array(
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
				)
			);
	
			/**
			 * Add support for core custom logo.
			 *
			 * @link https://codex.wordpress.org/Theme_Logo
			 */
			add_theme_support(
				'custom-logo',
				array(
					'height'      => 190,
					'width'       => 190,
					'flex-width'  => false,
					'flex-height' => false,
				)
			);
	
			// Add theme support for selective refresh for widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );
	
			// Add support for Block Styles.
			add_theme_support( 'wp-block-styles' );
	
			// Add support for full and wide align images.
			add_theme_support( 'align-wide' );
	
			// Add support for editor styles.
			add_theme_support( 'editor-styles' );
	
			// Enqueue editor styles.
			add_editor_style( 'style-editor.css' );
	
			// Add custom editor font sizes.
			add_theme_support(
				'editor-font-sizes',
				array(
					array(
						'name'      => __( 'Small', 'noman-cute' ),
						'shortName' => __( 'S', 'noman-cute' ),
						'size'      => 19.5,
						'slug'      => 'small',
					),
					array(
						'name'      => __( 'Normal', 'noman-cute' ),
						'shortName' => __( 'M', 'noman-cute' ),
						'size'      => 22,
						'slug'      => 'normal',
					),
					array(
						'name'      => __( 'Large', 'noman-cute' ),
						'shortName' => __( 'L', 'noman-cute' ),
						'size'      => 36.5,
						'slug'      => 'large',
					),
					array(
						'name'      => __( 'Huge', 'noman-cute' ),
						'shortName' => __( 'XL', 'noman-cute' ),
						'size'      => 49.5,
						'slug'      => 'huge',
					),
				)
			);
	
			// Editor color palette.
			add_theme_support(
				'editor-color-palette',
				array(
					array(
						'name'  => __( 'Primary', 'noman-cute' ),
						'slug'  => 'primary',
						'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
					),
					array(
						'name'  => __( 'Secondary', 'noman-cute' ),
						'slug'  => 'secondary',
						'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
					),
					array(
						'name'  => __( 'Dark Gray', 'noman-cute' ),
						'slug'  => 'dark-gray',
						'color' => '#111',
					),
					array(
						'name'  => __( 'Light Gray', 'noman-cute' ),
						'slug'  => 'light-gray',
						'color' => '#767676',
					),
					array(
						'name'  => __( 'White', 'noman-cute' ),
						'slug'  => 'white',
						'color' => '#FFF',
					),
				)
			);
	
			// Add support for responsive embedded content.
			add_theme_support( 'responsive-embeds' );
							
											
			add_theme_support(
				'custom-background',
				apply_filters(
					'noman_cute_custom_background_args',
					array(
						'default-color' => '#fff',
						'default-image'          => '',//get_stylesheet_directory_uri() . '/images/water-drink-bottle.jpg',
						'default-repeat'         => 'repeat-y',
						'default-position-x'     => 'center',
						'default-position-y'     => 'top',	
						'default-size'           => 'contain',					
					)
				)
			);			
		}
	endif;
	add_action( 'after_setup_theme', 'noman_cute_setup' );
		
	function noman_cute_content_width() {
		// This variable is intended to be overruled from themes.
		// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
		// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
		$GLOBALS['content_width'] = apply_filters( 'noman_cute_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'noman_cute_content_width', 0 );	
	
	function noman_cute_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Sidebar Main', 'noman-cute' ),
			'id'            => 'sidebar-main',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'noman-cute' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	
	}
	add_action( 'widgets_init', 'noman_cute_widgets_init' );		
	
	function noman_cute_body_classes( $classes ) {
		// Adds a class of custom-background-image to sites with a custom background image.
		if ( get_background_image() ) {
			$classes[] = 'custom-background-image';
		}
	
		// Adds a class of group-blog to sites with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
	
		// Adds a class of no-sidebar to sites without active sidebar.
		if ( ! is_active_sidebar( 'sidebar-main' ) ) {
			$classes[] = 'no-sidebar';
		}
	
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}
	
		return $classes;
	}
	add_filter( 'body_class', 'noman_cute_body_classes' );
		
	
	
	function noman_cute_get_svg( $args = array() ) {
		// Make sure $args are an array.
		if ( empty( $args ) ) {
			return __( 'Please define default parameters in the form of an array.', 'noman-cute' );
		}
	
		// Define an icon.
		if ( false === array_key_exists( 'icon', $args ) ) {
			return __( 'Please define an SVG icon filename.', 'noman-cute' );
		}
	
		// Set defaults.
		$defaults = array(
			'icon'     => '',
			'title'    => '',
			'desc'     => '',
			'fallback' => false,
		);
	
		// Parse args.
		$args = wp_parse_args( $args, $defaults );
	
		// Set aria hidden.
		$aria_hidden = ' aria-hidden="true"';
	
		// Set ARIA.
		$aria_labelledby = '';
	
		if ( $args['title'] ) {
			$aria_hidden     = '';
			$unique_id       = uniqid();
			$aria_labelledby = ' aria-labelledby="title-' . esc_attr($unique_id) . '"';
	
			if ( $args['desc'] ) {
				$aria_labelledby = ' aria-labelledby="title-' . esc_attr($unique_id) . ' desc-' . esc_attr($unique_id) . '"';
			}
		}
	
		// Begin SVG markup.
		$svg = '<svg class="icon icon-' . esc_attr( $args['icon'] ) . '"' . $aria_hidden . esc_attr($aria_labelledby) . ' role="img">';
	
		// Display the title.
		if ( $args['title'] ) {
			$svg .= '<title id="title-' . esc_attr($unique_id) . '">' . esc_html( $args['title'] ) . '</title>';
	
			// Display the desc only if the title is already set.
			if ( $args['desc'] ) {
				$svg .= '<desc id="desc-' . esc_attr($unique_id) . '">' . esc_html( $args['desc'] ) . '</desc>';
			}
		}
	
		/*
		 * Display the icon.
		 *
		 * The whitespace around `<use>` is intentional - it is a work around to a keyboard navigation bug in Safari 10.
		 *
		 * See https://core.trac.wordpress.org/ticket/38387.
		 */
		$svg .= ' <use href="#icon-' . esc_html( $args['icon'] ) . '" xlink:href="#icon-' . esc_html( $args['icon'] ) . '"></use> ';
	
		// Add some markup to use as a fallback for browsers that do not support SVGs.
		if ( $args['fallback'] ) {
			$svg .= '<span class="svg-fallback icon-' . esc_attr( $args['icon'] ) . '"></span>';
		}
	
		$svg .= '</svg>';
	
		return $svg;
	}		
	
	require get_stylesheet_directory() . '/inc/template-tags.php';